<!--
/***************************************************************************************************************
* Appirio, Inc
* Page Name: CaseEmailCFArticles 
* Description: [Task # T-610923]
* Created Date: [06/22/2017]
* Created By: [Poornima Bhardwaj] (Appirio)

*******************************************************************************************************************/
-->
<apex:page standardController="Case" extensions="CaseEmailCFArticlesController">
    <apex:sectionHeader title="Email Message" subtitle="Send an Email"/>
    <style>
        .boldText {
        font-weight:bold;
        }
        .labelText {
        font-weight:bold;
        display:inline-block;
        text-align:right;
        width:100px;
        }
        .wideTextArea {
        width:400px;
        }
        .mediumTextField {
        width:200px;
        }
        .longTextField {
        width:400px;
        }
        .extraDeepTextArea {
        width:1000px;
        height:200px;
        }
    </style>
    <script>
    function cvCheckAllOrNone(allOrNoneCheckbox) {
        // Find parent table
        var container = allOrNoneCheckbox;
        while (container.tagName != "TABLE") {
            container = container.parentNode;
        }
        // Switch all checkboxes
        var inputs = container.getElementsByTagName("input");
        var checked = allOrNoneCheckbox.checked;
        for (var i = 0; i < inputs.length; i++) { 
            var input = inputs.item(i);
            if (input.type == "checkbox") {
                if (input != allOrNoneCheckbox) {
                    input.checked = checked;
                }
            }
        }
    }
    </script>
    <apex:form >
        <apex:pageBlock id="dataSection">
            <apex:pageMessages id="pageMessages" />
            <apex:pageBlockButtons >
                <apex:commandButton value="Send" action="{!send}"/>
                <apex:commandButton value="Cancel" action="{!cancel}" immediate="true" />
            </apex:pageBlockButtons>            
            <apex:pageBlockSection title="Edit Email" collapsible="False">
                <apex:panelGrid columns="2" cellpadding="5%" cellspacing="5%">
                    <apex:outputLabel styleClass="labelText">From</apex:outputLabel>
                    <apex:inputText id="idSender" value="{!emailMsg.FromAddress}"
                                    styleClass="longTextField"/>
                    
                    <apex:outputLabel styleClass="labelText">To</apex:outputLabel>
                    <apex:inputText id="idRecipient" styleClass="longTextField" required="true"
                                    value="{!emailMsg.ToAddress}" />
                    
                    <apex:outputLabel styleClass="labelText">Related To</apex:outputLabel>
                    <apex:inputText value="{!emailMsg.Subject}" styleClass="longTextField"/>
                    
                    <apex:outputLabel styleClass="labelText">Additional To:</apex:outputLabel>
                    <apex:inputTextarea id="idAddlRecipients" styleClass="wideTextArea"                         
                                        value="{!addlRecipients}"/>
                    
                    <apex:outputLabel styleClass="labelText">CC:</apex:outputLabel>
                    <apex:inputTextarea id="idCcRecipients" styleClass="wideTextArea"                       
                                        value="{!emailMsg.CcAddress}"/>
                    
                    <apex:outputLabel styleClass="labelText">BCC:</apex:outputLabel>
                    <apex:inputTextarea id="idBccRecipients" styleClass="wideTextArea"                      
                                        value="{!emailMsg.BccAddress}"/>
                    
                    <apex:outputLabel styleClass="labelText">Subject:</apex:outputLabel>
                    <apex:inputText id="idSubject" required="true" styleClass="longTextField" 
                                    value="{!emailMsg.Subject}"/>
                    
                    <apex:outputLabel styleClass="labelText">Body:</apex:outputLabel>
                    <apex:inputTextarea richtext="true" id="idBody" styleClass="extraDeepTextArea"
                                        value="{!emailMsg.HtmlBody}"/>
                    
                </apex:panelGrid>    
            </apex:pageBlockSection>
            
        </apex:pageBlock>
        <apex:pageBlock title="Attachments">
            <apex:pageBlockTable value="{!articleVal}" var="articleObj" id="table">
            
                <apex:column >
                    <apex:facet name="header"> <apex:inputCheckbox onclick="cvCheckAllOrNone(this)">
                        </apex:inputCheckbox>
                    </apex:facet>
                    <apex:inputCheckbox value="{!articleObj.selected}"/>
                </apex:column>
                  <apex:column value="{!articleObj.Id}"  headerValue="Id" />
                <apex:column value="{!articleObj.Title}" headerValue="Title" />
            </apex:pageBlockTable>
        </apex:pageBlock>
    </apex:form>
</apex:page>