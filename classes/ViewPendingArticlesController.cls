/************************************************************************
* Appirio, Inc
* Page Name: ViewPendingArticlesController
* Description: [#T-593643 APex cntroller for ViewPendingArticles VF page]
* Created Date: [19/4/2017]
* Created By: [Neeraj Kumawat] (Appirio)
* 
* Date Modified                Modified By                  Description of the update
***********************************************************************/
public class ViewPendingArticlesController {
    public Map<Id, ArticleWrapper> articleWrapperMap {get;set;}
    //Map of workItemId
    public  Map<Id, ProcessInstanceWorkItem> workItemIdMap {get;set;}
    //List of KnowledgeArticle Version
    public List<KnowledgeArticleVersion> knowledgeList {get;set;}
    //Map of Article data category Name Label
    public static Map<String, String> dataCatNameLabel;
    //Region Data category
    public static String regionCategory = 'Region';
    //Intended Audience Data category
    public static String intendedAudienceCategory = 'Intended_Audience';
    //Article Wrapper class
    public class ArticleWrapper {
        public Id articleId{get;set;}
        public String title{get;set;}
        public String region{get;set;}
        public String intendedAudience{get;set;}
    }
    //****************************************************************************************
    // Default constrator for ViewPendingArticlesController
    // @return void
    //*****************************************************************************************
    public ViewPendingArticlesController() {
        List<String> objType = new List<String>();
        objType.add('KnowledgeArticleVersion');
        dataCatNameLabel = UtilityCls.getDataCategoriesNameLabelMap(objType);
        
        Id userId=UserInfo.getUserId();
        articleWrapperMap=new Map<Id,ArticleWrapper>();
        //Fetching groupMember list of loggedin user
        List<GroupMember> groupMemberList=[SELECT Id, Group.Name,Group.Id, UserOrGroupId, Group.Type 
                                           FROM GroupMember Where (Group.Name='PRT' OR Group.Name='Manager' 
                                                                   OR Group.Name='Compliance') 
                                           AND UserOrGroupId=:userId];
        //Set of Actor Ids
        Set<Id> actorIds=new Set<Id>();
        //List of user or group Id
        List<Id> UserOrGroupIdList=new List<Id>();
        for(GroupMember gm: groupMemberList){
            //when group Type is regular then user is part of public group for getting queue i we need to further query
            if(gm.Group.Type == 'Regular'){
                UserOrGroupIdList.add(gm.Group.Id);
            }else if(gm.Group.Type == 'Queue'){
                actorIds.add(gm.Group.Id);
            }
        }
        //Fetching  groupMember list when user is part of any public group
        List<GroupMember> groupMemList=[SELECT Id, Group.Name,Group.Id, UserOrGroupId, Group.Type 
                                        FROM GroupMember Where (Group.Name='PRT' OR Group.Name='Manager' 
                                                                OR Group.Name='Compliance') 
                                        AND UserOrGroupId IN :UserOrGroupIdList];
        for(GroupMember gm: groupMemList){
            if(gm.Group.Type == 'Queue'){
                actorIds.add(gm.Group.Id);
            }
        }
        system.debug('actorIds='+actorIds);
        //Fetch ProcessInstanceWorkItem records based on Actor Id (here Actor id is queue)
        List<ProcessInstanceWorkItem> workItemList = [SELECT Id,ProcessInstanceId,ProcessInstance.TargetObjectId,
                                                      ProcessInstance.SubmittedById,ProcessInstance.CreatedDate,ActorId,
                                                      OriginalActorId FROM ProcessInstanceWorkItem 
                                                      WHERE ProcessInstance.Status = 'Pending' AND ActorId IN :actorIds ];
        Set<Id> idSet= new Set<Id>();
        workItemIdMap= new Map<Id,ProcessInstanceWorkItem>();
        for(ProcessInstanceWorkItem workItem : workItemList){
            idSet.add(workItem.ProcessInstance.TargetObjectId);
            workItemIdMap.put(workItem.ProcessInstance.TargetObjectId, workItem);
        }
        
        knowledgeList=[Select Id,Title,ArticleType,Language From KnowledgeArticleVersion where Id IN : idSet];
        system.debug('knowledgeList='+knowledgeList);
        Set<String> dataCategorySelection = new Set<String>();
        for(KnowledgeArticleVersion kav: knowledgeList){
            ArticleWrapper articleWrapperObj = new ArticleWrapper();
            articleWrapperObj.articleId = kav.Id;
            articleWrapperObj.title = String.valueOf(kav.get('Title'));
            articleWrapperMap.put(kav.Id, articleWrapperObj);
            //Preparing data Category Selection set
            dataCategorySelection.add(kav.ArticleType.replace('kav', 'DataCategorySelection'));
        }
        //loop for individual Article type to get data category
        for(String objName: dataCategorySelection){
            //Fetching Data category from individual DataCategorySelection
            String queryString = 'SELECT id, ParentId, DataCategoryGroupName, DataCategoryName FROM ' + objName;
            for(sObject kav : database.query(queryString)) {                   
                for(Id articleId : articleWrapperMap.keySet()) {
                    if(kav.get('ParentId') == articleId) {
                        ArticleWrapper artWrapper=articleWrapperMap.get(articleId);
                        //When data Category is Region
                        if(String.valueOf(kav.get('DataCategoryGroupName')) == regionCategory) {
                            string dataCat = String.valueOf( kav.get('DataCategoryName'));
                            if (artWrapper.region == null) {
                                artWrapper.region = dataCatNameLabel.get(dataCat);
                            } else {
                                artWrapper.region += ', ' + dataCatNameLabel.get(dataCat); 
                            }  
                        }
                        //When data Category is intended Audience
                        if(String.valueOf(kav.get('DataCategoryGroupName')) == intendedAudienceCategory) {
                            string dataCat = String.valueOf( kav.get('DataCategoryName'));
                            if (artWrapper.intendedAudience == null) {
                                artWrapper.intendedAudience = dataCatNameLabel.get(dataCat);
                            } else {
                                artWrapper.intendedAudience += ', ' + dataCatNameLabel.get(dataCat);   
                            }  
                        }               
                    }
                }                 
            }
        }
    }
}