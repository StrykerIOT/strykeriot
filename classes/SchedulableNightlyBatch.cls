/***************************************************************************
// (c) 2015 Appirio, Inc.
//
// Description    : Schedulable Nightly Batch Class - Delete Old Attachments
//                : 
//                  
// 6 September 2016 Gaurav Dudani Original (T-532536)
//***************************************************************************/

global class SchedulableNightlyBatch implements Schedulable {
    global void execute(SchedulableContext sc) {
      EmailScheduleDeleteAttachments attachmentDeleteBatch = new EmailScheduleDeleteAttachments();
      Database.executeBatch(attachmentDeleteBatch);     
    }
}