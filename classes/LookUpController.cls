public class LookUpController{
    public String SearchContact { get; set; }
    public String AccountId { get; set; }
    public List<Contact> ContactList{set;get;}
    public Integer ContactListCount{set;get;}
    public Boolean ShowBackLink{set;get;}
    public LookUpController(){
        ShowBackLink=false;
        AccountId=System.currentPageReference().getParameters().get('AccountId');
        LoadContactsForAccount();
    }
    public void SearchContactEvent(){
    ShowBackLink=true;
        ContactList=[select Name,Id,Email from Contact where Name LIKE :('%'+SearchContact+'%') AND AccountId=:AccountId];
        ContactListCount=ContactList.size();
    }
    public void goBack(){
    ShowBackLink=false;
    LoadContactsForAccount();
    }
    public void LoadContactsForAccount(){
    ContactList=[select Name,Id,Email from Contact where AccountId=:AccountId];
    ContactListCount=ContactList.size();
    }
}