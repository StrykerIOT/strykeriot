/**********************************************************************
* Appirio, Inc
* Test Class Name: UpdateFAQArticleTest
* Class Name: [UpdateFAQArticle]
* Description: [T-564240 Test Class for UpdateFAQArticle]
* Created Date: [12/28/2016]
* Created By: [Gaurav Dudani] (Appirio)
* 
* Date Modified                Modified By                  Description of the update
* [12/29/2016]               [Gaurav Dudani]          [Bulikyfied the class and added Comments]
* [14/04/2016]               [Neeraj Kumawat]         [Added Summary field on test data creation to avoid failing of test method]
***********************************************************************/

@isTest
public class UpdateFAQArticleTest {

/**********************************************************************
* Test Method to create FAQ Articles
***********************************************************************/

  static testMethod void UpdateFAQArticleTestMethod(){
    List<FAQ__kav> faqList = new List<FAQ__kav>();
        for(FAQ__kav faq: TestUtilities.createFAQArticle(2,false)){
            faq.Article_Body__c = '#question 1. What is your Name? /n #answer 1. My name is Gaurav Dudani.';
            faq.expiration_type__c = 'Static - Historical';
            faq.Summary='Test Faq Article';
            faqList.add(faq);
        }
        
//DML for bulk insert of FAQ articles.
        insert faqList;    
//Fetching FAQ list from FAQ ID and testing functionality using Assertions.     
  List<FAQ__kav> objFAQList=[select ID,Article_Body__c,Article_Body_Backend__c from FAQ__kav  where ID =: faqList];
      for(FAQ__kav objFAQ: objFAQList){
         //String articleBody= objFAQ.Article_Body_Backend__c;
          if(objFAQ.Article_Body_Backend__c!=null){
             //System.debug('Article Body value='+articleBody);
               System.assert(objFAQ.Article_Body_Backend__c.contains('<div class="question">'), 'Article Body does not contain <div class ="question"> ' );          
            }else{
              System.assert(false, 'Article Body is Empty');
          }
      }
  }
}