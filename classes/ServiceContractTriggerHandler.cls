/*********************************************************************
* Appirio a WiPro Company 
* Name: ServiceContractTriggerHandler
* Description: Contains CRUD trigger event methods
* Created Date: 04-26-2017
* Created By: Bobby Cheek
* 
* Date Modified                Modified By                  Description of the update
*
**********************************************************************/
public class ServiceContractTriggerHandler {
  
  static String CLASSNAME = '\n\n**** ServiceContractTriggerHandler.METHODNAME()';
  
  /*  author : Appirio, Inc
    date : 04-26-2017
    description :  this method handles the 'before insert' events for the trigger
    paramaters : List of new ServiceContract objects
    returns : nothing
  */
    public static void onBeforeInsert(list<ServiceContract> newServiceContractList){
      
      string METHODNAME = CLASSNAME.replace('METHODNAME','onBeforeInsert');
      system.debug(LoggingLevel.INFO, 'INSIDE :: ' + METHODNAME);
      
      setCurrencyISOCodeOnServiceContract(newServiceContractList); 
    }
    
  /*  author : Appirio, Inc
    date : 04-26-2017
    description :  set CurrencyISOCode On Service Contract to match Account
    paramaters : List of new ServiceContract objects
    returns : nothing
  */
    private static void setCurrencyISOCodeOnServiceContract(list<ServiceContract> newServiceContractList){
      
      string METHODNAME = CLASSNAME.replace('METHODNAME','setCurrencyISOCodeOnServiceContract');
      system.debug(LoggingLevel.INFO, 'INSIDE :: ' + METHODNAME);
    
    set<Id> accountIdSet = new set<Id>();
    
    for(ServiceContract scon : newServiceContractList){
      if(scon.AccountId != null){
        accountIdSet.add(scon.AccountId);
      }
    }  
    
    map<Id, Account> accountIdToAccountMap = new map<Id, Account>([SELECT Id, CurrencyISOCode From Account WHERE Id IN : accountIdSet]);
      
    for(ServiceContract scon : newServiceContractList){
      if(accountIdToAccountMap.ContainsKey(scon.AccountId)){
        scon.CurrencyISOCode = accountIdToAccountMap.get(scon.AccountId).CurrencyISOCode;
      }
    }        
    }
}