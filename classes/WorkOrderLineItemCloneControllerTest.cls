@isTest
private class WorkOrderLineItemCloneControllerTest {
    static User relationshipUser;
    static List<Account> accList;
    static List<Case> caseList;
    static Id woliId;
    static boolean isRedirectVal;
    static String recordTypeVal;
    static String recordTypeVal1;
    Static String adjustmentWrkOrderTypeId;
    static list<WorkOrder> workOrderList;
    static Integer numberOfRecords;
    Static String naAdjustmentWorkOrderLineItemId;
    Static String naAdjustmentWorkOrderLineItemId1;
    Static List<WorkOrderLineItem> WOLineItemList;
    
    static void createTestData()
    {   
        System.debug('We are inside create testdata');
        recordTypeVal='NA_Adjustment_Work_Order_Line_Item';
        
        workOrderList=TestUtilities.createWorkOrder(1,false);
        for(WorkOrder workOrder:workOrderList){ 
           workOrder.status='New';
        }
        insert workOrderList;
        System.debug('workOrderList Value>>>>'+workOrderList);
        
        WOLineItemList=TestUtilities.createWorkOrderLineItem(1,false);
        
        Map<String, Schema.Recordtypeinfo> workOrderLineItemRecordTypes = WorkOrderLineItem.sObjectType.getDescribe().
            getRecordtypeinfosByName(); 
        naAdjustmentWorkOrderLineItemId=workOrderLineItemRecordTypes.get('NA Adjustment Work Order Line Item').getRecordTypeId();
        recordTypeVal=naAdjustmentWorkOrderLineItemId;
        naAdjustmentWorkOrderLineItemId1=workOrderLineItemRecordTypes.get('MX Fee Maintenance Work Order Line Item').getRecordTypeId();
        recordTypeVal1=naAdjustmentWorkOrderLineItemId1;
        
        for(WorkOrderLineItem woli:WOLineItemList){ 
            woli.RecordTypeId=NAAdjustmentWorkOrderLineItemId;
            woli.WorkOrderId=workOrderList[0].Id;
            woli.status='New';
            woli.Adjustment_Type__c='Customer Credit';
            woli.Show_on_Statement__c='Yes';
            woli.Adjustment_Reason__c='Discount Fees';
            woli.Adjustment_Reason_Category__c='Discount';
            woli.Department__c='All';
            woli.Line_Item_Amount__c=1000;
            woli.Line_Item_Quantity__c=10;
           // woli.Line_Item_Total__c=10000;
            woli.Actual_Fee_Billed__c=100;
        }
        insert WOLineItemList;
        System.debug('Id of WOLI'+WOLineItemList);
        woliId=WOLineItemList[0].Id;  
        system.debug('woliId'+woliId);
       
        Test.setCurrentPageReference(new PageReference('Page.WorkOrderLineItemClone'));         
        System.currentPageReference().getParameters().put('isRedirected','False');
        
        System.currentPageReference().getParameters().put('id',woliID);
    }
    
   /* static testMethod void testWorkOrderLineItemClone()
    {
        createTestData();
        System.currentPageReference().getParameters().put('RecordType',recordTypeVal);
       Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator('CollectionDetails'));
        //Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator('CollectionTotal'));
         Test.startTest();
            ApexPages.StandardController sc = new ApexPages.StandardController(WOLineItemList[0]);
            WorkOrderLineItemCloneController wolicon=new WorkOrderLineItemCloneController(sc); 
            wolicon.checkRecordType();
            wolicon.save();
            wolicon.cancel();
            
            Test.stopTest();
        }
    */
    static testMethod void testWorkOrderLineItemClone1()
    {
        createTestData();
        System.debug('Record type val 1'+recordTypeVal1);
        System.currentPageReference().getParameters().put('RecordType',recordTypeVal1);
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator('CollectionDetails'));
        Test.startTest();
            ApexPages.StandardController sc = new ApexPages.StandardController(WOLineItemList[0]);
            WorkOrderLineItemCloneController wolicon=new WorkOrderLineItemCloneController(sc); 
            wolicon.checkRecordType();
            Test.stopTest();
        }
    
}