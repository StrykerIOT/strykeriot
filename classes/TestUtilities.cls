/*******************************************************************************
Author        :  Appirio (Gaurav Dudani)
Created Date  :  Sep 13th, 2016
Updated Date  :  Sep 30th, 2016
Purpose       :  Class having common methods for test data creation
*******************************************************************************/
// 16 Sep 2016 Aashish Sajwan (T-534832)
// 03 Oct 2016 Gaurav Dudani (T-542464)
// 28 Dec 2016 Gaurav Dudani (T-564528)
// 04 Jan 2017 Gaurav Dudani (T-565229)
// 01 Mar 2017 Aashish Sajwan (T-578828)
// 21 MAr 2017 Neeraj Kumawat (T-586489)
// 21 MAr 2017 Neeraj Kumawat (T-586882)
// 21 MAr 2017 Neeraj Kumawat (T-596388)
// 26/4/2017   Neeraj Kumawat (#T-596676)
//16-May-2017  Neeraj Kumawat (#T-602715)
//25-May-2017 Neeraj Kumawat T-603560
public class TestUtilities {
    
    //****************************************************************************
    // Method to create test Case records
    //****************************************************************************  
    
    public static List<Case> createCase(Integer caseCount,Id ownerId, Boolean isInsert){
        List<Case> lstCase = new List<Case>();
        for(Integer i = 0; i < caseCount; i++){
            Case c = new Case(Origin = 'Email', Description='Test case description'+i);
            lstCase.add(c);
        }
        if(isInsert){
            insert lstCase;
        }
        return lstCase;
    }
    
    //****************************************************************************
    // Method to create test EmailMessage records
    //****************************************************************************  
    
    public static List<EmailMessage> createEmailMessage(Integer emailMessageCount, Id caseId,Boolean isInsert){
        List<EmailMessage> lstEmailMessage = new List<EmailMessage>();
        for(Integer i = 0; i < emailMessageCount; i++){
            EmailMessage em = new EmailMessage(FromAddress = 'gaurav.dudani@appirio.com',ParentId = caseId,
                                               FromName = 'Test from user'+i,ToAddress = 'test to user'+i,
                                               Subject = 'test subject'+i,TextBody = 'test',HtmlBody = 'test body'+i);
            lstEmailMessage.add(em);
        }
        if(isInsert){
            insert lstEmailMessage;
        }
        return lstEmailMessage;
    } 
    //****************************************************************************
    // Method to create test Attachment records
    //****************************************************************************  
    
    public static List<Attachment> createAttachment(Integer attachmentCount,  Id emailMessageId, Boolean isInsert){
        String bodyStr  = 'test body string';
        List<Attachment> attachmentList = new List<Attachment>();
        for(Integer i = 0; i < attachmentCount; i++){
            Attachment em = new Attachment(Body=Blob.valueOf(bodyStr),
                                           Name='Note' + '.txt',
                                           parentId=emailMessageId);
            attachmentList.add(em);
        }
        if(isInsert){
            insert attachmentList;
        }
        return attachmentList;
    }
    
    //****************************************************************************
    // Method to create a user associated to the given contact for testing
    // @param userNo an integer indicating the user number
    // @param ProfileId an id indicating the id of the profile to be set for the user
    // @param isInsert a boolean indicating whether the user is to be inserted or not
    // @return a User
    //****************************************************************************  
    public static User createTestUser(Integer userNo, Id ProfileId, Boolean isInsert) {
        User user = new User();
        user.FirstName = 'test'+ userNo;
        user.LastName = 'user'+userNo;
        user.Email = 'test'+userNo+'@testdomain.com';
        user.Username = 'test'+userNo+'@testdomain.comtestuser';
        user.ProfileId = ProfileId;
        user.Alias = 'test'+userNo;
        user.CommunityNickname = 'testusr'+userNo;
        user.TimeZoneSidKey = 'America/Los_Angeles';
        user.LocaleSidKey = 'en_US';
        user.EmailEncodingKey = 'UTF-8';
        user.LanguageLocaleKey = 'en_US'; 
        user.Phone = '+9009899900';
        user.IsActive = true;
        if(isInsert) {
            insert user;
        }
        return user;  
    }
    //***************************************************************************
    // Method to create Account record.
    //***************************************************************************
    public static List<Account> createAccount(Integer count, Boolean isInsert){
        List<Account> accountList = new List<Account>();
        for(Integer i=0; i<count; i++){
            accountList.add(new Account(Name = 'Test Account'+i));      
        }
        if(isInsert){
            insert accountList;     
        }
        return accountList;
    }
    //***************************************************************************
    // Method to create Account record with Processing Center Id.
    //***************************************************************************
    public static List<Account> createAccountWithProcessingCenterId(Integer count,String processingCenterId, Boolean isInsert){
        List<Account> accountList = new List<Account>();
        for(Integer i=0; i<count; i++){
            accountList.add(new Account(Processing_Center_ID__c=processingCenterId, Name = 'Test Account'+i));      
        }
        if(isInsert){
            insert accountList;     
        }
        return accountList;
    } 
    
    
    //***************************************************************************
    // Method to create Contact record.
    //***************************************************************************
    public static List<Contact> createContact(Integer count,Id accountId, Boolean isInsert){
        List<Contact> contactList = new List<Contact>();
        for(Integer i=0; i<count; i++){
            contactList.add(new Contact(firstName = 'Test',AccountId = accountId, lastName='Contact'+i));      
        }
        if(isInsert){
            insert contactList;     
        }
        return contactList;
    }  
    //***************************************************************************
    // Method to create Case Custom Setting
    //***************************************************************************
    public static Case_Custom_Setting__c createCaseCustomSetting(Decimal numberofDays, Boolean isInsert){
        Case_Custom_Setting__c objSetting =  new Case_Custom_Setting__c();
        objSetting.Name = 'Case Last Modify Number of Days';
        objSetting.Number_of_Days__c = numberofDays;
        if(isInsert) {
            insert objSetting;
        }
        return objSetting;
    }
    //***************************************************************************
    // Method to create Case Custom Setting
    //***************************************************************************
    public static Provided_Date__c createProvidedDateCustomSetting(Boolean isInsert){
        Provided_Date__c objSetting =  new Provided_Date__c();
        objSetting.Name = 'CaseRunAsDate';
        if(isInsert) {
            insert objSetting;
        }
        return objSetting;
    }
    //***************************************************************************
    // Method to create Task 
    //***************************************************************************
    public static List<Task> createTask(Integer count,Boolean isInsert){
        List<Task> taskList = new List<Task>();
        for(Integer i = 0; i < count; i++){
            Task t = new Task();
            taskList.add(t);
        }
        if(isInsert){
            insert taskList;
        }
        return taskList;  
    }   
    
    //***************************************************************************
    // Method to create Event as per Task # T-565229
    //***************************************************************************
    public static List<Event> createEvent(Integer count,Boolean isInsert){
        List<Event> evtLst = new List<Event>();
        for(Integer i = 0; i < count; i++){
            Event e = new Event();
            evtLst.add(e);
        }
        if(isInsert){
            insert evtLst;
        }
        return evtLst;  
    }        
    
    //***************************************************************************
    // Method to create FAQ Article
    //***************************************************************************
    
    public static List<FAQ__kav> createFAQArticle(Integer count,Boolean isInsert){
        List<FAQ__kav> fAQList = new List<FAQ__kav>();
        for(Integer i = 0; i < count; i++){
            FAQ__kav faq = new FAQ__kav();
            faq.title = 'testFAQ '+i;
            faq.UrlName = 'TestFAQ'+i;
            fAQList.add(faq);
        }
        if(isInsert){
            insert fAQList;
        }
        return fAQList;  
    }
    
    
    //***********************************************************************************
    // Method to create an Account 
    // @param ID -Account ID
    // @param MID 
    // @param isInsert a boolean indicating whether the user is to be inserted or not
    // @return an Account
    //************************************************************************************  
    public static Account createTestAccount(Boolean isInsert) {
        Account acc=new Account();
        acc.MID__c='8024778006';
        acc.Name='My Test Account';
       	acc.Processing_Center_ID__c='NA';
        acc.Tax_Id_Encrypted__c='123456789';
        acc.Client_Group__c='3';
        if(isInsert) {
            insert acc;
        }
        return acc;  
    } 
    //****************************************************************************
    // Method to create a Custom Setting
    // @param Name-Name
    // @param value -Value
    // @param isInsert a boolean indicating whether the user is to be inserted or not
    // @return an Run Apex record
    //****************************************************************************  
    public static Run_Apex__c createCustomSetting(String Name,String value,Boolean isInsert) {
        Run_Apex__c objRA=new Run_Apex__c();
        objRA.Name=Name;
        objRA.Apex_Class_Name__c=value;
        if(isInsert) {
            insert objRA ;
        }
        return objRA;  
    }     
    
    //***************************************************************************
    // Method to create Addtional Address record.
    //***************************************************************************
    public static List<Additional_Address__c> createAdditionalAddress(Id accountId, String accountType, Integer count, Boolean isInsert){ 
        List<Additional_Address__c> addList = new List<Additional_Address__c>();
        //Loop for creating multiple records
        for(integer i=0; i<count; i++){
            Additional_Address__c ad = new Additional_Address__c (
                Account__c = accountId,
                Address_Type__c = accountType,
                City__c = 'Some City ' + i,
                Country__c = 'Some Country ' + i,
                County_State__c = 'Some State ' + i,
                Street_1__c = 'Some Street 1 ' + i,
                Street_2__c = 'Some Street 2 ' + i,
                Street_3__c = 'Some Street 3 ' + i,
                Postal_Code__c = 'Some Postal Code ' + i  );
            addList.add(ad);
            
        }
        if(isInsert){
            insert addList;     
        }
        return addList; 
    }
    
    //Add  [Aashish Sajwan]    Utility method for External doc ,Content Version , Feed Item: 01-Mar-2017  #T-578828
    //Start
    
    
    //****************************************************************************
    // Method to create a External_Document__c record
    // @param AccountReference : accoundRecord 
    // @param string  : StatementId
    // @param string : MID
    // @param isInsert a boolean indicating whether the user is to be inserted or not
    // @return an External_Document__c object
    //****************************************************************************  
    public static External_Document__c SaveExternalDoc(Account accountRecord,String StatementId,String MID,boolean SendEmail,boolean isInsert){
        External_Document__c externalDoc = new External_Document__c();
        externalDoc.Account__c = accountRecord.Id; //MID related with Account
        externalDoc.Expiry_Date__c = (SendEmail==false)?system.Today().addDays(2):system.Today().addDays(7);
        externalDoc.ExternalID__c= StatementId;
        externalDoc.MPSMID__c = MID;  
        externalDoc.AccountOwnerName__c =  (accountRecord.Account_Owner_Role__c!=null? accountRecord.Account_Owner_Role__c:''); // Owner Role Name
        externalDoc.AccountProcessingCenterID__c = accountRecord.Processing_Center_ID__c;
        externalDoc.AccountName__c= accountRecord.Name;  
        if(isInsert){
            insert externalDoc;
        }
        return externalDoc;
    }
    
    //****************************************************************************
    // Method to create a ContentVersion record
    // @param string : FileType
    // @param string  : ImageData
    // @param string : MID
    // @param isInsert a boolean indicating whether the user is to be inserted or not
    // @return an ContentVersion object
    //****************************************************************************  
    public static ContentVersion SaveContentVersion(string FileType,string ImageData, string MID,boolean isInsert){
        ContentVersion  conVersion = new ContentVersion();
        conVersion.VersionData = (FileType=='pdf')? EncodingUtil.base64Decode(ImageData) : Blob.valueOf(ImageData);//Blob.valueOf(ImageData);
        conVersion.Title = 'Statement_'+MID+'_'+system.Today();
        conVersion.pathOnClient = '/StatementFile.'+FileType;
        if(isInsert){
            insert conVersion;
        }
        return conVersion;
    }
    
    //****************************************************************************
    // Method to create a FeedItem record 
    // @param string : MID
    // @param string  : externalDocId
    // @param string : contentId
    // @param isInsert a boolean indicating whether the user is to be inserted or not
    // @return an FeedItem object
    //****************************************************************************  
    public static FeedItem SaveFeedItems(string MID,External_Document__c externalDoc,ContentVersion cntentVersion,boolean isInsert){
        FeedItem post = new FeedItem();
        post.Body = 'Statement_'+MID+'_'+system.Today();
        post.ParentId = externalDoc!=null ?externalDoc.Id:'';
        post.RelatedRecordId  = cntentVersion!=null ?cntentVersion.Id:'';
        post.Type = 'ContentPost';
        post.Title = 'Statement_'+MID+'_'+system.Today();
        if(isInsert){
            insert post;
        }
        return post;
    }
    
    //End
    //****************************************************************************
    //Modify createReference method for creating reference record based on new External Key
    //26/4/2017 Neeraj Kumawat T-596676
    //****************************************************************************
    //***************************************************************************
    // Method to create Reference record.
    // @param Integer : count Counter for craeting number of records
    // @param Boolean  : isInsert Boolean variable to check insert of record or not
    // @return List of Reference Object
    //***************************************************************************
    public static List<Reference__c> createReference(Integer count, Boolean isInsert,String cleintGroup,
                                                     String ProcessingCenterId,Boolean isAssociationCodeEmpty){
                                                         List<Reference__c> referenceList=new List<Reference__c>();
                                                         for(Integer i=0; i<count; i++){
                                                             Reference__c ref=new Reference__c();
                                                             ref.Name='100'+i;
                                                             if(cleintGroup!=null && ProcessingCenterId!=null){
                                                                 if(isAssociationCodeEmpty){
                                                                     ref.External_ID__c=cleintGroup+'-'+ProcessingCenterId;
                                                                 }else{
                                                                     ref.External_ID__c=cleintGroup+'-100'+i+'-'+ProcessingCenterId;
                                                                 }
                                                                 
                                                             }else if(cleintGroup==null && ProcessingCenterId!=null){
                                                                 if(isAssociationCodeEmpty){
                                                                     ref.External_ID__c=ProcessingCenterId;
                                                                 }else{
                                                                     ref.External_ID__c='100'+i+'-'+ProcessingCenterId;  
                                                                 }
                                                             }else if(cleintGroup!=null && ProcessingCenterId==null){
                                                                 if(isAssociationCodeEmpty){
                                                                     ref.External_ID__c=cleintGroup+'-100'+i;
                                                                 }else{
                                                                     ref.External_ID__c=cleintGroup;
                                                                 }
                                                                 
                                                             }else if(cleintGroup==null && ProcessingCenterId==null){
                                                                 ref.External_ID__c='100'+i;
                                                             }
                                                             ref.Description__c='Test description'+i;
                                                             ref.Category__c='Association Code';
                                                             referenceList.add(ref);
                                                         }
                                                         if(isInsert){
                                                             insert referenceList;     
                                                         }
                                                         return referenceList;
                                                     }
    //End of createReference method//
    //***************************************************************************
    // Method to create Asset record.
    // @param Integer : count Counter for craeting number of records
    // @param Boolean  : isInsert Boolean variable to check insert of record or not
    // @return List of Asset Object
    //***************************************************************************
    public static List<Asset> createAsset(Integer count, Boolean isInsert){
        List<Asset> assetList=new List<Asset>();
        for(Integer i=0; i<count; i++){
            Asset asst=new Asset();
            asst.Name='Test Asset'+i;
            assetList.add(asst);
        }
        if(isInsert){
            insert assetList;     
        }
        return assetList;
    }
    //***************************************************************************
    // Method to create External_Link_Kav Article
    // Updated to add  counter in title and url name to generate unique url name records
    //***************************************************************************
    
    public static List<External_Link__kav> createExternal_linkArticle(Integer count,Boolean isInsert){
        List<External_Link__kav> External_Link_List = new List<External_Link__kav>();
        for(Integer i = 0; i < count; i++){
            External_Link__kav ext = new External_Link__kav();
            ext.title = 'testExternalLink'+i;
            ext.UrlName = 'testExternalLink'+i;
            ext.Summary='testExternalSummary';
            ext.Expiration_Type__c='Active - Compliance';
            External_Link_List.add(ext);
        }
        if(isInsert){
            insert External_Link_List;
        }
        return External_Link_List;  
    }
    //***************************************************************************
    // Method to create Service Contract record.
    // @param Integer : count Counter for craeting number of records
    // @param acctId : Id of account
    // @param Boolean  : isInsert Boolean variable to check insert of record or not
    // @return List of serviceContract Object
    //***************************************************************************
    public static List<ServiceContract> createServiceContract(Integer count, Id acctId, Boolean isInsert){
        List<ServiceContract> serviceContractList=new List<ServiceContract>();
        for(Integer i=0; i<count; i++){
            ServiceContract  serviceCont=new ServiceContract ();
            serviceCont.Name='Test ServiceContract'+i;
            serviceCont.AccountId=acctId;
            serviceContractList.add(serviceCont);
        }
        if(isInsert){
            insert serviceContractList;     
        }
        return serviceContractList;
    }
    //***************************************************************************
    // Method to create Tax Information record.
    // @param Integer : count Counter for craeting number of records
    // @param Boolean  : isInsert Boolean variable to check insert of record or not
    // @return List of tax Information Object
    //***************************************************************************
    public static List<Tax_Information__c> createTaxInformation(Integer count, Boolean isInsert){
        List<Tax_Information__c> taxInformationList=new List<Tax_Information__c>();
        for(Integer i=0; i<count; i++){
            Tax_Information__c  taxInfo=new Tax_Information__c();
            taxInformationList.add(taxInfo);
        }
        if(isInsert){
            insert taxInformationList;     
        }
        return taxInformationList;
    }
    
    //***************************************************************************
    // Method to create Opportunity record.
    //***************************************************************************
    public static List<Opportunity> createOpportunity(Integer count, Boolean isInsert){
        List<Opportunity> opportunityList = new List<Opportunity>();
        for(Integer i=0; i<count; i++){
            opportunityList.add(new Opportunity(Name = 'Test Opportunity'+i,CloseDate=System.now().Date(),StageName='Qualification'));      
        }
        if(isInsert){
            insert opportunityList;     
        }
        return opportunityList;
    }
    //***************************************************************************
    // Method to create Partner record.
    //***************************************************************************
    public static void createPartner(Boolean isInsert,Id AccountToIdValue,Id AccountFromIdVlaue){
        
        Partner par=new Partner();
        par.AccountToId=AccountToIdValue;
        par.AccountFromId=AccountFromIdVlaue;
        if(isInsert){
            insert par;
        }
    } 
    //***************************************************************************
    // Method to create workOrder record.
    // @param Integer : workOrderCount Counter for creating number of records
    // @param Boolean  : isInsert Boolean variable to check insert of record or not
    // @return WorkOrderList of workOrder Object
    // 16-May-2017 Neeraj Kumawat T-602715
    //***************************************************************************
    public static List<WorkOrder> createWorkOrder(Integer workOrderCount, Boolean isInsert)
    {
        List<WorkOrder> workOrderList = new List<WorkOrder>();
        for(Integer i = 0; i < workOrderCount; i++){
            WorkOrder wrkOrder = new WorkOrder();
            workOrderList.add(wrkOrder);
        }
        if(isInsert){
            insert workOrderList;
        }
        return workOrderList;  
    }   
    //***************************************************************************
    // Method to create workOrderLineItem record.
    // @param Integer : workOrderLineItemCount Counter for creating number of records
    // @param Boolean  : isInsert Boolean variable to check insert of record or not
    // @return WorkOrderLineItemList of workOrderLineItem Object
    // 25-May-2017 Neeraj Kumawat T-603560
    //***************************************************************************
    public static List<WorkOrderLineItem> createWorkOrderLineItem(Integer workOrderLineItemCount, Boolean isInsert)
    {
        List<WorkOrderLineItem> workOrderLineItemList = new List<WorkOrderLineItem>();
        for(Integer i = 0; i < workOrderLineItemCount; i++){
            WorkOrderLineItem wrkOrderLineItem = new WorkOrderLineItem();
            workOrderLineItemList.add(wrkOrderLineItem);
        }
        if(isInsert){
            insert workOrderLineItemList;
        }
        return workOrderLineItemList;  
    }
    //***************************************************************************
    // Method to create ReferenceList For WorkOrderLineItem record.
    // @param Integer : referenceCount Counter for creating number of records
    // @param Boolean  : isInsert Boolean variable to check insert of record or not
    // @return reference of workOrderLineItem Object
    // 25-May-2017 Neeraj Kumawat T-603560
    //***************************************************************************
    public static List<Reference__c> createReferenceListWOLI(Integer referenceCount,boolean isInsert)
    {   
        List<Reference__c> referenceListWOLI = new List<Reference__c>();
        for(Integer i = 0; i < referenceCount; i++){
            Reference__c reference = new Reference__c();
            reference.Name='Sample Reference';
            referenceListWOLI.add(reference);
        }
        if(isInsert){
            insert referenceListWOLI;
        }
        return referenceListWOLI;  
    }
    //*****************************************************************************
    // Method to create FeeChargeList for inserting FeeCharge record.
    // @param Integer : feeChargeCount Counter for creating number of records
    // @param Boolean  : isInsert Boolean variable to check insert of record or not
    // @return feeChargeList of FeeCharge Object
    // 1-June-2017 Neeraj Kumawat
    //*****************************************************************************
    public static List<Fee_Charge__c> createFeeCharge(Integer feeChargeCount,boolean isInsert)
    {   
        List<Fee_Charge__c> feeChargeList = new List<Fee_Charge__c>();
        for(Integer i = 0; i < feeChargeCount; i++){
            Fee_Charge__c fee = new Fee_Charge__c();
            fee.External_ID__c='Sample';
            feeChargeList.add(fee);
        }
        if(isInsert){
            insert feeChargeList;
        }
        return feeChargeList;  
    }
}