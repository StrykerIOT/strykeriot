/*********************************************************************
 * Appirio, Inc
 * Name: ExternalDocumentRecordsDeletion
 * Description: [Task# T-557407]
 * Created Date: [12/21/2016]
 * Created By: [Aashish Sajwan] (Appirio)
 * 
 * Date Modified                Modified By              Description of the update
 * [Wed 28, 2016]               [Aashish Sajwan]         [T-557407]
 **********************************************************************/
@isTest
public class ExternalDocumentRecordsDeletionTest
{
    public static External_Document__c objExtDoc;
    public static Date providedDate;
    public static FeedItem fd;
    public static ContentVersion con;
    public static List<External_Document__c> objEx;
    public testmethod static void testCallout()
    {
        ExternalDocumentRecordsDeletion objEDRD=new ExternalDocumentRecordsDeletion();

        TestData();
        
        //Parameter of Parameterised Constructors
        providedDate=Date.Today().addDays(-90);
        
        //IF Part
        objEDRD=new ExternalDocumentRecordsDeletion(providedDate,objExtDoc.Id);
        
        //Check Data
        objEx=[Select Name from External_Document__c];
        System.assertEquals(1,objEx.size());
        System.debug('Created Date Checked');
        
        Test.StartTest();
        
        //Else IF Parts
        objEDRD=new ExternalDocumentRecordsDeletion(null,objExtDoc.Id);
        
        //Else Parts
        objEDRD=new ExternalDocumentRecordsDeletion(providedDate,null);

        //Calling Batch Class
        objEDRD.scheduleClass(providedDate,objExtDoc.Id);
        
        Test.StopTest();
        //Check Data
        objEx=[Select Name from External_Document__c];
        System.assertEquals(0,objEx.size());
        System.debug('Deleted Date Checked');
    }
    //Create Test Data for External Document
    static void TestData()
    {
        Account Acc=TestUtilities.createTestAccount(true);                                   
        //Creating Test External Document Data               
        objExtDoc=TestUtilities.SaveExternalDoc(Acc,'2759115',Acc.MID__c,false,true);
        //Content Version
        con=TestUtilities.SaveContentVersion('pdf','jkhbjhb',Acc.MID__c,true);
        //FeedItem
        fd=TestUtilities.SaveFeedItems(Acc.MID__c,objExtDoc,con,true); 
    }
    
      //****************************************************************************
        // Calling the Scheduler class and executing it.
        //****************************************************************************
        static testmethod void testExternalDocumentBatchScheduler() {
        
        TestData();
        Test.startTest();   
        SchedulableExternalDocumentBatch obj = new SchedulableExternalDocumentBatch();
        obj.execute(null);    
        Test.stopTest();       
      
    }
    
}