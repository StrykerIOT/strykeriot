/*********************************************************************
* Appirio a WiPro Company
* Name: AccountDetailPageController 
* Description: [T-598751 - ControllerClass- For showing Partner and Relationship Account's Special Handling Instructions ]
* Created Date: [1/5/2017]
* Created By: [Aashish Sajwan] (Appirio)
* 
* Date Modified                Modified By                  Description of the update
* [03/5/2017]                 [Aashish Sajwan]             [Change in logic to get Relationship and Partner Account] 
* [13/06/2017] 			      [Gaurav Dudani]              [Added additional filters to show and hide pop-up as per issue # I-278944]
* [16 June 2017]              [Neeraj Kumawat]             [Updated logic to set value of ShowPopup based on SpecialHandlingInstructions value.#T-609951/#I-279350]
**********************************************************************/
public class AccountDetailPageController {
    public Id AccountId;
    public Boolean ShowPopup{set;get;}
    public List<Account> AccNameList{set;get;}
    public List<Account> PartnerNameList{set;get;}
    public List<Account> RelationshipNameList{set;get;}
    public AccountDetailPageController(ApexPages.StandardController controller) {
        PartnerNameList=new List<Account>();
        RelationshipNameList=new List<Account>();
        AccountId = (Id)controller.getRecord().id;
        AccNameList = [SELECT RecordType.Name,SpecialHandlingInstructions__c 
                       FROM Account 
                       WHERE Id in (Select AccountToId from Partner where AccountFromId = :AccountId) 
                       AND RecordType.Name='Partner'];
        for(Account a: AccNameList){
            if(a.RecordType.Name=='Partner'){
                PartnerNameList.add(a);
            }
        }
        Account objAccount = (Account)controller.getRecord();
        // 3 May 2017 Get RelationShip Account on Basis of account's Relationship field value 
        //Start
        //Get RelationShip Account value from current account
        Account objAcc = [Select Relationship__c from Account where Id=: objAccount.Id];
        if(objAcc !=null){
            // Get SpecialHandlingInstructions for relationship Account 
            for(Account accObj: [SELECT RecordType.Name,SpecialHandlingInstructions__c 
                                 FROM Account 
                                 WHERE Id =:objAcc.Relationship__c ]){
                                     RelationshipNameList.add(accObj);
                                     AccNameList.add(accObj);
                                 }
        }
        
        //End
        //Start
        // Add additional filters to display the pop-up.
        // By Gaurav Dudani on 13 June 2017 as per issue # I-278944.
        //Neeraj Kumawat T-609951 on 16 June 2017
        //Updated logic to set value of ShowPopup flag
        //isBlankCount: Integer variable to check SpecialHandlingInstructions count whihc has blank value
        Integer isBlankCount=0;
        for(Account acc : AccNameList) {
            if(String.isBlank(acc.SpecialHandlingInstructions__c)){
                isBlankCount++;
            } 
            // End    
        }
        //Neeraj Kumawat T-609951 on 16 June 2017
        //setting ShowPopup to false if all the Accounts has empty value of SpecialHandlingInstructions 
        if(isBlankCount==AccNameList.size()){
            ShowPopup=false; 
        }else{
            ShowPopup=true;
        }
    }
}