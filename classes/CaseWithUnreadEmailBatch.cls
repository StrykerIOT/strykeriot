/*********************************************************************
* Appirio a WiPro Company
* Name: CaseWithUnreadEmailBatch
* Description: Batch class that runs hourly to remove Cases from the "Article_for_Deletion" Queue if 
*               ALL emails fo rthe Case have been read post Case Close. See the 'CaseWithUnreadEmailBatchHelper' 
*               class for more details regarding specific functionality of individual methods.
* Created Date: 06/08/2017
* Created By: Bobby Cheek
* 
* Date Modified                Modified By                  Description of the update
*
**********************************************************************/
global class CaseWithUnreadEmailBatch implements Database.Batchable<Sobject>{

  CaseWithUnreadEmailBatchHelper helper = new CaseWithUnreadEmailBatchHelper();

  global void CaseWithUnreadEmailBatch() {}
  
  global Database.Querylocator start(Database.Batchablecontext BC)
  {
    //system.debug(LoggingLevel.INFO, '\n\n CaseWithUnreadEmailBatch.start()\n\n');
    return helper.GetQueryLocator(helper.GetQuery());
  }

  global void execute(Database.Batchablecontext BC, list<SObject> scope)
  {
    //system.debug(LoggingLevel.INFO, '\n\n CaseWithUnreadEmailBatch.execute()\n\n');
    helper.ProcessBatch(scope);
  }

  global void finish (Database.Batchablecontext BC)
  {
    //system.debug(LoggingLevel.INFO, '\n\n CaseWithUnreadEmailBatch.finish()\n\n');
  }
}