/************************************************************************
* Appirio, Inc
* Class Name: WorkOrderLineItemController 
* Description: [S-487527]
* Created Date: [06/01/2017]
* Created By: [Kate Jovanovic] (Appirio)

* Date Modified          Modified By             Description of the update
* [2-June-2017]         [Neeraj Kumawat]         [T-607444 Updated to get dependent picklist value. Created method checkDuplicateRecord]
* [7-June-2017]         [Neeraj Kumawat]         [T-607881 Updated to prepopulate workOrder Id in workorder lookup]
* [14-June-2017]        [Neeraj Kumawat]         [T-609310 Updated Save method to restrict WOLI creation on Closed/Canceled workOrder]
* [14-June-2017]        [Poornima Bhardwaj]      [T-609556 Updated checkRecordType method]
***********************************************************************/

public  class WorkOrderLineItemController {
    public String rtType{get;set;}
    public Integer size{get;set;}
    public Integer noOfRecords {get;set;}
    //Flag to check that page is already redirected
    public String isRedirected='';
    public  ApexPages.StandardController standardCont;
    public String duplateMessage {get;set;}
    public  List<WorkOrderLineItem> duplicateWorkOrderList {get;set;}
    public Integer saveCounter {get;set;}
    //WorkOrderLineItem to prepopulate workOrder Id in Work Order lookup
    public WorkOrderLineItem wrkOrderLineItem{get;set;}
    //WorkOrder_lkid to show in url parameter for showing workoderid in workorder lookup
    public String WorkOrder_lkid='';
    //WorkOrder to show in url parameter for showing workoderid in workorder lookup
    public String workOrder='';
    public WorkOrderLineItemController(ApexPages.StandardController controller) {
        size=10;
        duplateMessage='';
        rtType = ApexPages.currentPage().getParameters().get('RecordType');
        isRedirected=ApexPages.currentPage().getParameters().get('isRedirected');
        saveCounter =0;
        wrkOrderLineItem=new WorkOrderLineItem();
        // #T-607881 Neeraj Kumawat Updated to repopulate workOrder Id in workorder lookup
        WorkOrder_lkid=ApexPages.currentPage().getParameters().get('WorkOrder_lkid');
        wrkOrderLineItem.WorkOrderId=WorkOrder_lkid;
        workOrder=ApexPages.currentPage().getParameters().get('WorkOrder');
        standardCont=controller;
    }
    //******************************************************************************************
    // Method to override Standard Work Order Line Item page if Record Type =Adjustments
    // 1 June 2017 Kate Jovanovic
    //******************************************************************************************
    public PageReference checkRecordType(){
        //Not going to redirect the page if checkDuplicateRecord flag is set to true.
        if(isRedirected!='true'){
            List<RecordType> recordTypeList = [Select Id, Name,DeveloperName FROM RecordType where Id =:rtType];
            for(RecordType rt: recordTypeList){
                if(rt.DeveloperName =='NA_Adjustment_Work_Order_Line_Item' || rt.DeveloperName  =='MX_Adjustment_Work_Order_Line_Item'){

                    PageReference page = new PageReference('/apex/WorkOrderLineItem');
                    page.getParameters().put('sfdc.override', '1');
                    page.getParameters().put('RecordType', rtType);
                    page.getParameters().put('isRedirected', 'true');
                    // #T-607881 Neeraj Kumawat Updated to repopulate workOrder Id in workorder lookup
                    page.getParameters().put('WorkOrder', workOrder);
                    page.getParameters().put('WorkOrder_lkid', WorkOrder_lkid);
                    page.setRedirect(true);
                
                    return page;
                }
                else{
                    PageReference standardpage = new PageReference('/1WL/e?nooverride=1');
                    // #T-607881 Neeraj Kumawat Updated to repopulate workOrder Id in workorder lookup
                    standardpage.getParameters().put('WorkOrder', workOrder);
                    standardpage.getParameters().put('WorkOrder_lkid', WorkOrder_lkid);
                    //Added by Poornima Bhardwaj on 14-June-2017 as per Task #T-609556
                    //Start
                    standardpage.getParameters().put('RecordType', rtType);
                    //End
                    standardpage.setRedirect(true);
                    return standardpage;
                }

            

            }
        }
        return null;
    }

    //****************************************************************************************************
    // Method to override Save method to show duplicate matches
    // 1 June 2017 Kate Jovanovic
    // Modified on [14 June 2017] by Neeraj Kumawat as per #T-609310 
    //To restrict WOLI creation on Closed/Canceled workOrder #T-609310 
    //****************************************************************************************************
      public PageReference save(){
        WorkOrderLineItem workOrderLineItemObj = (WorkOrderLineItem)standardCont.getRecord();
        workOrderLineItemObj.WorkOrderId=wrkOrderLineItem.WorkOrderId;
        String workOrderId= workOrderLineItemObj.WorkOrderId;
        duplicateWorkOrderList= checkDuplicateRecord(workOrderLineItemObj);
        saveCounter++;
        WorkOrder wrkOrderObj=[Select Id,Status From WorkOrder Where Id =:workOrderId];
        String status=wrkOrderObj.Status;
        //Updated By Neeraj Kumawat #T-609310
        //Save the record when the user has clicked saved twice. On the first click the duplicate WOLI will show.
        //Start
        if ((status=='Closed' || status=='Canceled') && saveCounter >= 1)
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,System.Label.Restrict_WOLI_Creation_ON_Closed_OR_Canceled_WorkOrder));
        }
        else if(status!='Closed' && status!='Canceled' && saveCounter >= 2)
        {   
            upsert workOrderLineItemObj;
            PageReference page = new PageReference('/'+workOrderLineItemObj.Id);
            return page;
        }
        //End
        return null;
    }

    //******************************************************************************************
    // Method to override cancel button to bring the user back to the Work Order.
    // 5 June 2017 Kate Jovanovic
    //******************************************************************************************
    public PageReference cancel() {
        WorkOrderLineItem workOrderLineItemObj = (WorkOrderLineItem)standardCont.getRecord();
        PageReference page = new PageReference('/'+WorkOrder_lkid);
        return page;
        
    }
    //******************************************************************************************
    // Method to check Duplicate work order line ApexTestQueueItem
    // @param workOrderLineItemObj:workOrderLineItem record 
    // @return List of duplicate WorkOrderLineItem
    //  June 2017 Neeraj Kumawat #T-603560
    //******************************************************************************************
    public List<WorkOrderLineItem> checkDuplicateRecord(WorkOrderLineItem workOrderLineItemObj){
        List<WorkOrderLineItem> duplicateWorkOrderList=new List<WorkOrderLineItem>();
        Decimal lineItemTotal;
        String adjustmentType = workOrderLineItemObj.Adjustment_Type__c;
        Decimal lineItemQuantity = workOrderLineItemObj.Line_Item_Quantity__c;
        Decimal actualFeeBilled = workOrderLineItemObj.Actual_Fee_Billed__c;
        Decimal lineItemAmount = workOrderLineItemObj.Line_Item_Amount__c;
        String adjustmentReason = workOrderLineItemObj.Adjustment_Reason__c;
        //Set WorkOrder Id from user input work order lookup
        workOrderLineItemObj.WorkOrderId=wrkOrderLineItem.WorkOrderId;
        String workOrderId= workOrderLineItemObj.WorkOrderId;
        String workOrderAccountName=null;
        //set Line Item Total
        if(adjustmentType == 'Customer Credit'){
            lineItemTotal = ((lineItemQuantity*actualFeeBilled) - (lineItemQuantity*lineItemAmount)) * -1;
            
        }
        if((lineItemQuantity * actualFeeBilled) > (lineItemQuantity* lineItemAmount)){
            lineItemTotal = ((lineItemQuantity *actualFeeBilled) - (lineItemQuantity * lineItemAmount));
        }
        
        workOrderLineItemObj.Line_Item_Total__c = lineItemTotal;
        //Get created workOrderLineItem account Name from Work Order Account
        WorkOrder wrkOrderObj=[Select Id, Account.Name From WorkOrder Where Id =:workOrderId];
        if(wrkOrderObj.Account!=null){
            workOrderAccountName=wrkOrderObj.Account.Name;
        }
        //Fetch dulicate Work order line item records
        duplicateWorkOrderList=[Select Id, WorkOrderId, Account_Name__c,MID__c,Adjustment_Reason_Category__c,
                                Adjustment_Reason__c,Subject, Line_Item_Total__c,LastModifiedDate, Processing_Date__c
                                From WorkOrderLineItem Where Line_Item_Total__c=:lineItemTotal AND 
                                Adjustment_Reason__c=:adjustmentReason AND Account_Name__c=:workOrderAccountName AND recordTypeId =:rtType];
        if(duplicateWorkOrderList.isEmpty()){
            duplateMessage = System.Label.No_Duplicate_WorkOrderLineItem ;
        }
        else{
            duplateMessage = System.Label.Duplicate_WorkOrderLineItems ;
        }
        
        return duplicateWorkOrderList;
    }
    
}