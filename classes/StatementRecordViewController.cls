/*********************************************************************
* Appirio, Inc
* Name: ViewStatement
* Description: [Task# T-557407]
* Created Date: [11/30/2016]
* Created By: [Aashish Sajwan] (Appirio)
* 
* Date Modified                Modified By                  Description of the update
* [Mon 05, 2016]               [Aashish Sajwan]         [T:557407 ]
**********************************************************************/
public with sharing class StatementRecordViewController {
    //Variables 
    private static final String CONTENT_POST = 'ContentPost';
    public string StatementId {get;set;}
    public string AccountId {get;set;}
    public string MID {get;set;}
    public string ImageData {get;set;}
    public string ExternalDataId {get;set;}
  
    public Id contentId {get;set;}
    public Account accountRecord {get;set;}
    public String EndUserID = 'CSPR-int';
    public Map<string,External_Document__c> mapExternalDocs;                        //Added -SSA (Access Modifiers)
    
    
    //Constructor
    public StatementRecordViewController (){
        
        //Create Map of External Document Record
        mapExternalDocs = new Map<string, External_Document__c >();
        
        // Create Map for Parameter which pass on Page url
        Map<String, String> urlParams = Apexpages.currentPage().getParameters();  
        
        //Assign StatementId on basis of Url Parameter
        StatementId = urlParams.containsKey('StatementId') ? EncodingUtil.urlDecode(urlParams.get('StatementId'),'UTF-8') : '';
        
        //Assign AccountId on basis of Url Parameter
        AccountId = urlParams.containsKey('AccountId') ? EncodingUtil.urlDecode(urlParams.get('AccountId'),'UTF-8') : '';
        
        //Assign MID on basis of Url Parameter
        MID = urlParams.containsKey('MID') ? EncodingUtil.urlDecode(urlParams.get('MID'),'UTF-8') : '';
       
        system.debug('** StatementId  value **'+StatementId );
        system.debug('** ExternalDataId value **'+ExternalDataId);
        
        //Get External Document Record on basis of MID of Account which paased as parameter
        for(External_Document__c externalObj : ([SELECT Id, Expiry_Date__c,Account__c,MPSMID__c
                                                 ,ExternalID__c
                                                 FROM External_Document__c
                                                 Where MPSMID__c =: MID])){
          //Fill data in mapExternalDocs key will be External Id of record and value will be External record 
          mapExternalDocs.put(externalObj.ExternalID__c,externalObj);
        }
        system.debug('** mapExternalDocs Keys **'+mapExternalDocs.keySet());
        system.debug('** mapExternalDocs Values **'+mapExternalDocs.values());
        
        //Get Account Record on basis of AccountId passed on Page
        accountRecord = [Select Name,Processing_Center_ID__c,Owner.Name,Account_Owner_Role__c from Account Where Id =: AccountId ];
        
        
        
    }
    
    /**
     *  @description    :   This method to Process record It will Hit Api if record not found in salesforce
                               
     *
     *  @args           :  
     *
     *  @return         :   PageRefernece Opens Content Record in Browser
         
     *
     **/
        
    public PageReference ProcessRecord(){
       //Set PageReference Variable
       PageReference pg ;
       Id ObjId = null;
        try{ 
                              
            //Check StatementId is exist in External Doc or not
            if(mapExternalDocs.containsKey(StatementId)){
                system.debug('** mapExternalDocs Keys **'+mapExternalDocs.keySet());
                system.debug('** mapExternalDocs Values **'+mapExternalDocs.values());
                system.debug('** StatementId Values **'+StatementId);
                system.debug('** Key Contains in mapExternalDocs **'+mapExternalDocs.containsKey(StatementId));
                
                //Reterive External Doc record from mapExternalDocs basis of StatementId
                External_Document__c  objExternDoc = mapExternalDocs.get(StatementId);
               
                system.debug('** objExternDoc Id values **'+objExternDoc.Id);                
                system.debug('** objExternDoc  values **'+objExternDoc);
                
                //Check objExternDoc not null and its Id also not null
                if(objExternDoc!= null && objExternDoc.Id!=null){
                  //Get Feed Item related with Externaldoc Record
                        List<FeedItem> feedItems = [SELECT ParentId, RelatedRecordId 
                                                    FROM FeedItem 
                                                    WHERE ParentId = : objExternDoc.Id
                                                    AND Type = :CONTENT_POST 
                                                    AND HasContent = :true];
                       //Check FeedItem is not Empty
                        if(!feedItems.isEmpty()) {
                            //Create Map of feedItems related with External Document
                            Map<Id, External_Document__c> cv_to_ext = new Map<Id, External_Document__c>();
                            //Itereate Feed Item List 
                            for(FeedItem fItem : feedItems) {
                            
                                //Set contentId which is RelatedRecordId in FeedItem Record
                                contentId = fItem.RelatedRecordId;
                                
                                //Add FeedItem Releated Record Id as key and External Document Record as value in cv_to_ext map
                                cv_to_ext.put(fItem.RelatedRecordId,objExternDoc);
                            }
                            
                            /*List<ContentVersion> cvs = [SELECT Id, Title, VersionNumber, PathOnClient, 
                                                        FileType, FileExtension, ContentSize
                                                        FROM ContentVersion 
                                                        WHERE Id IN :cv_to_ext.keySet() AND IsLatest = true];
                            for(ContentVersion cv : cvs) {
                                contentId = cv.Id;
                                
                            }*/
                        }
                        
                    
                    
                }
                
            }else{
                //Create Request for API
                wwwElavonComServicesImagingserviceU.UAT01  request = new wwwElavonComServicesImagingserviceU.UAT01();
                
                // Get ImageList Response from API
                wwwElavonComSchemaImagingresourcesV.StatementImageResponse_element statementresponse = request.retrieveStatement(StatementId, System.Label.Elavon_Integration_End_User_ID);
                
                //Check Statement is not null 
                if(statementresponse!=null){
                    System.debug('@@@@ statement data is '+ statementresponse);
                    System.debug('@@@@ statement image data is '+ statementresponse.ImageData);
                    
                    //Get File Type on basis of Satementresponse Image Data
                    string FileType =  statementresponse.ImageData!=null?'pdf':'xml';
                   
                    //Set Image Data for Content by checking ImageData on response
                    ImageData = statementresponse.ImageData!=null ? statementresponse.ImageData : statementresponse.XMLData;
                   
                    //Invoke Save Data method by passing ImageData, StatementDate and FileType
                    SaveData(ImageData,FileType); 
                    
                    
                    System.debug('@@@@ statement xml data is '+ statementresponse.XMLData);
                }
                
            }
            
            
            
        }catch(Exception e){
            System.debug('@@@@ FAILURE');
        }
        
        //Check ContentId is not null 
        if(contentId!=null){
        
             System.debug('@@@@ contentId value'+contentId);
             //Set PageReference with ContentId
             pg = new PageReference('/'+contentId);
                     
        } 
        //Return PageReference 
        return pg;
        
    }
    
    
     /**
     *  @description    :   This method to save external document, Content Version and FeedItem record on salesforce
                               
     *
     *  @args           : ImageData: Value from Response
                          StatementDate :  Date : StatementDate from Response
                          FileType: String : FileType on basis of Response it will be pdf or xml
     *
     *  @return         :   Void
     *
     **/
    
    public void SaveData(string ImageData,String FileType){
       
        try{
            
            System.debug('@@@@ ImageData Values '+ImageData);
           
            //Create External Document Record
            External_Document__c externalDoc = new External_Document__c();
            externalDoc.Account__c = AccountId; //MID related with Account
            externalDoc.Expiry_Date__c = system.Today().addDays(2);// StatementDate.addDays(2);
            externalDoc.ExternalID__c= StatementId;
            externalDoc.MPSMID__c = MID;  
            externalDoc.AccountOwnerName__c =  (accountRecord.Account_Owner_Role__c!=null? accountRecord.Account_Owner_Role__c:''); // Owner Role Name
            externalDoc.AccountProcessingCenterID__c =accountRecord.Processing_Center_ID__c;
            externalDoc.AccountName__c=accountRecord.Name;                   
            
            //Insert External Document Record
            insert externalDoc;
            
            //Create Content Version Reord 
            ContentVersion  conVersion = new ContentVersion();
            conVersion.VersionData = (FileType=='pdf')? EncodingUtil.base64Decode(ImageData) : Blob.valueOf(ImageData);//Blob.valueOf(ImageData);
            conVersion.Title = 'Statement_'+MID+'_'+system.Today();
            conVersion.pathOnClient = '/StatementFile.'+FileType;
            
            //Insert Content Version Record
            insert conVersion;
            
            //Create FeedItem for External document Record
            FeedItem post = new FeedItem();
            post.Body = 'Statement_'+MID+'_'+system.Today();
            post.ParentId = externalDoc.Id;
            post.RelatedRecordId  = conVersion.Id;
            post.Type = 'ContentPost';
            post.Title = 'Statement_'+MID+'_'+system.Today();
            
            //Insert Feed Item
            insert post;
            
            system.debug('** Post Id **'+post.Id);
            system.debug('** conVersion Id **'+conVersion.Id);
            
            //Set Content Id on Variable
            contentId = conVersion.Id;
           
        }
        catch(Exception ex){
            System.debug('@@@@ FAILURE ex'+ex);
        }
        
    }
    
    
}