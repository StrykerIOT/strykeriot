/*********************************************************************
* Appirio, Inc
* Name: CommonUtilities
* Description: [T-558200-CommonUtilities Class for handling Common Activities.]
* Created Date: [01/12/2016]
* Created By: [Neeraj Kumawat] (Appirio)
* 
* Date Modified                Modified By                  Description of the update
* [26-May-2017]               [Aashish Sajwan]              [Added in Dev5 as per Dev4 and Dev5 sync]
**********************************************************************/
public class CommonUtilities {
    /**
* This method is used to check current user has specified custom Permission.
* @param customPermissionName : Name of Custom Permission
* @return Boolean: return true if current user has specified custom Permission else return false.
* */
    public static Boolean hasCustomPermission(String customPermissionName){
        Set<String> customPermissionsForCurrentUser = new Set<String>();
        Map<Id, String> customPermissionNamesById = new Map<Id, String>();
        // Query the full set of Custom Permissions for the given namespace
        List<CustomPermission> customPermissions = 
            [select Id, DeveloperName from CustomPermission];
        System.debug('CustomPermissions Value='+customPermissions);
        for(CustomPermission customPermission : customPermissions) {
            //customPermissionNames.add(customPermission.DeveloperName);
            customPermissionNamesById.put(customPermission.Id, customPermission.DeveloperName);
        }
        
        System.debug('customPermissionNamesById Values='+customPermissionNamesById);
        // Query to determine which of these custome settings are assigned to this user
        List<SetupEntityAccess> setupEntities = 
            [SELECT SetupEntityId
             FROM SetupEntityAccess
             WHERE SetupEntityId in :customPermissionNamesById.keySet() AND
             ParentId
             IN (SELECT PermissionSetId 
                 FROM PermissionSetAssignment
                 WHERE AssigneeId = :UserInfo.getUserId())];  
        
        System.debug('SetupEntities Values='+setupEntities);
        for(SetupEntityAccess setupEntity : setupEntities){
            customPermissionsForCurrentUser.add(customPermissionNamesById.get(setupEntity.SetupEntityId));
        }
        System.debug('CustomPermissions Value for CurrentUser='+customPermissionsForCurrentUser);
        return customPermissionsForCurrentUser.contains(customPermissionName);
    }
}