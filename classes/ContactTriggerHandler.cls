/***********************************************************************************************************************
* Appirio a WiPro Company
* Trigger Name: ContactTriggerHandler
* Description: Trigger updates contact name fields; created to replace Process Builder that hit query limits during 
*              integration calls (bulkification)
* Created Date: 02/20/2017
* Created By: Shannon Howe (Appirio)
* 
* Date Modified                Modified By                Description of the update
* [02/20/2017]              [Shannon Howe]               [Created to resolve process builder SOQL limits]
* [02/21/2017]              [Aashish Sajwan]             [Optimization of TriggerHandler #T-579350]
* [06/02/2017]              [Neeraj Kumawat]             [Optimize code for calling SOQL]
* [13/04/2017]              [Neeraj Kumawat]             [Added check for inactive contact status #T-593628]
* [04/26/2017]              [Bobby Cheek]                [Added Method setCurrencyISOCodeOnContact]
* [06/16/2017]              [Poornima Bhardwaj]          [#T-609690 Deleted updateContactTaxId and updateTaxId method]
**********************************************************************/
public class ContactTriggerHandler {
 
    /*****************************************************************
  * Methods for calling action on Before Insert Update of AdditionalAddress
  ******************************************************************/
    
    //insert
    public static void onBeforeInsert(list<Contact> newContactList){
        
        contactNameFieldsInsert(newContactList); 
        setCurrencyISOCodeOnContact(newContactList);
    }
    
    //update
    public static void onBeforeUpdate(list<Contact> updatedContactList, List<Contact> oldContactList, map<Id,Contact> updatedContactIdToContactMap, map<Id,Contact> oldContactIdToContactMap){
        
        //contactNameFieldsUpdate(oldConMap, nList);
        contactNameFieldsUpdate(oldContactIdToContactMap, updatedContactList); 
        //Added Code merge Prod and Dev Sync 
        //Checking condition on contact status change
        checkInactiveContactStatus(oldContactIdToContactMap,updatedContactList);
    }
    
  /*****************************************************************
  * Method for Updating Contacts On Insertion
  ******************************************************************/
    
    public static void contactNameFieldsInsert(List<Contact> conList){    
        
        List <Contact> uContactList = new List<Contact>();
        //Names are changed
        For (Contact c : conList){
            //Assign Standard Field when fields are out of sync and both
            //fields have a value
            if(c.suffix != null && c.suffix != c.suffix__c ){
                c.suffix__c = c.suffix;
                uContactList.add(c); 
            }
            //Assign custom field value when standard field is blank
            //on inserts
            if(c.suffix__c != null && c.suffix == null){
                
                c.suffix = c.suffix__c;
                uContactList.add(c); 
            }
            
            //Middlename standard field out of sync
            if(c.MiddleName != null && c.MiddleName != c.MiddleName__c ){
                c.MiddleName__c = c.MiddleName;
                uContactList.add(c); 
            }
            //Middlename custom field out of sync
            if(c.MiddleName__c != null && c.MiddleName == null){
                
                c.MiddleName = c.MiddleName__c;
                uContactList.add(c); 
            }
            
        }
    }
    
    /*****************************************************************
  * Method for Updating Contacts On Update
  ******************************************************************/
    public static void contactNameFieldsUpdate(Map <Id,Contact> cMap , List <Contact> ncList){
        
        List <Contact> uContactList = new List<Contact>();
        
        for(Contact nc: ncList){
            //Suffix is changed
            if(nc.Suffix != nc.Suffix__c && cMap.get(nc.Id).Suffix != nc.Suffix){
                
                nc.Suffix__c = nc.Suffix;
                
            }
            //Suffix__c is changed
            
            system.debug('** Old Map value of nc.Suffix**'+nc.Suffix );
            system.debug('** Old Map value of nc.Suffix__c**'+nc.Suffix__c );
            system.debug('** Old Map value of Suffix__c**'+cMap.get(nc.Id).Suffix__c );
            system.debug('** Old Map value of nc.Suffix__c**'+nc.Suffix__c);
            
            
            //IF Custom Field Change then update Standard Field if there are Std field  and Custom Field values are differnt current record
            if(nc.Suffix != nc.Suffix__c && cMap.get(nc.Id).Suffix__c != nc.Suffix__c){
                nc.Suffix = nc.Suffix__c;
                
            }
            //MiddleName is changed
            if(nc.MiddleName != nc.MiddleName__c && cMap.get(nc.Id).MiddleName != nc.MiddleName){
                
                nc.MiddleName__c = nc.MiddleName;
                
            }
            
            //MiddleName__c is changed
            if(nc.MiddleName__c != nc.MiddleName && cMap.get(nc.Id).MiddleName__c != nc.MiddleName__c){
                
                nc.MiddleName = nc.MiddleName__c;
                
                
            }
            uContactList.add(nc);
        }
        
    }
  
  

    
  /*  author : Appirio, Inc
    date : 04-26-2017
    description :  Set Contact Currency ISO Codes to match Account
    paramaters : list of new Contact objects
    returns : nothing
  */
  public static void setCurrencyISOCodeOnContact(list<Contact> newContactList){
    
    System.debug(LoggingLevel.INFO, 'INSIDE setCurrencyISOCodeOnContact');  
    
    set<Id> accountIdSet = new set<Id>();
    
    for(Contact con : newContactList){
      if(con.AccountId != null){
        accountIdSet.add(con.AccountId);
      }
    }  
    
    System.debug(LoggingLevel.INFO, 'ACCOUNT ID SET :: ' + accountIdSet + '\n\n');  
    
    map<Id, Account> accountIdToAccountMap = new map<Id, Account>([SELECT Id, CurrencyISOCode From Account WHERE Id IN : accountIdSet]);
    
    System.debug(LoggingLevel.INFO, 'ACCOUNT MAP :: ' + accountIdToAccountMap + '\n\n');
    
    for(Contact con : newContactList){
      if(accountIdToAccountMap.ContainsKey(con.AccountId)){
        con.CurrencyISOCode = accountIdToAccountMap.get(con.AccountId).CurrencyISOCode;
      }
    }  
  }
  
    //****************************************************************************
    // Method to show error message when contact status changed to inactive
    // @param oldMap: Map of old contact
    // @param newContactList: list of new Contact
    // @return void
    //****************************************************************************   
    public static void checkInactiveContactStatus(Map<Id,Contact> oldMap,List<Contact> newContactList){  
        Map<Id,Contact> contactMap=new Map<Id,Contact>([Select Id,(Select Id, Subject From Cases 
                                                                   Where status!='closed'),
                                                        (Select Id, Subject From Tasks Where status!='Complete') 
                                                        From Contact where Id IN: newContactList]);
        for(Contact objContact : newContactList){
            String oldContactStatus=oldMap.get(objContact.Id).Contact_Status__c;
            //Checking contact status is updated to inactive
            if(objContact.Contact_Status__c !=oldContactStatus && objContact.Contact_Status__c=='Inactive'){
                List<Case> openCaseList=contactMap.get(objContact.Id).Cases;
                List<Task> openTaskList=contactMap.get(objContact.Id).tasks;
                //Showing error message when there is open case or task on contact
                if(openCaseList.size()>0 || openTaskList.size()>0 ){
                    objContact.addError('Contact with open cases or tasks can not be inactive.');
                }
            }
        }
    }
}