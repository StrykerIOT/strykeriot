/*********************************************************************
 * Appirio, Inc
 * Name: ViewDocumentsController
 * Description: [Task# T-557407]
 * Created Date: [11/30/2016]
 * Created By: [Aashish Sajwan] (Appirio)
 * 
 * Date Modified                Modified By              Description of the update
 * [Tue 06, 2016]                [Aashish Sajwan]         [T-557407]
 * [Tue 21, 2016]                [Aashish Sajwan]         [T-557407]
 * [Web 8 Feb, 2017]             [Aashish Sajwan]         [I-253992]
 * [Fri 24 Mar, 2017]            [Aashish Sajwan]        [S-470290]
 * [Mon 3 Apr, 2017]             [Aashish Sajwan]         [Optimization of Code]
 * [Thu 15 Jun, 2017]            [Aashish Sajwan]         [I-279331 Fix null check for Content record]
 **********************************************************************/
 public class ViewDocumentsController{
    private static final String CONTENT_POST = 'ContentPost';
    public List<DocumentData> documentList {get;set;}
    public string AccountId {get;set;}
    public string MID {get;set;}
    public Account accountRecord{get;set;}
    public Id contentId {get;set;}
    public String documentIdValue;                                          //Tue 21, 2016 -SSA (Change is Access Modifier-public Added)
    public wwwElavonComServicesImagingserviceU.UAT01  request;
    public Map<string,External_Document__c> mapExternalDocs;                //Tue 21, 2016 -SSA (Change is Access Modifier-public Added)
    public wwwElavonComSchemaImagingresourcesV.DocumentImageResponse_element response;             //Added
    public UtilityCls.ExternalDocumentWrapper objExtDocWrp;                                        //Added by AS
    public String getdocumentIdValue(){
        return documentIdValue;
    }
    
    public void setdocumentIdValue(String documentIdValue){
        this.documentIdValue = documentIdValue;
    }
    
    //24 March 2017 Added new variable to hold processingCenterId,applicationId value of account #S-470290
    public string  processingCenterId {set;get;}
    public string  applicationId {set;get;}
    
    public ViewDocumentsController(){
        mapExternalDocs = new Map<string, External_Document__c >();
        documentList = new List<DocumentData>();
        request = new  wwwElavonComServicesImagingserviceU.UAT01();
        Map<String, String> urlParams = Apexpages.currentPage().getParameters();  
        AccountId = urlParams.containsKey('AccountId') ? EncodingUtil.urlDecode(urlParams.get('AccountId'),'UTF-8') : '';
        MID = urlParams.containsKey('MID') ? EncodingUtil.urlDecode(urlParams.get('MID'),'UTF-8') : '';
        accountRecord = [Select Name,Processing_Center_ID__c,Owner.Name,Account_Owner_Role__c from Account Where Id =: AccountId ];
        System.debug('Account Record Value'+accountRecord);
        //Get processingCenterID from Url Params (24-03-2017 - #S-470290)
        processingCenterId = urlParams.containsKey('PID') ? EncodingUtil.urlDecode(urlParams.get('PID'),'UTF-8') : '';
        applicationId = urlParams.containsKey('ApplicationID') ? EncodingUtil.urlDecode(urlParams.get('ApplicationID'),'UTF-8') : '';
        system.debug('Application Id value'+applicationId);
        
    }
    public void searchDocuments(){ 
        try{
        if(processingCenterId=='QUERE'){
            MID = 'IM'+applicationId;
            system.debug('Application Id value for MX'+applicationId);
        }
        wwwElavonComSchemaImagingresourcesV.RetrieveImagesResponse_element response = request.retrieveImages(MID,null,null,null);
        if(response!=null && response.Images!=null){
            wwwElavonComSchemaImagingresourcesV.Images_element imageData =  response.Images;
            if(imageData.Image!=null && imageData.Image.size()>0){
                for(wwwElavonComSchemaImagingresourcesV.Image imgData :  imageData.Image)
                {
                    
                        DocumentData docImage = new DocumentData();
                        docImage.docID = imgData.docID ;
                        docImage.sDocTypeID = imgData.sDocTypeID ;
                        docImage.indexDate = imgData.indexDate;
                        system.debug('@@ Index Date @@'+imgData.indexDate);
                        docImage.scanDate = imgData.scanDate  ;
                        docImage.dbaName = imgData.dbaName ;
                        docImage.mid = imgData.mid ;
                        docImage.imageData = imgData.imageData ;
                        docImage.mimeType = imgData.mimeType ;
                        documentList.add(docImage);
                    
                }
            }
            
            
        }
        }catch(Exception ex){
            system.debug('* Response Error** '+response);
            //system.debug('* Response ErrorMessage ** '+response.Error[0].ErrorMessage);
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage()));
        }
        
    }
    
    //****************************************************************************
    // Method to openRecord
    // Crated by : Aashish Singh Sajwan 06 Dec 2016 #T-557407
    // @param null
    // @return PageReference of content Record
    //**************************************************************************** 
    public PageReference openRecord(){
       system.debug('** Open Record **');
       system.Debug('** documentIdValue **'+documentIdValue);
        for(External_Document__c externalObj : ([SELECT Id, Expiry_Date__c,Account__c,MPSMID__c
                                                 ,ExternalID__c
                                                 FROM External_Document__c
                                                 Where MPSMID__c =: MID])){
          mapExternalDocs.put(externalObj.ExternalID__c,externalObj);
        }
        system.debug('** mapExternalDocs Keys **'+mapExternalDocs.keySet());
        system.debug('** mapExternalDocs Values **'+mapExternalDocs.values());
        
        if(mapExternalDocs.containsKey(documentIdValue)){
           objExtDocWrp = UtilityCls.CheckStatementExits(mapExternalDocs,documentIdValue);
           System.debug('** Return Object from Check Statement **'+objExtDocWrp);
           System.debug('objExtDocWrp.cv:'+objExtDocWrp.cv.id);
           contentId= (objExtDocWrp!=null && objExtDocWrp.cv!=null) ? objExtDocWrp.cv.id : '';
           System.debug('**Content Id**'+contentId);
           }else{
                 //Create Request object 
               request = new  wwwElavonComServicesImagingserviceU.UAT01();
              //Create Response object and retrieve response from API
               wwwElavonComSchemaImagingresourcesV.DocumentImageResponse_element response = request.retrieveDocument(documentIdValue,System.Label.Elavon_Integration_End_User_ID);
               
               //Check response is not null and Image is not null
               if(response!=null && response.Image!=null){
                 // Get Image Record from response
                 wwwElavonComSchemaImagingresourcesV.Image imageRecord = response.Image[0];
                  system.debug('&& Image Record value && '+imageRecord);
                 //Check file type as per mimeType
                 string fileType = (imageRecord.mimeType=='application/pdf')?'pdf':(imageRecord.mimeType=='image/tiff')?'tiff':'xml';    
                
                 //Invoke Save data method to save record of External Document
                 objExtDocWrp=UtilityCls.SaveData(accountRecord,MID,documentIdValue,imageRecord.imageData,fileType,true);
                 // 15 Jun 2017 Null check before assign content id #I-279331 Aashish Sajwan
                contentId =  (objExtDocWrp!=null && objExtDocWrp.cv!=null) ? objExtDocWrp.cv.id :'';
                 System.debug('**Content Id after Hit**'+contentId);
                   }
           } 
       //PageReference to open Content Record
       PageReference pg = new PageReference ('/'+contentId);
       return pg;
    }
    
   
    //Redirect to Account
    public PageReference redirectToAccount(){
       //PageReference of account page
       PageReference accountPage = new PageReference('/'+AccountId);
       //return Account Page Reference
       return accountPage;
      
      }

     //Wrapper Class for documument Data
     public class DocumentData{
   
        public String docID {get;set;}
        public String sDocTypeID {get;set;}
        public string indexDate {get;set;}
        public string scanDate {get;set;}
        public String dbaName {get;set;}
        public String mid {get;set;}
        public String imageData {get;set;}
        public String mimeType {get;set;}
      }
    
 }