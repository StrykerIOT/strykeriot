/*********************************************************************
* Appirio a WiPro Company
* Name: ScheduleAccountTaxInfoRelationship
* Description: Scheduleable class to execute Batch class. See the 'AccountTaxInfoRelationshipBatch' and 'AccountTaxInfoRelationshipBatchHelper' 
*              classes for more details.
* Created Date: 5/25/2017
* Created By: Bobby Cheek
*/
global class ScheduleAccountTaxInfoRelationship implements Schedulable{

   global void execute(SchedulableContext sc){
        Id batchProcessId = Database.ExecuteBatch(new AccountTaxInfoRelationshipBatch());
   }

}
/************* DO NOT DELETE *****************
        AccountTaxInfoRelationshipBatch stcba = new AccountTaxInfoRelationshipBatch();
        // Seconds Minutes Hours Day_of_monthMonth Day_of_week optional_year
        String Sched = '0 0 21 2 FEB THU 2012';  // runs at 9 on Feb 2, 2012
        System.Schedule('Add Sales Team Member to Orders', Sched, stcba);
*/