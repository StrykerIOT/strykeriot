/*********************************************************************************************************************
* Author        :  Appirio (Neeraj Kumawat)
* Date          :  Dec 28th, 2016
* Purpose       :  T-565232-Test class for AccountTriggerHandler.cls
* Updated Date  :  Nov 11th, 2016
* Purpose       :  Test class coverage for AccountTriggerHandler.cls
*
* Date Modified                Modified By                  Description of the update
* [Jan 02 2017]                [Neeraj Kumawat]                [Test Class Creation]
* [Apr 28 2017]                [Gaurav Dudani]                 [Updated Test Class for Code Coverage]
* [30-May-2017]                [Poornima Bhardwaj]             [Added test method testCurrencyISOCOde]
* [12-June-2017]               [Neeraj Kumawat]                [Updated Test method testCurrencyISOCOde]
* [12-June-2017]               [Neeraj Kumawat]                [Removed Debugs from ]
**********************************************************************************************************************/
@isTest
//****************************************************************************
// Test Class to test the AccountTriggerHandler.cls
//****************************************************************************
public class AccountTriggerHandlerTest{
    static string taxId='123456';
    static string newTaxId='123456789';
    static List<Account> accountLst;
    static List<Contact> contactLst;
    static List<workOrder> workOrderLst;
    public testmethod static void testCallout(){
        //Test For Insert
        //******************************************************************************************************
        // List of records created using createAccount & createContact method from TestUtilities class
        //******************************************************************************************************
        String profileId = [select id from Profile where name ='System Administrator' limit 1].Id;
        User usr = TestUtilities.createTestUser(1,profileId,true);
        System.runAs(usr){
            
            testForInsert();
            //Check for Insert
            //******************************************************************************************************
            // Check Tax ID Of Contacts with Related Account's ID
            //******************************************************************************************************
            //checkTaxId(taxId);
            //Test for Update
            //******************************************************************************************************
            // Update Account Tax Id to Check the Tax Id change in Related Contacts
            //******************************************************************************************************
            testForUpdate();
            //Check for Update
            //******************************************************************************************************
            // Check Tax ID Of updated Account in Releted Contacts
            //******************************************************************************************************
            //checkTaxId(newTaxId);
        }
    }
    //Test Account Record Create and Delete and Undelete it
    public testmethod static void testCallout1(){
       List<Account> acc=TestUtilities.createAccount(1, true);
        delete acc;
        undelete acc;
    }
    //Test Data For Account and Related Contacts
    static void testForInsert(){
        //Test Data For Account
        List<RecordType> recprdTypeList= [Select Id, Name, ISACtive from RecordType where Name='General' and IsActive= true];
        Id recordTypeId;
        if(recprdTypeList.size()>0){
            recordTypeId=recprdTypeList.get(0).Id;
        }
        accountLst = TestUtilities.createAccount(200,false);
        For(Integer i=0; i<accountLst.size(); i++){
            accountLst[i].Tax_Id_Encrypted__c=taxId;
            if(recordTypeId!=null){
                accountLst[i].RecordTypeId=recordTypeId;
                }
        }
        insert accountLst;
        
        //Test Data For Contact
        for(Integer i=0;i<accountLst.size();i++){
            contactLst=TestUtilities.createContact(200,accountLst[i].id,false);
        }
        insert contactLst;
    }
    //Test Data For Updated Account
    static void testForUpdate(){ 
        for(Account a:accountLst){
            a.Tax_Id_Encrypted__c=newTaxId;
            }
        update accountLst;
    }
   
    
    
   //Test For Insert
   //******************************************************************************************************
   // Method to insert service contracts and update Account,Contact and Service contract with currency as 
   // per value in funding currency code field on Account object.
   //******************************************************************************************************  
    public testmethod static void testCoverageMethod(){
      String profileId = [select id from Profile where name ='System Administrator' limit 1].Id;
        User usr = TestUtilities.createTestUser(1,profileId,true);
        System.runAs(usr){
            
            testForInsert();
            List<ServiceContract> servContractList = new List<ServiceContract>();
            for(Integer i=0; i<100 ; i++){
              servContractList.add(new ServiceContract(Name = 'test'+i, AccountId = accountLst[0].Id));
            }
            insert servContractList;
            
            for(Account acc: accountLst){
                      acc.Funding_Currency_Code__c = 'USD';
            }
            update accountLst;
            
            List<ServiceContract> servContractList1 = [SELECT CurrencyIsoCode FROM ServiceContract WHERE CurrencyIsoCode = 'USD'];
            System.assertEquals(100, servContractList1.size(), 'List size are not equals');
            
            for(Account acc: accountLst){
                      acc.Funding_Currency_Code__c = 'MXN';
            }
            update accountLst;
            
            List<ServiceContract> servContractList2 = [SELECT CurrencyIsoCode FROM ServiceContract WHERE CurrencyIsoCode = 'MXN'];
            System.assertEquals(100, servContractList2.size(), 'List size are not equals');
            
            for(Account acc: accountLst){
                      acc.Funding_Currency_Code__c = 'CAD';
            }
            update accountLst;
            
            List<ServiceContract> servContractList3 = [SELECT CurrencyIsoCode FROM ServiceContract WHERE CurrencyIsoCode = 'CAD'];
            System.assertEquals(100, servContractList3.size(), 'List size are not equals');        
        }   
    } 
    
    //***************************************************************************************************
    // Method to verify that workorder currencyISOCOde gets updated when Funding Currency Code is Changed
    // @return void
    // Added by Poornima Bhardwaj On 30 May 2017 for task # T-606531
    //***************************************************************************************************
    public testmethod static void testCurrencyISOCOde(){
         //[12-June-2017 Neeraj Kumawat]
         //Updated WebService Callout for successfully getting the test class passed 
         Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator(''));
         List<workOrder> wkList=new List<workOrder>();
         List<Account> accList=new List<Account>();
         //creating Account & WorkOrder Test Data
         accountLst = TestUtilities.createAccount(10,true);
         workOrderLst = TestUtilities.createWorkOrder(10,false);
         Test.startTest();
         workOrderLst[0].AccountId=accountLst[1].Id;
         for(Integer i=1;i<accountLst.size();i++){
             workOrderLst[i].AccountId=accountLst[i].Id;
         }
         insert workOrderLst;
         for(Account acc:accountLst){
            acc.Funding_Currency_Code__c='CAD';
             
         }
         update accountLst;
         List<WorkOrder> wrkOrderCAD=[Select CurrencyISOCode from WorkOrder where CurrencyISOCode='CAD'];
         System.AssertEquals(10,wrkOrderCAD.size());
         for(Account acc:accountLst){
            acc.Funding_Currency_Code__c='MXN';
         }
         update accountLst;
         List<WorkOrder> wrkOrderMXN=[Select CurrencyISOCode from WorkOrder where CurrencyISOCode='MXN'];
         System.AssertEquals(10,wrkOrderMXN.size());
         
         for(Account acc:accountLst){
            acc.Funding_Currency_Code__c='USD';
            
         }
         update accountLst;
         List<WorkOrder> wrkOrderUSD=[Select CurrencyISOCode from WorkOrder where CurrencyISOCode='USD'];
         System.AssertEquals(10,wrkOrderUSD.size());
         Test.stopTest();
 }    
}