/*********************************************************************
* Appirio, Inc
* Name: ViewDocumentsControllerTest
* Description: [Task# T-557407]
* Created Date: [12/21/2016]
* Created By: [Aashish Sajwan] (Appirio)
* 
* Date Modified                Modified By             Description of the update
* [Tue 21, 2016]               [Aashish Sajwan]        [T-557407]
* [Wed 09 Feb, 2017]           [Aashish Sajwan]        [I-253992]
* [Thu 15 June, 2017]			[Neeraj Kumawat]		[Added test method named testCallout2 to separate Webservice Callout and DML operations]	
**********************************************************************/
@isTest
public class ViewDocumentsControllerTest{
    static String documentIdValue;
    static String docID;  
    public static String AccountId; 
    public static String MID;
    public static String ApplicationIDVal;
    public static String ProcessingCenterId;
    public static Account accRecord;
    static ViewDocumentsController obj;
    static ElavonServicesImagingserviceUMock objMock;
    public static Account acc;
    
    public testmethod static void testCallout()
    {	
        TestData();
        //Parameter Passing onto the Page
        Test.setCurrentPageReference(new PageReference('Page.ViewDocuments'));         
        System.currentPageReference().getParameters().put('AccountId', AccountId);
        System.currentPageReference().getParameters().put('MID', MID);
        System.currentPageReference().getParameters().put('PID', ProcessingCenterId);
        System.currentPageReference().getParameters().put('ApplicationID', ApplicationIDVal);  
        //Object Creation
        ViewDocumentsController obj=new ViewDocumentsController();
        accRecord=obj.accountRecord;
        obj.redirectToAccount();
        //Calling Search Record
        TestSearchDocument();
    }
    //Added on 18 June 2017 to modify test class to separate Webservice Callout and DML Operation in a single Transaction
    public static testmethod void testCallout2()
    {	
        Test.startTest();
        //Creating Test User Data with Custom:Relationship Manager Profile
        List<UserRole> agentRole=[select id,name from UserRole where name='Service NA Manager'];
        List<Profile> agentProId=[select id,name from Profile where name='Custom: Relationship Manager'];
        User agentUser=TestUtilities.createTestUser(1,agentProId[0].id,false);
        agentUser.UserRole=agentRole[0];
        agentUser.Username='Sample@testing.com';
        agentUser.CommunityNickname='testingUSerSample1';
        insert agentUser;
        System.runAs(agentUser)
        {
            TestData();
            Test.setCurrentPageReference(new PageReference('Page.ViewDocuments'));   
            System.currentPageReference().getParameters().put('AccountId',AccountId);
            System.currentPageReference().getParameters().put('MID', MID);
            System.currentPageReference().getParameters().put('PID', ProcessingCenterId);
            System.currentPageReference().getParameters().put('ApplicationID',ApplicationIDVal);
            obj=new ViewDocumentsController();
            accRecord=obj.accountRecord;
        }
        //Create Test User data with System Admin Profile
        List<UserRole> systemRole=[select id,name from UserRole where name='Global Ops'];
        List<Profile> systemProId=[select id,name from Profile where name='System Administrator'];
        User systemUser=TestUtilities.createTestUser(1,systemProId[0].id,false);
        systemUser.Username='Sample2@testing.com';
        systemUser.UserRole=systemRole[0];
        insert systemUser;
        //Webservice Callout Block which is being called by System Admin Test User
        System.runAs(systemUser)
        {
            objMock=new ElavonServicesImagingserviceUMock();
            objMock.setVariable('OpenRecord');
            Test.setMock(WebServiceMock.class, objMock);
            TestMapData();
            obj.openRecord();
            //Checking Search Document & Open Record Lists & Variables
            System.assert(obj.contentId!=null);
            List<ViewDocumentsController.DocumentData> dataobj=new List<ViewDocumentsController.DocumentData>();
            dataobj=obj.documentList;        
            if(dataobj!=null && dataobj.size()>0){
                for(ViewDocumentsController.DocumentData imgData :  dataobj)
                {
                    docID = imgData.docID ;
                }
            }
            
            //setting documentIdValue
            obj.setdocumentIdValue(docID);
            documentIdValue=obj.getdocumentIdValue();
            // Verify that a fake result is returned
            System.assertEquals(documentIdValue, docID); 
            //Calling Open Record Method for If Part
            TestMapData();
            obj.OpenRecord();
            Test.stopTest();
        }
    }
    //Created Test Account & External Document Data
    static void TestData()
    {
        Acc=TestUtilities.createTestAccount(true);                                   
        Acc.Processing_Center_ID__c='QUERE';
        AccountId=Acc.Id;                                                      
        MID=Acc.MID__c;
        ProcessingCenterId = 'QUERE';
        ApplicationIDVal='2345670';
    }
    //Calls Search Doc
    static void TestSearchDocument()
    {
        //Calling Search Record Method from Mock up Class
        ViewDocumentsController obj=new ViewDocumentsController();
        objMock=new ElavonServicesImagingserviceUMock();
        objMock.setVariable('SearchRecord');
        String Check=objMock.getVariable();
        Test.setMock(WebServiceMock.class,objMock);
        
        //Clear External Document Data
        obj.mapExternalDocs.clear();
        
        //All Mock up Calls in this Section/
        Test.StartTest();
        
        //Calling Search Document Method
        obj.searchDocuments();
    }
    //Creates External Doc,ContentVersion & FeedItems
    static void TestMapData()
    { 	
        obj=new ViewDocumentsController();
        accRecord=obj.accountRecord;
        External_Document__c objExt=new External_Document__c();
        objExt=TestUtilities.SaveExternalDoc(accRecord,'2759115',MID,false,true);
        obj.mapExternalDocs.put(documentIdValue,objExt);
        
        ContentVersion con=new ContentVersion();
        con=TestUtilities.SaveContentVersion('pdf','jhbjkbk',MID,true);
        
        FeedItem fd=new FeedItem();
        fd=TestUtilities.SaveFeedItems(MID,objExt,con,true);
    }
}