/***************************************************************************
// (c) 2015 Appirio, Inc.
//
// Description    : Schedulable Hourly Batch Class - Work-Order StampDates Update
//                : 
//                  
// 8 June 2017 Aashish Sajwan Original (S-442186)
//***************************************************************************/
global  class ScheduleWorkOrderStampDatesBatchClass implements Schedulable {
   global void execute(SchedulableContext sc) {
      WorkOrderStampDatesUpdateBatch workOrderStampBatchObj= new WorkOrderStampDatesUpdateBatch();
      database.executeBatch(workOrderStampBatchObj,75);
   }
}