/*********************************************************************
* Appirio a WiPro Company
* Name: RefreshDateStampsController
* Description: [T-598751 - ControllerClass- to get response recieved via services ]
* Created Date: [23/5/2017]
* Created By: [Poornima Bhardwaj] (Appirio)
* 
* Date Modified                Modified By                  Description of the update
* [23-May-2017]                [Aashish Sajwan]             [T-604071]
* [26-May-2017]                [Aashish Sajwan]             [Udpated Param value in GetRefreshDateStamps]
* [2-June-2017]                [Poornima Bhardwaj]          [Updated the condition of calling GetRefreshDateStamps based on MX and NA]
* [06-Jun-2017]                 [Aashish Sajwan]             [#T-607012 Update parameter in GetCollectionDetails]
**********************************************************************/
public without sharing class RefreshDateStampsController{
    public string AccountId {get;set;}
    public string midValue {get;set;}
    public string midGroupVal {get;set;}    
    public List<UtilityCls.Value> valueList { get; set; }
    public boolean showMessage{get;set;}
    public RefreshDateStampsController() {
        
        Map<String, String> urlParams = Apexpages.currentPage().getParameters();  
        //Fetching accountId from URL
        AccountId = urlParams.containsKey('AccountId') ? EncodingUtil.urlDecode(urlParams.get('AccountId'),'UTF-8') : '';
        //Fetching mid from URL
        midValue = urlParams.containsKey('Mid') ? EncodingUtil.urlDecode(urlParams.get('Mid'),'UTF-8') : '';
        //Fetching MidGroup from URL
        midGroupVal = urlParams.containsKey('MidGroup') ? EncodingUtil.urlDecode(urlParams.get('MidGroup'),'UTF-8') : '';
        system.debug('&& midValue values &&'+midValue);
        system.debug('&& midGroupVal values &&'+midGroupVal);
        system.debug('&& AccountId values &&'+AccountId);
        //Get Current User Profile Id 
        string ProfileId = userinfo.getProfileId();
        string ProfileName = [Select Name 
                              FROM Profile
                              WHERE Id =:ProfileId].Name;
        system.debug('&& Profile Name &&'+ProfileName);
        //Updated by Poornima Bhardwaj on 02-June-2017 
        //Changed the if condition
        if(ProfileName=='Custom: MX Agent' || ProfileName =='Custom: MX Manager'){
          //Calling GetRefreshDateStamps method 
          GetRefreshDateStamps(True);
          showMessage= False;
        }else{
         GetRefreshDateStamps(False);
         showMessage= True;
        }
     
    }
    
    //****************************************************************************
    // Method to GetRefreshDateStamps
    // Crated by : Aashish Singh Sajwan 24 May 2017 #T-604071/#T-604078
    // @return void
    //****************************************************************************
    public void GetRefreshDateStamps(boolean isMaxRegion){
        try {
        if(isMaxRegion){
            valueList   = new List<UtilityCls.Value>();
         
        }else{
        //Hit web service when Profile is NA
        valueList  = UtilityCls.CallElavonApi('RefreshDateStamps',midValue,midGroupVal,'','');
            system.debug('&& valueList values &&'+valueList);
        
        }
           
        }catch(Exception ex){
            
        }
        
    }
    
    //Redirect To Account 
    public PageReference redirectToAccount(){
        // Create Page Reference for Account
        PageReference accountPage = new PageReference('/'+AccountId);
        //return account page reference
        return accountPage;
        
    }
    
    
}