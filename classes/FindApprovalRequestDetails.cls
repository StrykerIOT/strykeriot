/****************************************************************************************
* Appirio, Inc
* Class Name: FindApprovalRequestDetail
* Description: [Controller class for Visualforce component WorkOrderDetailsComponent as per Story# S-460959 ]
* Created Date: [23/05/2017]
* Created By: [Gaurav Dudani] (Appirio)
* 
* Date Modified                Modified By                  Description of the update
*****************************************************************************************/
public with sharing class FindApprovalRequestDetails {
public String objId { get; set; }
public String StepStatusMessage { get; set; }
 
 public FindApprovalRequestDetails() {}
 
 public ProcessInstanceStep getApprovalStep() {
    ProcessInstanceStep piStep = new ProcessInstanceStep();
    system.debug('IdandStatus '+objId+StepStatusMessage);
    List<ProcessInstanceStep> piSteps = [Select Id, Comments, CreatedDate 
                                        FROM ProcessInstanceStep  
                                        WHERE ProcessInstance.TargetObjectID = :objId 
                                        AND StepStatus = :StepStatusMessage
                                        LIMIT 1];
    system.debug('piStepss '+piSteps);
    if(piSteps.size() > 0) {
        piStep = piSteps[0];
    }
    return piStep;
 }
}