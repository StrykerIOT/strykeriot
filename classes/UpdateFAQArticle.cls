/*************************************************************************************************
* Appirio, Inc
* Name: UpdateFAQArticle
* Description: [T-564240-ApexClass For updating FAQ Article Body]
* Created Date: [12/27/2016]
* Created By: [Neeraj Kumawat] (Appirio)
* 
* Date Modified                Modified By                  Description of the update
*  [12/27/2016]              [Gaurav Dudani]          [Added Comments and Modified Select Query]
***************************************************************************************************/
public class UpdateFAQArticle {
    
    /*************************************************************************************************************    
* Method to invoke process builder "Update FAQ Article Type" and to update article Body type of FAQ articles.
**************************************************************************************************************/
    @InvocableMethod
    public static void UpdateFAQArticleBody(List<Id> articleIds){       
        //define variables;create list for code bulkification
        List <FAQ__kav> objFAQList = new List <FAQ__kav>();
        List<FAQ__kav> faqList=[Select Id, title, Article_Body__c from FAQ__kav where ID IN: articleIds];
        System.debug('Article Detail Value='+faqList);
        // if(faqList.size()>0){
        for(FAQ__kav objFAQ: faqList){
            // FAQ__kav objFAQ = faqList.get(0);
            String articleBody=objFAQ.Article_Body__c;
            if(articleBody!=null){
            // Unescapeing article body using unescapeHtml4
            articleBody=articleBody.unescapeHtml4();
            /* Splitting Article body with # character */
            List<String> strListSplitWithHash= articleBody.split('#');
            system.debug('Article Body Value='+articleBody);
            system.debug('Article Body Value With unescapeHtml4='+articleBody.unescapeHtml4());
            for(String str: strListSplitWithHash){
                List<String> strListSplitWithSpace= str.split(' ');
                String wordWithoutHash=strListSplitWithSpace.get(0);
                String wordWithHash='#'+wordWithoutHash+' ';
                if(articleBody.contains(wordWithHash)){
                    system.debug('Word without Hash Word Value='+wordWithoutHash);
                    system.debug('Word With Hash Value='+wordWithHash);
                    articleBody=articleBody.replaceAll(wordWithHash,'</div><div class="'+wordWithoutHash+'">');
                    articleBody=articleBody.replaceAll('<br></div>', '</div>');
                }
                //update Article Body Backend field which is displayed on page layout (not on edit)
                objFAQ.Article_Body_Backend__c=articleBody;
              }
            }
            objFAQList.add(objFAQ);
        } 
        //update list for bulkification     
        if(objFAQList.size()>0)
            update objFAQList;
    }
}