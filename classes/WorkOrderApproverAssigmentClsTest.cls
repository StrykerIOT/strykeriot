/*****************************************************************************************************
* Appirio, Inc
* Test Class Name: WorkOrderApproverAssigmentClsTest
* Class Name: WorkOrderApproverAssigmentCls
* Description: (#T-608729) Test Class for WorkOrderApproverAssigmentCls
* Created Date: [June 12,2017]
* Created By: [Poornima Bhardwaj] (Appirio)
* 
* Date Modified          Modified By              Description of the update

******************************************************************************************************/
@isTest
public class WorkOrderApproverAssigmentClsTest {
    static user agentUser;
    static List<Case> caseList;
    static List<Account> accountList;
    static List<WorkOrder> workOrderList;
    static List<workOrderLineItem> workOrderLineItemList;
    static User managerUser;
    static User managerUser1;
    static User managerUser2;
    static User managerUser3;
    static User managerUser4;
    static User managerUser5;
    static User managerUser6;
    static User User1; 
    Static List<User> updateUser=new List<User>();
    //****************************************************************************************************
    // Method to create Test data with CurrencyIsoCode of Workorder as USD
    // @return void
    // Added by Poornima Bhardwaj on 9 June 2017 (#T-608729)
    //****************************************************************************************************
    static testMethod void testCreateUserData(){
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator('CollectionTotal'));
        //Fetching profile name and Id having name as 'Custom: NA Agent'
        List<Profile> managerProId=[select id,name from Profile where name='Custom: NA Manager'];
        //Creating Test User
        managerUser6=TestUtilities.createTestUser(1,managerProId[0].id,false);
        managerUser6.Username='test6@testdomain.comtestuser';
        managerUser6.CommunityNickname='kjbmjhvlobhj';
        managerUser6.Adjustment_Approval_Limit_USD__c=56.6;
        managerUser6.Adjustment_Approval_Limit_MXN__c=67.9;
        managerUser6.CurrencyIsoCode='USD';
        insert managerUser6;
        //Creating Test User
        managerUser5=TestUtilities.createTestUser(1,managerProId[0].id,false);
        managerUser5.Username='test5@testdomain.comtestuser';
        managerUser5.CommunityNickname='kjklbmjhvlbhj';
        managerUser5.Adjustment_Approval_Limit_USD__c=56.6;
        managerUser5.Adjustment_Approval_Limit_MXN__c=67.9;
        managerUser5.CurrencyIsoCode='USD';
        managerUser5.ManagerId = managerUser6.Id;
        insert managerUser5;       
        //Creating Test User
        managerUser4=TestUtilities.createTestUser(1,managerProId[0].id,false);
        managerUser4.Username='test4@testdomain.comtestuser';
        managerUser4.CommunityNickname='kjbmjhvlbhj';
        managerUser4.Adjustment_Approval_Limit_USD__c=56.6;
        managerUser4.Adjustment_Approval_Limit_MXN__c=67.9;
        managerUser4.CurrencyIsoCode='USD';
        managerUser4.ManagerId = managerUser5.Id;
        insert managerUser4;
        //Creating Test User
        managerUser3=TestUtilities.createTestUser(1,managerProId[0].id,false);
        managerUser3.Username='test3@testdomain.comtestuser';
        managerUser3.CommunityNickname='kjbmjhvkbhj';
        managerUser3.Adjustment_Approval_Limit_USD__c=56.6;
        managerUser3.Adjustment_Approval_Limit_MXN__c=67.9;
        managerUser3.CurrencyIsoCode='USD';
        managerUser3.ManagerId = managerUser4.Id;
        insert managerUser3;
        //Creating Test User
        managerUser2=TestUtilities.createTestUser(1,managerProId[0].id,false);
        managerUser2.Username='test2@testdomain.comtestuser';
        managerUser2.CommunityNickname='kjbmjhvbhj';
        managerUser2.Adjustment_Approval_Limit_USD__c=56.6;
        managerUser2.Adjustment_Approval_Limit_MXN__c=67.9;
        managerUser2.CurrencyIsoCode='USD';
        managerUser2.ManagerId = managerUser3.Id;
        insert managerUser2;
        //Creating Test User
        managerUser1=TestUtilities.createTestUser(1,managerProId[0].id,false);
        managerUser1.Username='test1@testdomain.comtestuser';
        managerUser1.CommunityNickname='kjbmjhvbkhj';
        managerUser1.Adjustment_Approval_Limit_USD__c=56.6;
        managerUser1.Adjustment_Approval_Limit_MXN__c=67.9;
        managerUser1.CurrencyIsoCode='USD';
        managerUser1.ManagerId = managerUser2.Id;
        insert managerUser1;
        //Creating Test User
        managerUser=TestUtilities.createTestUser(1,managerProId[0].id,false);
        managerUser.Username='test0@testdomain.comtestuser';
        managerUser.CommunityNickname='kkjbjhvbhj';
        managerUser.ManagerId = managerUser1.Id;
        managerUser.Adjustment_Approval_Limit_USD__c=56.6;
        managerUser.Adjustment_Approval_Limit_MXN__c=67.9;
        managerUser.CurrencyIsoCode='USD';
        insert managerUser;
        //Fetching profile name and Id having name as 'Custom: NA Agent'
        List<Profile> agentProId=[select id,name from Profile where name='Custom: NA Agent'];
        //Creating User
        user1=TestUtilities.createTestUser(1,agentProId[0].id,false);
        User1.Username='test10@testdomain.comtestuser';
        User1.CommunityNickname='kkjbjkhvbhj';
        user1.ManagerId = managerUser.Id;
        user1.Adjustment_Approval_Limit_USD__c=56.6;
        user1.Adjustment_Approval_Limit_MXN__c=67.9;
        user1.CurrencyIsoCode='USD';
        insert user1;
        //Creating Test records for Account 
        accountList=TestUtilities.createAccount(1,false);
        for(Account acc:accountList){
            acc.Processing_Center_ID__c='QUERE';
            acc.MID__c='8026984123';
        }
        insert accountList;
        //Fetching Record Type Id for Adjustment Type WorkOrder
        Id devRecordTypeId = Schema.SObjectType.WorkOrder.getRecordTypeInfosByName().get('Adjustment').getRecordTypeId();
        //Creating Test Records for WorkOrder 
        workOrderList=TestUtilities.createWorkOrder(5,false);
        for(WorkOrder wrk:workOrderList)
        {   
            wrk.accountId=accountList[0].Id;
           // wrk.CurrencyIsoCode='USD';
            wrk.RecordTypeId=devRecordTypeId;
            wrk.Work_Order_Approval_Status__c='Draft';
            wrk.Subject='Testing';
        }        
        insert workOrderList; 
        //Creating Test Records for WorkOrderLineItems
        workOrderLineItemList=TestUtilities.createWorkOrderLineItem(1,false); 
        workOrderLineItemList[0].Line_Item_Total__c=3.8;
        workOrderLineItemList[0].WorkOrderId=workOrderList[0].Id;
        insert workOrderLineItemList;     
    }    
    //****************************************************************************************************
    // Method to Create Test Data with CurrencyIsoCode of Workorder as MXN
    // @return void
    // Added by Poornima Bhardwaj on 9 June 2017 (#T-608729)
    //****************************************************************************************************
    static testMethod void testCreateUserData2(){
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator('CollectionTotal'));
        //Fetching Users having profile 'Custom : NA Manager'
        List<Profile> managerProId=[select id,name from Profile where name='Custom: NA Manager'];
        //Creating Test User
        managerUser6=TestUtilities.createTestUser(1,managerProId[0].id,false);
        managerUser6.Username='test6@testdomain.comtestuser';
        managerUser6.CommunityNickname='kjjbmjhvlbhj';
        managerUser6.Adjustment_Approval_Limit_USD__c=56.6;
        managerUser6.Adjustment_Approval_Limit_MXN__c=67.9;
        managerUser6.CurrencyIsoCode='MXN';
        insert managerUser6;
        //Creating Test User
        managerUser5=TestUtilities.createTestUser(1,managerProId[0].id,false);
        managerUser5.Username='test5@testdomain.comtestuser';
        managerUser5.CommunityNickname='khjbmjhvlbhj';
        managerUser5.Adjustment_Approval_Limit_USD__c=56.6;
        managerUser5.Adjustment_Approval_Limit_MXN__c=67.9;
        managerUser5.CurrencyIsoCode='MXN';
        managerUser5.ManagerId = managerUser6.Id;
        insert managerUser5;
        //Creating Test User
        managerUser4=TestUtilities.createTestUser(1,managerProId[0].id,false);
        managerUser4.Username='test4@testdomain.comtestuser';
        managerUser4.CommunityNickname='kjhbmjhvlbhj';
        managerUser4.Adjustment_Approval_Limit_USD__c=56.6;
        managerUser4.Adjustment_Approval_Limit_MXN__c=67.9;
        managerUser4.CurrencyIsoCode='MXN';
        managerUser4.ManagerId = managerUser5.Id;
        insert managerUser4;
        //Creating Test User
        managerUser3=TestUtilities.createTestUser(1,managerProId[0].id,false);
        managerUser3.Username='test3@testdomain.comtestuser';
        managerUser3.CommunityNickname='kjbmjhvkbhj';
        managerUser3.Adjustment_Approval_Limit_USD__c=56.6;
        managerUser3.Adjustment_Approval_Limit_MXN__c=67.9;
        managerUser3.CurrencyIsoCode='MXN';
        managerUser3.ManagerId = managerUser4.Id;
        insert managerUser3;
        //Creating Manager User Test Data 
        managerUser2=TestUtilities.createTestUser(1,managerProId[0].id,false);
        managerUser2.Username='test2@testdomain.comtestuser';
        managerUser2.CommunityNickname='kjbmjhvbhj';
        managerUser2.Adjustment_Approval_Limit_USD__c=56.6;
        managerUser2.Adjustment_Approval_Limit_MXN__c=67.9;
        managerUser2.CurrencyIsoCode='MXN';
        managerUser2.ManagerId = managerUser3.Id;
        insert managerUser2;
        //Creating Test User
        managerUser1=TestUtilities.createTestUser(1,managerProId[0].id,false);
        managerUser1.Username='test1@testdomain.comtestuser';
        managerUser1.CommunityNickname='kjbmjhvbkhj';
        managerUser1.Adjustment_Approval_Limit_USD__c=56.6;
        managerUser1.Adjustment_Approval_Limit_MXN__c=67.9;
        managerUser1.CurrencyIsoCode='MXN';
        managerUser1.ManagerId = managerUser2.Id;
        insert managerUser1;
        //Creating Test User
        managerUser=TestUtilities.createTestUser(1,managerProId[0].id,false);
        managerUser.Username='test0@testdomain.comtestuser';
        managerUser.CommunityNickname='kkjbjhvbhj';
        managerUser.ManagerId = managerUser1.Id;
        managerUser.Adjustment_Approval_Limit_USD__c=56.6;
        managerUser.Adjustment_Approval_Limit_MXN__c=67.9;
        managerUser.CurrencyIsoCode='MXN';
        insert managerUser;
        //Fetching UserId and Name from UserTest Data whose profile is 'Custom NA Agent'
        List<Profile> agentProId=[select id,name from Profile where name='Custom: NA Agent'];
        //Creating User test data
        user1=TestUtilities.createTestUser(1,agentProId[0].id,false);
        User1.Username='test10@testdomain.comtestuser';
        User1.CommunityNickname='kkjbjkhvbhj';
        user1.ManagerId = managerUser.Id;
        user1.Adjustment_Approval_Limit_USD__c=56.6;
        user1.Adjustment_Approval_Limit_MXN__c=67.9;
        user1.CurrencyIsoCode='MXN';
        insert user1;
        //Creating Account records with specific Processing Center Id & MID
        accountList=TestUtilities.createAccount(1,false);
        for(Account acc:accountList){
            acc.Processing_Center_ID__c='QUERE';
            acc.Funding_Currency_Code__c='MXN';
            acc.MID__c='8026984123';
        }
        insert accountList;
        //Fetching Record Type Id of Adjustment Type WorkOrder 
        Id devRecordTypeId = Schema.SObjectType.WorkOrder.getRecordTypeInfosByName().get('Adjustment').getRecordTypeId();
        //Creating WorkOrder Test Records 
        workOrderList=TestUtilities.createWorkOrder(5,false);
        for(WorkOrder wrk:workOrderList)
        {   
            wrk.accountId=accountList[0].Id;
            wrk.CurrencyIsoCode='MXN';
            wrk.RecordTypeId=devRecordTypeId;
            wrk.Work_Order_Approval_Status__c='Draft';
            wrk.Subject='Testing';
        }        
        insert workOrderList;
        //Creating WorkOrder Line Items records
        workOrderLineItemList=TestUtilities.createWorkOrderLineItem(1,false); 
        workOrderLineItemList[0].Line_Item_Total__c=3.8;
        workOrderLineItemList[0].WorkOrderId=workOrderList[0].Id;
        insert workOrderLineItemList;
    }
    //****************************************************************************************************
    // Method to verify Approver1,Approver2 field of WorkOrder when Approver1 User is not null
    // @return void
    // Added by Poornima Bhardwaj on 9 June 2017 (#T-608729)
    //****************************************************************************************************
    static testMethod void testWorkOrderApproverAssigmentClsUSD(){
        //Web Service Callout
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator('CollectionTotal'));
        //Calling Method to create test User data 
        testCreateUserData();
        Test.startTest();
        WorkOrderApproverAssigmentCls.assignApprover(user1.Id,workOrderList[0].Id);
        Test.stopTest();
        //Fetching approver1,approver2 from workOrder where workorder subject is Testing
        List<WorkOrder> wk=[Select Approver_1__c,Approver_2__c from WorkOrder where Subject='Testing'];
        //Fetching test User Data to verify that Approver Id and the Test User Id is same
        User usr=[Select Id from User where username='test0@testdomain.comtestuser' LIMIT 1];
        User usr1=[Select Id from User where username='test1@testdomain.comtestuser' LIMIT 1];
        System.assertEquals(usr.Id, wk[0].Approver_1__c);
        System.assertEquals(usr1.Id, wk[0].Approver_2__c);
        
    }    
    //****************************************************************************************************
    // Method to verify Approver2 field of WorkOrder when Approver3 User is not null
    // @return void
    // Added by Poornima Bhardwaj on 9 June 2017 (#T-608729)
    //****************************************************************************************************
    static testMethod void testWorkOrderApproverAssigmentClsUSD1(){
        //WebService Callout
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator('CollectionTotal'));
        //Creating test User data
        testCreateUserData();
        managerUser1.Adjustment_Approval_Limit_USD__c=1.6;
        update managerUser1;
        Test.startTest();
        //Calling assignApprover method of WorkOrderApproverAssigmentCls Class
        WorkOrderApproverAssigmentCls.assignApprover(user1.Id,workOrderList[0].Id);
        Test.stopTest();
        //Fetching approver2 from workOrder where workorder subject is Testing
        List<WorkOrder> wk=[Select Approver_2__c from WorkOrder where Subject='Testing'];
        //Fetching test User Data to verify that Approver Id and the Test User Id is same
        User usr=[Select Id from User where username='test2@testdomain.comtestuser' LIMIT 1];
        System.assertEquals(usr.Id, wk[0].Approver_2__c);
        
    }
    //****************************************************************************************************
    // Method to verify Approver2 field of WorkOrder when Approver4 User is not null
    // @return void
    // Added by Poornima Bhardwaj on 9 June 2017 (#T-608729)
    //****************************************************************************************************
    static testMethod void testWorkOrderApproverAssigmentClsUSD2(){
        //WebService Callout
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator('CollectionTotal'));
        //Calling method to create Test User Data 
        testCreateUserData();
        managerUser1.Adjustment_Approval_Limit_USD__c=1.6;
        updateUser.add(managerUser1);
        managerUser2.Adjustment_Approval_Limit_USD__c=1.6;
        updateUser.add(managerUser2);
        update updateUser;
        Test.startTest();
        //Calling assignApprover method of WorkOrderApproverAssigmentCls Class
        WorkOrderApproverAssigmentCls.assignApprover(user1.Id,workOrderList[0].Id);
        Test.stopTest();
        //Fetching approver2 from workOrder where workorder subject is Testing
        List<WorkOrder> wk=[Select Approver_2__c from WorkOrder where Subject='Testing'];
        //Fetching test User Data to verify that Approver Id and the Test User Id is same
        User usr=[Select Id from User where username='test3@testdomain.comtestuser' LIMIT 1];
        System.assertEquals(usr.Id, wk[0].Approver_2__c);
    }     
    
    //****************************************************************************************************
    // Method to verify Approver2 field of WorkOrder when Approver5 User is not null
    // @return void
    // Added by Poornima Bhardwaj on 9 June 2017 (#T-608729)
    //****************************************************************************************************
    static testMethod void testWorkOrderApproverAssigmentClsUSD3(){
        //WebService Callout
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator('CollectionTotal'));
        //Calling method to create Test User Data 
        testCreateUserData();
        managerUser1.Adjustment_Approval_Limit_USD__c=1.6;
        updateUser.add(managerUser1);
        managerUser2.Adjustment_Approval_Limit_USD__c=1.6;
        updateUser.add(managerUser2);
        managerUser3.Adjustment_Approval_Limit_USD__c=1.6;
        updateUser.add(managerUser3);
        update updateUser;
        Test.startTest();
        //Calling assignApprover method of WorkOrderApproverAssigmentCls Class
        WorkOrderApproverAssigmentCls.assignApprover(user1.Id,workOrderList[0].Id);
        Test.stopTest();
        //Fetching approver2 from workOrder where workorder subject is Testing
        List<WorkOrder> wk=[Select Approver_2__c from WorkOrder where Subject='Testing'];
        //Fetching test User Data to verify that Approver Id and the Test User Id is same
        User usr=[Select Id from User where username='test4@testdomain.comtestuser' LIMIT 1];
        System.assertEquals(usr.Id, wk[0].Approver_2__c);
        
    }           
    //****************************************************************************************************
    // Method to verify Approver2 field of WorkOrder when Approver6 User is not null
    // @return void
    // Added by Poornima Bhardwaj on 9 June 2017 (#T-608729)
    //****************************************************************************************************
    static testMethod void testWorkOrderApproverAssigmentClsUSD4(){
        //Webservice Callout
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator('CollectionTotal'));
        //Creating Test User Data
        testCreateUserData();
        managerUser1.Adjustment_Approval_Limit_USD__c=1.6;
        updateUser.add(managerUser1);
        managerUser2.Adjustment_Approval_Limit_USD__c=1.6;
        updateUser.add(managerUser2);
        managerUser3.Adjustment_Approval_Limit_USD__c=1.6;
        updateUser.add(managerUser3);
        managerUser4.Adjustment_Approval_Limit_USD__c=1.6;
        updateUser.add(managerUser4);
        update updateUser;
        Test.startTest();
        //Calling assignApprover method of WorkOrderApproverAssigmentCls Class
        WorkOrderApproverAssigmentCls.assignApprover(user1.Id,workOrderList[0].Id);
        Test.stopTest();
        //Fetching approver2 from workOrder where workorder subject is Testing
        List<WorkOrder> wk=[Select Approver_2__c from WorkOrder where Subject='Testing'];
        //Fetching test User Data to verify that Approver Id and the Test User Id is same
        User usr=[Select Id from User where username='test5@testdomain.comtestuser' LIMIT 1];
        System.assertEquals(usr.Id, wk[0].Approver_2__c);
        
    } 
    //****************************************************************************************************
    // Method to verify Approver2 field of WorkOrder when Approver7 User is not null
    // @return void
    // Added by Poornima Bhardwaj on 9 June 2017 (#T-608729)
    //****************************************************************************************************
    static testMethod void testWorkOrderApproverAssigmentClsUSD5(){
        //WebService Callout
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator('CollectionTotal'));
        //Calling Method To create Test user data
        testCreateUserData();
        managerUser1.Adjustment_Approval_Limit_USD__c=1.6;
        updateUser.add(managerUser1);
        managerUser2.Adjustment_Approval_Limit_USD__c=1.6;
        updateUser.add(managerUser2);
        managerUser3.Adjustment_Approval_Limit_USD__c=1.6;
        updateUser.add(managerUser3);
        managerUser4.Adjustment_Approval_Limit_USD__c=1.6;
        updateUser.add(managerUser4);
        managerUser5.Adjustment_Approval_Limit_USD__c=1.6;
        updateUser.add(managerUser5);
        update updateUser;
        Test.startTest();
        //Calling assignApprover method of WorkOrderApproverAssigmentCls Class
        WorkOrderApproverAssigmentCls.assignApprover(user1.Id,workOrderList[0].Id);
        Test.stopTest();
        //Fetching approver2 from workOrder where workorder subject is Testing
        List<WorkOrder> wk=[Select Approver_2__c from WorkOrder where Subject='Testing'];
        //Fetching test User Data to verify that Approver Id and the Test User Id is same
        User usr=[Select Id from User where username='test6@testdomain.comtestuser' LIMIT 1];
        System.assertEquals(usr.Id, wk[0].Approver_2__c); 
    }  
    //****************************************************************************************************
    // Method to verify Approver1,Approver2 field of WorkOrder when Approver1 User is not null
    // @return void
    // Added by Poornima Bhardwaj on 9 June 2017 (#T-608729)
    //****************************************************************************************************
    static testMethod void testWorkOrderApproverAssigmentClsMXN(){
        //calling method to create the Test User Data
        testCreateUserData2();
        Test.startTest();
        //Calling assignApprover method of WorkOrderApproverAssigmentCls Class
        WorkOrderApproverAssigmentCls.assignApprover(user1.Id,workOrderList[0].Id);
        Test.stopTest();
        //Fetching approver1,approver2 from workOrder where workorder subject is Testing
        List<WorkOrder> wk=[Select Approver_1__c,Approver_2__c from WorkOrder where Subject='Testing'];
        //Fetching test User Data to verify that Approver Id and the Test User Id is same
        User usr=[Select Id from User where username='test0@testdomain.comtestuser' LIMIT 1];
        User usr1=[Select Id from User where username='test1@testdomain.comtestuser' LIMIT 1];
        System.assertEquals(usr.Id, wk[0].Approver_1__c);
        System.assertEquals(usr1.Id, wk[0].Approver_2__c);
    }            
    //****************************************************************************************************
    // Method to verify Approver2 field of WorkOrder when Approver3 User is not null
    // @return void
    // Added by Poornima Bhardwaj on 9 June 2017 (#T-608729)
    //****************************************************************************************************
    static testMethod void testWorkOrderApproverAssigmentClsMXN1(){
        //WebService Callout
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator('CollectionTotal'));
        //Creating Test User data 'NA'
        testCreateUserData2();
        managerUser1.Adjustment_Approval_Limit_MXN__c=1.6;
        update managerUser1;
        Test.startTest();
        //Calling assignApprover method of WorkOrderApproverAssigmentCls Class
        WorkOrderApproverAssigmentCls.assignApprover(user1.Id,workOrderList[0].Id);
        Test.stopTest();
        //Fetching approver2 from workOrder where workorder subject is Testing
        List<WorkOrder> wk=[Select Approver_2__c from WorkOrder where Subject='Testing'];
        //Fetching test User Data to verify that Approver Id and the Test User Id is same
        User usr=[Select Id from User where username='test2@testdomain.comtestuser' LIMIT 1];
        System.assertEquals(usr.Id, wk[0].Approver_2__c);
    }            
    //****************************************************************************************************
    // Method to verify Approver2 field of WorkOrder when Approver4 User is not null
    // @return void
    // Added by Poornima Bhardwaj on 9 June 2017 (#T-608729)
    //****************************************************************************************************
    static testMethod void testWorkOrderApproverAssigmentClsMXN2(){
        //Webservice Callouts
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator('CollectionTotal'));
        //Creating test User Data 'NA'
        testCreateUserData2();
        managerUser1.Adjustment_Approval_Limit_MXN__c=1.2;
        updateUser.add(managerUser1);
        managerUser2.Adjustment_Approval_Limit_MXN__c=1.2;
        updateUser.add(managerUser2);
        update updateUser;
        Test.startTest();
        //Calling assignApprover method of WorkOrderApproverAssigmentCls Class
        WorkOrderApproverAssigmentCls.assignApprover(user1.Id,workOrderList[0].Id);
        Test.stopTest();
        //Fetching approver2 from workOrder where workorder subject is Testing
        List<WorkOrder> wk=[Select Approver_2__c from WorkOrder where Subject='Testing'];
        //Fetching test User Data to verify that Approver Id and the Test User Id is same
        User usr=[Select Id from User where username='test3@testdomain.comtestuser' LIMIT 1];
        System.assertEquals(usr.Id, wk[0].Approver_2__c);
    }            
    //****************************************************************************************************
    // Method to verify Approver2 field of WorkOrder when Approver5 User is not null
    // @return void
    // Added by Poornima Bhardwaj on 9 June 2017 (#T-608729)
    //****************************************************************************************************
    static testMethod void testWorkOrderApproverAssigmentClsMXN3(){
        //WebService Callout
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator('CollectionTotal'));
        //Creating Test User data 'NA'
        testCreateUserData2();
        managerUser1.Adjustment_Approval_Limit_MXN__c=1.2;
        updateUser.add(managerUser1);
        managerUser2.Adjustment_Approval_Limit_MXN__c=1.2;
        updateUser.add(managerUser2);
        managerUser3.Adjustment_Approval_Limit_MXN__c=1.2;
        updateUser.add(managerUser3);
        update updateUser;
        Test.startTest();
        //Calling assignApprover method of WorkOrderApproverAssigmentCls Class
        WorkOrderApproverAssigmentCls.assignApprover(user1.Id,workOrderList[0].Id);
        Test.stopTest();
        //Fetching approver2 from workOrder where workorder subject is Testing
        List<WorkOrder> wk=[Select Approver_2__c from WorkOrder where Subject='Testing'];
        //Fetching test User Data to verify that Approver Id and the Test User Id is same
        User usr=[Select Id from User where username='test4@testdomain.comtestuser' LIMIT 1];
        System.assertEquals(usr.Id, wk[0].Approver_2__c);
    }            
    //****************************************************************************************************
    // Method to verify Approver2 field of WorkOrder when Approver6 User is not null
    // @return void
    // Added by Poornima Bhardwaj on 9 June 2017 (#T-608729)
    //****************************************************************************************************
    static testMethod void testWorkOrderApproverAssigmentClsMXN4(){
        //Creating Test Data
        testCreateUserData2();
        managerUser1.Adjustment_Approval_Limit_MXN__c=1.2;
        updateUser.add(managerUser1);
        managerUser2.Adjustment_Approval_Limit_MXN__c=1.2;
        updateUser.add(managerUser2);
        managerUser3.Adjustment_Approval_Limit_MXN__c=1.2;
        updateUser.add(managerUser3);
        managerUser4.Adjustment_Approval_Limit_MXN__c=1.2;
        updateUser.add(managerUser4);
        update updateUser;
        Test.startTest();
        //Calling assignApprover method of WorkOrderApproverAssigmentCls Class
        WorkOrderApproverAssigmentCls.assignApprover(user1.Id,workOrderList[0].Id);
        Test.stopTest();
        //Fetching approver2 from workOrder where workorder subject is Testing
        List<WorkOrder> wk=[Select Approver_2__c from WorkOrder where Subject='Testing'];
        //Fetching test User Data to verify that Approver Id and the Test User Id is same
        User usr=[Select Id from User where username='test5@testdomain.comtestuser' LIMIT 1];
        System.assertEquals(usr.Id, wk[0].Approver_2__c);
        
    }  
    //****************************************************************************************************
    // Method to verify Approver2 field of WorkOrder when Approver7 User is not null
    // @return void
    // Added by Poornima Bhardwaj on 9 June 2017 (#T-608729)
    //****************************************************************************************************
    static testMethod void testWorkOrderApproverAssigmentClsMXN5(){
        //Creating Test Data
        testCreateUserData2();
        //Setting Approval Limits for managerUser 
        managerUser1.Adjustment_Approval_Limit_MXN__c=1.2;
        updateUser.add(managerUser1);
        managerUser2.Adjustment_Approval_Limit_MXN__c=1.2;
        updateUser.add(managerUser2);
        managerUser3.Adjustment_Approval_Limit_MXN__c=1.2;
        updateUser.add(managerUser3);
        managerUser4.Adjustment_Approval_Limit_MXN__c=1.2;
        updateUser.add(managerUser4);
        managerUser5.Adjustment_Approval_Limit_MXN__c=1.2;
        updateUser.add(managerUser5);
        update updateUser;
        Test.startTest();
        //Calling assignApprover method of WorkOrderApproverAssigmentCls Class
        WorkOrderApproverAssigmentCls.assignApprover(user1.Id,workOrderList[0].Id);
        Test.stopTest();
        //Fetching approver2 from workOrder where workorder subject is Testing
        List<WorkOrder> wk=[Select Approver_2__c from WorkOrder where Subject='Testing'];
        //Fetching test User Data
        User usr=[Select Id from User where username='test6@testdomain.comtestuser' LIMIT 1];
        //Verifying whether user Id is same as the approver user Id
        System.assertEquals(usr.Id, wk[0].Approver_2__c);
    }
    
}