/**********************************************************************
* Appirio, Inc
* Test Class Name: ViewPendingArticlesControllerTest
* Class Name: [ViewPendingArticlesController]
* Description: [T-596388 Test class for ViewPendingArticlesController]
* Created Date: [24/04/2017]
* Created By: [Neeraj Kumawat] (Appirio)
* 
* Date Modified                Modified By                  Description of the update
***********************************************************************/
@isTest
public class ViewPendingArticlesControllerTest {
    public static Integer numberOfRecord=1;
    //***************************************************************************
    // Method to Associate Data Category Record to External Articles
    // @param externalArticleList : list of External Article
    // @return List of External Link Article Object
    //***************************************************************************
    public static List<External_Link__kav> assignDataCategorytoArticle(List<External_Link__kav> externalArticleList){
        List <String> objType = new List<String>();
        objType.add('KnowledgeArticleVersion');
        List<External_Link__DataCategorySelection> externalLinkCategoryList= new List<External_Link__DataCategorySelection>();
        //DescribeDataCategoryGroupResult[] results = Schema.describeDataCategoryGroups(objType); 
        for(External_Link__kav externalLink : externalArticleList){  
            //List of Region DaatCategoryGroupName
            List<String> dataCategoryRegionList= new List<String>();
            dataCategoryRegionList.add('United_States');
            dataCategoryRegionList.add('Canada');            
            externalLinkCategoryList.addAll(createDataCategory('Region',dataCategoryRegionList,externalLink.Id));
            
            //List of Intended_Audience DaatCategoryGroupName
            List<String> dataCategoryIntended_AudienceList= new List<String>();
            dataCategoryIntended_AudienceList.add('Boarding');
            dataCategoryIntended_AudienceList.add('BRU');
            externalLinkCategoryList.addAll(createDataCategory('Intended_Audience',dataCategoryIntended_AudienceList,
                                                                  externalLink.Id));
        }
        insert externalLinkCategoryList;
        return externalArticleList;
    }
    
    //***************************************************************************
    // Method to Create Data Cateogory
    // @param externalArticleList : Id of External Article
    // @param DataCategoryGroupName : String of DataCategoryGroupName
    // @param externalArticleList : List of String of DataCategoryName
    // @return List of External Link Article Object
    //***************************************************************************    
    public static List<External_Link__DataCategorySelection> createDataCategory(String dataCategoryGroupName, 
                                                                                List<String> dataCategoryName,
                                                                                Id externalArticleList){
        List<External_Link__DataCategorySelection> externalLinkCategoryList= new List<External_Link__DataCategorySelection>();
        for(String dcs: dataCategoryName ){
            External_Link__DataCategorySelection tempCat = new External_Link__DataCategorySelection();
            tempCat.DataCategoryGroupName = dataCategoryGroupName;
            tempCat.DataCategoryName = dcs;
            tempCat.ParentId = externalArticleList;
            externalLinkCategoryList.add(tempCat);
        }  
        return externalLinkCategoryList;        
    }
    
    
    //***************************************************************************
    // Test Method to check Pending Article Approval.
    // @return void
    //***************************************************************************
    public static testMethod void testArticleApproval(){
        List<Profile> profileList=[Select id,name from Profile Where name='System Administrator'];
        User customManager = TestUtilities.createTestUser(1, profileList[0].Id, false);
        customManager.UserPermissionsKnowledgeUser =true;
        insert customManager;
        Group g = [select Id,Name from Group where Name='PRT' AND Type = 'Queue'];
        id runningUserId = UserInfo.getUserId();
        System.runAs(new User(Id = runningUserId )) {
            GroupMember member = new GroupMember();
            member.UserOrGroupId = customManager.Id;
            member.GroupId = g.Id;
            insert member;
        }
        System.runAs(customManager){
            List<External_Link__kav> externalArticleList= TestUtilities.createExternal_linkArticle(numberOfRecord,true);
            assignDataCategorytoArticle(externalArticleList);
            Test.startTest();
            // Create an approval request for the article
            Approval.ProcessSubmitRequest req1 =new Approval.ProcessSubmitRequest();
            req1.setComments('Submitting request for approval.');
            for(External_Link__kav exLink: externalArticleList){
                req1.setObjectId(exLink.id);
            }
            // Submit the approval request for the Article
            Approval.ProcessResult result = Approval.process(req1);
            ViewPendingArticlesController viewPendingArticle= new ViewPendingArticlesController();
            System.assert(viewPendingArticle.knowledgeList.size()>0);
            Test.stopTest();
        }
    }   
}