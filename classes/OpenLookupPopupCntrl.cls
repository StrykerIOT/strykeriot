public without sharing class OpenLookupPopupCntrl {
    
    public List<Contact> listRecords {get;set;}
    public List<String> listFieldNames {get; set;}
    public String selectedObject;
    public String fieldName;
    public String searchText {get;set;}
    public String fieldSetName;
    public String fieldToPopulateId {get;set;}
    
    //
    public String fieldAccountId{set;get;}
     
    public OpenLookupPopupCntrl(){
        selectedObject = ApexPages.currentPage().getParameters().get('Object');
        fieldName = ApexPages.currentPage().getParameters().get('fieldName');
        searchText = ApexPages.currentPage().getParameters().get('searchText');
        fieldSetName = ApexPages.currentPage().getParameters().get('fieldSetName');
        fieldAccountId  = ApexPages.currentPage().getParameters().get('filterByAccountId');
        fieldToPopulateId = ApexPages.currentPage().getParameters().get('fieldToPopulateId');
        listRecords = new List<Contact>();
        runSearch();
    }
    
    /********************************************************************************************************************
    * Method to run the search with parameters passed via Page Parameters
    *********************************************************************************************************************/
    public PageReference runSearch() {
        
        String fieldsToFetch = getFieldsForSelectedObject();
        String whereClause = ''; 
         String searchString = 'Select Id,Name, ' + fieldsToFetch + ' From ' + selectedObject;
        if(fieldsToFetch != null && fieldsToFetch != ''){
            whereClause = ' Where AccountId =:fieldAccountId ';
        }
        system.debug(' @@ Account Search @@'+searchString + whereClause );
        listRecords = Database.query(searchString + whereClause + ' order by Name limit 50');
        //listRecords = [select Id,Name,Email from Contact];
        return null;
    }
    
    /********************************************************************************************************************
    * Method to Get all Fields of the selected object
    *********************************************************************************************************************/
    public String getFieldsForSelectedObject(){    
       listFieldNames = new List<String>(); 
       List<Schema.FieldSetMember> fieldSetMemberList = new List<Schema.FieldSetMember>();
       String fieldsToFetch = '';
       try{
           if(selectedObject != null && selectedObject != ''){             

                Map<String, Schema.SObjectType> GlobalDescribeMap = Schema.getGlobalDescribe(); 
                Schema.SObjectType SObjectTypeObj = GlobalDescribeMap.get(selectedObject);
                Schema.DescribeSObjectResult DescribeSObjectResultObj = SObjectTypeObj.getDescribe();
                
                if(DescribeSObjectResultObj.FieldSets.getMap().ContainsKey(fieldSetName)){
                    Schema.FieldSet fieldSetObj = DescribeSObjectResultObj.FieldSets.getMap().get(fieldSetName);
                    fieldSetMemberList =  fieldSetObj.getFields(); 
                }
                
                for(Schema.FieldSetMember fieldSetMemberObj : fieldSetMemberList)
                {
                    listFieldNames.add(string.ValueOf(fieldSetMemberObj.getFieldPath()));
                }
            }
           
           //Building Query with the fields
            Integer i = 0; 
            Integer len = listFieldNames.size();
            for(String temp:listFieldNames){
                if(i==len-1){
                    fieldsToFetch = fieldsToFetch + temp;
                } else {
                    fieldsToFetch = fieldsToFetch + temp + ',';
                }
                i++;
            }
        }catch(Exception ex){          
            apexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'There is no Field for selected Object!'));
        }           
        return fieldsToFetch;
    }

}