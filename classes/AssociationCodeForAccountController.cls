/*********************************************************************
* Appirio, Inc
* Name: AssociationCodeForAccountController
* Description: [Task# T-585630 Controller class to fetch reference records based on Account Associaotion codes.]
* Created Date: [16/3/2017]
* Created By: [Neeraj Kumawat] (Appirio)
* 
* Date Modified                Modified By           Description of the update
*[17/3/2017]                  Neeraj Kumawat         Used referenceValue to store association code and description
*[22/4/2017]                  Neeraj Kumawat         Generated key based on CLIENT_GROUP__C||�-�||ASSOCIATION_CODE__C||�-�
||PROCESSING_CENTER_ID__C and use this key to retrice referece description
*[25/4/2017]                  Neeraj Kumawat         Show only Association code-Description instead of whole key(#T-596676)
*[26/4/2017]                  Neeraj Kumawat         Split association code based on comma instead of semicolon(#T-596676)
*[02/06/2017]                 Bobby Cheek            Ensure that Sharing rules are enforced in AssociationCodeForAccountController.cls 
*                                                    by adding "with sharing" keyword to the class declaration(#T-600207)
**********************************************************************/
public with sharing class AssociationCodeForAccountController {
    //Account Id variable
    private string accountId;
    //Reference value variable used to show reference association code and description on general account page
    public String refrenceValue{set;get;}
    //****************************************************************************
    // Constructor Method of AssociationCodeForAccountController  
    // @param controller: Apex standard Controller
    // @return void
    //****************************************************************************
    public AssociationCodeForAccountController(ApexPages.StandardController controller) {
        //list for associationCode
        List<String> associationCodeList = new List<String>();
        //Map of External Key and association code value
        Map<String,String> externalKeyMap = new Map<String,String>();
        List<Reference__c> referenceList = new List<Reference__c>();
        //Getting account Id from Standard Controller
        accountId = ((Account)controller.getRecord()).id;
        //Fetching account Association code value based on AccountId
        Account acct=[Select Id,Client_Group__c,Association_Code__c,Processing_Center_ID__c 
                      From Account
                      Where Id=:accountId];
        // check null condition for account
        if(acct!=null && String.isNotBlank(acct.Association_Code__c)){
            //Split Account Association code with ; value
            associationCodeList=acct.Association_Code__c.split(',');
        }
        if(associationCodeList.size()>0){
            //****************************************************************************
            // Updated code to Generated key based on CLIENT_GROUP__C||�-�||ASSOCIATION_CODE__C||�-�
            //||PROCESSING_CENTER_ID__C and use this key to retrice referece description
            //22/4/2017 Neeraj Kumawat T-596676
            //****************************************************************************
            //Start//
            //when association code is not empty
            for(String assCode: associationCodeList){
                String  externalKey=null;
                //condition for adding client group
                if(acct.Client_Group__c!=null && acct.Client_Group__c!=''){
                    externalKey=acct.Client_Group__c;
                }
                //condition for adding association code
                if(assCode!=null && assCode!=''){
                    if(acct.Client_Group__c!=null && acct.Client_Group__c!=''){
                        externalKey+='-'+assCode;
                    }else{
                        externalKey=assCode;
                    }
                }
                //condition for adding processing center id
                if(acct.Processing_Center_ID__c!=null && acct.Processing_Center_ID__c!=''){
                    if(acct.Client_Group__c!=null && acct.Client_Group__c!=''){
                        externalKey+='-'+acct.Processing_Center_ID__c;
                    }else if(assCode!=null && assCode!=''){
                        externalKey+='-'+acct.Processing_Center_ID__c;
                    }else{
                        externalKey=acct.Processing_Center_ID__c;
                    }
                }
                if(externalKey!=null){
                    externalKeyMap.put(externalKey, assCode);
                }
            }
        }else{
            //when association code is empty then add only Client Group and Processing center id
            String  externalKey=null;
            //condition for adding client group
            if(acct.Client_Group__c!=null && acct.Client_Group__c!=''){
                externalKey=acct.Client_Group__c;
            }
            //condition for adding processing center id
            if(acct.Processing_Center_ID__c!=null && acct.Processing_Center_ID__c!=''){
                if(acct.Client_Group__c!=null && acct.Client_Group__c!=''){
                    externalKey+='-'+acct.Processing_Center_ID__c;
                }else{
                    externalKey=acct.Processing_Center_ID__c;
                }
            }
            if(externalKey!=null){
                externalKeyMap.put(externalKey, '');
            }
        }
        //End //
        //Fetching reference records based on Association code
        if(externalKeyMap.size()>0){
            referenceList=[Select Id, External_Id__C, Description__c 
                           From Reference__c 
                           Where External_ID__c IN: externalKeyMap.keySet()
                           And Category__c='Association Code'];
        }
        //Initializing refrenceValue to empty string
        refrenceValue='';
        //Iterating referenceList value and setting it to refrenceValue
        for(Reference__c ref:referenceList){
            //Check for description null value
            if(ref.Description__c!=null){
                //****************************************************************************
                //Split association code based on comma instead of semicolon
                //26/4/2017 Neeraj Kumawat T-596676
                //****************************************************************************
                //Start//
                refrenceValue += externalKeyMap.get(ref.External_ID__c)+'-'+ref.Description__c+',';
            }else{
                refrenceValue += externalKeyMap.get(ref.External_ID__c)+'-'+' '+',';
                //End//
            }
        }
        //remove , from end of the reference Value
        refrenceValue=refrenceValue.removeEnd(',');
    }
}