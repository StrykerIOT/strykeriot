/***********************************************************************************************************************
* Appirio, Inc
* Trigger Name: AdditionalAddressTriggerHandler
* Description: Trigger updates account address fields; created to replace Process Builder that hit query limits during 
*              integration calls (bulkification)
* Created Date: 02/19/2017
* Created By: Shannon Howe (Appirio)
* 
* Date Modified              Modified By                Description of the update
* [02/19/2017]              [Shannon Howe]             [Created to resolve process builder SOQL limits]
* [02/20/2017]              [Shannon Howe]             [Optimized] 
* [05/31/2017]              [Bobby Cheek]              [Enforcing CRUD and FLS Security in the Apex Class - T-600205]
***********************************************************************************************************************/
public class AdditionalAddressTriggerHandler {
    
    /*****************************************************************
* Method for calling action on After Insert Update of AdditionalAddress
******************************************************************/
    
    public static void onAfterInsertUpdate(List<Additional_Address__c> newAddress){
        
        updateAccountFields(newAddress);
        
    }
    
    
    /*****************************************************************
* Method for Updating Accounts
******************************************************************/
    public static void updateAccountFields(List<Additional_Address__c> newAddress){
        
        List<Account> accToUpdate = new List<Account>();
        Set<Id> aidSet = new Set<Id>();
        // List<Account> accList = new List<Account>();
        for(Additional_Address__c a : newAddress){
            aidSet.add(a.account__c);
        }
        
        //get account records
        List<Account> accList =  [SELECT Id, BillingStreet, ShippingStreet, BillingCity, ShippingCity,
                                  BillingState, ShippingState, BillingCountry, ShippingCountry, BillingPostalCode,
                                  ShippingPostalCode FROM Account 
                                  WHERE Id IN : aidSet];
        
        
        if(accList.size()>0){
            
            //for accounts, update addresses
            for(Account acc :accList) {
                
                //update addresses
                for(Additional_Address__c add : newAddress) {
                    String street;
                    String city;
                    String state;
                    String postal;
                    String country;
                    
                    if(add.Street_1__c != null){  
                        street = add.Street_1__c + ' ';
                    }
                    
                    if(add.Street_2__c != null){
                        street = street + add.Street_2__c + ' ';
                    }
                    
                    if(add.Street_3__c != null){
                        street = street +  + add.Street_3__c;
                    }
                    
                    if(add.city__c != null) {
                        city = add.City__c;
                    }
                    
                    if(add.county_state__c != null){
                        state = add.County_State__c;
                    }
                    
                    if(add.country__c != null){
                        country = add.Country__c;
                    }
                    
                    if(add.postal_code__c != null){
                        postal = add.Postal_Code__c; 
                    }
                    
                    if(add.address_type__c != null) {                   
                        String atype = add.Address_Type__c;
                    }
                    
                    //update billing address
                    if (add.Address_Type__c =='Account' && add.account__c == acc.id) {
                        
                        if(street!= null && Schema.sObjectType.Account.fields.BillingStreet.isUpdateable()){ 
                            acc.BillingStreet = street.abbreviate(255);
                        }
                        
                        if(street == null && Schema.sObjectType.Account.fields.BillingStreet.isUpdateable()){
                            acc.BillingStreet = street;
                        }
                        
                        if(city != null && Schema.sObjectType.Account.fields.BillingStreet.isUpdateable()){
                            acc.BillingCity = city.abbreviate(40);
                        }
                        
                        if(city==null && Schema.sObjectType.Account.fields.BillingCity.isUpdateable()){
                            acc.BillingCity = city;
                        }
                        
                        if(state != null && Schema.sObjectType.Account.fields.BillingState.isUpdateable()){
                            acc.BillingState = state.abbreviate(20);
                        }
                        
                        if(state == null && Schema.sObjectType.Account.fields.BillingState.isUpdateable()){
                            acc.BillingState = state;
                        }
                        
                        
                        if(country != null && Schema.sObjectType.Account.fields.BillingCountry.isUpdateable()){
                            acc.BillingCountry =  country.abbreviate(40);
                        }
                        
                        if(country == null && Schema.sObjectType.Account.fields.BillingCountry.isUpdateable()){
                            acc.BillingCountry =  country;
                        }
                        
                        
                        if(postal != null && Schema.sObjectType.Account.fields.BillingPostalCode.isUpdateable()){  
                            acc.BillingPostalCode = postal.abbreviate(20);
                        }
                        
                        if(postal == null && Schema.sObjectType.Account.fields.BillingPostalCode.isUpdateable()){  
                            acc.BillingPostalCode = postal;
                        }
                        
                        
                    }
                    
                    //update shipping address
                    if (add.Address_Type__c == 'Legal'&& add.account__c == acc.id) {
                        
                        if(street!= null && Schema.sObjectType.Account.fields.ShippingStreet.isUpdateable()){ 
                            acc.ShippingStreet = street.abbreviate(255);
                        }
                        
                        if(street == null && Schema.sObjectType.Account.fields.ShippingStreet.isUpdateable()){
                            acc.ShippingStreet = street;
                        }
                        
                        if(city != null && Schema.sObjectType.Account.fields.ShippingCity.isUpdateable()){
                            acc.ShippingCity = city.abbreviate(40);
                        }
                        
                        if(city==null && Schema.sObjectType.Account.fields.ShippingCity.isUpdateable()){
                            acc.ShippingCity = city;
                        }
                        
                        if(state != null && Schema.sObjectType.Account.fields.ShippingState.isUpdateable()){
                            acc.ShippingState = state.abbreviate(20);
                        }
                        
                        if(state == null && Schema.sObjectType.Account.fields.ShippingState.isUpdateable()){
                            acc.ShippingState = state;
                        }
                        
                        
                        if(country != null && Schema.sObjectType.Account.fields.ShippingCountry.isUpdateable()){
                            acc.ShippingCountry =  country.abbreviate(40);
                        }
                        
                        if(country == null && Schema.sObjectType.Account.fields.ShippingCountry.isUpdateable()){
                            acc.ShippingCountry =  country;
                        }
                        
                        
                        if(postal != null && Schema.sObjectType.Account.fields.ShippingPostalCode.isUpdateable()){  
                            acc.ShippingPostalCode = postal.abbreviate(20);
                        }
                        
                        if(postal == null && Schema.sObjectType.Account.fields.ShippingPostalCode.isUpdateable()){  
                            acc.ShippingPostalCode = postal;
                        }
                        
                        
                    }
                    
                }
                
                
                
                //update accounts
                accToUpdate.add(acc);   
            }
            
        }
        //only update accounts when the account exists
        if(accToUpdate.size()>0){
            update accToUpdate;  
            
        }
        
        
    }
}