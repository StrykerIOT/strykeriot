/*********************************************************************
* Appirio, Inc
* Name: ViewDocumentsController
* Description: [Task# T-557407]
* Created Date: [12/21/2016]
* Created By: [Aashish Sajwan] (Appirio)
* 
* Date Modified                Modified By              Description of the update
* [Tue 21, 2016]               [Aashish Sajwan]         [T-557407]
**********************************************************************/
@isTest
global class ElavonServicesImagingserviceUMock implements WebServiceMock {
    private static string statementID='OD_djcxMjYtNzM3NC03Mzc2LTczNzgtSlNBMTUtMjI5MkZBQUEtMz'+
        'IzOTcyLTYzMzYwLTAtMTIxMTQ0LTc4LTc5LTI2NC0xMjYtMC1eATAwMDAwMDgwMDY1NTkwNDQBTUVSIFRSQU4BMD'+
        'AwMDMBNjEzMDMBMDAwMDABMDAwMDABODAwNjU1OTA0NAFVUyBURVNUIE1FUkNIQU5UIFZJU0EgU01LVCBUSUVSMQE'+
        'CATE3MTQxAU1QU1NUTVRQICAgICAgICABMA==_Disk_A_MPSSTMTP_MERCH_DATE|||12/05/2016_ACCOUNT|||0000008006559044';
    private static string imageData='JVBERi0xLjQKJeLjz9MKNCAwIG9iagpbMSAwIFIvWFlaIDAgNzE0IDBdCmVuZG9ia'+
        'go1IDAgb2JqClsxIDAgUi9YWVogMCA3MTQgMF0KZW5kb2JqCjYgMCBvYmoKPDwvTmFtZXNbKLU9xuxGgm2SUTyXb5G+UykgNCA'+
        'wIFIomVwoxONHgGKSeSOJaJqpUKCMvSkgNSAwIFJdPj4KZW5kb2JqCjcgMCBvYmoKPDwvRGVzdHMgNiAwIFI+PgplbmRvYmoKOC'+
        'AwIG9iago8PC9GaWx0ZXIvRmxhdGVEZWNvZGUvTGVuZ3RoIDYxMD4+c3RyZWFtCgZre00+HPu6s+DyI9hYLWRoQAZFszt/aQKRqDq'+
        'Lt/0Xg/zPd6sIrt9QGaiiYbGRiKgv3jITRj4poy7q2PSN2WWWMIYIWIUkyPEWz3TJFEjQuR9X2GVDFY37Yc8BgFWCGjKHGTjwZvilt7'+
        'hWbm+zwnaYAJ5RkI8kPyJ3aLAws3rMOd8wkDh3zyF27NVhifc/bL1ksuOU9sgaXwow9JEUkMfQXF5QaDdgPRWseg9T2sk9+aHbkVtAdxF'+
        'g+S2FODRosldSuiLo4j5VBzqxuuHvPwplMyYzRbkyZCx4on/LkFYlie6z8OcjBf2U4fRjl7wgSVzeI7I3iwpBF1MsZ6QgITG0lvnj2NvsNO'+
        'O05W38kVq3qktHC7hievRn+wg9Wb89OhAedwQTpuYvjIf852T0L+EfZFzG0nTTuWyeqdrBGlHK71rBZWfj+tnuglL7Vtb68XdD4za9D3g4Ti'+
        'OkqNDNmdzAk2cBicIWscLVa+ZnbAp5bN0jyFyXZ8u5NszbJR2R+NnXznaL+UXb+WKxAh6Wn8yjYKDekZpueTkZbL8RbIGQzXA8eHqbyJVCOzD'+
        'xYyHFAxzoKqZHg9x+X67qqn7NozyZfp6GrWodYJ7TpzAzAh3nlTK/4Cz7/0oxLKl0kcNe6l6sQhYCfItPU5444B5brQfdu/3iQGxNbXaPNzkRIJ'+
        'dVfTnX0DtVost7+9cA8Lob1H+4JjZCdGn8YDo4466pkJwbLHCAZITq3vJNe7OF2Tibp4FD4Fh+0O68i6ZrsR2Newx8Gvr+FOqyYSb6QgE0goK2fD6'+
        'N+1+XJeDQaT6UA3M/g0MKZW5kc3RyZWFtCmVuZG9iagoxIDAgb2JqCjw8L0NvbnRlbnRzIDggMCBSL1R5cGUvUGFnZS9SZXNvdXJjZXM8PC9Qcm9jU2'+
        'V0IFsvUERGIC9UZXh0IC9JbWFnZUIgL0ltYWdlQyAvSW1hZ2VJXS9Gb250PDwvRjEgMiAwIFIvRjIgMyAwIFI+Pj4+L1BhcmVudCA5IDAgUi9NZWRpYU'+
        'JveFswIDAgNjEyIDc5Ml0+PgplbmRvYmoKMiAwIG9iago8PC9TdWJ0eXBlL1R5cGUxL1R5cGUvRm9udC9CYXNlRm9udC9IZWx2ZXRpY2EvRW5jb2Rp'+
        'bmcvV2luQW5zaUVuY29kaW5nPj4KZW5kb2JqCjMgMCBvYmoKPDwvU3VidHlwZS9UeXBlMS9UeXBlL0ZvbnQvQmFzZUZvbnQvSGVsdmV0aWNhLUJv'+
        'bGQvRW5jb2RpbmcvV2luQW5zaUVuY29kaW5nPj4KZW5kb2JqCjkgMCBvYmoKPDwvS2lkc1sxIDAgUl0vVHlwZS9QYWdlcy9Db3VudCAxL0lUWF'+
        'QoGUvFljYpPj4KZW5kb2JqCjEwIDAgb2JqCjw8L05hbWVzIDcgMCBSL1R5cGUvQ2F0YWxvZy9QYWdlcyA5IDAgUj4+CmVuZG9iagoxMSAwIG'+
        '9iago8PC9Nb2REYXRlKGTn4ddeeiUWRGF0nsvT6iIFpuYtPy1gKS9DcmVhdGlvbkRhdGUoZOfh1156JRZEYXSey9PqIgWm5i0/LWApL1B'+
        'yb2R1Y2VyKEmJtp8bbCZcdEd3coyYk/knfKWLXikvVGl0bGUoYa2jiwYvdVMfNiuMu468eEzjvik+PgplbmRvYmoKMTIgMCBvYmoKP'+
        'DwvTyAoSHW+GRMa/XxpbZiNJBbAuoy2pnDSJ5SovRuCjakEdF8pL1AgLTE1ODAvUiAzL0ZpbHRlci9TdGFuZGFyZC9VIChmK'+
        'uHaN1+2yJODiZnZGmN7AAAAAAAAAAAAAAAAAAAAACkvViAyL0xlbmd0aCAxMjg+PgplbmRvYmoKeHJlZgowIDEzCjAwMDA'+
        'wMDAwMDAgNjU1MzUgZiAKMDAwMDAwMDg3MiAwMDAwMCBuIAowMDAwMDAxMDM4IDAwMDAwIG4gCjAwMDAwMDExMjY'+
        'gMDAwMDAgbiAKMDAwMDAwMDAxNSAwMDAwMCBuIAowMDAwMDAwMDUwIDAwMDAwIG4gCjAwMDAwMDAwODUgMDAwMD'+
        'AgbiAKMDAwMDAwMDE2MyAwMDAwMCBuIAowMDAwMDAwMTk1IDAwMDAwIG4gCjAwMDAwMDEyMTkgMDAwMDAg'+
        'biAKMDAwMDAwMTI4MiAwMDAwMCBuIAowMDAwMDAxMzQwIDAwMDAwIG4gCjAwMDAwMDE0OTEgMDAwMDAg'+
        'biAKdHJhaWxlcgo8PC9JbmZvIDExIDAgUi9FbmNyeXB0IDEyIDAgUi9JRCBbPGMyNzliZGUyYmJiZGM'+
        '1MjU5ODY3NmRjZjhhNzAzMWM1PjwzMWJmMzkxNjA0YmY0OTQ0OWNjZTE1ZTIzYmQ2NzIzMz5d'+
        'L1Jvb3QgMTAgMCBSL1NpemUgMTM+PgpzdGFydHhyZWYKMTYyOQolJUVPRgo=';
    private static string resType;
    public void setVariable(string s)
    {
        resType = s;
    }
    public string getVariable()
    {
        return resType;
    }
    global void doInvoke(
        Object stub,
        Object request,
        Map<String, Object> response,
        String endpoint,
        String soapAction,
        String requestName,
        String responseNS,
        String responseName,
        String responseType) {
            System.debug('** Search  Calling ** '+  request);
            /* Start */
            /* Modified by Gaurav Dudani on 28-March-2017 as per task # - T-583606. */
            if(resType=='SearchRecord' && request instanceof wwwElavonComSchemaImagingresourcesV.RetrieveImagesRequest_element)
            /* End */
            {
                System.debug('** Search Record Calling **');
                wwwElavonComSchemaImagingresourcesV.Images_element objImages = new wwwElavonComSchemaImagingresourcesV.Images_element();  
                wwwElavonComSchemaImagingresourcesV.Image objImage = new  wwwElavonComSchemaImagingresourcesV.Image();
                objImage.docID = '2759115';
                objImage.sDocTypeID = 'APPLI';
                objImage.indexDate = '11/30/2016 12:00:00 AM';
                objImage.scanDate = '11/30/2016 12:00:00 AM';
                objImage.dbaName = 'CSPR NEW MID FILE1';
                objImage.mid = '8024778006';
                objImage.imageData = null;
                objImage.mimeType = null;
                wwwElavonComSchemaImagingresourcesV.Image[] arrImages = new wwwElavonComSchemaImagingresourcesV.Image[]{objImage};
                    objImages.Image = arrImages;
                wwwElavonComSchemaImagingresourcesV.RetrieveImagesResponse_element response_x = new wwwElavonComSchemaImagingresourcesV.RetrieveImagesResponse_element();
                response_x.Images = objImages;
               /* Start */
               /* Modified by Gaurav Dudani on 28-March-2017 as per task # - T-583606. */
                //response = new Map<string,Object>();
               /* End */ 
                response.put('response_x', response_x);
            }
            /* Start */
            /* Modified by Gaurav Dudani on 28-March-2017 as per task # - T-583606. */
            else if(resType=='OpenRecord' && request instanceof wwwElavonComSchemaImagingresourcesV.DocumentImageRequest_element)
            /* End */
            { 
                System.debug('** Open Record Calling **');
                wwwElavonComSchemaImagingresourcesV.Image objImage = new  wwwElavonComSchemaImagingresourcesV.Image();
                objImage.docID = '2759115';
                objImage.sDocTypeID = 'APPLI';
                objImage.indexDate = '11/30/2016 12:00:00 AM';
                objImage.scanDate = '11/30/2016 12:00:00 AM';
                objImage.dbaName = 'CSPR NEW MID FILE1';
                objImage.mid = '8024778006';
                objImage.imageData = imageData;
                objImage.mimeType = null;
                wwwElavonComSchemaImagingresourcesV.Image[] arrImages = new wwwElavonComSchemaImagingresourcesV.Image[]{objImage};
                    
                    wwwElavonComSchemaImagingresourcesV.DocumentImageResponse_element response_x = new wwwElavonComSchemaImagingresourcesV.DocumentImageResponse_element();
                
                response_x.Image = arrImages;
               /* Start */
               /* Modified by Gaurav Dudani on 28-March-2017 as per task # - T-583606. */
                //response = new Map<string,Object>();
               /* End */ 
                response.put('response_x', response_x);
            }
            /* Start */
            /* Modified by Gaurav Dudani on 28-March-2017 as per task # - T-583606. */
            else if(resType=='ProcessRecord' && request instanceof wwwElavonComSchemaImagingresourcesV.StatementImageRequest_element)
            /* End */
            { 
                System.debug('** Process Record Calling **');
                wwwElavonComSchemaImagingresourcesV.StatementImageResponse_element response_x =new wwwElavonComSchemaImagingresourcesV.StatementImageResponse_element();
                response_x.StatementID='2759115';
                response_x.ImageData=imageData;
                response_x.XMLData=null;
               /* Start */
               /* Modified by Gaurav Dudani on 28-March-2017 as per task # - T-583606. */
                //response = new Map<string,Object>();
               /* End */ 
                response.put('response_x', response_x);
            }
            /* Start */
            /* Modified by Gaurav Dudani on 28-March-2017 as per task # - T-583606. */
            else if(resType=='uploadDocuments' && request instanceof wwwElavonComSchemaImagingresourcesV.UploadImageRequest_element)
            /* End */
            { 
                System.debug('** Upload Documents Calling **');
                wwwElavonComSchemaImagingresourcesV.UploadImageResponse_element response_x=new wwwElavonComSchemaImagingresourcesV.UploadImageResponse_element();
                response_x.ImageID='someid';
               /* Start */
               /* Modified by Gaurav Dudani on 28-March-2017 as per task # - T-583606. */
                //response = new Map<string,Object>();
               /* End */ 
                response.put('response_x', response_x);
                
            }
            /* Start */
            /* Modified by Gaurav Dudani on 28-March-2017 as per task # - T-583606. */
            else if(resType=='NULLuploadDocuments' && request instanceof wwwElavonComSchemaImagingresourcesV.UploadImageRequest_element)
            /* End */
            {
                System.debug('** Upload Documents NULL Calling **');
                wwwElavonComSchemaImagingresourcesV.UploadImageResponse_element response_x=null;
               /* Start */
               /* Modified by Gaurav Dudani on 28-March-2017 as per task # - T-583606. */
                //response = new Map<string,Object>();
               /* End */ 
                response.put('response_x', response_x);
            }
            /* Start */
            /* Modified by Gaurav Dudani on 28-March-2017 as per task # - T-583606. */
            else if(resType=='searchStatement' && request instanceof wwwElavonComSchemaImagingresourcesV.StatementListRequest_element)
            /* End */
            {
                System.debug('** searchStatement  Calling **'+  response);
                System.debug('Search Statement New');
                //response.clear();
                
                wwwElavonComSchemaImagingresourcesV.StatementListHeader RequestNew=new wwwElavonComSchemaImagingresourcesV.StatementListHeader();
                RequestNew.FromDate=String.valueof('05/01/2016');
                RequestNew.ToDate=String.valueof('10/31/2016');
                RequestNew.Type_x='MER_BILL';
                wwwElavonComSchemaImagingresourcesV.AccountNumberType objArrAN=new wwwElavonComSchemaImagingresourcesV.AccountNumberType();
                objArrAN.ClientGroup=32;
                objArrAN.MPSMID='jhbscdjh';
                objArrAN.ChainNumber='jbjc kbj';
                objArrAN.MerchantNumber='kjnkxjbxkj';
                objArrAN.ParentEntity='klnjkcn';
                objArrAN.PrincipleChain='jkbcjhdb';
                wwwElavonComSchemaImagingresourcesV.AccountNumberType[] AccountNumberArr=new wwwElavonComSchemaImagingresourcesV.AccountNumberType[]{objArrAN};
                    RequestNew.AccountNumber=AccountNumberArr;
                
                wwwElavonComSchemaImagingresourcesV.StatementData objSL=new wwwElavonComSchemaImagingresourcesV.StatementData();
                objSL.ID='12345678';
                objSL.MediaType='Disk';
                objSL.Date_x='12/05/2016';
                objSL.Description=null;
                objSL.AccountNumber=null;
                objSL.Type_x=null;
                wwwElavonComSchemaImagingresourcesV.StatementData[] StatementListObj=new wwwElavonComSchemaImagingresourcesV.StatementData[]{objSL};
                wwwElavonComSchemaImagingresourcesV.StatementListResponse_element objSLR=new wwwElavonComSchemaImagingresourcesV.StatementListResponse_element();
                objSLR.Request=RequestNew;
                objSLR.StatementList=StatementListObj;
               /* Start */
               /* Modified by Gaurav Dudani on 28-March-2017 as per task # - T-583606. */
                //response = new Map<string,Object>();
               /* End */ 
                response.put('response_x', objSLR);
                system.debug('responsee '+response);
            }
            /* Start */
            /* Modified by Gaurav Dudani on 28-March-2017 as per task # - T-583606. */
            else if(resType=='NULLsearchStatement' && request instanceof wwwElavonComSchemaImagingresourcesV.StatementListRequest_element)
            /* End */
            {
                System.debug('** Search Statement Calling **');
                wwwElavonComSchemaImagingresourcesV.StatementListResponse_element objSLR=null;
               /* Start */
               /* Modified by Gaurav Dudani on 28-March-2017 as per task # - T-583606. */
                //response = new Map<string,Object>();
               /* End */ 
                response.put('response_x', objSLR);
            }
        }
}