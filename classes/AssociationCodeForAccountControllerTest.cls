/*********************************************************************
* Appirio, Inc
* Name: AssociationCodeForAccountControllerTest
* Description: [Task#   T-586489 Test Class for AssociationCodeForAccountController]
* Created Date: [20/03/2016]
* Created By: [Neeraj Kumawat] (Appirio)
* 
* Date Modified                Modified By              Description of the update
*[25/4/2017]                  Neeraj Kumawat            Added extra test method to cover requiremnet of External Id key change(#T-596676)
*[26/4/2017]                  Neeraj Kumawat            Split association code based on comma instead of semicolon(#T-596676)
**********************************************************************/
@isTest
public class AssociationCodeForAccountControllerTest {
    //Reference Value variable
    public static String referenceValue='';
    //****************************************************************************************
    // Test Method to check Account Association code with Description section 
    // when Reference Rrcord has description value
    // @return void
    //*****************************************************************************************
    public static testmethod void testAccountAssociationCodeWithReferenceDescriptionValue(){
        //Calling Create Test Data Method
        List<Account> accountList=createTestData(2,false,'123','EU',false);
        Test.startTest();
        //Set Account Id
        Apexpages.currentpage().getparameters().put('id', accountList[0].Id);
        ApexPages.StandardController sc = new ApexPages.StandardController(accountList[0]);
        //Calling Constructor of AssociationCodeForAccountController
        AssociationCodeForAccountController associationCodeObj= new AssociationCodeForAccountController(sc);
        System.debug('Reference Value in testAccountAssociationCodeWithReferenceDescriptionValue='+referenceValue);
        System.assertEquals(referenceValue,associationCodeObj.refrenceValue);
        Test.stopTest();
    }
    //***************************************************************************
    // Test Method to check Account Association code with Description section 
    // when Reference Rrcord has description value empty
    // @return void
    //***************************************************************************
    public static testmethod void testAccountAssociationCodeWithReferenceDescriptionValueEmpty(){
        //Calling Create Test Data Method
        List<Account> accountList=createTestData(2,true,'123','EU',false);
        Test.startTest(); 
        //Set Account Id
        Apexpages.currentpage().getparameters().put('id', accountList[0].Id);
        ApexPages.StandardController sc = new ApexPages.StandardController(accountList[0]);
        //Calling Constructor of AssociationCodeForAccountController
        AssociationCodeForAccountController associationCodeObj= new AssociationCodeForAccountController(sc);
        System.debug('Reference Value in testAccountAssociationCodeWithReferenceDescriptionValueEmpty='+referenceValue);
        System.assertEquals(referenceValue,associationCodeObj.refrenceValue);
        Test.stopTest();
    }
    //***************************************************************************
    // Test Method to check Account Association code with Description section 
    // when Account Association code is empty
    // @return void
    //***************************************************************************
    public static testmethod void testAccountAssociationCodeWithEmptyValue(){
        referenceValue='';
        List<Account> accountList=TestUtilities.createAccount(1, true);
        Test.startTest();
        //Set Account Id
        Apexpages.currentpage().getparameters().put('id', accountList[0].Id);
        ApexPages.StandardController sc = new ApexPages.StandardController(accountList[0]);
        //Calling Constructor of AssociationCodeForAccountController
        AssociationCodeForAccountController associationCodeObj= new AssociationCodeForAccountController(sc);
        System.debug('Reference Value in testAccountAssociationCodeWithEmptyValue='+referenceValue);
        System.assertEquals(referenceValue,associationCodeObj.refrenceValue);
        Test.stopTest();
    }
     //*******************************************************************************************
     //Added Test Method to cover Generation of new key based  on CLIENT_GROUP__C||’-‘||ASSOCIATION_CODE__C||’-‘
     //||PROCESSING_CENTER_ID__C 
     //26/4/2017 Neeraj Kumawat T-596676
     //*******************************************************************************************
     //Start//
    //***************************************************************************
    // Test Method to check Account Association code without Client group and Processing Center Id
    // @return void
    //***************************************************************************
    public static testmethod void testAccountAssociationCodeWithoutclientGroupAndProcessingId(){
        //Calling Create Test Data Method
        List<Account> accountList=createTestData(2,true,null,null,false);
        Test.startTest(); 
        //Set Account Id
        Apexpages.currentpage().getparameters().put('id', accountList[0].Id);
        ApexPages.StandardController sc = new ApexPages.StandardController(accountList[0]);
        //Calling Constructor of AssociationCodeForAccountController
        AssociationCodeForAccountController associationCodeObj= new AssociationCodeForAccountController(sc);
        System.debug('Reference Value in testAccountAssociationCodeWithReferenceDescriptionValueEmpty='+referenceValue);
        System.assertEquals(referenceValue,associationCodeObj.refrenceValue);
        Test.stopTest();
    }
    //***************************************************************************
    // Test Method to check Account Association code without Client Group
    // @return void
    //***************************************************************************
    public static testmethod void testAccountAssociationCodeWithoutClientGroup(){
        //Calling Create Test Data Method
        List<Account> accountList=createTestData(2,true,null,'EU',false);
        Test.startTest(); 
        //Set Account Id
        Apexpages.currentpage().getparameters().put('id', accountList[0].Id);
        ApexPages.StandardController sc = new ApexPages.StandardController(accountList[0]);
        //Calling Constructor of AssociationCodeForAccountController
        AssociationCodeForAccountController associationCodeObj= new AssociationCodeForAccountController(sc);
        System.debug('Reference Value in testAccountAssociationCodeWithReferenceDescriptionValueEmpty='+referenceValue);
        System.assertEquals(referenceValue,associationCodeObj.refrenceValue);
        Test.stopTest();
    }
    //***************************************************************************
    // Test Method to check With out Association code
    // @return void
    //***************************************************************************
    public static testmethod void testAccountWithoutAssociationCode(){
        //Calling Create Test Data Method
        List<Account> accountList=createTestData(1,true,'123','EU',true);
        Test.startTest(); 
        //Set Account Id
        Apexpages.currentpage().getparameters().put('id', accountList[0].Id);
        ApexPages.StandardController sc = new ApexPages.StandardController(accountList[0]);
        //Calling Constructor of AssociationCodeForAccountController
        AssociationCodeForAccountController associationCodeObj= new AssociationCodeForAccountController(sc);
        System.debug('Reference Value in testAccountAssociationCodeWithReferenceDescriptionValueEmpty='+referenceValue);
        System.assertEquals(referenceValue,associationCodeObj.refrenceValue);
        Test.stopTest();
    }
    //***************************************************************************
    // Test Method to check With out Association code and Client Group
    // @return void
    //***************************************************************************
    public static testmethod void testAccountWithoutAssociationCodeAndWithoutClientGroup(){
        //Calling Create Test Data Method
        List<Account> accountList=createTestData(1,true,null,'EU',true);
        Test.startTest(); 
        //Set Account Id
        Apexpages.currentpage().getparameters().put('id', accountList[0].Id);
        ApexPages.StandardController sc = new ApexPages.StandardController(accountList[0]);
        //Calling Constructor of AssociationCodeForAccountController
        AssociationCodeForAccountController associationCodeObj= new AssociationCodeForAccountController(sc);
        System.debug('Reference Value in testAccountAssociationCodeWithReferenceDescriptionValueEmpty='+referenceValue);
        System.assertEquals(referenceValue,associationCodeObj.refrenceValue);
        Test.stopTest();
    }
    //End of Test Methods for Generation of new key//
    //*******************************************************************************************
     //Modify Test create Data method for creating record for new  key
     //26/4/2017 Neeraj Kumawat  T-596676
     //*******************************************************************************************
     //Start//
    //***************************************************************************
    // Method to create test data
    // @param Boolean : isReferenceDescriptionEmpty Boolean variable to check reference object
    // description value empty or not
    // @return List of Account Object
    //***************************************************************************
    private static List<Account> createTestData(Integer numberofRecord,Boolean isReferenceDescriptionEmpty,String clientGroup,
                                                String ProcessingId,Boolean isAssociationCodeEmpty){
        List<Reference__c> refList=TestUtilities.createReference(numberofRecord,false,clientGroup,ProcessingId,
                                                                 isAssociationCodeEmpty);
        String associationCode='';
        for(Reference__c ref: refList){
            //Getting assocaition code value from name of reference record
            if(!isAssociationCodeEmpty){
                associationCode+=ref.Name+',';
            }
            if(isReferenceDescriptionEmpty){
                ref.Description__c=null;
                if(isAssociationCodeEmpty){
                    referenceValue+='-'+' '+',';
                }else{
                    referenceValue+=ref.Name+'-'+' '+',';
                }
            }else{
                if(isAssociationCodeEmpty){
                    referenceValue+='-'+ref.Description__c+',';
                }else{
                    referenceValue+=ref.Name+'-'+ref.Description__c+',';
                }
                
            }
        }
        referenceValue=referenceValue.removeEnd(',');
        insert refList;
        associationCode=associationCode.removeEnd(',');
        List<Account> accountList=TestUtilities.createAccount(1, false);
        for(Account acct:accountList){
            acct.Association_Code__c=associationCode;
            if(clientGroup!=null){
                acct.Client_Group__c=clientGroup;
            }
            if(ProcessingId!=null){
                acct.Processing_Center_ID__c=ProcessingId;
            }
        }
        insert accountList;
        return accountList;
    }
    //End of creation of Test Data//
}