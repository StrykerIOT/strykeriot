/*********************************************************************
* Appirio, Inc
* Name: ViewStatementControllerTest
* Description: [Task# T-557407]
* Created Date: [12/21/2016]
* Created By: [Aashish Sajwan] (Appirio)
* 
* Date Modified                Modified By              Description of the update
* [Thu 22, 2016]              [Aashish Sajwan]         [T-557407]
* [Tue 7, 2017]               [Aashish Sajwan]         [T-578828]
* [Tue 9-Mar, 2017]           [Aashish Sajwan]         [T-578828]
* [Tue 17-Mar, 2017]          [Aashish Sajwan]         [T-583606]
* [Tue 23-May, 2017]          [Aashish Sajwan]         [T-603941]
**********************************************************************/
@isTest
public class ViewStatementControllerTest{
    static string StatementType;
    static string AccountName;
    static string AccountId;
    static string MID;
    static string ClientGroupId;
    static string AccountTaxId;
    //Added for T-603941 - 23May-2017 Aashish Sajwan
    static string TaxEncryptedId;
    static List<UtilityCls.StatmentWrapper> Wrapperlst;
    public testmethod static void testCallout1() { 
        //Create Test Data
        TestData(True);
        
        //Parameter Passing
        TestPassingParameters('NA');
        
        //Calling redirectToAccount() Method
        TestRedirectToAccount();
        
        ElavonServicesImagingserviceUMock objMock=new ElavonServicesImagingserviceUMock();
        
        ViewStatementController obj=new ViewStatementController();
        Test.StartTest();
        //******************************** Positive Use Cases *******************************
        TestSearchStatement(true,false,'MER_BILL','3');
        //
        Test.StopTest();
    }
    public testmethod static void testCallout2() { 
        //Create Test Data
        TestData(True);
        
        //Parameter Passing
        TestPassingParameters('EU');
        
        //Calling redirectToAccount() Method
        TestRedirectToAccount();
        
        ElavonServicesImagingserviceUMock objMock=new ElavonServicesImagingserviceUMock();
        
        ViewStatementController obj=new ViewStatementController();
        Test.StartTest();
        //******************************** Positive Use Cases *******************************
        TestSearchStatement(true,false,'MER_BILL','3');
        //
        Test.StopTest();
    }
    public testmethod static void testCallout3() { 
        //Create Test Data
        TestData(True);
        
        //Parameter Passing
        TestPassingParameters('QUERE');
        
        //Calling redirectToAccount() Method
        TestRedirectToAccount();
        
        ElavonServicesImagingserviceUMock objMock=new ElavonServicesImagingserviceUMock();
        
        ViewStatementController obj=new ViewStatementController();
        Test.StartTest();
        //******************************** Positive Use Cases *******************************
        TestSearchStatement(true,false,'MER_BILL','3');
        TestOpenRecord();
        //
        Test.StopTest();
    }
    public testmethod static void testCallout4() { 
        //Create Test Data
        TestData(True);
        
        //Parameter Passing
        TestPassingParameters('NA');
        
        //Calling redirectToAccount() Method
        TestRedirectToAccount();
        
        ElavonServicesImagingserviceUMock objMock=new ElavonServicesImagingserviceUMock();
        
        ViewStatementController obj=new ViewStatementController();
        Test.StartTest();
        //******************************** Negative Use Cases *******************************
        ViewStatementControllerTest.TestPassingParameters('EU');
        ViewStatementControllerTest.TestSearchStatement(true,false,'MER_BILL','3');
        //Calling Catch Block
        ViewStatementControllerTest.TestSearchStatement(false,false,'MER_BILL','3');
        
        //Calling If Block Of Search Statement
        System.debug('*************************************************************************************************');
        TestPassingParameters('QUERE');
        TestSearchStatement(true,false,'CHAIN_BILL','3');
        //
        Test.StopTest();
    }
    public testmethod static void testCallout5() { 
        //Create Test Data
        TestData(True);
        
        //Parameter Passing
        TestPassingParameters('NA');
        
        //Calling redirectToAccount() Method
        TestRedirectToAccount();
        
        ElavonServicesImagingserviceUMock objMock=new ElavonServicesImagingserviceUMock();
        
        ViewStatementController obj=new ViewStatementController();
        Test.StartTest();
        //******************************** Positive Use Cases *******************************
        TestSearchStatement(true,false,'CHAIN_BAT','3');
        //
        Test.StopTest();
    }
    //Test Method for Statement Type VAT 
    public testmethod static void testCalloutForVatStatementType() { 
        Test.startTest();
        //PermissionSet ps = [SELECT ID From PermissionSet WHERE Name = 'View_Encrypted_Data'];
        //insert new PermissionSetAssignment(AssigneeId = Userinfo.getUserId(), PermissionSetId = ps.Id);
        Test.StopTest();
        
        //Create Test Data
        TestData(False);
       
        //Parameter Passing
        TestPassingParameters('QUERE');
        
        //Calling redirectToAccount() Method
        TestRedirectToAccount();
        
        ElavonServicesImagingserviceUMock objMock=new ElavonServicesImagingserviceUMock();
        
        ViewStatementController obj=new ViewStatementController();
        //Test.StartTest();
        //******************************** Positive Use Cases *******************************
        //Creating an Account Where Processing_Center_ID__c=: stmntType
        List<Account> accountList=[select id,MID__c,Client_Group__c,Tax_Id_Encrypted__c,Processing_Center_ID__c from Account where id=:AccountId];
        for(Account a:accountList){   
            a.MID__c=MID;
            a.Client_Group__c=ClientGroupId;
            a.Tax_Id_Encrypted__c=TaxEncryptedId; //Added for T-603941 - 23May-2017 Aashish Sajwan
            a.Processing_Center_ID__c='QUERE';
        }
        update accountList;
        TestSearchStatement(true,false,'VAT',AccountTaxId);
        TestOpenRecord();
        //
        //Test.StopTest();
    }
    
    //Creating Test Data for Account
    static void TestData(boolean IsRecordSave)
    {
        Account acc=TestUtilities.createTestAccount(IsRecordSave);
        if(!IsRecordSave){
           //update for for T-603941 - 23May-2017 Aashish Sajwan
            acc.Tax_Id_Encrypted__c = '123456';
            insert acc;
        }
        StatementType='NA';
        AccountName=acc.Name;
        AccountId=acc.Id;
        MID=acc.MID__c;
        AccountTaxId = 'Test-123';  
        TaxEncryptedId = acc.Tax_Id_Encrypted__c;//Added for T-603941 - 23May-2017 Aashish Sajwan
        ClientGroupId=acc.Client_Group__c;
    }
    //Calling Open Record Method
    static void TestOpenRecord()
    {
        //Calling Open Record Method
        ViewStatementController obj=new ViewStatementController();
        obj.openRecord();
    }
    //Creating and Passing Parameters
    static void TestPassingParameters(String StatementTypeParam)
    {
        Test.setCurrentPageReference(new PageReference('Page.ViewStatement'));  
        System.currentPageReference().getParameters().put('AccountName', AccountName);       
        System.currentPageReference().getParameters().put('AccountId', AccountId);
        System.currentPageReference().getParameters().put('MID', MID);
        System.currentPageReference().getParameters().put('StatementType', StatementTypeParam);
        System.currentPageReference().getParameters().put('ClientGroupId', ClientGroupId);
        System.currentPageReference().getParameters().put('AccountTaxId', ClientGroupId);
        
        //Controller Object Creation
        ViewStatementController obj=new ViewStatementController();
    }
    //Calling Search Statement Positive Blocks and Testing Results
    static void TestSearchStatement(Boolean NotNull,Boolean updateRecords,string selectedType,string acc_taxId)
    {
        
        
        //Calling searchStatement
        ElavonServicesImagingserviceUMock objMock=new ElavonServicesImagingserviceUMock();
        ViewStatementController obj=new ViewStatementController();
        obj.isPageLoad=false;
        obj.selectedType=selectedType;
        obj.accountTaxId=acc_taxId;
        objMock.setVariable('searchStatement');
        Test.setMock(WebServiceMock.class,objMock);
        obj.searchStatement();
        //System.assertEquals(1,obj.statmentList.size());
        Wrapperlst=new List<UtilityCls.StatmentWrapper>();
        Wrapperlst=obj.statmentList;
        //**** Creating Data & Calling Open Record
        //Else Condition
        obj.setstatmentIdValue('12345678');
        obj.openRecord();
        
        //If Condition
        
        External_Document__c ExtDocNew=TestUtilities.SaveExternalDoc(obj.accountRecord,obj.getstatmentIdValue(),obj.MID,false,true);
        //ExtDocNew.ExternalID__c=obj.getstatmentIdValue();
        //ExtDocNew.MPSMID__c =obj.MID;
        //insert ExtDocNew;
        ContentVersion contVerNew=TestUtilities.SaveContentVersion('.pdf','TestContentVersion',obj.MID,true);
        TestUtilities.SaveFeedItems(obj.MID,ExtDocNew,contVerNew,true);
        obj.openRecord();
        //End Calling Open Record
        //Test Set Get
        for(UtilityCls.StatmentWrapper objWd:obj.statmentList)
        {
            obj.setstatmentIdValue(objWd.ID);
            String check=obj.getstatmentIdValue();
            System.assertEquals(check,objWd.ID);
            System.debug('set get Verified');
        }
        if(!NotNull)
        {
            obj.statmentList.clear();
            objMock.setVariable('NULLsearchStatement');
            Test.setMock(WebServiceMock.class,objMock);
            obj.searchStatement();
        }
    }
    
    //Calling Redirect To Account Method and Testing Result
    static void TestRedirectToAccount()
    {
        ViewStatementController obj=new ViewStatementController();
        obj.redirectToAccount();
        System.assert(obj.AccountId!=null);
        System.debug('Check Account Id: '+obj.AccountId);
    }
    
}