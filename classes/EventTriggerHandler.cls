/*********************************************************************************************************************************
* Appirio, Inc
* Name: EventTriggerHandler
* Description: [T-565229 - HandlerClass - For updating Case Fields from Events]
* Created Date: [01/03/2017]
* Created By: [Gaurav Dudani] (Appirio)
* 
* Date Modified                Modified By                  Description of the update
*  [02/03/2017]              [Aashish Sajwan]             [Update to increase to count on case on send email and code opitimization task # T-575846]
*  [02/08/2017]              [Neeraj Kumawat]             [Update Counter On Event Update # T-576694]
**********************************************************************************************************************************/
public class EventTriggerHandler {

/*****************************************************************
* Method for calling action on Before Insert Update of Event
******************************************************************/

   public static void onBeforeInsertUpdate(List<Event> newEventList){
        updateCaseStatusByEvent(newEventList);
   } 

  //Changed by Aashish Sajwan 4 Feb 2017
  public static void onAfterInsert(List<Event> newEventList){
  	    system.debug('&& filterCases calling on AfterInsert &&'+newEventList);
  		filterCases(newEventList);  
  }
  
  //Changed by Aashish Sajwan 4 Feb 2017
   public static void onAfterDelete(List<Event> oldEventList){
  	    system.debug('&& filterCases calling on onAfterDelete &&'+oldEventList);
  		filterCases(oldEventList);
  }
  /**
    Method for Updating Case Counter on Update of Event. Changed by Neeraj Kumawat 8 Feb 2017
  **/
  
  public static void onAfterUpdate(List<Event> newEventList){
        system.debug('&& filterCases calling on AfterUpdate &&'+newEventList);
        filterCases(newEventList);
  }  
/*****************************************************************
* Method for Filtering Parent Case from Event 
******************************************************************/

    private static void filterCases(List<Event> EventList){
    	//Set of String for CaseId
        Set<String> caseIdSet = new Set<String>();
        //Loop through EventList
        for (Event evt: EventList){
            String whatIdStr = evt.WhatId;
            //check whatId is not blank and start with 500 for case record
            if (!String.IsBlank(whatIdStr) 
                    && whatIdStr.substring(0, 3)=='500'){
                //Add case Id in caseIdset    	
                caseIdSet.add(whatIdStr);
            }
        }
        //Check CaseIdSet size
        if (caseIdSet.size() > 0) {
        	//Calling Utility updateCaseFields method parameter is caseID set
           UtilityCls.updateCaseFields(caseIdSet);
         
        } 
    }
/*****************************************************************
* Method for Updating Case Status from Event
******************************************************************/

    public static void updateCaseStatusByEvent(List<Event> newEventList){
        Set<String> caseIdSet = new Set<String>();
        List<Event> filteredEvents = new List<Event>(); 
        for (Event objEvent: newEventList){
            String whatIdStr=objEvent.WhatId;
            if(whatIdStr!=null && whatIdStr!='' && whatIdStr.substring(0, 3)=='500'){
                if(objEvent.Case_Status__c!=null){
                    caseIdSet.add(objEvent.WhatId);
                    filteredEvents.add(objEvent);
                }
            }
        }       
        if (caseIdSet.size() > 0) {
            Map<Id, Case> caseMap = new Map<Id, Case>([Select Id, Status from Case where Id IN :caseIdSet]);
            
            if (caseMap.size() > 0) {
                for (Event objEvent: filteredEvents) {
                    caseMap.get(objEvent.WhatId).Status = objEvent.Case_Status__c;
                }
            }
           if(caseMap!=null && caseMap.size()>0){
           	try{
            update caseMap.values();
           	}catch(System.DmlException ex){
             	  for (Event objEvent: filteredEvents) {
                    objEvent.addError(ex.getDmlMessage(0));
                   }
             }
           }
            
        }
    }
}