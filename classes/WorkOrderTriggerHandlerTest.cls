/*****************************************************************************************************
* Appirio, Inc
* Test Class Name: WorkOrderTriggerHandlerTest
* Class Name: WorkOrderTriggerHandler
* Description: (#T-602715) Handler test for WorkOrder Trigger bulk testing
* Created Date: [May 17, 2017]
* Created By: [Neeraj Kumawat] (Appirio)
* 
* Date Modified          Modified By              Description of the update
* 18-May-2017            [Neeraj Kumawat]         (#T-602715) Updated case closed comments message.
* 19-May-2017            [Neeraj Kumawat]         (#T-602715) Updated Error message on General Release
workorder status update.
* 24-May-2017            [Gaurav Dudani]          (#T-605132) Added Test methods to check case closes if
all status is either canceled,Closed or completed on
for work orders on a case.
* 30-May-2017            [Poornima Bhardwaj]      Added test method testStampedDates which verify that
StampedDates are updated as per Approval Status Field value 
* 8-June-2017            [Poornima Bhardwaj]      Deleted test Method testStampedDates.
Added two new Methods testStampedDatesWriteOffSummary and
testStampedDatesCollectionTotal
* 12-June-2017           [Neeraj Kumawat]         Added Test Method testUpdateAdjsutmentWrkOrderApprovalStatus
for coverage of WorkOrderTriggerHandler
******************************************************************************************************/
@isTest
public class WorkOrderTriggerHandlerTest {
    static List<Case> caseList;
    static List<Account> accountList;
    static List<WorkOrder> workOrderList;
    static List<workOrderLineItem> workOrderLineItemList;
    static set<Id> workOrderSet;
    static user agentUser;
    static user testUser;
    static user systemAdmin;
    static Integer numberOfRecords = 1;
    static User managerUser;
    static User managerUser1;
    static User managerUser2;
    static User managerUser3;
    static User managerUser4;
    static User managerUser5;
    static User managerUser6;
    static User User1;
    //******************************************************************************************
    // Method to create test data for Case and related workOrders to test WorkOrderTriggerHandler
    // @return void
    //******************************************************************************************
    static void CreateTestData()
    {
        //Custom Agent User Data
        List<UserRole> agentRole=[select id,name from UserRole where name='Service NA Manager'];
        List<Profile> agentProId=[select id,name from Profile where name='Custom: NA Agent'];
        agentUser=TestUtilities.createTestUser(1,agentProId[0].id,false);
        agentUser.UserRole=agentRole[0];
        insert agentUser;
        //Case Creation from Agent User
        System.runAs(agentUser)
        {   
            caseList=TestUtilities.createCase(numberOfRecords,agentUser.id,false);
            //creating Cases
            for(Case c: caseList)
            {
                c.subject='Testing Case for WorkOrderCreation';
                c.Work_Items__c='General Release,Fee Maintenance';
            }   
            insert caseList;
        }
    }
    //******************************************************************************************
    // Method to create test data for Case and related workOrders to test WorkOrderTriggerHandler
    // @return void
    //******************************************************************************************
    static void CreateTestDataSystemAdmin()
    {
        //System Admin User Data
        List<UserRole> systemRole=[select id,name from UserRole where name='Global Ops'];
        List<Profile> systemProId=[select id,name from Profile where name='System Administrator'];
        systemAdmin=TestUtilities.createTestUser(1,systemProId[0].id,false);
        systemAdmin.UserRole=systemRole[0];
        insert systemAdmin;
        //Case Creation from Agent User
        System.runAs(systemAdmin)
        {   
            caseList=TestUtilities.createCase(numberOfRecords,systemAdmin.id,false);
            //creating Cases
            for(Case c: caseList)
            {
                c.subject='Testing Case for WorkOrderCreation';
                c.Work_Items__c='General Release,Fee Maintenance';
            }   
            insert caseList;
        }
    }
    //******************************************************************************************
    // Method to test Restriction of WorkOrder Status if there is a adjustment WorkOrder on Case
    // @return void
    //******************************************************************************************
    static testMethod void testRestrictWorkOrderStatus()
    {
        //Create Test Data
        CreateTestData();
        Test.startTest();
        //Web Service Callout
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator(''));
        //RUnning test as agentUser
        System.runAs(agentUser)
        {
            List<Case> newCaseList=[Select Id, Subject,Status, (Select Id, Subject,RecordTypeId,Status 
                                                                From WorkOrders)
                                    From Case Where Id IN :caseList];
            Map<String, Schema.Recordtypeinfo> workOrderRecordTypes = WorkOrder.sObjectType.getDescribe().
                getRecordtypeinfosByName(); 
            //General Relase Work Order Record Type Id
            String genReleaseWekOrderTypeId=workOrderRecordTypes.get('General Release').getRecordTypeId();
            //Adjustment Work Order Record Type Id  
            String adjustWrkOrderTypeId=workOrderRecordTypes.get('Adjustment').getRecordTypeId();
            //Creating WorkOrder of record type Adjustment
            for(Case cse: newCaseList)
            {
                workOrderList=TestUtilities.createWorkOrder(numberOfRecords,false);
                for(WorkOrder wrk:workOrderList)
                {   
                    wrk.RecordTypeId=adjustWrkOrderTypeId;
                    wrk.Status='New';
                    wrk.CaseId=cse.Id;
                }
            }
            insert workOrderList;
            List<WorkOrder> genReleaseWorkOrderList= new List<WorkOrder>();
            for(Case cse: newCaseList)
            {
                List<WorkOrder> caseWorkOrderList=cse.WorkOrders;
                //Checking work order status
                for(WorkOrder wrk: caseWorkOrderList){
                    if(wrk.RecordTypeId==genReleaseWekOrderTypeId){
                        System.assertEquals(wrk.Subject,'General Release');
                        //Changing Status of General Release record type 
                        wrk.status='Closed';
                        genReleaseWorkOrderList.add(wrk);
                    }
                }
            }
            try
            {
                Database.SaveResult[] resultList=Database.update(genReleaseWorkOrderList,true);
                for (Database.SaveResult result : resultList) 
                {   
                    System.assert(!result.isSuccess());
                    if (!result.isSuccess()) 
                    {   // Operation failed, so get all errors               
                        for(Database.Error err : result.getErrors()) 
                        {
                            //19-May-2017 (#T-602715) Updated Error message. NK
                            System.assertEquals(System.Label.Restrict_Closing_of_Work_Order, err.getMessage());
                            
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                System.debug('Exception='+ex.getMessage());
            }
        }
        Test.stopTest();
    }
    //**********************************************************************************************
    // Method to verify that Case status should be closed on changing all workOrder status as Closed
    // @return void
    //**********************************************************************************************
    // Start
    // Updated method name 
    // By Gaurav Dudani on 24 May 2017 as per task # T-605132.
    static testMethod void testCaseIsClosedWhenWorkOrderStatusIsClosed()
        //End
    {
        //Create Test Data
        CreateTestDataSystemAdmin();
        Test.startTest();
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator(''));
        System.runAs(systemAdmin)
        {
            List<Case> newCaseList=[Select Id, Subject,Status, (Select Id, Subject,RecordTypeId,Status 
                                                                From WorkOrders)
                                    From Case Where Id IN :caseList];
            List<WorkOrder> caseCloseWorkOrderList=new List<WorkOrder>();
            for(Case cse: newCaseList)
            {
                workOrderList=cse.WorkOrders;
                //Creating WorkOrder with status 'closed'
                for(WorkOrder wrk:workOrderList)
                {   
                    wrk.Status='Closed';
                    caseCloseWorkOrderList.add(wrk);
                }
            }
            //updating the WorkOrders with closed status
            update caseCloseWorkOrderList;
            List<Case> closeCaseList=[Select Id, Status, Internal_Comments__c from Case Where Id IN: newCaseList];
            for(Case cseObj: closeCaseList){
                System.assertEquals('Closed',cseObj.status);
                //(#T-602715) Updated case closed comments message.NK
                System.assertEquals(System.Label.Work_Orders+' General Release,Fee Maintenance '+ System.Label.Case_Internal_Comments_On_WorkOrder_Close,cseObj.Internal_Comments__c);
            }
        }
        Test.stopTest();
    }
    //**********************************************************************************************
    // Method to verify that Case status should be closed on changing all workOrder status as Completed
    // @return void
    // Added by Gaurav Dudani On 24 May 2017 for task # T-605132.
    //**********************************************************************************************
    static testMethod void testCaseIsClosedWhenWorkOrderStatusIsCompleted()
    {
        //Create Test Data
        CreateTestDataSystemAdmin();
        Test.startTest();
        //Webservice Callout
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator(''));
        //Running Test As System Admin
        System.runAs(systemAdmin)
        {
            List<Case> newCaseList=[Select Id, Subject,Status, (Select Id, Subject,RecordTypeId,Status 
                                                                From WorkOrders)
                                    From Case Where Id IN :caseList];
            List<WorkOrder> caseCloseWorkOrderList=new List<WorkOrder>();
            for(Case cse: newCaseList)
            {
                workOrderList=cse.WorkOrders;
                //Creating WorkOrder with status 'Completed'
                for(WorkOrder wrk:workOrderList)
                {   
                    wrk.Status='Completed';
                    caseCloseWorkOrderList.add(wrk);
                }
            }
            //updating the WorkOrders with Completed status
            update caseCloseWorkOrderList;
            List<Case> closeCaseList=[Select Id, Status, Internal_Comments__c from Case Where Id IN: newCaseList];
            //Verifying the Comments added on WorkOrder after Closing Case
            for(Case cseObj: closeCaseList){
                System.assertEquals('Closed',cseObj.status);
                System.assertEquals(System.Label.Work_Orders+' General Release,Fee Maintenance '+ System.Label.Case_Internal_Comments_On_WorkOrder_Close,cseObj.Internal_Comments__c);
            }
        }
        Test.stopTest();
    }
    //**********************************************************************************************
    // Method to verify that Case status should be closed on changing all workOrder status as Canceled
    // @return void
    // Added by Gaurav Dudani On 24 May 2017 for task # T-605132.
    //**********************************************************************************************
    
    static testMethod void testCaseIsClosedWhenWorkOrderStatusIsCanceled()
    {
        //Create Test Data
        CreateTestData();
        Test.startTest();
        //Running Test As agent User
        System.runAs(agentUser)
        {
            List<Case> newCaseList=[Select Id, Subject,Status, (Select Id, Subject,RecordTypeId,Status 
                                                                From WorkOrders)
                                    From Case Where Id IN :caseList];
            List<WorkOrder> caseCloseWorkOrderList=new List<WorkOrder>();
            for(Case cse: newCaseList)
            {
                workOrderList=cse.WorkOrders;
                //Creating WorkOrder with status 'Canceled'
                for(WorkOrder wrk:workOrderList)
                {   
                    wrk.Status='Canceled';
                    caseCloseWorkOrderList.add(wrk);
                }
            }
            //updating the WorkOrders with Canceled status
            update caseCloseWorkOrderList;
            List<Case> closeCaseList=[Select Id, Status, Internal_Comments__c from Case Where Id IN: newCaseList];
            for(Case cseObj: closeCaseList){
                System.assertEquals('Closed',cseObj.status);
                System.assertEquals(System.Label.Work_Orders+' General Release,Fee Maintenance '+ System.Label.Case_Internal_Comments_On_WorkOrder_Close,cseObj.Internal_Comments__c);
            }
        }
        Test.stopTest();
    }
    //****************************************************************************************************
    // Method to verify that Case status should not be closed if status of all the workOrder is not Closed
    // @return void
    //****************************************************************************************************
    static testMethod void testCaseIsNotClosed()
    {
        //Create Test Data
        CreateTestData();
        Test.startTest();
        System.runAs(agentUser)
        {
            List<Case> newCaseList=[Select Id, Subject,Status, (Select Id, Subject,RecordTypeId,Status 
                                                                From WorkOrders)
                                    From Case Where Id IN :caseList];
            List<WorkOrder> caseCloseWorkOrderList=new List<WorkOrder>();
            for(Case cse: newCaseList)
            {
                workOrderList=cse.WorkOrders;
                //updating only first record of WorkOrder with status as closed
                // So that case has one workorder with new status
                if(workOrderList.size()>0){
                    workOrderList[0].Status='Closed';
                    caseCloseWorkOrderList.add(workOrderList[0]);
                }
            }
            //updating List of WorkOrders 
            update caseCloseWorkOrderList;
            List<Case> closeCaseList=[Select Id, Status from Case Where Id IN: newCaseList];
            //verifying that the caseStatus is Not Closed
            for(Case cseObj: closeCaseList){
                System.assertNotEquals('Closed',cseObj.status);
            }
        }
        Test.stopTest();
    }   
    
    //****************************************************************************************************
    // Method to verify that StampedDates are updated as per Approval Status Field value
    // @return void
    // Added by Poornima Bhardwaj on 8 June 2017 as per #T-606670
    //****************************************************************************************************
    static testMethod void testStampedDatesCollectionTotal(){
        CreateTestData();
        //Calling Web Service of Collection Total api
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator('CollectionTotal'));
        //creating Test Data for account records
        accountList=TestUtilities.createAccount(1,false);
        for(Account acc:accountList){
            acc.MID__c='8026984123';
        }
        insert accountList;
        //Getting Record Type Id of Adjustment record type
        Id devRecordTypeId = Schema.SObjectType.WorkOrder.getRecordTypeInfosByName().get('Adjustment').getRecordTypeId();
        //creating WorkOrder records with Aprroval status Draft and Status as Canceled
        workOrderList=TestUtilities.createWorkOrder(numberOfRecords,false);
        for(WorkOrder wrk:workOrderList)
        {   
            wrk.AccountId=accountList[0].Id;
            wrk.RecordTypeId=devRecordTypeId;
            wrk.Status='Canceled';
            wrk.Work_Order_Approval_Status__c='Draft';
        }
        Test.startTest();
        //Inserting WorkOrder
        insert workOrderList;
        Test.stopTest();
        //Fetching LastStampedCollectionsBalance Value from workOrder to verify it as per the returned value from WebService  
        List<workOrder> wrkOrder=[Select Last_Stamped_Collections_Balance__c,Last_Stamped_Write_Off_Balance__c from workOrder 
                                  WHERE ID IN:workOrderList];
        System.assertEquals(10,wrkOrder[0].Last_Stamped_Collections_Balance__c);
        
    }
    //****************************************************************************************************
    // Method to verify that StampedDates are updated as per Approval Status Field value
    // @return void
    // Added by Poornima Bhardwaj on 8 June 2017 as per #T-606670
    //****************************************************************************************************
    static testMethod void testStampedDatesWriteOffSummary(){
        //Calling CreateTestData method
        CreateTestData();
        //Calling WebService of WriteOffSummary
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator('WriteoffSummary'));
        Test.startTest();
        //Creating Test Data for Account Records
        accountList=TestUtilities.createAccount(1,false);
        for(Account acc:accountList){
            acc.MID__c='8026984123';
        }
        insert accountList;
        //Getting Record Type Id of Adjustment record type
        Id devRecordTypeId = Schema.SObjectType.WorkOrder.getRecordTypeInfosByName().get('Adjustment').getRecordTypeId();
        //Creating WorkOrder of approval status as Draft Type and Status as Canceled
        workOrderList=TestUtilities.createWorkOrder(numberOfRecords,false);
        for(WorkOrder wrk:workOrderList)
        {   
            wrk.AccountId=accountList[0].Id;
            wrk.RecordTypeId=devRecordTypeId;
            wrk.Status='Canceled';
            wrk.Work_Order_Approval_Status__c='Draft';
        }        
        insert workOrderList;
        Test.stopTest();
        //Fetching Last Stamped Write Off Balance Value from workOrder List
        List<workOrder> wrkOrder=[Select Last_Stamped_Write_Off_Balance__c from workOrder 
                                  WHERE ID IN:workOrderList];
        System.assertEquals(0,wrkOrder[0].Last_Stamped_Write_Off_Balance__c);
    }
    
    //****************************************************************************************************
    // Method to verify that StampedDates are updated as per Approval Status Field value
    // @return void
    // Added by Poornima Bhardwaj on 8 June 2017 as per #T-606670
    //****************************************************************************************************
    static testMethod void testStampedDatesException(){
        //Calling CreateTestData method
        CreateTestData();
        //Calling WebService of WriteOffSummary
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator('WriteoffSummary'));
        Test.startTest();
        //Creating Test Data for Account Records
        accountList=TestUtilities.createAccount(1,false);
        for(Account acc:accountList){
            acc.MID__c='8026984123';
        }
        insert accountList;
        //Getting Record Type Id of Adjustment record type
        Id adjRecordTypeId = Schema.SObjectType.WorkOrder.getRecordTypeInfosByName().get('Adjustment').getRecordTypeId();
        //Creating WorkOrder of approval status as Draft Type and Status as Canceled
        workOrderList=TestUtilities.createWorkOrder(numberOfRecords,false);
        for(WorkOrder wrk:workOrderList)
        {   
            wrk.AccountId=accountList[0].Id;
            wrk.RecordTypeId=adjRecordTypeId; 
            wrk.status='New';
            wrk.Work_Order_Approval_Status__c='Approved - Pending';
            
        }
        insert workOrderList;
        Test.stopTest();
    }    
    
    
}