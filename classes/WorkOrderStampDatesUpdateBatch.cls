/*********************************************************************
* Appirio a WiPro Company
* Name: WorkOrderStampDatesUpdateBatch
* Description: Batch class that runs daily to update WorkOrder.Last_Stamped_Collections_Balance__c and Last_Stamped_Write_Off_Balance__c field with 
*               Web Service Response .
* Created Date: 06/06/2017
* Created By: Aashish Sajwan
* 
* Date Modified                Modified By                  Description of the update
* [8-Jun-2017]                [Aashish Sajwan]                [#T-607904:Webservice callouts to Stamp Write Off and Collections balances for the Merchant on the Work Order record]
**********************************************************************/
global class WorkOrderStampDatesUpdateBatch implements Database.Batchable<Sobject>,Database.AllowsCallouts, Database.Stateful{

  WorkOrderStampDatesUpdateHelper helper = new WorkOrderStampDatesUpdateHelper();

  global void WorkOrderStampDatesUpdateBatch() {}
  
  global Database.Querylocator start(Database.Batchablecontext BC)
  {
    //system.debug(LoggingLevel.INFO, '\n\n WorkOrderStampDatesUpdateBatch.start()\n\n');
    return helper.GetQueryLocator(helper.GetQuery());
  }

  global void execute(Database.Batchablecontext BC, list<SObject> scope)
  {
    //system.debug(LoggingLevel.INFO, '\n\n WorkOrderStampDatesUpdateBatch.execute()\n\n');
    helper.ProcessBatch(scope);
  }

  global void finish (Database.Batchablecontext BC)
  {
    //system.debug(LoggingLevel.INFO, '\n\n WorkOrderStampDatesUpdateBatch.finish()\n\n');
  }
}