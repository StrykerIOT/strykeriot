/***************************************************************************
// (c) 2015 Appirio, Inc.
//
// Description    : Schedulable AutoCloseCase Batch - Auto Close Case
//                : 
//                  
// 21 January 2017 Aashish Sajwan Original (T-554439)
//***************************************************************************/

global class SchedulableAutoCloseCaseBatch implements Schedulable {
    global void execute(SchedulableContext sc) {
      CaseAutoClose autoCloseCaseBatch = new CaseAutoClose();
      Database.executeBatch(autoCloseCaseBatch);     
    }
}