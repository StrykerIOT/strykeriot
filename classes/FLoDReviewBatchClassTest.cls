/****************************************************************************************
* Appirio, Inc
* Test Class Name: FLoDReviewBatchClassTest
* Class Name: [FLoDReviewBatchClass]
* Description: [Create as per Story# S-442186 ]
* Created Date: [06/02/2017]
* Created By: [Gaurav Dudani] (Appirio)
* 
* Date Modified                Modified By                  Description of the update
*****************************************************************************************/
@isTest
public class FLoDReviewBatchClassTest {
  
  public static testmethod void FLoDReviewBatchClassTestMethod(){
    List<sObject> sObjInsList = new List<sObject>(); 
    // Creating Test Data for external link article type.
    External_Link__kav elk = new External_Link__kav(Summary = 'test',title='test',
                                                    UrlName = 'Test-1',Expiration_Date__c = System.today(),
                                                    Expiration_Type__c = 'Static - Fixed',
                                                    Language='en_US',FLoD_Review__c=true);
    sObjInsList.add(elk);                                           
    
    // Creating Test Data for FAQ article type.
    FAQ__kav faq = new FAQ__kav(Summary = 'test',title='test',
                                               UrlName = 'Test-2',Expiration_Date__c = System.today(),
                                               Expiration_Type__c = 'Static - Fixed',
                                               Language='en_US',FLoD_Review__c=true);
    sObjInsList.add(faq);                                           
    
    // Creating Test Data for General article type.
    General__kav gk = new General__kav(Summary = 'test',title='test',
                                       UrlName = 'Test-3',Expiration_Date__c = System.today(),
                                       Expiration_Type__c = 'Static - Fixed',
                                       Language='en_US',FLoD_Review__c=true);
    sObjInsList.add(gk);                                           
    
    // Creating Test Data for Marketing article type.
    Marketing__kav mk = new Marketing__kav(Summary = 'test',title='test',
                                           UrlName = 'Test-4',Expiration_Date__c = System.today(),
                                           Expiration_Type__c = 'Static - Fixed',
                                           Language='en_US',FLoD_Review__c=true);
    sObjInsList.add(mk);                                           
    
    // Creating Test Data for Policy article type.
    Policy__kav pk = new Policy__kav(Summary = 'test',title='test',
                                     UrlName = 'Test-5',Expiration_Date__c = System.today(),
                                     Expiration_Type__c = 'Static - Fixed',
                                     Language='en_US',FLoD_Review__c=true);
    sObjInsList.add(pk);                                           
    
    // Creating Test Data for Procedure article type.
    Procedure__kav proKav = new Procedure__kav(Summary = 'test',title='test',
                                               UrlName = 'Test-6',Expiration_Date__c = System.today(),
                                               Expiration_Type__c = 'Static - Fixed',
                                               Language='en_US',FLoD_Review__c=true);
    sObjInsList.add(proKav);                                           
    
    // Creating Test Data for Training article type.
    Training__kav trainingKav = new Training__kav(Summary = 'test',title='test',
                                                  UrlName = 'Test-7',Expiration_Date__c = System.today(),
                                                  Expiration_Type__c = 'Static - Fixed',
                                                  Language='en_US',FLoD_Review__c=true);
    sObjInsList.add(trainingKav);                                           
    
    // Creating Test Data for Troubleshooting article type.
    Troubleshooting__kav troubleKav = new Troubleshooting__kav(Summary = 'test',title='test',
                                                               UrlName = 'Test-8',Expiration_Date__c = System.today(),
                                                               Expiration_Type__c = 'Static - Fixed',
                                                               Language='en_US',FLoD_Review__c=true);
    sObjInsList.add(troubleKav);                                           
    insert sObjInsList;
    
    
    // Publishing the External Knowledge Article.
    elk = [SELECT KnowledgeArticleId FROM External_Link__kav WHERE Id = :elk.Id];
    //publish it
    KbManagement.PublishingService.publishArticle(elk.KnowledgeArticleId, true);
    
    // Publishing the FAQ Knowledge Article.
    faq = [SELECT KnowledgeArticleId FROM FAQ__kav WHERE Id = :faq.Id];
    //publish it
    KbManagement.PublishingService.publishArticle(faq.KnowledgeArticleId, true);
    
    // Publishing the General Knowledge Article.
    gk = [SELECT KnowledgeArticleId FROM General__kav WHERE Id = :gk.Id];
    //publish it
    KbManagement.PublishingService.publishArticle(gk.KnowledgeArticleId, true);
    
    // Publishing the Marketing Knowledge Article.
    mk = [SELECT KnowledgeArticleId FROM Marketing__kav WHERE Id = :mk.Id];
    //publish it
    KbManagement.PublishingService.publishArticle(mk.KnowledgeArticleId, true);
        
    // Publishing the Policy Knowledge Article    
    pk = [SELECT KnowledgeArticleId FROM Policy__kav WHERE Id = :pk.Id];
    //publish it
    KbManagement.PublishingService.publishArticle(pk.KnowledgeArticleId, true);
    
    // Publishing the Procedure Knowledge Article
    proKav = [SELECT KnowledgeArticleId FROM Procedure__kav WHERE Id = :proKav.Id];
    //publish it
    KbManagement.PublishingService.publishArticle(proKav.KnowledgeArticleId, true);
    
    // Publishing the Training Knowledge Article
    trainingKav = [SELECT KnowledgeArticleId FROM Training__kav WHERE Id = :trainingKav.Id];
    //publish it
    KbManagement.PublishingService.publishArticle(trainingKav.KnowledgeArticleId, true);
    
    // Publishing the Troubleshooting Knowledge Article
    troubleKav = [SELECT KnowledgeArticleId FROM Troubleshooting__kav WHERE Id = :troubleKav.Id];
    //publish it
    KbManagement.PublishingService.publishArticle(troubleKav.KnowledgeArticleId, true);
    system.assertequals(sObjInsList.size(), 8 , 'List size are not equals');
   }
  
   //****************************************************************************
   // Calling the Scheduler class and executing it.
   //****************************************************************************
    static testmethod void testScheduleFLoDReviewBatchClass() {
      FLoDReviewBatchClassTestMethod();
      Test.startTest();  
        ScheduleFLoDReviewBatchClass obj = new ScheduleFLoDReviewBatchClass();
        obj.execute(null);    
      Test.stopTest();
    }
}