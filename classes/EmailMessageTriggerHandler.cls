/***********************************************************************************************
* Appirio, Inc
* Name: EmailMessageTriggerHandler
* Description:[T-558200-HandlerClass-Added preventDeletionEmailMessage Method to prevent deletion of Email Message.]
* Created Date: [01/12/2016]
* Created By: [Neeraj Kumawat] (Appirio)
 
* Date Modified                Modified By                  Description of the update
* [19/05/2017]                [Poornima Bhardwaj]            [T-603553 email message is unread/new and related to closed case then assign to a queue]
* [15 June 2017]              [Bobby Cheek]                  [Enforcing CRUD and FLS Security in the Apex Class] 
*************************************************************************************************/
public class EmailMessageTriggerHandler{
    
     /**
     * Method for calling action on After Deletion of Email Message
     * @param pEmailMessage : List of new Email Message
     * @param pOldEmailMessage : List of old Email Message
     * @return void 
     **/
    public static void onAfterDelete(List<EmailMessage> pEmailMessage, List<EmailMessage> pOldEmailMessage){
        preventDeletionEmailMessage(pOldEmailMessage);
    }
    /**
     * This method is use to prevent deletion of Email Message
     * @param pOldEmailMessage : List of old Email Message
     * @return void
     **/
    public static void preventDeletionEmailMessage( EmailMessage [] pOldEmailMessage) {
        //EmailMessage deletion is only allowed for administrator and user with System permission
        String profileName='';
        if(userinfo.getProfileId()!=null){
         List<Profile> objProfileList=[Select Id,Name from Profile where Id=:userinfo.getProfileId()];
          if(objProfileList.size()>0){
             profileName=objProfileList.get(0).Name;
          }
        }
        Boolean hasSystemPermission=  CommonUtilities.hasCustomPermission('System'); 
        System.debug('Current user has System Permission='+hasSystemPermission);
        for(EmailMessage currentEmailMessage : pOldEmailMessage) {
            //Check if current user is not a system administrator
            System.debug('Email Message Detail='+currentEmailMessage);
            String parentId= currentEmailMessage.ParentId;
            System.debug('Email Message Parent Id='+parentId);
            if(parentId!=null){
                //Checking Email Message parent id is case or not
                System.debug('Start with Email Message Parent Id='+parentId.substring(0, 3));
                if(parentId.substring(0, 3)=='500'){
                    if(profileName !='System Administrator' && !hasSystemPermission){
                        currentEmailMessage.addError('You do not have permission to delete Email Message.');
                    }
                }
            }
        }
    }
    
    
     /**
     * Method for calling action on Before Insert of Email Message
     * @param pEmailMessage : List of new Email Message     
     * @return void 
     **/
     
    public static void onBeforeInsert(List<EmailMessage> pEmailMessage){
        assignCaseToQueue(pEmailMessage);
    }
    
       /* public static void onBeforeUpdate(List<EmailMessage> rEmailMessage){
        EmailMessageTriggerHandler.assignCaseBackToUser(rEmailMessage);
    }*/    
    
    /**
     * This method assign email message which is unread/new and related to closed case to a queue.
     * @param newEmailMessage: List of newEmailMessage
     * @return void
     * Added by Poornima Bhardwaj 
     **/
    
    public static void assignCaseToQueue(List<EmailMessage> newEmailMessage) {
        Set<Id> emailId=new Set<Id>();
        List<Case> caseList=new List<Case>();
        List<Case> caseToBeUpdated=new List<Case>();
       
        for(EmailMessage emailMsg:newEmailMessage){
            {   
                //Checking if Email Message is new/Unread and is related to a case
                if(emailMsg.Status=='0' && emailMsg.RelatedToId!=null){
                    emailId.add(emailMsg.RelatedToId);
                }
            }
        }
        //Querying status and ownerId from Case
        caseList=[Select status,OwnerId from Case where Id IN:emailId and status='Closed'];
        System.debug('>>>>>>>>>>>'+caseList);
        // Removed the query from inside of for loop and added outside for loop 
        // By Gaurav Dudani on 6th June 2017 as per Story # S-477693.
        Group g=[Select Id from Group where name='Closed Case Queue With New/Unread Emails'];
        for(Case cas: caseList){
             // 15 June 2017 Bobby Cheek checking FLS on Case fields
             if(Schema.sObjectType.Case.fields.OwnerId.isUpdateable()){
                cas.ownerId=g.Id;         
                caseToBeUpdated.add(cas);
             }
        }
        //Checking the size of caseToBeUpdated List
        if(caseToBeUpdated.size()>0){
          //Updating Case
          update caseToBeUpdated;
    }     
   }
    /**
     * This method is use to assign email message is Read,Currently assigned to a queue and related to closed case then assign to a Prior Owner.
     * @param newEmailMessage: List of readEmailMessage
     * @return void
     * Added By Gaurav Dudani on 6th June 2017 for Story # S-477693.
     **/
     
    /*public static void assignCaseBackToUser(List<EmailMessage> readEmailMessage) {
        Set<Id> emailId=new Set<Id>();
        List<Case> caseList=new List<Case>();
        List<Case> caseToBeUpdated=new List<Case>();
       
        for(EmailMessage emailMsg:readEmailMessage){
            {
                if(emailMsg.Status=='1' && emailMsg.RelatedToId!=null){
                    emailId.add(emailMsg.RelatedToId);
                }
            }
        }
        caseList=[Select status,OwnerId from Case where Id IN:emailId and status='Closed'];
        System.debug('>>>>>>>>>>>'+caseList);
        for(Case cas: caseList){
         cas.Return_Case_to_Previous_Owner__c= true;
         caseToBeUpdated.add(cas);
        }
        if(caseToBeUpdated.size()>0){
          update caseToBeUpdated;
    }     
   }*/
  }