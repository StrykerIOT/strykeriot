/*********************************************************************
* Appirio, Inc
* Name: ArticleDetailsComponentControllerTest
* Description: [S-470753-Controller for ArticleDetials Component]
* Created Date: [21/04/2017]
* Created By: [Aashish Sajwan] (Appirio)
* 
* Date Modified                Modified By                  Description of the update
[27/04/2017]                   [Aashish Sajwan]             
**********************************************************************/
@isTest
public class ArticleDetailsComponentControllerTest{
    public static Id ArticleId;
    public static List<External_Link__kav> ArticleIdList;
    public testmethod static void Callout1(){
        TestData();
        ArticleId=ArticleIdList[0].id;
        //Check Article
        System.assertEquals(ArticleId,CheckArticleCreated(ArticleId));
        DataCategorySelection(ArticleId);
        //Check Data Category
        System.assertEquals(true,CheckDataCategorySelection(ArticleId));
        ApproveArticle(ArticleId);
    }
    static void TestData(){
        ArticleIdList=new List<External_Link__kav>();
        ArticleIdList=TestUtilities.createExternal_linkArticle(1,true);
        System.debug('Test Data Created'+ArticleIdList);
    } 
    static void ApproveArticle(Id ArtiId){
        //Create an approval request for the change
        Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
        req1.setComments('Submitting request for approval.');
        req1.setObjectId(ArtiId);
        req1.setNextApproverIds(new Id[] {UserInfo.getUserId()});

        //Submit the approval request for the change
        Approval.ProcessResult result = Approval.process(req1);
        //Verify the result
        System.assert(result.isSuccess());

    }
    static Id CheckArticleCreated(Id ArtiId){
        List<External_Link__kav> checkList=[select id from External_Link__kav where id=:ArtiId];
        system.debug('CheckArticleCreated: '+checkList);
        return checkList[0].id;
    }
    static Boolean CheckDataCategorySelection(Id ArtiId){
        Boolean flag=false;
        List<External_Link__DataCategorySelection> checkList=[select id from External_Link__DataCategorySelection where parentid=:ArtiId];
        system.debug('CheckDataCategorySelection: '+checkList);
        if(checkList.size()>0){
        flag=true;
        }else{
        flag=false;
        }
        return flag;
    }
    static void DataCategorySelection(Id ArtiId){
        External_Link__DataCategorySelection el_obj_reg=new External_Link__DataCategorySelection();
        el_obj_reg.DataCategoryGroupName='Region';
        el_obj_reg.DataCategoryName='North_America';
        el_obj_reg.ParentId=ArtiId;
        insert el_obj_reg;
        
        External_Link__DataCategorySelection el_obj_aud=new External_Link__DataCategorySelection();
        el_obj_aud.DataCategoryGroupName='Intended_Audience';
        el_obj_aud.DataCategoryName='Boarding';
        el_obj_aud.ParentId=ArtiId;
        insert el_obj_aud;
        
        system.debug('Data Category Created: '+el_obj_reg+' '+el_obj_aud);
    }
}