/**********************************************************************
* Appirio, Inc
* Test Class Name: additionAddressTriggerHandlerTEST
* Class Name: additionAddressTriggerHandler
* Description: Handler test for additional address; bulk testing
* Created Date: [17/Feb/2017]
* Created By: [Shannon Howe] (Appirio)
* 
* Date Modified                Modified By              Description of the update
* Feb 19 2017           Shannon Howe        Created
* Feb 21 2017                  Neeraj Kumawat           Updated test Class to increase code coverage
* Apr 24 2017                  Aashish Sajwan           Updated test Class resolve test class failure
***********************************************************************/

@isTest
public class AdditionalAddressTriggerHandlerTEST {
    //Buld Record creation limit
    //Descrease bulk record count to resolve test class failure in QA3 and UAT : Aashish Sajwan 24Apr17
    static Integer bulkRecordCount=100;
    /***************************************************************************************
// Test method to create test data for creating Account and Addiotional address
****************************************************************************************/
    static List<Additional_Address__c>  createTestData(String accountName, String accountType, Boolean isNull){
        List<Account> accList = new List<Account>();
        //Creating accounts
        accList= TestUtilities.createAccount(bulkRecordCount, false);
        //Setting Account Name , Billing and Shipping address value
        for(integer i=0; i<bulkRecordCount; i++){
            accList[i].Name = accountName + i;
            accList[i].CurrencyIsoCode = 'MXN';
            accList[i].Processing_Center_Id__c = 'QUERE';
            accList[i].BillingCity = 'Fun';
            accList[i].BillingCountry = 'Country Fun';
            accList[i].BillingState = 'Some state';
            accList[i].BillingStreet = 'Some street test';
            accList[i].BillingPostalCode = 'Billing Postal code';
            accList[i].ShippingCity = 'Shipping Fun';
            accList[i].ShippingState = 'Shipping Some state';
            accList[i].ShippingCountry = 'Shipping Country Fun';
            accList[i].ShippingStreet = 'Shipping Some street test';
            accList[i].ShippingPostalCode = 'Shipping Postal code';
        }
        
        System.debug('Account List size=' + accList.size());
        System.debug('ACC List at 1 - ' + accList[1]);
        insert accList;
        //Creating Additional Address on Account
        List<Additional_Address__c> addList = new List<Additional_Address__c>();
        for(integer i=0; i<bulkRecordCount; i++){
            //Created single Additional address on each account
            List<Additional_Address__c> additonalList = TestUtilities.createAdditionalAddress(accList[i].id,accountType,1,false);
            if(additonalList.size()>0){
                Additional_Address__c addAddress=additonalList.get(0);
                //Setting additional address field to null when isNull true
                if(isNull){
                    addAddress.City__c = null;
                    addAddress.Country__c = null;
                    addAddress.County_State__c = null;
                    addAddress.Street_1__c = null;
                    addAddress.Street_2__c = null;
                    addAddress.Street_3__c = null;
                    addAddress.Postal_Code__c =null;
                }
                addList.add(addAddress);
            }
        }
        insert addList;
        return addList;
    }
    
    /***********************************************************************************************************
// Test method to validate billing address on accounts When Addtional address is created with type Account
************************************************************************************************************/
    static testMethod void insertAccountTypeAdditionalAddressesTestMethod(){
        List<Additional_Address__c> addList= createTestData('TestingAccountTypeAdditionalAddress', 'Account', false);
        Test.startTest();
        List<Account> accList = [SELECT Id, CreatedDate, Name, BillingCity, BillingPostalCode, BillingStreet, 
                                 BillingState, BillingCountry, ShippingCity, ShippingPostalCode, ShippingStreet,
                                 ShippingState, ShippingCountry
                                 FROM Account WHERE Name LIKE 'TestingAccountTypeAdditionalAddress%' ORDER BY CreatedDate Asc];
        System.debug('Account List value when Account type addtional Address created= '+ accList);
        System.debug('Additional Address  List value when Account type addtional Address created= '+ addList);
        //Checking account Billing address values with Account Type additional Address
        for(Integer ia=0; ia<bulkRecordCount; ia++){
            System.assertEquals(accList[ia].billingcity, addList[ia].city__c);
            System.assertEquals(accList[ia].billingstate, addList[ia].County_State__c);
            System.assertEquals(accList[ia].billingcountry, addList[ia].Country__c);
            System.assertEquals(accList[ia].billingstreet, addList[ia].Street_1__c + ' '+ addList[ia].Street_2__c + ' '+ addList[ia].Street_3__c);
            System.assertEquals(accList[ia].billingpostalcode, addList[ia].Postal_Code__c);
        }
        Test.stopTest();
    }
    /***********************************************************************************************************
// Test method to validate shipping address on accounts When Addtional address is created with type Legal
************************************************************************************************************/      
    static testMethod void insertLegalTypeAdditionalAddressesTestMethod(){
        List<Additional_Address__c> addList= createTestData('TestingLegalTypeAdditionalAddress', 'Legal', false);
        Test.startTest();
        List<Account> accList = [SELECT Id, CreatedDate, Name, BillingCity, BillingPostalCode, BillingStreet, 
                                 BillingState, BillingCountry, ShippingCity, ShippingPostalCode, ShippingStreet,
                                 ShippingState, ShippingCountry
                                 FROM Account WHERE Name LIKE 'TestingLegalTypeAdditionalAddress%' ORDER BY CreatedDate Asc];
        System.debug('Account List value when Legal type addtional Address created= '+ accList);
        System.debug('Additional Address List value when Legal type addtional Address created= '+ addList);
        //Checking account Shipping address values with Account Type additional Address
        for(Integer ia=0; ia<bulkRecordCount; ia++){
            System.assertEquals(accList[ia].ShippingCity, addList[ia].city__c);
            System.assertEquals(accList[ia].ShippingState, addList[ia].County_State__c);
            System.assertEquals(accList[ia].ShippingCountry, addList[ia].Country__c);
            System.assertEquals(accList[ia].ShippingStreet, addList[ia].Street_1__c + ' '+ addList[ia].Street_2__c + ' '+ addList[ia].Street_3__c);
            System.assertEquals(accList[ia].ShippingPostalCode, addList[ia].Postal_Code__c);
        }
        Test.stopTest();
    }
    /********************************************************************************************************
// Test method to validate billing address on accounts when Addtional address is created with null values
*********************************************************************************************************/          
    static testMethod void insertAccountTypeAdditionalAddressesWithNullValueTestMethod(){  
        List<Additional_Address__c> addList= createTestData('TestingAccountTypeAdditionalAddressWithNullValue', 'Account', true);
        Test.startTest();
        List<Account> accList = [SELECT Id, CreatedDate, Name, BillingCity, BillingPostalCode, BillingStreet, 
                                 BillingState, BillingCountry, ShippingCity, ShippingPostalCode, ShippingStreet,
                                 ShippingState, ShippingCountry
                                 FROM Account WHERE Name LIKE 'TestingAccountTypeAdditionalAddressWithNullValue%' ORDER BY CreatedDate Asc];
        //Checking account Billing address values with Account Type additional Address
        for(Integer ia=0; ia<bulkRecordCount; ia++){
            System.assertEquals(accList[ia].billingcity, addList[ia].city__c);
            System.assertEquals(accList[ia].billingstate, addList[ia].County_State__c);
            System.assertEquals(accList[ia].billingcountry, addList[ia].Country__c);
            System.assertEquals(accList[ia].billingstreet, addList[ia].Street_1__c);
            System.assertEquals(accList[ia].billingpostalcode, addList[ia].Postal_Code__c);
        }
        Test.stopTest();
    }
    /**********************************************************************************************************
// Test method to validate shipping address on accounts when Addtional address is created with null values
***********************************************************************************************************/           
    static testMethod void insertLegalTypeAdditionalAddressesWithNullValueTestMethod(){
        List<Additional_Address__c> addList= createTestData('TestingLegalTypeAdditionalAddressWithNullValue', 'Legal', true);
        Test.startTest();
        List<Account> accList = [SELECT Id, CreatedDate, Name, BillingCity, BillingPostalCode, BillingStreet, 
                                 BillingState, BillingCountry, ShippingCity, ShippingPostalCode, ShippingStreet,
                                 ShippingState, ShippingCountry
                                 FROM Account WHERE Name LIKE 'TestingLegalTypeAdditionalAddressWithNullValue%' ORDER BY CreatedDate Asc];
        //Checking account Shipping address values with Account Type additional Address
        for(Integer ia=0; ia<bulkRecordCount; ia++){
            System.assertEquals(accList[ia].ShippingCity, addList[ia].city__c);
            System.assertEquals(accList[ia].ShippingState, addList[ia].County_State__c);
            System.assertEquals(accList[ia].ShippingCountry, addList[ia].Country__c);
            System.assertEquals(accList[ia].ShippingStreet, addList[ia].Street_1__c);
            System.assertEquals(accList[ia].ShippingPostalCode, addList[ia].Postal_Code__c);
        }
        Test.stopTest();
    }
    /********************************************************************************************************************************
// Test method to validate shipping and billing address when addional address type is not Account or Legal
*********************************************************************************************************************************/        
    static testMethod void insertCorporateTypeAdditionalAddressesTestMethod(){
        List<Additional_Address__c> addList= createTestData('TestingCorporateTypeAdditionalAddress', 'Corporate', false);
        Test.startTest();
        List<Account> accList = [SELECT Id, CreatedDate, Name, BillingCity, BillingPostalCode, BillingStreet, 
                                 BillingState, BillingCountry, ShippingCity, ShippingPostalCode, ShippingStreet,
                                 ShippingState, ShippingCountry
                                 FROM Account WHERE Name LIKE 'TestingCorporateTypeAdditionalAddress%' ORDER BY CreatedDate Asc];
        //Checking account Billing address values with Account Address
        for(Integer ia=0; ia<bulkRecordCount; ia++){
            System.assertEquals(accList[ia].billingcity, 'Fun');
            System.assertEquals(accList[ia].billingstate, 'Some state');
            System.assertEquals(accList[ia].billingcountry,'Country Fun');
            System.assertEquals(accList[ia].billingstreet, 'Some street test');
            System.assertEquals(accList[ia].billingpostalcode, 'Billing Postal code');
        }
        Test.stopTest();
    }
    
    /*****************************************************************************************
// Test method to validate billing address on accounts when additional address are updated 
******************************************************************************************/
    static testMethod void updateAccountTypeAdditionalAddressesTestMethod(){
        List<Additional_Address__c> addList= createTestData('TestingAccountTypeUpdateAdditionalAddress', 'Account', false);
        Test.startTest();
        for(Integer i=0; i<addList.size(); i++){
            addList[i].City__c = 'Updated City ' + i;
            addList[i].Country__c = 'Updated Country ' + i;
            addList[i].County_State__c = 'Updated State ' + i;
            addList[i].Street_1__c = 'Updated Street 1 ' + i;
            addList[i].Street_2__c = 'Updated Street 2 ' + i;
            addList[i].Street_3__c = 'Updated Street 3 ' + i;
            addList[i].Postal_Code__c = 'Updated Code ' + i;
            
        }
        update addList;
        Map<Id,Account> accMap = new Map<Id,Account>([SELECT Id, CreatedDate, Name, BillingCity, BillingPostalCode, BillingStreet, 
                                                      BillingState, BillingCountry, ShippingCity, ShippingPostalCode, ShippingStreet,
                                                      ShippingState, ShippingCountry
                                                      FROM Account WHERE Name LIKE 'TestingAccountTypeUpdateAdditionalAddress%' ORDER BY LastModifiedDate Asc]);
        //Checking account Billing address values with Account Type additional Address
        for(Integer ia=0; ia<bulkRecordCount; ia++){
            System.assertEquals(accMap.get(addList[ia].Account__c).billingcity, addList[ia].city__c);
            System.assertEquals(accMap.get(addList[ia].Account__c).billingstate, addList[ia].County_State__c);
            System.assertEquals(accMap.get(addList[ia].Account__c).billingcountry, addList[ia].Country__c);
            System.assertEquals(accMap.get(addList[ia].Account__c).billingstreet, addList[ia].Street_1__c + ' '+ addList[ia].Street_2__c + ' '+ addList[ia].Street_3__c);
            System.assertEquals(accMap.get(addList[ia].Account__c).billingpostalcode, addList[ia].Postal_Code__c);
        }
        Test.stopTest();
    }
    /*******************************************************************************************
// Test method to validate Shipping address on accounts when additional address are updated 
********************************************************************************************/ 
    static testMethod void updateLegalTypeAdditionalAddressesTestMethod(){
        List<Additional_Address__c> addList= createTestData('TestingLegalTypeUpdateAdditionalAddress', 'Legal', false);
        Test.startTest();
        for(Integer i=0; i<addList.size(); i++){
            addList[i].City__c = 'Updated City ' + i;
            addList[i].Country__c = 'Updated Country ' + i;
            addList[i].County_State__c = 'Updated State ' + i;
            addList[i].Street_1__c = 'Updated Street 1 ' + i;
            addList[i].Street_2__c = 'Updated Street 2 ' + i;
            addList[i].Street_3__c = 'Updated Street 3 ' + i;
            addList[i].Postal_Code__c = 'Updated Code ' + i;
            
        }
        update addList;
        Map<Id,Account> accMap = new Map<Id,Account>([SELECT Id, CreatedDate, Name, BillingCity, BillingPostalCode, BillingStreet, 
                                                      BillingState, BillingCountry, ShippingCity, ShippingPostalCode, ShippingStreet,
                                                      ShippingState, ShippingCountry
                                                      FROM Account WHERE Name LIKE 'TestingLegalTypeUpdateAdditionalAddress%' ORDER BY LastModifiedDate Asc]);
        System.debug('Account List value when Legal type addtional Address updated= '+ accMap);
        System.debug('Additional Address  List value when Legal type addtional Address updated= '+ addList);
        //Checking account Shipping address values with Account Type additional Address
        for(Integer ia=0; ia<bulkRecordCount; ia++){
            System.assertEquals(accMap.get(addList[ia].Account__c).ShippingCity, addList[ia].city__c);
            System.assertEquals(accMap.get(addList[ia].Account__c).ShippingState, addList[ia].County_State__c);
            System.assertEquals(accMap.get(addList[ia].Account__c).ShippingCountry, addList[ia].Country__c);
            System.assertEquals(accMap.get(addList[ia].Account__c).ShippingStreet, addList[ia].Street_1__c + ' '+ addList[ia].Street_2__c + ' '+ addList[ia].Street_3__c);
            System.assertEquals(accMap.get(addList[ia].Account__c).ShippingPostalCode, addList[ia].Postal_Code__c);
        }
        Test.stopTest();
    }
    /***********************************************************************************************************
// Test method to validate billing address on accounts When multiple  Addtional address is created with type Account
************************************************************************************************************/
    static testMethod void insertMultipleAccountTypeAdditionalAddressesTestMethod(){
        List<Account> accountList = new List<Account>();
        //Creating account
        accountList= TestUtilities.createAccount(1, false);
        if(accountList.size()>0){
            accountList[0].Name='TestingAccountTypeMultipleAdditionalAddress';
            insert accountList;
            List<Additional_Address__c> additonalList = TestUtilities.createAdditionalAddress(accountList[0].id,'Account',bulkRecordCount,true);
        }
        Test.startTest();
        //Fetching account List from DB
        List<Account> accList = [SELECT Id, CreatedDate, Name, BillingCity, BillingPostalCode, BillingStreet, 
                                 BillingState, BillingCountry, ShippingCity, ShippingPostalCode, ShippingStreet,
                                 ShippingState, ShippingCountry
                                 FROM Account WHERE Name LIKE 'TestingAccountTypeMultipleAdditionalAddress%' ORDER BY CreatedDate Asc];
        Account acc = new Account();
        if(accList.size()>0){
            acc= accList.get(0);
        }
        //Fetch Additional Address records
        List<Additional_Address__c> addList = [SELECT Id, CreatedDate, city__c, County_State__c, Country__c, Street_1__c, 
                                               Street_2__c, Street_3__c, Postal_Code__c
                                               FROM Additional_Address__c WHERE Account__c =: acc.Id ORDER BY CreatedDate Asc];
        Additional_Address__c addAddress=addList.get(addList.size()-1);
        System.debug('Account value when Account type multiple addtional Address created= '+ acc);
        System.debug('Size of additional list='+addList.size());
        System.debug('Additional Address  value when Account type multiple addtional Address created= '+ addAddress);
        //Checking account Billing address value with Account Type additional Address
        
        System.assertEquals(acc.billingcity, addAddress.city__c);
        System.assertEquals(acc.billingstate, addAddress.County_State__c);
        System.assertEquals(acc.billingcountry, addAddress.Country__c);
        System.assertEquals(acc.billingstreet, addAddress.Street_1__c + ' '+ addAddress.Street_2__c + ' '+ addAddress.Street_3__c);
        System.assertEquals(acc.billingpostalcode, addAddress.Postal_Code__c);
        
        Test.stopTest();
    }
    /*****************************************************************************************************************
// Test method to validate Shipping address on accounts When multiple  Addtional address is created with type Legal
*******************************************************************************************************************/
    static testMethod void insertMultipleLegalTypeAdditionalAddressesTestMethod(){
        List<Account> accountList = new List<Account>();
        //Creating accounts
        accountList= TestUtilities.createAccount(1, false);
        if(accountList.size()>0){
            accountList[0].Name='TestingLegalTypeMultipleAdditionalAddress';
            insert accountList;
            List<Additional_Address__c> additonalList = TestUtilities.createAdditionalAddress(accountList[0].id,'Legal',bulkRecordCount,true);
        }
        Test.startTest();
        List<Account> accList = [SELECT Id, CreatedDate, Name, BillingCity, BillingPostalCode, BillingStreet, 
                                 BillingState, BillingCountry, ShippingCity, ShippingPostalCode, ShippingStreet,
                                 ShippingState, ShippingCountry
                                 
                                 FROM Account WHERE Name LIKE 'TestingLegalTypeMultipleAdditionalAddress%' ORDER BY CreatedDate Asc];
        Account acc = new Account();
        if(accList.size()>0){
            acc= accList.get(0);
        }
        List<Additional_Address__c> addList = [SELECT Id, CreatedDate, city__c, County_State__c, Country__c, Street_1__c, 
                                               Street_2__c, Street_3__c, Postal_Code__c
                                               FROM Additional_Address__c WHERE Account__c =: acc.Id ORDER BY CreatedDate Asc];
        Additional_Address__c addAddress=addList.get(addList.size()-1);
        System.debug('Account value when Legal type multiple addtional Address created= '+ acc);
        System.debug('Size of additional list='+addList.size());
        System.debug('Additional Address  value when Legal type multiple addtional Address created= '+ addAddress);
        //Checking account Shipping address values with Account Type additional Address
        
        System.assertEquals(acc.ShippingCity, addAddress.city__c);
        System.assertEquals(acc.ShippingState, addAddress.County_State__c);
        System.assertEquals(acc.ShippingCountry, addAddress.Country__c);
        System.assertEquals(acc.ShippingStreet, addAddress.Street_1__c + ' '+ addAddress.Street_2__c + ' '+ addAddress.Street_3__c);
        System.assertEquals(acc.ShippingPostalCode, addAddress.Postal_Code__c);
        
        Test.stopTest();
    }
}