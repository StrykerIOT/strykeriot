/*********************************************************************
* Appirio a WiPro Company
* Name: CaseEmailCFArticlesController
* Description: [T-610923 - ControllerClass- For showing the email page and all the articles which are marked as customer facing ]
* Created Date: [06/22/2017]
* Created By: [Poornima Bhardwaj] (Appirio)
* 
* Date Modified                Modified By                  Description of the update

**********************************************************************/

public with sharing class CaseEmailCFArticlesController {
    
    public Case caseWithArticle; 
    public List<articleWrapper> articleList {get; set;}
    public List<articleWrapper> articleListTest {get; set;}
    public List<sObject> selectedArticles {get;set;}
    
    /* public CaseEmailCFArticlesController(ApexPages.StandardController controller){
this.caseWithArticle = (Case)controller.getRecord(); 

//Case caseWRelatedArticles = [SELECT Id, (Select id, KnowledgeArticleId, KnowledgeArticleVersionId from CaseArticles) from Case Where Id ='500V0000005xUlsIAE'];

set<string> knowledgeArticleVersionId = new set<string>();
//for(CaseArticle caseArt : caseWRelatedArticles.CaseArticles){
//  knowledgeArticleVersionId.add(caseArt.KnowledgeArticleVersionId);
//}

//SELECT Id,KnowledgeArticleId,KnowledgeArticleVersionId FROM CaseArticle WHERE KnowledgeArticleId in :kknowledgeArticleIdSet

//system.debug(LoggingLevel.DEBUG, '\n\nCASE :: ' + caseWithArticle + '\n\n');
}
*/
    public String addlRecipients {get; set;}
    public Case   ourCase {get; set;}
    public EmailMessage emailMsg {get; private set;}
    private OrgWideEmailAddress sender = null;
    public CaseEmailCFArticlesController(ApexPages.StandardController controller) {
        ourCase = (Case)controller.getRecord();
        emailMsg = new EmailMessage();
        articleList  = new List<ArticleWrapper>();
        articleListTest  = new List<ArticleWrapper>();
        
    }
    
    public PageReference send() {
        
        return null;
    }
    
    public PageReference cancel() {
        // no need to do anything - just return to calling page.
        PageReference pgRef = new PageReference('/' + ourCase.Id);
        pgRef.setRedirect(true);
        return pgRef;
    }
    
    public List<articleWrapper> articleValues(String objName,Set<Id> knowledgeArticleIdSet) {
        List<sObject> sObjectQuery;
        articleList = new List<articleWrapper>();
        system.debug('knowledgeArticleIdSet ='+knowledgeArticleIdSet );
        String query='select Title,id,KnowledgeArticleId,Language,Publishstatus from '+objName+''+
            ' where customer_Facing__c = True ' +
            ' and Language=\'en_US\' and publishstatus=\'online\' and KnowledgeArticleId In:knowledgeArticleIdSet';
        
        system.debug('****Query Values***'+query);
        
        sObjectQuery=database.Query(query);
        for(sObject objArticle:sObjectQuery) {
            ArticleWrapper articleObj=new articleWrapper();
            system.debug('objArticle values **'+objArticle);
            articleObj.Title=(String)objArticle.get('Title');
            articleObj.Id=(Id)objArticle.get('Id');
            articleList.add(articleObj);
            system.debug('articleList values **'+articleList);
        }
        
        return articleList;
    }
    public List<articleWrapper> getarticleVal(){
        System.debug('Inside getArticleVal');
        List<String> articleName=new List<String>{'general__kav','External_Link__kav','FAQ__kav','Marketing__kav','Policy__kav','Procedure__kav','Training__kav','Troubleshooting__kav'};
            System.debug('articleName List'+articleName);
          List<Case> caseList=[Select Id, (Select Id, CaseId, KnowledgeArticleId From CaseArticles) From Case c where Id=:ourCase.Id];
          Set<Id> knowledgeArticleIdSet=new Set<Id>();
          if(caseList.size()>0){
           List<CaseArticle> caseArticleList=caseList.get(0).CaseArticles;
           for(CaseArticle caseArticleObj : caseArticleList){
            knowledgeArticleIdSet.add(caseArticleObj.KnowledgeArticleId) ;
          }
          }
        for(integer i=0; i < articleName.size()-1;i++){
            System.debug('before method call');
            List<articleWrapper> articleListReturnVal=new List<articleWrapper>();
            articleListReturnVal= (articleValues(articleName[i],knowledgeArticleIdSet));
            articleListTest.addAll(articleListReturnVal);
            System.debug('After method call');
        }
        
        return articleListTest;
        
    }
    
    public class articleWrapper {
        
        public String Title{get;set;}
        public Id Id{get;set;}
        public Boolean selected {get; set;}
     
    }
    
}