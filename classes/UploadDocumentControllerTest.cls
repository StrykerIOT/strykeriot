/*********************************************************************
* Appirio, Inc
* Name: UploadDocumentControllerTest
* Description: [Task# T-557407]
* Created Date: [12/07/2016]
* Created By: [Aashish Sajwan] (Appirio)
* 
* Date Modified                Modified By              Description of the update
* [Wed 07, 2016]               [Aashish Sajwan]         [T:557407 ]
**********************************************************************
//****************************************************************************
// Test Class to test the UploadDocumentController.cls
//****************************************************************************/
@istest
public class UploadDocumentControllerTest {
static String AccountId;
static String MID;
static String AccountName;
static Document doc,docGet;
public testmethod static void testCallout1() {  
        //for Creating Account Test Data                                               
        UploadDocumentControllerTEST.TestData();
        
        //Parameter Passing onto the Page
        Test.setCurrentPageReference(new PageReference('Page.UploadDocument'));         
        System.currentPageReference().getParameters().put('AccountId', AccountId);
        System.currentPageReference().getParameters().put('MID', MID);
        System.currentPageReference().getParameters().put('AccountName', AccountName);
        
        UploadDocumentController obj=new UploadDocumentController();
        
        ElavonServicesImagingserviceUMock objMock=new ElavonServicesImagingserviceUMock();
        
        //Assigning Document
        UploadDocumentControllerTEST.TestDocCreation();
        obj.document=doc;
        docGet= obj.document;
        
        //Calling redirectToAccount
        UploadDocumentControllerTEST.TestRedirectToAccount();
        
        //Calling uploadDocuments
        Test.StartTest();
        //**********************Positive Use Case****************************
        //IF Block of uploadDocuments
        objMock.setVariable('uploadDocuments');
        Test.setMock(WebServiceMock.class,objMock);
        obj.uploadDocuments();

        Test.StopTest();
    }
    public testmethod static void testCallout2() {  
        //for Creating Account Test Data                                               
        UploadDocumentControllerTEST.TestData();
        
        //Parameter Passing onto the Page
        Test.setCurrentPageReference(new PageReference('Page.UploadDocument'));         
        System.currentPageReference().getParameters().put('AccountId', AccountId);
        System.currentPageReference().getParameters().put('MID', MID);
        System.currentPageReference().getParameters().put('AccountName', AccountName);
        
        UploadDocumentController obj=new UploadDocumentController();
        
        ElavonServicesImagingserviceUMock objMock=new ElavonServicesImagingserviceUMock();
        
        //Assigning Document
        UploadDocumentControllerTEST.TestDocCreation();
        obj.document=doc;
        docGet= obj.document;
        
        //Calling redirectToAccount
        UploadDocumentControllerTEST.TestRedirectToAccount();
        
        //Calling uploadDocuments
        Test.StartTest();
        //**********************Negative Use Case****************************
        //Catch Exception
        objMock.setVariable('NULLuploadDocuments');
        Test.setMock(WebServiceMock.class,objMock);
        obj.uploadDocuments();
        objMock.setVariable('uploadDocuments');
        Test.setMock(WebServiceMock.class,objMock);
        obj.uploadDocuments();
        Test.StopTest();
    }
    //Creating Account Test data
    static void TestData()
    {
        Account Acc=TestUtilities.createTestAccount(true);                                   
        AccountId=Acc.Id;                                                      
        MID=Acc.MID__c;
        AccountName=Acc.Name;
    }
    //Testing Redirection To account
    static void TestRedirectToAccount()
    {
        UploadDocumentController obj=new UploadDocumentController();
        obj.redirectToAccount();
    }
    //Creating Test Document Data
    static void TestDocCreation()
    {
        doc=new Document();
        doc.body=Blob.valueOf('20061');
        doc.Name='demoform1.pdf';
    }
}