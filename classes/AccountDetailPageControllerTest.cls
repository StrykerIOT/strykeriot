/*********************************************************************************************************************
* Author        :  Appirio (Poornima Bhardwaj)
* Date          :  May 4th, 2017
* Purpose       :  Test class for AccountDetailPageController
* Updated Date  : 
* Purpose       : Test class coverage of AccountDetailPageController
*
* Date Modified                Modified By                  Description of the update
*
**********************************************************************************************************************/
@isTest
//****************************************************************************
// Test Class to test the AccountDetailPageController
//****************************************************************************
public class AccountDetailPageControllerTest {
    public static List<Account> acntList=new List<Account>();
    public static List<Account> accList=new List<Account>();
    public static List<Opportunity> opportunityList=new List<Opportunity>();
    public static List<Opportunity> opportunityLi=new List<Opportunity>();
    public static Account accRec;
    
    @isTest
    
    //***************************************************************************************************
    // Test Method for calling default construtor of class
    //*****************************************************************************************************
    public static void TestData(){
        createTestData();
        ApexPages.StandardController sc = new ApexPages.StandardController(accRec);
        AccountDetailPageController controller = new AccountDetailPageController(sc);
        System.assertEquals(controller.PartnerNameList[0].SpecialHandlingInstructions__c,accList[0].SpecialHandlingInstructions__c);
        System.assertEquals(controller.RelationshipNameList[0].SpecialHandlingInstructions__c,acntList[0].SpecialHandlingInstructions__c);
    }
    public static void createTestData(){
        //Getting RecordType id of 'Partner' RecordType of Account Object
        Id AccountPartnerRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Partner').getRecordTypeId();
        Id AccountRelationshipRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Relationship').getRecordTypeId();
        List<Account> accountList=TestUtilities.CreateAccount(1,False);
        for(Account acc:accountList){
            acc.SpecialHandlingInstructions__c='TestData';
            acc.Name='TestName';
            acc.RecordTypeId=AccountPartnerRecordTypeId;
            accList.add(acc);
        }
        insert accList;
        
        
        //****************************************************************************
        // List of records created using createAccount method from TestUtilities class
        //@param Integer count
        //@param Boolean isInsert
        // @return aaccountList: List of Accounts
        //****************************************************************************
        List<Account> accntList=TestUtilities.createAccount(1,false);
        for(Account acc:accntList){
            acc.SpecialHandlingInstructions__c='TestData';
            acc.RecordTypeId=AccountRelationshipRecordTypeId;
            acntList.add(acc);
        }
        insert acntList;
        
        //****************************************************************************
        // List of records created using createOpportunity method from TestUtilities class
        //@param Integer count
        //@param Boolean isInsert
        // @return opportunityList: List of Opportunity
        //****************************************************************************
        opportunityList=TestUtilities.createOpportunity(1,False);
        for(Opportunity opp:opportunityList){
            opp.AccountId=accList[0].Id;   
            opportunityLi.add(opp);  
        }
        insert opportunityLi;
        
        accRec=TestUtilities.createTestAccount(false);
        accRec.Relationship__c=acntList[0].ID;
        insert accRec;
        
        //****************************************************************************
        // List of records created using createPartner method from TestUtilities class
        //@param Boolean isInsert
        //@param Id AccountToIdValue
        //@param Id AccountFromIdValue
        // @return void
        //****************************************************************************
        TestUtilities.createPartner(True,accList[0].Id,accRec.ID);
        
    }
    
}