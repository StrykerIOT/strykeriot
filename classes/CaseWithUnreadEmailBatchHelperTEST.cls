/****************************************************************************************
* Appirio, Inc
* Test Class Name: CaseWithUnreadEmailBatchHelperTEST
* Class Name: [CaseWithUnreadEmailBatch]
* Description: [Create as per task# T-608727 ]
* Created Date: [06/09/2017]
* Created By: [Gaurav Dudani] (Appirio)
* 
* Date Modified                Modified By                  Description of the update
*****************************************************************************************/
/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class CaseWithUnreadEmailBatchHelperTEST {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        
        // Creating Account Record
        List<Account> accountList=TestUtilities.createAccount(1,true);
        
        // Quering to get the queue name
        List<Group> grpList = [SELECT Name FROM Group WHERE Name = 'Closed Case Queue With New/Unread Emails' AND Type = 'Queue' LIMIT 1];
        
        // Creating case records where case status is closed and case owner is 'Closed Case Queue With New/Unread Emails' queue.
        List<Case> caseList = TestUtilities.createCase(1,UserInfo.getUserId(),false);
        for(Case objCase:caseList){
            objCase.Status= 'Çlosed';
            objCase.AccountId=accountList.get(0).Id;
            objCase.Future_Action_Date__c=System.today()+2;
            objCase.Prior_Owner__c=UserInfo.getUserId();
            if(grpList.size() >0 ){
              objCase.ownerId = grpList[0].id;  
            }
        }
        insert caseList;
        System.debug('Closed Queue caseList is '+caseList);
        
        // Creating New email message on Case with Status read.
        List<EmailMessage> emailMessageList=TestUtilities.createEmailMessage(1,caseList[0].id,false);
        emailMessageList[0].Status = '1';
        Insert emailMessageList;
        
        // Execute Batch
        CaseWithUnreadEmailBatch obj = new CaseWithUnreadEmailBatch();
        obj.start(null);
        obj.execute(null,caseList);   
        obj.CaseWithUnreadEmailBatch();  
        obj.finish(null);
        
    }
    
       static testMethod void myUnitTest1() {
        // TO DO: implement unit test
        
        // Creating Account Record
        List<Account> accountList=TestUtilities.createAccount(1,true);
        
        // Quering to get the queue name
        List<Group> grpList = [SELECT Name FROM Group WHERE Name = 'Closed Case Queue With New/Unread Emails' AND Type = 'Queue' LIMIT 1];
        
        // Creating case records where case status is closed and case owner is 'Closed Case Queue With New/Unread Emails' queue.
        List<Case> caseList = TestUtilities.createCase(1,UserInfo.getUserId(),false);
        for(Case objCase:caseList){
            objCase.Status= 'Çlosed';
            objCase.AccountId=accountList.get(0).Id;
            objCase.Future_Action_Date__c=System.today()+2;
            objCase.Prior_Owner__c=UserInfo.getUserId();
            if(grpList.size() >0 ){
              objCase.ownerId = grpList[0].id;  
            }
        }
        insert caseList;
        System.debug('Closed Queue caseList is '+caseList);
        
        // Creating New email message on Case with Status new.
        List<EmailMessage> emailMessageList=TestUtilities.createEmailMessage(1,caseList[0].id,false);
        emailMessageList[0].Status = '0';
        Insert emailMessageList;
        
        // Execute Batch
        CaseWithUnreadEmailBatch obj = new CaseWithUnreadEmailBatch();
        obj.start(null);
        obj.execute(null,caseList); 
        obj.CaseWithUnreadEmailBatch();  
        obj.finish(null);
    }
   
   //****************************************************************************
   // Calling the Scheduler class and executing it.
   //****************************************************************************
    static testmethod void testScheduleCaseWithUnreadEmail() {
      myUnitTest();
      myUnitTest1();
      Test.startTest();  
        ScheduleCaseWithUnreadEmail obj = new ScheduleCaseWithUnreadEmail();
        obj.execute(null);    
      Test.stopTest();
    }
}