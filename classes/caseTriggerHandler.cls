/*****************************************************************************************************************************
* Appirio, Inc
* Name: caseTriggerHandler
* Description: [Create Handler to replace Process Builder (Case Proess)]
* Created Date: [3/6/2017]
* Created By: [Kate Jovanovic] (Appirio)
* 
* Date Modified           Modified By              Description of the update
* [23-Mar2017]            [Aashish Sajwan]        [Optimization of code]
* [12-April-2017]         [Neeraj Kumawat]        [T-590411 Update to create case comments]
* [18-April-2017]         [Gaurav Dudani ]        [T-592212 updated to create complaint]
* [25-April-2017]         [Gaurav Dudani ]        [T-594827 updated to create case using Email2case functionality]
* [1 May 2017]            [Kate Jovanovic]        [updating OnAfterInsert to make Email-to-case work]
* [4 May 2017]            [Neeraj Kumawat]        [Added method insertUserLanguage to add user language to case T-599785]
* [11 May 2017]           [Neeraj Kumawat]        [Added method createWorkOrder to create workorder on case and checkWorkOrderExist
And Update the caseToCreateTask method for new Profile. T-601144] 
* [17/05/2017]            [Aashish Sajwan]        [Added required field for Complaint object record]       
* [18/05/2017]            [Gaurav Dudani]         [Added method to prevent Prevent Case Closer with new unread email as 
per task # T-603030]     
* [12/05/2017]            [Poornima Bhardwaj]     [Added code to createWorkOrder method as per task # T-592161]  
* [22 May 2017]           [Neeraj Kumawat]        [Added account and Contact Id to WorkOrder Creation from Case. T-601144] 
* [23 May 2017]           [Neeraj Kumawat]        [Updated Assigning Work order based on Processing center id. T-601144]
* [24 May 2017]           [Poornima Bhardwaj]     [Updated create work order as per Task T-604033]
* [31 May 2017]           [Neeraj Kumawat]        [Added preventCaseClosureForOpenWorkOrders for prevent case case closure
when there are open work order on case.T-606953] 
* [31 May 2017]           [Bobby Cheek]           [Enforcing CRUD and FLS Security in the Apex Class - T-600197] 
* [01/06/2017]            [Gaurav Dudani]         [Added to create a task on Complaint creation (On Complaint) When new case is created with Complaint Applicability is "Yes" as per S-470695.]
* [05/06/2017]            [Gaurav Dudani]         [Added comments to the code]
* [12 June 2017]          [Neeraj Kumawat]        [Added Method preventUnselectingWorkItemMultiPickList #T-608973]
* [13 June 2017]          [Gaurav Dudani]         [Added Required fields on createCaseComplaint method as per issue # I-278864.]
* [14 June 2017]          [Gaurav Dudani]         [Optimied code to update complaint creation filter criteria]
* [14 June 2017]          [Neeraj Kumawat]        [Updated insertUserLanguage method for getting user language instead of user Locale T-609575]
* [15 June 2017]          [Gaurav Dudani]         [Added nullifyCaseContact method for inserting contact on case as null as per I-242653/T-609666.]
* [15 June 2017]          [Bobby Cheek]           [Enforcing CRUD and FLS Security in the Apex Class] 
* [15 June 2017]          [Neeraj Kumawat]        [Added Method updateUserLanguage to updated case userLanguage field when case owner changed T-609952]
* [15 June 2017]          [Neeraj Kumawat]        [T-610217 Added Custom Label on showing error message on preventCaseClosureForUnreadEmails]
* [20 June 2017]          [Kate Jovanovic]        [I-280192 removing soql query for profile in method createWorkOrder ]
* [22 June 2017]          [Neeraj Kumawat]        [T-608730/I-278559 Updated createWorkOrder to get the value of queue id from common method od utilitycls class.]
* [23 June 2017]          [Neeraj Kumawat]        [T-610968 Getting record type Id from UtilityCls.getRecordTypeId and fetching record type name from Custom Label.]
* [26 June 2017]          [Kate Jovanovic]        [Deactivating all Complaint methods/references]
******************************************************************************************************************************/

public class caseTriggerHandler {
    
    /* Keeps Case Currency Matching Account Currency */
    public static void onBeforeInsert (List<Case> newCases) {
        updateCaseCurrency(newCases);
        //Method to insert user language to case field
        //Date 4 May 2017 Neeraj Kumawat
        insertUserLanguage(newCases);
        // Calling Method to insert case Contact as Null.
        // Added By Gaurav Dudani on 15 June 2017 as per I-242653/T-609666.
        //nullifyCaseContact(newCases);
    }
    
    /* Updates Prior Owner and Owner on Cases by Manual Case Change, Queue Case Change or Returning to Previous User */
    public static void onBeforeUpdate (Map<Id,Case> oldMap, List<Case> newCases){
        updateCaseOwner(newCases);
        UpdateCasePriorOwner(oldMap,newCases);
        updateCaseCurrency(newCases);
        //Calling preventCaseClosureForUnreadEmails method
        //Date 18 May 2017 Gaurav Dudani T-603030.
        preventCaseClosureForUnreadEmails(oldMap,newCases);
        //Calling preventCaseClosureForOpenWorkOrders method
        //31 May 2017 Neeraj Kumawat T-606953.
        preventCaseClosureForOpenWorkOrders(oldMap,newCases);
        // #T-608973 12 June 2017  Neeraj Kumawat calling preventUnselectingWorkItemMultiPickList 
        preventUnselectingWorkItemMultiPickList(oldMap,newCases);
        //16 June 2017 Neeraj Kumawat T-609952 calling updateUserLanguage method
        updateUserLanguage(oldMap,newCases);
    }
    
    /* Collection of newly created cases by Custom Agent or Manager*/
    public static void OnAfterInsert(List<Case> newCases) {
        caseToCreateTask(newCases);
        createCaseComment(null,newCases);
       // createCaseComplaint(null,newCases);
        //Calling createWorkOrder method
        //Date 11 May 2017 Neeraj Kumawat T-601144
        createWorkOrder(newCases);
        // Calling Method to insert case Contact as Null.
        // Added By Gaurav Dudani on 15 June 2017 as per I-242653/T-609666.
        //nullifyCaseContact(newCases);
    }
    /* This method will call on after update of case*/
    public static void OnAfterUpdate(Map<Id,Case>oldMap,List<Case> newCases) {
        createCaseComment(oldMap,newCases);
        //createCaseComplaint(oldMap,newCases);
        //Calling createWorkOrder method
        //Date 11 May 2017 Neeraj Kumawat T-601144
        createWorkOrder(newCases);
    }
    
    
    /*Creates a new task when a case was created by a Agent or Manager */
    public static void caseToCreateTask(List<Case> newCases) {
        
        Id profileId=userinfo.getProfileId();
        // Start
        // Updated by Gaurav Dudani on 25th April 2017 to fix issue as per task # T-594827.
        String profileName = '';
        for(Profile objProfile: [Select Id,Name from Profile where Id=:profileId]){
            profileName = objProfile.Name;
        }
        //End
        
        List<Case> caseToTask = new List<Case>();
        // Start
        // Updated by Gaurav Dudani on 25th April 2017 to fix issue as per task # T-594827.
        // Updated By Neeraj Kumawat as per new Profile for Agent and Manager. 22-May-2017
        if(!string.isBlank(profileName) && (profileName == 'Custom: NA Manager' || profileName == 'Custom: NA Agent' || 
                                            profileName == 'Custom: MX Manager' || profileName == 'Custom: MX Agent')){ 
                                                // End    
                                                for(Case c : newCases){
                                                    caseToTask.add(c); 
                                                }
                                            }
        
        createTask(caseToTask);
    }
    
    
    /*Creates Task and inserts to associated Case */
    public static void createTask(List<Case> newCases){
        
        Id userId = UserInfo.getUserId();
        
        
        List<Task> insertNewTask = new List<Task>();
        for(Case c : newCases){
            
            Task newTask = new Task();
            // 07 June 2017 Bobby Cheek checking FLS on Task fields
            if(Schema.sObjectType.Task.fields.ActivityDate.isCreateable())
                newTask.ActivityDate = Date.today();
            if(Schema.sObjectType.Task.fields.CurrencyISOCode.isCreateable())
                newTask.CurrencyISOCode = c.CurrencyIsoCode;
            if(Schema.sObjectType.Task.fields.Description.isCreateable())
                newTask.Description = c.Description;
            if(Schema.sObjectType.Task.fields.OwnerId.isCreateable())               
                newTask.OwnerId = userId;
            if(Schema.sObjectType.Task.fields.Priority.isCreateable())
                newTask.Priority = 'Normal';
            if(Schema.sObjectType.Task.fields.Status.isCreateable())
                newTask.Status = 'Complete';
            if(Schema.sObjectType.Task.fields.Subject.isCreateable())
                newTask.Subject = c.Subject;
            if(Schema.sObjectType.Task.fields.Type.isCreateable())
                newTask.Type = 'Call';
            if(Schema.sObjectType.Task.fields.WhatId.isCreateable())
                newTask.WhatId = c.Id;
            
            insertNewTask.add(newTask);
            
        }
        if(Schema.sObjectType.Task.isCreateable())
            insert insertNewTask;
    }
    
    
    /*If Case Currency and Account Currency does not match or is null.
Update the Case currency with the Account currency */
    public static void updateCaseCurrency (List<Case> newCases){
        
        List<Case> caseList = new List<Case>();
        Set<Id> caseAccount = new Set<Id>();
        for(Case c : newCases){
            caseAccount.add(c.AccountId);
        }
        
        List<Account> accList = [SELECT ID, CurrencyIsoCode 
                                 FROM Account
                                 WHERE ID IN : caseAccount];
        
        for(Account accounts : accList){
            for(Case ca : newCases){
                if(accounts.CurrencyIsoCode != ca.CurrencyIsoCode){
                    // 15 June 2017 Bobby Cheek checking FLS on Case fields
                    if(Schema.sObjectType.Case.fields.CurrencyIsoCode.isUpdateable()){
                        ca.CurrencyIsoCode = accounts.CurrencyIsoCode;
                        caseList.add(ca);
                    }                    
                }
            }
        }
    }
    
    /*Case owner and previous Owner is updated to when cases are return if 'Return to Previous Owner' is true.  */
    public static void updateCaseOwner(List<Case> newCases){
        
        // List<Case> caseToUpdate = new List<Case>();
        String oldOwner;
        
        for(Case c : newCases){
            
            if(c.Return_Case_to_Previous_Owner__c && c.Prior_Owner__c !=null){
                c.ownerID = c.Prior_Owner__c;
                c.Return_Case_to_Previous_Owner__c = false;
                //caseToUpdate.add(c);
            }
            //if there is no previous owner, set return to previous owner as false
            c.Return_Case_to_Previous_Owner__c = false;
            // c.addError(Label.Prior_Owner_Error);
        } 
    }
    
    /*If Case Owner is manually changed (NOT BY RETURN TO PREVIOUS OWNER) set prior owner. */
    public static void UpdateCasePriorOwner(Map<Id,Case> oldMap, List<Case> newCases){
        String setPriorOwner;
        for(case c : newCases){
            if(oldMap.get(c.Id).ownerId != c.ownerId) {
                setPriorOwner = oldMap.get(c.Id).OwnerId;
                c.Prior_Owner__c = setPriorOwner;
            }  
        }
        
    }
    
    //****************************************************************************
    // Method to Creates case  comments when case internal comments field is changed 
    // @param oldMap: Map of old case
    // @param newCases: List of Case object
    // @return void
    //****************************************************************************
    public static void createCaseComment(Map<Id,Case> oldMap,List<Case> newCases) {
        List<CaseComment> caseCommentList = new List<CaseComment>();
        for(Case objCase: newCases){
            if(objCase.Internal_Comments__c!=null){
                if(oldMap!=null){
                    //update case comment list on case update
                    String oldCaseComment=oldMap.get(objCase.Id).Internal_Comments__c;
                    //check is internal comment change
                    if(oldCaseComment!=objCase.Internal_Comments__c){
                        updateCaseCommentList(caseCommentList,objCase);
                    }
                    
                }else{
                    //update case comment list on insert
                    updateCaseCommentList(caseCommentList,objCase);
                }
            }
        }      
        
        insert caseCommentList;
    }
    //****************************************************************************
    // Method to update provided caseCommentList
    // @param caseCommentList: List of Case Comment
    // @param objCase: Case object
    // @return void
    //****************************************************************************
    public static void updateCaseCommentList(List<CaseComment> caseCommentList,Case objCase){
        
        CaseComment objCaseComment=new CaseComment();
        
        // 07 June 2017 Bobby Cheek checking FLS on CaseComment fields
        if(Schema.sObjectType.CaseComment.fields.CommentBody.isCreateable())
            objCaseComment.CommentBody=objCase.Internal_Comments__c;
        if(Schema.sObjectType.CaseComment.fields.isPublished.isCreateable())
            objCaseComment.isPublished=false;
        if(Schema.sObjectType.CaseComment.fields.ParentId.isCreateable())
            objCaseComment.ParentId=objCase.Id;
        
        caseCommentList.add(objCaseComment);
    }
    
    //****************************************************************************
    // Method to systemically activated complaint record when case is created and 
    // when Complaint Applicability is selected as "Yes" or updated to "Yes".
    // By Gaurav Dudani on 18 April 2017 as per Task # T-592212.
    //Deactivating this method on June 26 2017 - Kate Jovanovic
    //****************************************************************************
  
  /*  public static void createCaseComplaint(Map<Id,Case> oldMap,List<Case> newCases){
        List<Complaint__c> complaintToInsert = new List<Complaint__c>();
        for(Case cas : newCases ){
            // Start
            // Optimied code to update complaint creation filter criteria
            // By Gaurav Dudani On 14 June 2017.
            If((oldMap == null
                && cas.Complaint_Applicability__c == 'Yes') 
               || (oldMap != null
                   // End   
                   && cas.Complaint_Applicability__c == 'Yes' 
                   && cas.Complaint_Applicability__c != oldMap.get(cas.Id).Complaint_Applicability__c)) {
                       // 17 May 2017 Aashish Sajwan - Added Required Communication_Method__c 
                       // field for creation Compalaint object
                       /*Complaint__c complain = new Complaint__c(What_is_the_customer_s_complaint__c = Label.New_Complaint_From_Case, 
Communication_Method__c = 'Verbal', 
Elavon_Channels__c = 'City National',
Issue_Resolved__c = 'Yes',
Resolution_communicated_via__c = 'Verbal',
Date_complaint_received__c=Date.today(),
Case__c = cas.Id);

                       // 07 June 2017 Bobby Cheek checking FLS on Complaint__c fields
                       Complaint__c complain = new Complaint__c();
                       if(Schema.sObjectType.Complaint__c.fields.What_is_the_customer_s_complaint__c.isCreateable())
                           complain.What_is_the_customer_s_complaint__c = Label.New_Complaint_From_Case;
                       if(Schema.sObjectType.Complaint__c.fields.Communication_Method__c.isCreateable())
                           complain.Communication_Method__c = 'Verbal'; 
                       if(Schema.sObjectType.Complaint__c.fields.Elavon_Channels__c.isCreateable())
                           complain.Elavon_Channels__c = 'City National';
                       if(Schema.sObjectType.Complaint__c.fields.Issue_Resolved__c.isCreateable())
                           complain.Issue_Resolved__c = 'Yes';
                       if(Schema.sObjectType.Complaint__c.fields.Resolution_communicated_via__c.isCreateable())
                           complain.Resolution_communicated_via__c = 'Verbal';
                       if(Schema.sObjectType.Complaint__c.fields.Date_complaint_received__c.isCreateable())
                           complain.Date_complaint_received__c=Date.today();
                       // Start
                       // Added the required fields when Complaint is created on case creation.
                       // By Gaurav Dudani on 13 June 2017 as per Issue # I-278864.
                       if(Schema.sObjectType.Complaint__c.fields.How_and_when_did_you_resolve_the_issue__c.isCreateable())
                           Complain.How_and_when_did_you_resolve_the_issue__c='Test Complaint';
                       if(Schema.sObjectType.Complaint__c.fields.Your_Department__c.isCreateable())
                           Complain.Your_Department__c='CAM';
                       // End      
                       if(Schema.sObjectType.Complaint__c.fields.Case__c.isCreateable())
                           complain.Case__c = cas.Id;
                       
                       complaintToInsert.add(complain);       
                   }
        }
        
        if (complaintToInsert.size() > 0) {
            insert complaintToInsert;
            // Calling createTaskOnComplaintCreation method on complaint creation with Complaint Applicability selected as "Yes".
            // Added on 1st June 2017 Gaurav Dudani S-470695.
            createTaskOnComplaintCreation(complaintToInsert);
        } 
    }
    */
    
    //****************************************************************************
    // Method to Create task when an Complaint gets created and display the reminder pop-up.
    // 1st June 2017 Gaurav Dudani S-470695.
    //June 26 2017 - Deactivating this method due to removing of the Complaint Object
    //****************************************************************************
  
 /*   public static void createTaskOnComplaintCreation(List<Complaint__c> newComplaintList) {
        List<Task> taskListToInsert = new List<Task>();
        for(Complaint__c comp: newComplaintList){
            /*taskListToInsert.add(new Task(whatId = comp.Id, 
ActivityDate = System.today(),
Status = 'New',
Priority =  'Normal',
Subject = 'Complaint Informaion Incomplete:Please Furnish the necessary details.',
isreminderset = true, 
ReminderDateTime = System.now().addMinutes(1)));

            
            Task complaintTask = new Task();
            
            // 07 June 2017 Bobby Cheek checking FLS on Task fields
            if(Schema.sObjectType.Task.fields.whatId.isCreateable())
                complaintTask.whatId = comp.Id;
            if(Schema.sObjectType.Task.fields.ActivityDate.isCreateable())
                complaintTask.ActivityDate = System.today();
            if(Schema.sObjectType.Task.fields.Status.isCreateable())
                complaintTask.Status = 'New';
            if(Schema.sObjectType.Task.fields.Priority.isCreateable())
                complaintTask.Priority =  'Normal';
            if(Schema.sObjectType.Task.fields.Subject.isCreateable())
                complaintTask.Subject = 'Complaint Informaion Incomplete:Please Furnish the necessary details.';
            if(Schema.sObjectType.Task.fields.isreminderset.isCreateable())
                complaintTask.isreminderset = true; 
            if(Schema.sObjectType.Task.fields.ReminderDateTime.isCreateable())
                complaintTask.ReminderDateTime = System.now().addMinutes(1);
            
            taskListToInsert.add(complaintTask);        
        }
        
        if(taskListToInsert.size() > 0){
            insert taskListToInsert;
        }
    }
    */
    
    //****************************************************************************
    // Method to Insert User Language in User_Language__c Field
    // @param newCases: List of Case
    // @return void
    // 4 May 2017 Neeraj Kumawat T-599785
    //14 June Neeraj Kumawat Updated for getting user language instead of user Locale T-609575
    //****************************************************************************
    public static void insertUserLanguage(List<Case> newCases){
        string Language=UserInfo.getLanguage();
        for(Case c: newCases){
            c.User_Language__c=Language;
        }
    }
    //****************************************************************************
    // Method to update User Language in User_Language__c Field when case owner changed
    // @param newCases: List of Case
    // @param newCases: Map of old Case
    // @return void
    // 16 May 2017 Neeraj Kumawat  T-609952
    //Updated for setting user language from case owner language when case owner id changed
    //****************************************************************************
    public static void updateUserLanguage(Map<Id,Case> oldMap,List<Case> newCases){
        for(Case caseObj: newCases){
            Case oldCase=oldMap.get(caseObj.Id);
            if(caseObj.OwnerId!=oldCase.OwnerId){
                List<User> userList=[Select Id, LanguageLocaleKey From User Where Id=:caseObj.OwnerId];
                if(userList.size()>0){
                    caseObj.User_Language__c=userList.get(0).LanguageLocaleKey;
                }
            }
        }
    }
    //****************************************************************************
    // Method to create Work Order on case
    // @param newCases: List of Case
    // @return void
    // 11 May 2017 Neeraj Kumawat T-601144
    //****************************************************************************
    public static void createWorkOrder(List<Case> newCases){
        List<WorkOrder> workOrderList=new List<WorkOrder>();
        Map<Id,Case> caseMap=new Map<Id,Case>([Select Id, Subject,Account.Processing_Center_ID__c, (Select Id, Subject,RecordTypeId From WorkOrders) 
                                               From Case Where Id IN :newCases]);
        //Calling getCETQueueMap to get queueName with Id
        Map<String,String> queueNameMap= UtilityCls.getCETQueueMap();
        String  naCetQueueId=queueNameMap.get('NA_CET_Team_General_Release');
        String mxCetQueueId=queueNameMap.get('MX_CET_Team_General_Release');
        //Fetching work Order record type details
        for(Case cse: newCases){
            Id caseId=cse.Id;
            String workItems=cse.Work_Items__c;
            // Creating General Release Work Order
            if(workItems!=null && workItems.contains('General Release')){
                //T-610968 Getting record type Id from UtilityCls.getRecordTypeId
                String genReleaseWekOrderTypeId=UtilityCls.getRecordTypeId('WorkOrder',System.Label.General_Release);
                Boolean isWorkOrderExists=checkWorkOrderExist(caseId,caseMap,genReleaseWekOrderTypeId);
                if(!isWorkOrderExists){
                    
                    WorkOrder wrkOrder= new WorkOrder();
                    
                    // 07 June 2017 Bobby Cheek checking FLS on WorkOrder fields
                    if(Schema.sObjectType.WorkOrder.fields.Subject.isCreateable())
                        wrkOrder.Subject=System.Label.General_Release;
                    if(Schema.sObjectType.WorkOrder.fields.caseId.isCreateable())
                        wrkOrder.caseId=cse.Id;
                    if(Schema.sObjectType.WorkOrder.fields.recordTypeId.isCreateable())
                        wrkOrder.recordTypeId=genReleaseWekOrderTypeId;
                    //Added account and Contact Id to WorkOrder Creation from Case. 
                    //22-May-2017 Neeraj Kumawat T-601144
                    if(Schema.sObjectType.WorkOrder.fields.accountId.isCreateable())
                        wrkOrder.accountId=cse.AccountId;
                    if(Schema.sObjectType.WorkOrder.fields.contactId.isCreateable())
                        wrkOrder.contactId=cse.contactId;
                    Case cseObj=caseMap.get(cse.Id);
                    //Updated assiging of workorder based on processing center id
                    //Updated by Neraj Kumawat 23-May-2017  T-601144
                    if(cseObj.Account!=null && cseObj.Account.Processing_Center_ID__c!=null){
                        if(naCetQueueId!=null && cseObj.Account.Processing_Center_ID__c=='NA'){
                            if(Schema.sObjectType.WorkOrder.fields.OwnerId.isCreateable())
                                wrkOrder.OwnerId=naCetQueueId;
                        }else if(mxCetQueueId!=null && cseObj.Account.Processing_Center_ID__c=='QUERE'){
                            if(Schema.sObjectType.WorkOrder.fields.OwnerId.isCreateable())
                                wrkOrder.OwnerId=mxCetQueueId;
                        }
                    }
                    workOrderList.add(wrkOrder);
                }
            }
            //Creates workOrder when Fee Maintenance is selected in WorkLine Item
            //12-May-2017 Poornima Bhardwaj T-592161
            if(workItems!=null && workItems.contains('Fee Maintenance')){
                //T-610968 Getting record type Id from UtilityCls.getRecordTypeId
                String feeMaintainanceWekOrderTypeId=UtilityCls.getRecordTypeId('WorkOrder',System.Label.Fee_Maintenance);
                Boolean isWorkOrderExists=checkWorkOrderExist(caseId,caseMap,feeMaintainanceWekOrderTypeId);
                if(!isWorkOrderExists){
                    
                    WorkOrder wrkOrder= new WorkOrder();
                    
                    // 07 June 2017 Bobby Cheek checking FLS on WorkOrder fields
                    if(Schema.sObjectType.WorkOrder.fields.Subject.isCreateable())
                        wrkOrder.Subject=System.Label.Fee_Maintenance;
                    if(Schema.sObjectType.WorkOrder.fields.caseId.isCreateable())
                        wrkOrder.caseId=cse.Id;
                    if(Schema.sObjectType.WorkOrder.fields.recordTypeId.isCreateable())
                        wrkOrder.recordTypeId=feeMaintainanceWekOrderTypeId;
                    //Added account and Contact Id to WorkOrder Creation from Case. 
                    //22-May-2017 Neeraj Kumawat T-601144
                    if(Schema.sObjectType.WorkOrder.fields.accountId.isCreateable())
                        wrkOrder.accountId=cse.AccountId;
                    if(Schema.sObjectType.WorkOrder.fields.contactId.isCreateable())
                        wrkOrder.contactId=cse.contactId;
                    
                    workOrderList.add(wrkOrder);
                }                             
            }
            //Creates workOrder when Adjustment is selected in WorkLine Item
            //12-May-2017 Poornima Bhardwaj T-603987
            if(workItems!=null && workItems.contains('Adjustment')){
                //T-610968 Getting record type Id from UtilityCls.getRecordTypeId
                String adjustmentWekOrderTypeId=UtilityCls.getRecordTypeId('WorkOrder',System.Label.Adjustment);
                Boolean isWorkOrderExists=checkWorkOrderExist(caseId,caseMap,adjustmentWekOrderTypeId);
                if(!isWorkOrderExists){
                    
                    WorkOrder wrkOrder= new WorkOrder();
                    
                    // 07 June 2017 Bobby Cheek checking FLS on WorkOrder fields
                    if(Schema.sObjectType.WorkOrder.fields.Subject.isCreateable())
                        wrkOrder.Subject=System.Label.Adjustment;
                    if(Schema.sObjectType.WorkOrder.fields.caseId.isCreateable())
                        wrkOrder.caseId=cse.Id;
                    if(Schema.sObjectType.WorkOrder.fields.recordTypeId.isCreateable())
                        wrkOrder.recordTypeId=adjustmentWekOrderTypeId;
                    //Added account and Contact Id to WorkOrder Creation from Case. 
                    //22-May-2017 Neeraj Kumawat T-601144
                    if(Schema.sObjectType.WorkOrder.fields.accountId.isCreateable())
                        wrkOrder.accountId=cse.AccountId;
                    if(Schema.sObjectType.WorkOrder.fields.contactId.isCreateable())
                        wrkOrder.contactId=cse.contactId;
                    
                    workOrderList.add(wrkOrder);
                }                             
            }            
        }
        insert workOrderList;
    }
    //****************************************************************************
    // Method to check Work Order exist of not in case
    // @param newCases: List of Case
    // @return void
    // 11 May 2017 Neeraj Kumawat T-601144
    //****************************************************************************
    public static Boolean checkWorkOrderExist(Id caseId, Map<Id,Case> caseMap, String recordTypeId){
        Case caseObj=caseMap.get(caseId);
        for(WorkOrder wrk: caseObj.WorkOrders){
            if(wrk.RecordTypeId==recordTypeId){
                return true;
            }
        }
        return false;
    }
    //****************************************************************************
    // Method to prevent Prevent Case Closer with new unread email.
    // 18 May 2017 Gaurav Dudani as per task # T-603030
    //****************************************************************************
    public static void preventCaseClosureForUnreadEmails(Map<Id,Case> oldMap,List<Case> newCases){
        Map<Id, Case> closedCaseMap = new Map<Id, Case>();
        for(Case cas : newCases){
            if (cas.status != oldMap.get(cas.Id).status 
                && cas.status == 'Closed'){
                    closedCaseMap.put(cas.Id, cas);
                }
        }
        Set<Id> caseIdWithUnreadEmails= new Set<Id>();
        for(EmailMessage emailMsg :[SELECT Id, RelatedToID, ParentID
                                    FROM EmailMessage
                                    WHERE RelatedToID IN :closedCaseMap.keySet() 
                                    AND Status = '0']){  
                                        closedCaseMap.get(emailMsg.RelatedToID).addError(System.Label.CaseClosureOnUnreadEmail);                                                                
                                    }
    }
    //****************************************************************************
    // Method to prevent Case Closer when there are open Workorder on case
    // @param oldMap: Map of old Case
    // @param newCases: List of new Case
    // @return void
    // 31 May 2017 Neeraj Kumawat as per task #T-606953
    //****************************************************************************
    public static void preventCaseClosureForOpenWorkOrders(Map<Id,Case> oldMap,List<Case> newCases){
        //List of Work Order open status
        List<String> workOrderOpenStatus=new List<String>{'New','In Progress','On Hold'};
            //Map of case with closed status
            Map<Id, Case> closedCaseMap = new Map<Id, Case>();
        for(Case cas : newCases){
            if (cas.status != oldMap.get(cas.Id).status && cas.status == 'Closed'){
                closedCaseMap.put(cas.Id, cas);
            }
        }
        List<Case> caseList=[Select Id, Subject,Status, (Select Id, Subject,Status 
                                                         From WorkOrders Where Status IN :workOrderOpenStatus)
                             From Case Where Id IN :closedCaseMap.keySet()];
        for(Case cse: caseList){
            List<WorkOrder> openWorkOrderList=cse.WorkOrders;
            //Showing error message on case clsoe when there is open work order on it
            if(openWorkOrderList.size()>0){
                closedCaseMap.get(cse.Id).addError(Label.Case_Close_When_Open_WorkOrder);                                                                
            }
        }
    }
    //****************************************************************************
    // Method to prevent unselecting the WorkItem MultiPickList values
    // @param oldMap: Map of old Case
    // @param newCases: List of new Case
    // @return void
    // 12 June 2017 Neeraj Kumawat as per task #T-608973
    //****************************************************************************
    public static void preventUnselectingWorkItemMultiPickList(Map<Id,Case> oldMap,List<Case> newCases){
        //List of updated cases 
        for(Case cse: newCases){
            String newWorkItems=cse.Work_Items__c;
            String oldWorkItems=oldMap.get(cse.Id).Work_Items__c;
            //Check work item multi select value is changed or not
            if(newWorkItems!=oldWorkItems){
                if(oldWorkItems==null){
                    oldWorkItems='';
                }
                if(newWorkItems==null){
                    newWorkItems='';
                }
                //Showing error when user try to unselect Work item value in case 
                if(!newWorkItems.contains(oldWorkItems)){
                    cse.addError(System.Label.PreventUnselectWorkItem); 
                }
            }
        }
    }
    //****************************************************************************
    // Method to insert case contact as null.
    // 15 June 2017 Gaurav Dudani as per I-242653/T-609666.
    //****************************************************************************
    public static void nullifyCaseContact(List<Case> newCaseList) {
        List<Case> caseToUpdate = new List<Case>();
        for (Case cas : [SELECT ContactId, CreatedBy.LastName 
                         FROM Case 
                         WHERE Id IN :newCaseList]) {
                             System.debug('Cass '+cas.CreatedById+' '+cas.CreatedBy.LastName);
                             // Checking if Created By lastname is System.
                             if(cas.CreatedBy.LastName.endsWith('System')
                                && cas.ContactId != null) {
                                    cas.ContactId = null;
                                    caseToUpdate.add(cas);
                                }  
                         }
        if (caseToUpdate.size()>0) {
            update caseToUpdate;
        }
    }
}