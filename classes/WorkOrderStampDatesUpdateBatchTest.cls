/*****************************************************************************************************
* Appirio, Inc
* Test Class Name: WorkOrderStampDatesUpdateBatchTest
* Class Name: WorkOrderStampDatesUpdateBatch
* Description: (#T-608729) Test Class for WorkOrderStampDatesUpdateBatch
* Created Date: [June 9, 2017]
* Created By: [Poornima Bhardwaj] (Appirio)
* Date Modified          Modified By              Description of the update
* 
******************************************************************************************************/
@isTest
private class WorkOrderStampDatesUpdateBatchTest {
    //******************************************************************************************
    // Method to verify that Last_Stamped_Collections_Balance__c is as per the WebService returns
    // @return void
    //******************************************************************************************
    static testMethod void createWorkOrderCollectionDetails()
    {    
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator('CollectionTotal'));
        Test.startTest(); 
        List<WorkOrder> workOrderlist = new List<WorkOrder>();
        List<Account> accList= new List<Account>();
        accList=TestUtilities.createAccount(1,false);
        accList[0].MID__c='8026984123';
        insert accList;
        workOrderlist =TestUtilities.createWorkOrder(5,false);
        for(Integer i=0;i<5;i++){
            workOrderlist[i].AccountId=accList[0].Id;
            workOrderlist[i].Work_Order_Approval_Status__c='Submitted';
        }
        insert workOrderlist;
        ScheduleWorkOrderStampDatesBatchClass scheduleObj=new ScheduleWorkOrderStampDatesBatchClass();
        String sch = '0 0 23 * * ?'; 
        system.schedule('Test Territory Check', sch, scheduleObj);
        WorkOrderStampDatesUpdateBatch workOrderStampBatchObj= new WorkOrderStampDatesUpdateBatch();
        database.executeBatch(workOrderStampBatchObj,75);  
        Test.stopTest();
        //Fetching Last Stamped WriteOff balance from Workorder for checking assert
        List<workOrder> wrkOrder=[Select Last_Stamped_Collections_Balance__c from workOrder WHERE ID IN:workOrderList];
        System.assertEquals(10,wrkOrder[0].Last_Stamped_Collections_Balance__c);
    }
    //******************************************************************************************
    // Method to verify that Last_Stamped_Write_Off_Balance__c is as per the WebService returns
    // @return void
    //******************************************************************************************
    static testMethod void createWorkOrderWriteOffSummary()
    {   
        //Calling MockGenerator Class for WriteOffSummary api callout
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator('WriteoffSummary'));
        List<WorkOrder> workOrderlist = new List<WorkOrder>();
        List<Account> accList= new List<Account>();
        Test.startTest();
        accList=TestUtilities.createAccount(1,false);
        accList[0].MID__c='8026984123';
        insert accList;
        //creating workOrder records with approval status as Submitted
        workOrderlist =TestUtilities.createWorkOrder(5,false);
        for(Integer i=0;i<5;i++){
            workOrderlist[i].AccountId=accList[0].Id;
            workOrderlist[i].Work_Order_Approval_Status__c='Submitted';
        }
        insert workOrderlist;
        //Calling Schedulable Batch Class
        ScheduleWorkOrderStampDatesBatchClass scheduleObj=new ScheduleWorkOrderStampDatesBatchClass();
        String sch = '0 0 23 * * ?'; 
        system.schedule('Test Territory Check', sch, scheduleObj);
        WorkOrderStampDatesUpdateBatch workOrderStampBatchObj= new WorkOrderStampDatesUpdateBatch();
        database.executeBatch(workOrderStampBatchObj,75);
        Test.stopTest();
        //Fetching Last Stamped WriteOff balance from Workorder for checking assert
        List<workOrder> wrkOrder=[Select Last_Stamped_Write_Off_Balance__c from workOrder WHERE ID IN:workOrderList];
        System.assertEquals(0,wrkOrder[0].Last_Stamped_Write_Off_Balance__c);
    }
}