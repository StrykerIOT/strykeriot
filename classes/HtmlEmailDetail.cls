/*********************************************************************************************************************
Author        :  Appirio (Neeraj Kumawat) (T-549511)
Date          :  Oct 27th, 2016 
Purpose       :  Pojo class for Html Email Status

**********************************************************************************************************************/
public class HtmlEmailDetail {
    // Defining setter getter for Html email status to show it on case
    public String subject{get;set;}
    public String status{get;set;}
    public String dateSent{get;set;}
    public String dateOpened{get;set;}
    public integer timesOpened {get;set;}
    public String lastOpened{get;set;}    
    public String customStatus{get;set;}

}