/*****************************************************************************************************
* Appirio, Inc
* Test Class Name: WorkOrderLineItemTriggerHandlerTest
* Class Name: WorkOrderLineItemTriggerHandler
* Description: (#T-603560) Handler test for WorkOrderLineItem Trigger bulk testing
* Created Date: [May 25, 2017]
* Created By: [Neeraj Kumawat] (Appirio)
* 
* Date Modified          Modified By              Description of the update
* [30-May-2017]         [Neeraj Kumawat]         [Added test method for updating work order status.#T-605293/S-470343]
* [7-June-2017]			[Neeraj Kumawat]		 [Added test method for updating work order status.#T-608155/I-277751]
* [7-June-2017]			[Neeraj Kumawat]		 [Added test method for updating work order status.#T-607554/S-470343]
******************************************************************************************************/
@isTest
private class WorkOrderLineItemTriggerHandlerTest {
    static List<Case> caseList;
    static List<Account> accList;
    static Integer numberOfRecords = 2;
    Static user relationshipUser;
    //Record Type Id of Adjustment Type WorkOrder
    Static String adjustmentWrkOrderTypeId;
    //Record Type Id of WorkOrderLineItem
    Static String workOrderLineItemRecordTypeId;
    Static List<WorkOrder> workOrderList;
    Static List<Reference__c> referenceListWOLI;
    //******************************************************************************************
    // Method to create test data for User, Account, Case to test WorkOrderLineItemTriggerHandler
    // @param TransCR: value of transaction credit
    // @param TransDB: value of transaction debit
    // @param Reportable1099: value of Reportable1099
    // @return void
    // 25 May 2017 Neeraj Kumawat #T-603560
    //******************************************************************************************
    static void createTestData(Boolean transCR,Boolean transDB,Boolean reportable1099)
    {	//Custom Relationship Manager User Data
        List<UserRole> globalOpsRole=[select id,name from UserRole where name='Global Ops'];
        List<Profile> relationshipProfileId=[select id,name from Profile where name='Custom: Relationship Manager'];
        relationshipUser=TestUtilities.createTestUser(1,relationshipProfileId[0].id,false);
        relationshipUser.Username='testrelationship@testdomain.comtestuser';
        relationshipUser.CommunityNickname='test';
        relationshipUser.UserRole=globalOpsRole[0];
        insert relationshipUser;
        //Account,Case Creation from Custom Relationship Manager User
        System.runAs(relationshipUser){
            accList= TestUtilities.createAccount(1,false);
            for(Account a:accList){
                a.Processing_Center_ID__c ='NA';
            }
            insert accList;
            //creating Case Test Data
            caseList=TestUtilities.createCase(1,relationshipUser.Id, false); 
            Map<String, Schema.Recordtypeinfo> workOrderRecordTypes = WorkOrder.sObjectType.getDescribe().
                getRecordtypeinfosByName(); 
            //Adjustment Work Order Record Type Id
            adjustmentWrkOrderTypeId=workOrderRecordTypes.get('Adjustment').getRecordTypeId();
            for(case c:caseList)
            {
                c.subject='Sample Case';
                c.Type='Maintenance';
                c.AccountId =accList[0].Id;
                c.Work_Items__c='Adjustment';
            }
            insert CaseList;
            //creating Reference Test Data
            referenceListWOLI=TestUtilities.createReferenceListWOLI(1, false);
            for(Reference__c reference: referenceListWOLI){
                reference.Department__c='All';
                reference.Processor_ID__c='NA';
                reference.Category__c='WorkOrderLineItem';
                reference.Adjustment_Reason_Category__c='Discount';
                reference.Description__c='Discount Fees';
                reference.TRANS_DB__c=transDB;
                reference.TRANS_CR__c=transCR;
                reference.Reportable_1099__c=reportable1099;
                reference.CGT_NUM__c='60';
                reference.Description_Code__c='D1';
                reference.Department_Code__c='40';
                reference.GL_Account__c='4860245';
            }
            insert referenceListWOLI;
        }
    }
    //******************************************************************************************
    // Method to create test data for WorkOrder to test WorkOrderLineItemTriggerHandler
    // @return void
    // 29 May 2017 Neeraj Kumawat #T-603560
    //******************************************************************************************
    static void createWorkOrder()
    {	//creating WorkOrder Test Data
        workOrderList=TestUtilities.createWorkOrder(1,false);
        for(WorkOrder workOrder:workOrderList){	
            //setting WorkOrder Status -- New
            workOrder.status='New';
        }
        insert workOrderList;
    }
    //*********************************************************************************************
    // Method to create test data for WorkOrderLineItem to test WorkOrderLineItemTriggerHandler
    // @param Status: Status, workOrderId: WorkOrder Id,adjustmentType: Adjustment Type and showOnStatement:Show On Statement 
    // of workorder line item
    // @return WOLineItemList-List Of WorkOrderLineItems
    // 25 May 2017 Neeraj Kumawat #T-603560
    //*********************************************************************************************
    static List<WorkOrderLineItem> createWorkOrderLineItem(String Status,String workOrderId,String adjustmentType,String showOnStatement,String recordType)
    {
        List<WorkOrderLineItem> WOLineItemList=TestUtilities.createWorkOrderLineItem(numberOfRecords,false);
        //NA Adjustment Work Order Line Item Record Type Id
        Map<String, Schema.Recordtypeinfo> workOrderLineItemRecordTypes = WorkOrderLineItem.sObjectType.getDescribe().
            getRecordtypeinfosByName(); 
        if(recordType!=null && recordType=='NA Fee Maintenance'){
             workOrderLineItemRecordTypeId=workOrderLineItemRecordTypes.get('NA Fee Maintenance Work Order Line Item').getRecordTypeId();
        }else{
            workOrderLineItemRecordTypeId=workOrderLineItemRecordTypes.get('NA Adjustment Work Order Line Item').getRecordTypeId();
        }
        for(WorkOrderLineItem woli:WOLineItemList){	
            woli.RecordTypeId=workOrderLineItemRecordTypeId;
            woli.WorkOrderId=workOrderId;
            woli.status=Status;
            //Adding item amount and quantity when record type is not Fee Maintenance
            if(recordType!=null && recordType!='NA Fee Maintenance'){
                woli.Adjustment_Type__c=adjustmentType;
                woli.Show_on_Statement__c=showOnStatement;
                woli.Adjustment_Reason__c='Discount Fees';
                woli.Adjustment_Reason_Category__c='Discount';
                woli.Department__c='All';
                woli.Line_Item_Amount__c=1000;
                woli.Line_Item_Quantity__c=10;
                woli.Line_Item_Total__c=10000;
            }
        }
        return WOLineItemList;
    }
    
    //******************************************************************************************
    // Method to create test data for WorkOrderLineItem of Customer Credit Adjustment Type 
    // for related Account,Case and WorkOrder to test WorkOrderLineItemTriggerHandler
    // @return void
    // 25 May 2017 Neeraj Kumawat #T-603560
    //******************************************************************************************
    static testMethod void createWorkOrderLineItemWithCredit()
    {
        createTestData(True,False,True);
        //creating reference record for Adjustment Type--customer credit WorkOrderLineItem
        System.runAs(relationshipUser){
            Test.startTest();
            //WebService Callout
            Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator('CollectionTotal'));
            //newCaseList for fetching related WorkOrder on a Case
            List<Case> newCaseList=[Select Id, Subject,Status,AccountId, (Select Id, Subject,RecordTypeId,Status ,AccountId
                                                                          From WorkOrders)
                                    From Case Where Id IN :caseList];
            List<WorkOrderLineItem> WOLIList=new List<WorkOrderLineItem>();
            for(Case cse: newCaseList){
                List<WorkOrder> caseWorkOrderList=cse.WorkOrders;
                //creating WorkOrderLineItem of Adjustment Type -- Customer Credit and status -- New
                for(WorkOrder wrk: caseWorkOrderList){	
                    WOLIList.addAll(createWorkOrderLineItem('New',wrk.id,'Customer Credit','Yes','NA Adjustment'));
                }
            }
            insert WOLIList;
            //Query for fetching Offset Comment,Charge Type,X1099 Tax Report Flag,Department Code,Transaction Code and 
            //General Ledger Fields from WorkOrderLineItem List
            List<WorkOrderLineItem> wrkOrderLineItemListCredit=new List<WorkOrderLineItem>([Select Id,Offset_Comment__c,
                                                                                            Charge_Type__c,
                                                                                            X1099_Tax_Report_Flag__c,
                                                                                            Department_Code__c,	General_Ledger__c,
                                                                                            Adjustment_Reason_Code__c,
                                                                                            Adjustment_Type_Indicator__c,
                                                                                            Transaction_Code__c                                                                                            From WorkOrderLineItem
                                                                                            Where Id IN:WOLIList]);
            //Iterating WorkOrderLineItemList with Customer credit adjustment type to check the actual and 
            //expected values of updated fields on WOLI
            for(WorkOrderLineItem Item:wrkOrderLineItemListCredit)
            {
                System.assertEquals('D140-4860245-',Item.Offset_Comment__c);
                System.assertEquals('M',Item.Adjustment_Type_Indicator__c);                
                System.assertEquals('60',Item.Charge_Type__c);
                System.assertEquals('Yes',Item.X1099_Tax_Report_Flag__c);
                System.assertEquals('86',Item.Adjustment_Reason_Code__c );
                System.assertEquals('138',Item.Transaction_Code__c);
                System.assertEquals('40',Item.Department_Code__c);
                System.assertEquals('4860245',Item.General_Ledger__c) ;
            }
        }
        Test.stopTest();
    }
    //******************************************************************************************
    // Method to create test data for WorkOrderLineItem of Customer Debit Adjustment Type 
    // for related Account,Case and WorkOrder to test WorkOrderLineItemTriggerHandler
    // @return void
    // 25 May 2017 Neeraj Kumawat #T-603560
    //******************************************************************************************
    static testMethod void testCreateWorkOrderLineItemWithDebit()
    {
        createTestData(False,True,False);
        //creating reference record for Adjustment Type -- Customer Debit WorkOrderLineItem 
        System.runAs(relationshipUser){
            Test.startTest();
            //WebService Callout
            Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator('CollectionTotal'));
            //newCaseList for fetching related WorkOrder on a Case
            List<Case> newCaseList=[Select Id, Subject,Status,AccountId, (Select Id, Subject,RecordTypeId,Status ,AccountId
                                                                          From WorkOrders)
                                    From Case Where Id IN :caseList];
            List<WorkOrderLineItem> WOLIList=new List<WorkOrderLineItem>();
            for(Case cse: newCaseList)
            {
                List<WorkOrder> caseWorkOrderList=cse.WorkOrders;
                //creating WorkOrderLineItem of Adjustment Type -- Customer Debit,Status -- New
                for(WorkOrder wrk: caseWorkOrderList){
                    WOLIList.addAll(createWorkOrderLineItem('New',wrk.id,'Customer Debit','Yes','NA Adjustment'));
                }
            }
            insert WOLIList;
            //Query for fetching Offset Comment,Charge Type,X1099 Tax Report Flag,Department Code,Transaction Code and 
            //General Ledger Fields from WorkOrderLineItem List
            List<WorkOrderLineItem> wrkOrderLineItemListDebit=new List<WorkOrderLineItem>([Select Id,Offset_Comment__c,
                                                                                           Charge_Type__c,
                                                                                           X1099_Tax_Report_Flag__c,
                                                                                           Department_Code__c,	General_Ledger__c,
                                                                                           Adjustment_Reason_Code__c,
                                                                                           Adjustment_Type_Indicator__c,
                                                                                           Transaction_Code__c                                                                                            From WorkOrderLineItem
                                                                                           Where Id IN:WOLIList]);
            //Iterating WorkOrderLineItemList with Customer debit adjustment type to check the actual and 
            //expected values of updated fields on WOLI
            for(WorkOrderLineItem Item:wrkOrderLineItemListDebit){
                System.assertEquals('D140-4860245-',Item.Offset_Comment__c);
                System.assertEquals('M',Item.Adjustment_Type_Indicator__c);                
                System.assertEquals('60',Item.Charge_Type__c);
                System.assertEquals('No',Item.X1099_Tax_Report_Flag__c);
                System.assertEquals('86',Item.Adjustment_Reason_Code__c );
                System.assertEquals('238',Item.Transaction_Code__c);
                System.assertEquals('40',Item.Department_Code__c);
                System.assertEquals('4860245',Item.General_Ledger__c) ;
            }
        }
        Test.stopTest();
    }
    //******************************************************************************************
    // Method to create test data for WorkOrderLineItem of Customer Credit Adjustment Type with 
    // Show on statement as false for related WorkOrder to test WorkOrderLineItemTriggerHandler 
    // @return void
    // 25 May 2017 Neeraj Kumawat #T-603560
    //******************************************************************************************
    static testMethod void testCreateWoliCreditWhenShowStatementFalse()
    {
        createTestData(True,False,True);
        //creating reference record for Adjustment Type -- Customer Credit WorkOrderLineItem 
        System.runAs(relationshipUser){
            Test.startTest();
            //WebService Callout
            Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator('CollectionTotal'));
            //newCaseList for fetching related WorkOrder on a Case
            List<Case> newCaseList=[Select Id, Subject,Status,AccountId, (Select Id, Subject,RecordTypeId,Status ,AccountId
                                                                          From WorkOrders)
                                    From Case Where Id IN :caseList];
            List<WorkOrderLineItem> WOLIList=new List<WorkOrderLineItem>();
            for(Case cse: newCaseList){
                List<WorkOrder> caseWorkOrderList=cse.WorkOrders;
                //creating WorkOrderLineItem of Adjustment Type -- Customer Credit and Status -- New
                for(WorkOrder wrk: caseWorkOrderList){
                    WOLIList.addAll(createWorkOrderLineItem('New',wrk.id,'Customer Credit','No','NA Adjustment'));
                }
            }
            insert WOLIList;
            //Query for fetching Offset Comment,Charge Type,X1099 Tax Report Flag,Department Code,Transaction Code and 
            //General Ledger Fields from WorkOrderLineItem List
            List<WorkOrderLineItem> wrkOrderLineItemListCredit=new List<WorkOrderLineItem>([Select Id,Offset_Comment__c,
                                                                                            Charge_Type__c,
                                                                                            X1099_Tax_Report_Flag__c,
                                                                                            Department_Code__c,	General_Ledger__c,
                                                                                            Adjustment_Reason_Code__c,
                                                                                            Adjustment_Type_Indicator__c,
                                                                                            Transaction_Code__c                                                                                            From WorkOrderLineItem
                                                                                            Where Id IN:WOLIList]);
            //Iterating WorkOrderLineItemList with Customer credit adjustment type to check the actual and 
            //expected values of updated fields on WOLI
            for(WorkOrderLineItem Item:wrkOrderLineItemListCredit){	
                System.assertEquals('D140-4860245-',Item.Offset_Comment__c);
                System.assertEquals('M',Item.Adjustment_Type_Indicator__c);                
                System.assertEquals('60',Item.Charge_Type__c);
                System.assertEquals('Yes',Item.X1099_Tax_Report_Flag__c);
                System.assertEquals('86',Item.Adjustment_Reason_Code__c );
                System.assertEquals('149',Item.Transaction_Code__c);
                System.assertEquals('40',Item.Department_Code__c);
                System.assertEquals('4860245',Item.General_Ledger__c) ;
            }
        }
        Test.stopTest();
    }
    //******************************************************************************************
    // Method to create test data for WorkOrderLineItem of Customer Debit Adjustment Type with 
    // Show on statement as false for related WorkOrder to test WorkOrderLineItemTriggerHandler 
    // @return void
    // 25 May 2017 Neeraj Kumawat #T-603560
    //******************************************************************************************
    static testMethod void testCreateWoliDebitWhenShowStatementFalse()
    {
        createTestData(False,True,False);
        //creating reference record for Adjustment Type -- Customer Debit WorkOrderLineItem 
        System.runAs(relationshipUser){
            Test.startTest();
            //WebService Callout
            Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator('CollectionTotal'));
            List<Case> newCaseList=[Select Id, Subject,Status,AccountId, (Select Id, Subject,RecordTypeId,Status ,AccountId
                                                                          From WorkOrders)
                                    From Case Where Id IN :caseList];
            List<WorkOrderLineItem> WOLIList=new List<WorkOrderLineItem>();
            for(Case cse: newCaseList){
                List<WorkOrder> caseWorkOrderList=cse.WorkOrders;
                //creating WorkOrderLineItem of Adjustment Type -- Customer Debit and Status -- New
                for(WorkOrder wrk: caseWorkOrderList){   
                    WOLIList.addAll(createWorkOrderLineItem('New',wrk.id,'Customer Debit','No','NA Adjustment'));
                }
            }
            insert WOLIList;
            //Query for fetching Offset Comment,Charge Type,X1099 Tax Report Flag,Department Code,Transaction Code and 
            //General Ledger Fields from WorkOrderLineItem List
            List<WorkOrderLineItem> wrkOrderLineItemListDebit=new List<WorkOrderLineItem>([Select Id,Offset_Comment__c,
                                                                                           Charge_Type__c,
                                                                                           X1099_Tax_Report_Flag__c,
                                                                                           Department_Code__c,	General_Ledger__c,
                                                                                           Adjustment_Reason_Code__c,
                                                                                           Adjustment_Type_Indicator__c,
                                                                                           Transaction_Code__c                                                                                            From WorkOrderLineItem
                                                                                           Where Id IN:WOLIList]);
            //Iterating WorkOrderLineItemList with Customer credit adjustment type to check the actual and 
            //expected values of updated fields on WOLI
            for(WorkOrderLineItem Item:wrkOrderLineItemListDebit){
                System.assertEquals('D140-4860245-',Item.Offset_Comment__c);
                System.assertEquals('M',Item.Adjustment_Type_Indicator__c);                
                System.assertEquals('60',Item.Charge_Type__c);
                System.assertEquals('No',Item.X1099_Tax_Report_Flag__c);
                System.assertEquals('86',Item.Adjustment_Reason_Code__c );
                System.assertEquals('249',Item.Transaction_Code__c);
                System.assertEquals('40',Item.Department_Code__c);
                System.assertEquals('4860245',Item.General_Ledger__c) ;
            }
        }
        Test.stopTest();
    }
    //*********************************************************************************************************************
    // Method to verify that WorkOrder status should be In Progress on changing all workOrder status as Sent For Processing
    // @return void
    // 30 May 2017 Neeraj Kumawat T-605293/S-470343
    //*********************************************************************************************************************
    static testMethod void testWorkOrderInProgressWhenWOLIStatusSentForProcessing(){
        Test.startTest();
        //WebService Callout
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator('CollectionTotal'));
        List<WorkOrder> WOList=new List<WorkOrder>();
        List<WorkOrderLineItem> WOLIList=new List<WorkOrderLineItem>();
        createWorkOrder();
        for(WorkOrder wrk:workOrderList){	   
            WOLIList.addAll(createWorkOrderLineItem('Sent For Processing',wrk.id,'','','NA Adjustment'));
        }
        
        insert WOLIList;
        //Fetching workorder based on newly created workOrderlist
        List<WorkOrder> newWOList=[Select Status,(Select Id From WorkOrderLineItems Where Id IN:WOLIList) 
                                   FROM WorkOrder
                                   Where Id IN:workOrderList];
        //Checking Status of Workorder when there are WorkOrderLineItem with status Sent For Processing
        for(WorkOrder wrkOrder:newWOList){
            system.assertEquals('In Progress',wrkOrder.status);
        }
        Test.stopTest();
    }
    //************************************************************************************************************
    // Method to verify that WorkOrder status should be Closed on changing all workOrderLineItems status as Closed
    // @return void
    // 30 May 2017 Neeraj Kumawat T-605293/S-470343
    //************************************************************************************************************
    static testMethod void testWorkOrderClosedWhenWOLIStatusClosed(){
        Test.startTest();
        //WebService Callout
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator('CollectionTotal'));
        List<WorkOrderLineItem> WOLIList=new List<WorkOrderLineItem>();
        createWorkOrder();
        //Creating WorkOrderLineItem with Status As Adjustment Failed
        for(WorkOrder wrk:workOrderList){
            WOLIList.addAll(createWorkOrderLineItem('New',wrk.id,'','','NA Fee Maintenance'));
        }
        insert WOLIList;
        //Updating work order line item status to closed
        for(WorkOrderLineItem woli: WOLIList){
            woli.Status='Closed';
        }
        update WOLIList;
        //Fetching workorder based on newly created workOrderlist
        List<WorkOrder> newWOList=[Select Status,(Select Id From WorkOrderLineItems Where Id IN:WOLIList) 
                                   FROM WorkOrder
                                   Where Id IN:workOrderList];
        //Checking Status of Workorder when there are WorkOrderLineItem with status Adjustment Failed
        for(WorkOrder wrkOrder:newWOList){
            system.assertEquals('Closed',wrkOrder.status);
        }
        Test.stopTest();    
    }
    //************************************************************************************************************
    // Method to verify that WorkOrder status should be new if any workOrderLineItems status is New on a WorkOrder
    // @return void
    // 30 May 2017 Neeraj Kumawat T-605293/S-470343
    //************************************************************************************************************
    static testMethod void testWorkOrderNotClosedWhenWOLIStatusNotClosed(){
        Test.startTest();
        //WebService Callout
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator('CollectionTotal'));
        List<WorkOrderLineItem> WOLIList=new List<WorkOrderLineItem>();
        createWorkOrder();
        //Creating WorkOrderLineItem with Status As New
        for(WorkOrder wrk:workOrderList){
            WOLIList.addAll(createWorkOrderLineItem('New',wrk.id,'','','NA Adjustment'));
        }
        insert WOLIList;
        List<WorkOrderLineItem> closedWOLIList=new List<WorkOrderLineItem>();
        //Creating WorkOrderLineItem with Status As Adjustment Failed
        for(WorkOrder wrk:workOrderList){	   
            closedWOLIList.addAll(createWorkOrderLineItem('Adjustment Failed',wrk.id,'','','NA Adjustment'));
        }
        insert closedWOLIList;
        //Fetching workorder based on newly created workOrderlist
        List<WorkOrder> newWOList=[Select Status,(Select Id From WorkOrderLineItems Where Id IN:WOLIList) 
                                   FROM WorkOrder
                                   Where Id IN:workOrderList];
        //Checking Status of Workorder when there are WorkOrderLineItem with status New,Adjustment failed
        for(WorkOrder wrkOrder:newWOList){
            system.assertEquals('New',wrkOrder.status);
        }
        Test.stopTest();    
    }
    //************************************************************************************************************
    // Method to verify that WorkOrder status should be On Hold on changing all workOrderLineItems status as OnHold
    // @return void
    // 7 June 2017 Neeraj Kumawat T-608155/I-277751
    //************************************************************************************************************
    static testMethod void testWorkOrderOnHoldWhenWOLIStatusOnHold(){
        Test.startTest();
        //WebService Callout
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator('CollectionTotal'));
        List<WorkOrderLineItem> WOLIList=new List<WorkOrderLineItem>();
        createWorkOrder();
        //Creating WorkOrderLineItem with Status As On Hold
        for(WorkOrder wrk:workOrderList){	   
            WOLIList.addAll(createWorkOrderLineItem('On Hold',wrk.id,'','','NA Adjustment'));
        }
        insert WOLIList;
        //Fetching workorder based on newly created workOrderlist
        List<WorkOrder> newWOList=[Select Status,(Select Id From WorkOrderLineItems Where Id IN:WOLIList) 
                                   FROM WorkOrder
                                   Where Id IN:workOrderList];
        //Checking Status of Workorder when there are WorkOrderLineItem with status OnHold
        for(WorkOrder wrkOrder:newWOList){
            system.assertEquals('On Hold',wrkOrder.status);
        }
        Test.stopTest();    
    }
    //************************************************************************************************************
    // Method to verify that WorkOrder status should be Canceled on changing all workOrderLineItems status as Canceled
    // @return void
    // 7 June 2017 Neeraj Kumawat T-607554/S-470343
    //************************************************************************************************************
    static testMethod void testWorkOrderCanceledWhenWOLIStatusCanceled(){
        Test.startTest();
        //WebService Callout
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator('CollectionTotal'));
        List<WorkOrderLineItem> WOLIList=new List<WorkOrderLineItem>();
        createWorkOrder();
        //Creating WorkOrderLineItem with Status As Canceled
        for(WorkOrder wrk:workOrderList){	   
            WOLIList.addAll(createWorkOrderLineItem('Canceled',wrk.id,'','','NA Adjustment'));
        }
        insert WOLIList;
        //Fetching workorder based on newly created workOrderlist
        List<WorkOrder> newWOList=[Select Status,(Select Id From WorkOrderLineItems Where Id IN:WOLIList) 
                                   FROM WorkOrder
                                   Where Id IN:workOrderList];
        //Checking Status of Workorder when there are WorkOrderLineItem with status Canceled
        for(WorkOrder wrkOrder:newWOList){
            system.assertEquals('Canceled',wrkOrder.status);
        }
        Test.stopTest();    
    }
    //*********************************************************************************************************************
    // Method to verify that WorkOrder status should be not canceled if any workOrderLineItems status is New on a WorkOrder
    // @return void
    // 7 June 2017 Neeraj Kumawat T-607554/S-470343
    //*********************************************************************************************************************
    static testMethod void testWorkOrderNotCanceledWhenWOLIStatusNotCanceled(){
        Test.startTest();
        //WebService Callout
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator('CollectionTotal'));
        List<WorkOrderLineItem> WOLIList=new List<WorkOrderLineItem>();
        createWorkOrder();
        //Creating WorkOrderLineItem with Status As New
        for(WorkOrder wrk:workOrderList){
            WOLIList.addAll(createWorkOrderLineItem('New',wrk.id,'','','NA Adjustment'));
        }
        insert WOLIList;
        List<WorkOrderLineItem> canceledWOLIList=new List<WorkOrderLineItem>();
        //Creating WorkOrderLineItem with Status As Canceled
        for(WorkOrder wrk:workOrderList){	   
            canceledWOLIList.addAll(createWorkOrderLineItem('Canceled',wrk.id,'','','NA Adjustment'));
        }
        insert canceledWOLIList;
        //Fetching workorder based on newly created workOrderlist
        List<WorkOrder> newWOList=[Select Status,(Select Id From WorkOrderLineItems Where Id IN:WOLIList) 
                                   FROM WorkOrder
                                   Where Id IN:workOrderList];
        //Checking Status of Workorder when there are WorkOrderLineItem with status New,Canceled
        for(WorkOrder wrkOrder:newWOList){
            system.assertEquals('New',wrkOrder.status);
        }
        Test.stopTest();    
    }
}