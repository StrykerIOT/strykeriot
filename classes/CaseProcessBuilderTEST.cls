/*********************************************************************************************************************
Author        :  Appirio (Aashish Singh Sajwan)
Date          :  Sep 14th, 2016
Purpose       :  Test class for Case's Process builder

Created By    : Aashish Singh Sajwan
Created Date  : Sep 14th, 2016
Purpose       : Test class coverage for Case's Process Builder
**********************************************************************************************************************/
@istest

//****************************************************************************
// Test Class to test the for Case's Process Builder
//****************************************************************************

public class CaseProcessBuilderTEST {
    static testmethod void test() {
        
        Test.startTest();
        
        //Create User profile 
        String userProfile = 'Custom: NA Agent';
        
        //Get profile record
        List<Profile> profiles = [Select Id From Profile where Name = :userProfile limit 1];
        
        //Get current user Id
        Id userId = UserInfo.getUserId();
        
        //Create Case with current user id 
        List<Case> caseList = TestUtilities.createCase(1,userId ,true);
        Case c1 = [select id, OwnerId, Prior_Owner__c from case where id=:caseList[0].id];
        system.debug('** c1 owner value before**'+c1.OwnerID);
        system.debug('** c1 Prior_Owner__c  value before**'+c1.Prior_Owner__c );
        
        //Create test user record
        User  usr = TestUtilities.createTestUser(34, profiles.get(0).Id, true);
        
        //Update Case Owner Id
        system.debug('** owner value before**'+c1.OwnerId);
        c1.OwnerID= usr.id; 
        system.debug('** owner value  after**'+c1.OwnerID); 
        update c1;
        
        //Get Case records details 
        Case caseObj = [select id, OwnerId,Return_Case_to_Previous_Owner__c, Prior_Owner__c from case where id=:caseList[0].id];
        
        
         //Update Case Owner Id
        system.debug('** owner value before**'+caseObj .OwnerId);
        caseObj .Return_Case_to_Previous_Owner__c= true; 
        system.debug('** owner value  after**'+caseObj .OwnerID); 
        update caseObj ;
        //Check prior owner value to verify process builder execute
        system.debug('** owner value  after update**'+caseObj.OwnerID); 
        system.debug('** Prior owner value **'+caseObj.Prior_Owner__c);
          system.debug('** userId value **'+userId);
        System.assertEquals(caseObj.Prior_Owner__c,userId,'Prior Owner equal to user Id');  
        
        Test.stoptest(); 
        
    }
}