/***********************************************************************************************************************
* Appirio, Inc
* Name: WorkOrderApproverAssigmentCls
* Description: [# WorkOrder ]
* Created Date: [09/06/2017]
* Created By: [Aashsish Sajwan] (Appirio)
* 
* Date Modified                Modified By               Description of the update
***************************************************************************************************************/
global class WorkOrderApproverAssigmentCls
{
    webservice static void assignApprover(string userId,string WorkOrderId) // you can pass parameters
    { 
        system.debug('&& User Id&&'+userId);
        system.debug('&& WorkOrder Id&&'+WorkOrderId);
     
        User SubmitterUser = GetUserManagerDetails(userId);
        
        User Approver1 =  (SubmitterUser !=null && SubmitterUser.ManagerId!=null) ? GetUserManagerDetails(SubmitterUser.ManagerId) : null;
        system.debug('&& Approver 1 Value &&'+Approver1);
        User Approver2 =  (Approver1!=null && Approver1.ManagerId!=null) ? GetUserManagerDetails(Approver1.ManagerId) : null;
         system.debug('&& Approver 2 Value &&'+Approver2);
        User Approver3 =  (Approver2!=null && Approver2.ManagerId!=null) ? GetUserManagerDetails(Approver2.ManagerId) : null;
        system.debug('&& Approver 3 Value &&'+Approver3);
        User Approver4 =  (Approver3!=null && Approver3.ManagerId!=null) ? GetUserManagerDetails(Approver3.ManagerId) : null;
         system.debug('&& Approver 4 Value &&'+Approver4);
        User Approver5 =  (Approver4!=null && Approver4.ManagerId!=null) ? GetUserManagerDetails(Approver4.ManagerId) : null;
         system.debug('&& Approver 5 Value &&'+Approver5);
        User Approver6 =  (Approver5!=null && Approver5.ManagerId!=null) ? GetUserManagerDetails(Approver5.ManagerId) : null;
         system.debug('&& Approver 6 Value &&'+Approver6);
        User Approver7 =  (Approver6!=null && Approver6.ManagerId!=null) ? GetUserManagerDetails(Approver6.ManagerId) : null;
         system.debug('&& Approver 7 Value &&'+Approver7);
        
        WorkOrder objWorkOrder = [SELECT Id, Account_Processing_Center_ID__c,Work_Order_Total__c ,
                                  Approver_1__c,Approver_2__c,CurrencyIsoCode 
                                  From WorkOrder 
                                  WHERE Id =:WorkOrderId 
                                  LIMIT 1];
        boolean isChange = false;
        if(objWorkOrder != null){
          system.debug('&& Approver1 Value &&'+Approver1.Adjustment_Approval_Limit_USD__c);
          system.debug('&& CurrencyIsoCode  Value &&'+Approver1.CurrencyIsoCode );
          system.debug('&& Account_Processing_Center_ID__c Value &&'+objWorkOrder.Account_Processing_Center_ID__c);
          if(Approver1!=null && (objWorkOrder.Account_Processing_Center_ID__c =='NA' || objWorkOrder.Account_Processing_Center_ID__c =='QUERE')  && objWorkOrder.CurrencyIsoCode =='USD'){
               objWorkOrder.Approver_1__c = Approver1.Id;
               if(Approver2!=null && Approver2.Adjustment_Approval_Limit_USD__c!=null && objWorkOrder.Work_Order_Total__c < Approver2.Adjustment_Approval_Limit_USD__c){
                 isChange = true;
                 objWorkOrder.Approver_2__c =  Approver2.Id; 
               }
               else if(Approver3!=null && Approver3.Adjustment_Approval_Limit_USD__c!=null && objWorkOrder.Work_Order_Total__c < Approver3.Adjustment_Approval_Limit_USD__c){
                isChange = true;
                 objWorkOrder.Approver_2__c =  Approver3.Id;
               }
               else if(Approver4!=null && Approver4.Adjustment_Approval_Limit_USD__c!=null && objWorkOrder.Work_Order_Total__c < Approver4.Adjustment_Approval_Limit_USD__c){
                isChange = true;
                 objWorkOrder.Approver_2__c =  Approver4.Id;
               }
               else if(Approver5!=null && Approver5.Adjustment_Approval_Limit_USD__c!=null && objWorkOrder.Work_Order_Total__c < Approver5.Adjustment_Approval_Limit_USD__c){
                isChange = true;
                objWorkOrder.Approver_2__c =  Approver5.Id;               
               }else if(Approver6!=null && Approver6.Adjustment_Approval_Limit_USD__c!=null && objWorkOrder.Work_Order_Total__c < Approver6.Adjustment_Approval_Limit_USD__c){
               isChange = true;
                objWorkOrder.Approver_2__c =  Approver6.Id;
               }else if(Approver7!=null && Approver7.Adjustment_Approval_Limit_USD__c!=null && objWorkOrder.Work_Order_Total__c < Approver7.Adjustment_Approval_Limit_USD__c){
                isChange = true;
                objWorkOrder.Approver_2__c =  Approver7.Id;
               }
                
              
          }else if(Approver1!=null && objWorkOrder.Account_Processing_Center_ID__c =='QUERE' && objWorkOrder.CurrencyIsoCode == 'MXN'){
              objWorkOrder.Approver_1__c = Approver1.Id;
               if(Approver2!=null && Approver2.Adjustment_Approval_Limit_MXN__c!=null && objWorkOrder.Work_Order_Total__c < Approver2.Adjustment_Approval_Limit_MXN__c){
                isChange = true;
                 objWorkOrder.Approver_2__c =  Approver2.Id; 
              
               }else if(Approver3!=null && Approver3.Adjustment_Approval_Limit_MXN__c!=null && objWorkOrder.Work_Order_Total__c < Approver3.Adjustment_Approval_Limit_MXN__c){
               isChange = true;
                 objWorkOrder.Approver_2__c =  Approver3.Id;
               
               }
               else if(Approver4!=null && Approver4.Adjustment_Approval_Limit_MXN__c!=null && objWorkOrder.Work_Order_Total__c < Approver4.Adjustment_Approval_Limit_MXN__c){
               isChange = true;
                 objWorkOrder.Approver_2__c =  Approver4.Id;
               
               }else if(Approver5!=null && Approver5.Adjustment_Approval_Limit_MXN__c!=null && objWorkOrder.Work_Order_Total__c < Approver5.Adjustment_Approval_Limit_MXN__c){
                isChange = true;
                objWorkOrder.Approver_2__c =  Approver5.Id;               
              
               }else if(Approver6!=null && Approver6.Adjustment_Approval_Limit_MXN__c!=null && objWorkOrder.Work_Order_Total__c < Approver6.Adjustment_Approval_Limit_MXN__c){
                isChange = true;
                objWorkOrder.Approver_2__c =  Approver6.Id;
               
               }else if(Approver7!=null && Approver7.Adjustment_Approval_Limit_MXN__c!=null && objWorkOrder.Work_Order_Total__c < Approver7.Adjustment_Approval_Limit_MXN__c){
               isChange = true;
                objWorkOrder.Approver_2__c =  Approver7.Id;
               
               }
           
           
          }
           system.debug('objWorkOrder value@@@@='+objWorkOrder );
           system.debug('objWorkOrder Approver 1 value='+objWorkOrder.Approver_1__c );
           system.debug('objWorkOrder Approver 2 value='+objWorkOrder.Approver_2__c );
           if(isChange){
            update objWorkOrder;
           }
        }
        
        
    }
    
    //GetUserManagerDetails 
    private static User GetUserManagerDetails(string UserId){
        User userManager = null;
        if(UserId!=null){
            userManager=[SELECT Id, Adjustment_Approval_Limit_USD__c,ManagerId,
                         Adjustment_Approval_Limit_MXN__c,
                         Profile.Name,CurrencyIsoCode
                         FROM User
                         WHERE Id =: UserId
                         Limit 1];
        }
        system.debug('userManager value='+userManager);
        return userManager;
    }
    
    
}