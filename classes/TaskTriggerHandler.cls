/*********************************************************************************************************************************
* Appirio, Inc
* Name: TaskTriggerHandler
* Description: [T-561974-HandlerClass - For updating Case Status from Task]
* Created Date: [12/15/2016]
* Created By: [Neeraj Kumawat] (Appirio)
* 
* Date Modified                Modified By                  Description of the update
*  [01/04/2017]              [Gaurav Dudani]              [Updated to increase the count of field on case as per task # T-565229]
*  [02/03/2017]              [Aashish Sajwan]             [Update to increase to count on case on send email and code opitimization task # T-575846]
*  [02/08/2017]              [Neeraj Kumawat]             [Update Counter On Task Update # T-576694]
*  [02/27/2017]              [Kate Jovanovic]             [Removing OnBeforeInsert T-580692]
*  [03/03/2017]              [Shannon Howe]               [Fix test class errors]
*  [05/11/2017]              [Neeraj Kumawat]             [Added Method restrictSendingEmailOnInactiveContact #T-597354]
**********************************************************************************************************************************/
public class TaskTriggerHandler {
    
/*****************************************************************
* Method for calling action on Before Insert of Task
******************************************************************/
    
    //Changed by Aashish Sajwan 4 Feb 2017
    public static void onAfterInsert(List<Task> newTaskList){
        system.debug('&& filterCases calling on AfterInsert &&'+newTaskList);
        filterCases(newTaskList);
        /*****************************************************************
        //Added this method as per task T-597354
        //05/11/2017 Neeraj Kumawat T-597354
        ******************************************************************/
        restrictSendingEmailOnInactiveContact(newTaskList);
    }
    
    //Changed by Aashish Sajwan 4 Feb 2017
    public static void onAfterDelete(List<Task> newTaskList){
        system.debug('&& filterCases calling on onAfterDelete &&'+newTaskList);
        filterCases(newTaskList);
    }
/**
Method for Updating Case Counter on Update of Task. Changed by Neeraj Kumawat 8 Feb 2017
**/
    public static void onAfterUpdate(List<Task> newTaskList,Map<Id,Task> oldMap){
        system.debug('&& filterCases calling on AfterUpdate &&'+newTaskList);
        filterCases(newTaskList);
    }  
/*****************************************************************
* Method for Filtering Parent Case from Task 
******************************************************************/
    
    private static void filterCases(List<Task> taskList){
        system.debug('&& taskList values &&'+taskList);
        Set<String> caseIdSet = new Set<String>();
        for (Task objTask: taskList){
            String whatIdStr=objTask.WhatId;
            if (!String.IsBlank(whatIdStr) 
                && whatIdStr.substring(0, 3)=='500'){
                    caseIdSet.add(whatIdStr);
                }
        }
        system.debug('&& caseIdSet values &&'+caseIdSet);
        if (caseIdSet.size() > 0) {
            UtilityCls.updateCaseFields(caseIdSet);
            //updateCaseFields(caseIdSet,taskList);
        }
    }
    //****************************************************************************
    // Method to restrict send email on Inactive Contact
    // @param taskList: List of Task
    // @return void
    // 05/11/2017 Neeraj Kumawat T-597354
    //****************************************************************************
    private static void restrictSendingEmailOnInactiveContact(List<Task> taskList){
        Set<Id> contactIds=new Set<Id>();
        for(Task tsk: taskList){
            if(tsk.Type=='Email'){
                contactIds.add(tsk.WhoId);
            }
        }
        Map<Id,Contact> contactMap=new Map<Id,Contact>([Select Id, Contact_Status__c From Contact Where Id IN: contactIds]);
        for(Task tsk: taskList){
            if(tsk.WhoId!=null){
                Contact con= contactMap.get(tsk.WhoId);
                //Checking contact status
                if(con!=null && con.Contact_Status__c=='Inactive'){
                    //Throwing error when contact is inactive
                    tsk.addError('You cannot email an Inactive Contact');
                }
            }
        }
    }
}