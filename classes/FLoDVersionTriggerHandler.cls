/***********************************************************************************************************************
 * Appirio, Inc
 * Class Name: FLoDVersionTriggerHandler
 * Description: [T-602006 FLoDVersionTrigger handler class]
 * Created Date: [12/05/2017]
 * Created By: [Gaurav Dudani] (Appirio)
 * 
 * Date Modified                Modified By               Description of the update
 * [14-06-2017]               [Gaurav Dudani]             [Added comments to the code]
 ***********************************************************************************************************************/
// 
public class FLoDVersionTriggerHandler {
  
    //****************************************************************************
    // Method for calling action on Before Insert
    // By Gaurav Dudani on 12 May 2017 as per task # T-602006.
    //****************************************************************************
    public static void beforeInsert(List<FLoD_Review__c> newFLoDList) {
      associateChildToParnetRecord(newFLoDList);
    }
    
    public static void associateChildToParnetRecord(List<FLoD_Review__c> newFLoDList){
      
      // Set of New master Article Id
      Set<String> newMasterArticleIdSet = new Set<String>();
      // Set of New master Article Id
      Set<String> oldMasterArticleIdSet = new Set<String>();
      
      // Fetching Articles with version greater then 1 and adding it to new master article Id set.
      for(FLoD_Review__c flodReview: newFLoDList){
        if(Integer.valueOf(flodReview.Version_Number__c) > 1 && flodReview.Master_Article_Id__c !=null){
          newMasterArticleIdSet.add(flodReview.Master_Article_Id__c);
        }
      }
      
      // Comparing the old and new master Id set and added new article as child of old if Verision 1 already exists.
      for(FLoD_Review__c oldFlodReview: [SELECT Id, Master_Article_Id__c from FLoD_Review__c WHERE Master_Article_Id__c IN:newMasterArticleIdSet AND Version_Number__c ='1']){
         for(FLoD_Review__c newFlodReview :newFLoDList) {
           if(oldFlodReview.Master_Article_Id__c == newFlodReview.Master_Article_Id__c) {
             newFlodReview.Article_Audit__c = oldFlodReview.Id;
           }
         }
      } 
    }
}