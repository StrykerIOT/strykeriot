/***************************************************************************
// (c) 2015 Appirio, Inc.
//
// Description    : SchedulableExternalDocumentBatch  - Delete Old ExternalDocumentRecord
//                : 
//                  
// 5 January 2016 Aahish Sajwan Original (T-563913)
//***************************************************************************/

global class SchedulableExternalDocumentBatch implements Schedulable {
    global void execute(SchedulableContext sc) {
      ExternalDocumentRecordsDeletion externalDocDeleteBatch = new ExternalDocumentRecordsDeletion();
      Database.executeBatch(externalDocDeleteBatch);     
    }
}