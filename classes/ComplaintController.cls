/************************************************************************
* Appirio, Inc
* Class Name: ComplaintDetailsSave 
* Description: [Task # T-590429 Extension Controller for VF Page ComplaintDetailsSave.]
* Created Date: [03/31/2017]
* Created By: [Gaurav Dudani] (Appirio)
* Updated On: 15 May 2017 As per Story # S-470559 By Poornima Bhardwaj
* [30-May-2017]            [Aashish Sajwan]                [Apply Users Profile Condition as well current user is member of Check CET Permission Set]
***********************************************************************/
public with sharing class ComplaintController {
    private ApexPages.StandardController sc;
    public User usr {get;set;}
    public String RecTypeName{set;get;}                                    //Added for Record Type Name
    public Boolean IsChecked_Significant_Ops_Loss{set;get;}
    public Boolean IsCETMember{set;get;}
    public string attemptError ='How and when did you resolve the issue: value required.' ;
    public String otherDepartmentError='Since you have selected "other" as your deparment, department other value is required';
    // Constructor for ComplaintController Class.
    public ComplaintController(ApexPages.StandardController sc){
        this.sc = sc;
        String userId = UserInfo.getUserId();
        
        usr = [SELECT name from User where ID=:UserInfo.getUserId()];
        // 30-May-2017  Aashish Sajwan Find Current User Profile and Check Current user is member of CET Permission Set
        //Start
        String ProfileName = [SELECT Name 
                             FROM profile
                             Where Id =:userinfo.getProfileId()].Name;
        List<PermissionSetAssignment> PermissionSetList = [SELECt PermissionSet.Name
                                FROM PermissionSetAssignment
                                WHERE Assignee.Id = : UserInfo.getUserId() 
                                AND PermissionSet.Name = 'Customer_Engagement_Team'];  
       //Set Boolean Variable if user is member of Customer_Engagement_Team permission set or not
       IsCETMember  = (PermissionSetList!=null && PermissionSetList.size()>0) ?true:false;
                                
       //Check Current User Profile and set RecTypeName variable accordingly                   
       if(ProfileName =='Custom: MX Agent' || ProfileName =='Custom: MX Manager'){
           RecTypeName = 'MX';
           Complaint__c complaint = (Complaint__c)sc.getRecord();
           complaint.Other_Agency_ID__c='SAN';
           complaint.OwnerID=userinfo.getuserid();
          
        }else{
          RecTypeName = 'NA';
        }
       
       //End
       
       // string CaseId = ApexPages.currentPage().getParameters().get('q_lkid')!=''?ApexPages.currentPage().getParameters().get('q_lkid'):'';
       /*
        List<RecordType> RecTypeList= [select Name from RecordType where id=:(ApexPages.currentPage().getParameters().get('RecordType'))];
        if(RecTypeList.size()>0){
            RecTypeName=RecTypeList[0].Name;
            Complaint__c complaint = (Complaint__c)sc.getRecord();
            if(RecTypeName=='MX'){
                complaint.Other_Agency_ID__c='SAN';
                complaint.OwnerID=userinfo.getuserid();
            }
        }
        */
        



       
    }
    // Method to override Satandard Save & New button.
    public Pagereference saveAndNew() {
        SObject so = sc.getRecord();
        upsert so;
        string s = '/' + ('' + so.get('Id')).subString(0, 3) + '/e?';
        ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.Info, s));
        return new Pagereference(s);
    } 
 }