/**********************************************************************
* Appirio, Inc
* Test Class Name: ContactTriggerHandlerTEST
* Class Name: ContactTriggerHandler
* Description: Handler test for contact; bulk testing
* Created Date:  Dec 28th, 2016
* Created By: [Neeraj Kumawat] (Appirio)
* 
* Date Modified                Modified By              Description of the update
* [Dec 02, 2016]               [Neeraj Kumawat]              [Test Class Creation]
* [Feb 19 2017]                  [Shannon Howe]             [Created]
* [Feb 21 2017]                  [Aashish Sajwan]           [Optimization and Code Coverage #T-579350]
* [Mar 3 2017]                   [Aashish Sajwan]           [Optimization and Code Coverage #T-579350]
* [Mar 30 2017]                  [Aashish Sajwan]           [Sync'd class name as ContactTriggerHandlerTEST ]
* [13/04/2017]                   [Neeraj Kumawat]           [Added test method to check for inactive contact status #T-593628]
* [31/05/2017]                   [Neeraj Kumawat]           [Updated the method to create the case as in progress and then close it.]
**********************************************************************************************************************/
@isTest
//****************************************************************************
// Test Class to test the ContactTriggerHandler.cls
//****************************************************************************
public class ContactTriggerHandlerTEST{
    
    /***************************************************************************************
// Test method to validate billing address and legal address on accounts are updated
****************************************************************************************/
    
    static string taxId='123456';
    static string newTaxId='1234567890';
    static List<Account> accountlst;
    static List<Contact> contactLst = new List<Contact>();    
    private static Integer counter=10;
    static List<Case> caseList=new List<Case>();
    
    
    //****************************************************************************
    // Method to create case on provide contact list
    // @param status: status of case
    // @param contactList: list of Contact
    // @return void
    //**************************************************************************** 
    private static  void createCaseRecord(String status,List<Contact> contactList){
        caseList=TestUtilities.createCase(counter,null,false);
        for(Integer i=0; i<counter; i++){
            caseList[i].ContactId=contactList.get(i).Id;
            caseList[i].status=status;
            if(status=='closed'){
                caseList[i].Internal_Comments__c='test internal comments';
                caseList[i].Reason='Cancelled';
            }
        }
        insert caseList;
    }
    public testmethod static void testCallout(){
        //Test for Insert
        //***************************************************************************************************
        // List of records created using createAccount & createContact method from TestUtilities class
        //***************************************************************************************************
        ContactTriggerHandlerTest.testForInsert();
        System.debug('account Created Tax Id'+accountlst);
        System.debug('contact Created with Tax Id of Account'+contactLst);
        //Check taxId
        //****************************************************************************
        // Check Tax Id of Related Contacts and Accounts
        //****************************************************************************
        //ContactTriggerHandlerTest.checkTaxId(taxId);
        
        //Test for Update
        //****************************************************************************
        // Test Tax Id with account change and Check Contacts Tax Id
        //****************************************************************************
        ContactTriggerHandlerTest.testForUpdate();
        System.debug('New Account Created Tax Id'+accountlst);
        //Check taxId
        //****************************************************************************
        // Check Tax Id with updated Account 
        //****************************************************************************
        //ContactTriggerHandlerTest.checkTaxId(newTaxId);
    }
    //****************************************************************************
    // Method to create Contact and Delete it and then undelete it
    // @return void
    //**************************************************************************** 
    public testmethod static void testCallout2(){
        List<Account> acc=TestUtilities.createAccount(1, true);
        List<Contact> con=TestUtilities.createContact(1, acc[0].id, true);
        delete con;
        undelete con;
    }
    //Test Data For Account and Related Contacts and Check Insert Functionality
    static void testForInsert(){
        accountlst=TestUtilities.createAccount(200,false);
        for(Integer i=0; i<accountlst.size(); i++){
            accountlst[i].Tax_Id_Encrypted__c=taxId+i;
        }
        insert accountlst;
        System.debug('Account List value='+accountlst);
        for(account a: accountlst){
            contactLst.addAll(TestUtilities.createContact(1,a.id,false));
        }
        insert contactLst;
        
    }
    //Test Data for New Account and old Contacts for Testing update
    static void testForUpdate(){
        accountlst=TestUtilities.createAccount(200,false);
        for(Integer i=0; i<accountlst.size(); i++){
            accountlst[i].Tax_Id_Encrypted__c=newTaxId+i;
        }
        insert accountlst;
        
        //update account for all contacts to check the allocation on Tax Id of New Account 
        for(Integer i=0; i<contactLst.size(); i++){
            contactLst[i].AccountId=accountlst[i].Id;
        }
        update contactLst;
    }
    
    
    
    //Helper method for CreateTestData for Account and Contact
    private static void CreateTestData(String Suffix, String MiddleName,string Suffix_Custom,String MiddleName_Custom){
        
        //Create test accounts
        Id RecordType = [Select Id from RecordType Where Name='Relationship'].Id;
        List<Account> accntList = new List<Account>();
        for(integer i=0; i<counter; i++){            
            Account a = new Account(Name = 'Testing '+ i,
                                    RecordTypeId  = RecordType,
                                    CurrencyIsoCode = 'MXN',
                                    Processing_Center_Id__c = 'QUERE'
                                   );
            accntList.add(a);
        }
        
        insert accntList;
        
        
        
        List<Contact> c1List = new List<Contact>();
        
        for(integer i=0; i<counter; i++){
            Contact c1 = new Contact (
                
                AccountId = accntList[i].id,
                FirstName = 'Testing First Name ' + i,
                LastName = 'Last Name' + i,
                MiddleName = (MiddleName!=null)?  MiddleName + i: MiddleName,
                MiddleName__c = (MiddleName_Custom!=null)?  MiddleName_Custom + i: MiddleName_Custom,
                Suffix = (Suffix!=null)?  Suffix + i: Suffix,
                Suffix__C = (Suffix_Custom!=null)? Suffix_Custom + i :Suffix_Custom //
            );
            c1List.add(c1);
            
        }
        insert c1List;
    }
    
    //Get Contact Method to retrive contact records
    private Static List<Contact> GetContact(){
        List<Contact> cList =new List<Contact>();
        cList = [SELECT Id, AccountId, FirstName, LastName, MiddleName, 
                 MiddleName__c, Suffix,Suffix__c
                 FROM Contact 
                 WHERE FirstName 
                 LIKE 'Testing%'
                 Limit: Counter];
        return cList;
        
    }
    
    //Scenario-1:Standard Suffix and Standard Middle Name is null
    static testMethod void insertContactTestMethod(){
        String profileId = [select id from Profile where name ='Custom: Relationship Manager' limit 1].Id;
        User usr = TestUtilities.createTestUser(1,profileId,true);      
        System.runAs(usr){
            test.startTest();
            //First Condition when Custom Suffix ans Custom Middle Name is null
            CreateTestData(null,null,'Suffix Custom ','Middle Custom '); 
            List<Contact> cList2 = GetContact();            
            system.debug('** cList2 Values **'+cList2);
            system.debug('** cList2 Values **'+cList2.size());
            for(Integer ia=0; ia<counter; ia++){
                System.assertEquals(cList2[ia].MiddleName__c, cList2[ia].MiddleName);
                System.assertEquals(cList2[ia].Suffix__c, cList2[ia].Suffix);
                
            }
            test.stopTest();
        }
    }//end insert test method
    
    
    //Scenario-2:Custom Middle Name__c and Custom Suffix__c null
    static testMethod void insertContactTestMethod1(){
        String profileId = [select id from Profile where name ='Custom: Relationship Manager' limit 1].Id;
        User usr = TestUtilities.createTestUser(1,profileId,true);      
        System.runAs(usr){
            test.startTest();
            CreateTestData('Middle','Suffix',null,null);
            List<Contact> cList2 = GetContact();
            
            for(Integer ia=0; ia<counter; ia++){
                System.assertEquals(cList2[ia].MiddleName__c, cList2[ia].MiddleName);
                System.assertEquals(cList2[ia].Suffix__c, cList2[ia].Suffix);
                
            }
            test.stopTest();
        }
        
        
    }//end insert test method
    
    //Secnario-3: Standard Suffix , Standard Middle Name, Middle Name__c and Suffix__c not null
    static testMethod void insertContactTestMethod2(){
        String profileId = [select id from Profile where name ='Custom: Relationship Manager' limit 1].Id;
        User usr = TestUtilities.createTestUser(1,profileId,true);      
        System.runAs(usr){
            test.startTest();
            
            CreateTestData('Suffix ','Middle ','Middle Custom Test ','Suffix Custom Test ');
            List<Contact> cList2 = GetContact();
            
            for(Integer ia=0; ia<counter; ia++){
                System.assertEquals(cList2[ia].MiddleName__c, cList2[ia].MiddleName);
                System.assertEquals(cList2[ia].Suffix__c, cList2[ia].Suffix);
                
            }
            test.stopTest(); 
        }
    }//end insert test method
    
    
    //Secnario-3: update Standard Suffix , Standard Middle Name, Middle Name__c and Suffix__c 
    static testMethod void updateContactTestMethod1(){
        String profileId = [select id from Profile where name ='Custom: Relationship Manager' limit 1].Id;
        User usr = TestUtilities.createTestUser(1,profileId,true);      
        System.runAs(usr){
            test.startTest();
            CreateTestData('Suffix ','Middle ','Middle Custom ','Suffix Custom ');
            List<Contact> cList2 = GetContact();
            
            for(Integer ia=0; ia<counter; ia++){
                System.assertEquals(cList2[ia].MiddleName__c, cList2[ia].MiddleName);
                System.assertEquals(cList2[ia].Suffix__c, cList2[ia].Suffix);
                
            }
            
            List<Contact> cList3 = GetContact();
            
            List<Contact> c4List = new List<Contact>();
            //loop through list to test updates
            for(Integer ia=0; ia<counter; ia++){
                Contact objCon =new Contact();
                objCon.Id = cList3[ia].Id;
                objCon.FirstName = 'Testing Second Name_'+ia;
                objCon.LastName =  'Testing Last Name_'+ia;
                objCon.MiddleName = 'Middle ' ;
                objCon.MiddleName__c = 'Middle Custom Test';
                objCon.Suffix = 'Suffix Test' ;
                objCon.Suffix__C = 'Suffix Custom-Test' ;
                c4List.add(objCon);
            }
            
            update c4List;
            
            
            //loop through list to test updates
            for(Integer ia=0; ia<counter; ia++){
                c4List[ia].FirstName = 'Testing Second_'+ia;
                c4List[ia].LastName = 'Last Name_Test_'+ia ;
                c4List[ia].MiddleName = 'Middle ' ;
                c4List[ia].MiddleName__c = 'Middle Custom Test';
                c4List[ia].Suffix = 'Suffix' ;
                c4List[ia].Suffix__C = 'Suffix Custom-Test - Again' ;
                
            }
            
            update c4List;
            
            List<Contact> c4List2 = [SELECT Id, AccountId, FirstName, LastName, MiddleName, MiddleName__c, Suffix,
                                     Suffix__c
                                     FROM Contact WHERE FirstName LIKE 'Testing Second%' LIMIT :counter];
            for(Integer ia=0; ia<counter; ia++){
                System.assertEquals(c4List2[ia].MiddleName__c, c4List2[ia].MiddleName);
                System.assertEquals(c4List2[ia].Suffix__c, c4List2[ia].Suffix);
                
            }
            
            
            
            //New Code Aashish 3-March 2017
            //Start
            c4List.clear();
            for(Integer ia=0; ia<counter; ia++){
                cList3[ia].FirstName = 'Testing Second Name_'+ia;
                cList3[ia].LastName = 'Last Test Name '+ia;
                cList3[ia].MiddleName = 'Middle Again' ;
                cList3[ia].MiddleName__c = 'Middle Custom Test ';
                cList3[ia].Suffix = 'Suffix' ;
                cList3[ia].Suffix__C = 'Suffix Custom-Test - ' ;
                c4List.add(cList3[ia]);
            }
            
            update c4List;
            
            
            //End 
            
            
            
            test.stopTest();
        }
    }//end Update test method
    
    //****************************************************************************
    // Method to show error message when contact status changed to inactive which has open cases
    // @return void
    //****************************************************************************  
    static testMethod void testContactInactiveStatusWithOpenCase(){
        String profileId = [select id from Profile where name ='Custom: Relationship Manager' limit 1].Id;
        User usr = TestUtilities.createTestUser(1,profileId,true);
        //Run with custom Relationship Manager
        System.runAs(usr){
            test.startTest();
            CreateTestData('Suffix','Middle','Suffix Custom ','Middle Custom '); 
            List<Contact> cList2 = GetContact();
            createCaseRecord('In Progress',cList2);
            for(Contact objContact : cList2){
                objContact.Contact_Status__c='Inactive';
            }
            List<Database.SaveResult> results =Database.update(cList2,false);
            for(Database.SaveResult result : results){
                System.assert(!result.isSuccess());
                if (!result.isSuccess()){
                    for (Database.Error err : result.getErrors()){
                        System.assertEquals('Contact with open cases or tasks can not be inactive.', err.getMessage());
                    }
                }
            }
            test.stopTest();
        }
    }
    //****************************************************************************
    // Method to check error message when contact status changed to inactive which has close cases
    // @return void
    // 31 May 2017 Neeraj Kumawat Updated the method to create the case as in progress and then close it.
    //****************************************************************************  
    static testMethod void testContactInactiveStatusWithCloseCase(){
        String profileId = [select id from Profile where name ='Custom: Relationship Manager' limit 1].Id;
        User usr = TestUtilities.createTestUser(1,profileId,true);      
        System.runAs(usr){
            test.startTest();
            CreateTestData('Suffix','Middle','Suffix Custom ','Middle Custom '); 
            List<Contact> cList2 = GetContact();
            createCaseRecord('In Progress',cList2);
            //Closing the case
            for(Case cse: caseList){
                cse.Status='Closed';
                cse.Internal_Comments__c='test internal comments';
                cse.Reason='Cancelled';
            }
            update caseList;
            for(Contact objContact : cList2){
                objContact.Contact_Status__c='Inactive';
            }
            List<Database.SaveResult> results =Database.update(cList2,false);
            for(Database.SaveResult result : results){
                System.assert(result.isSuccess());
            }
            test.stopTest();
        }
    }
    
    //****************************************************************************
    // Method to check error message when contact status changed to inactive which has open task
    // @return void
    //**************************************************************************** 
    static testMethod void testContactInactiveStatusWithOpenTask(){
        String profileId = [select id from Profile where name ='Custom: Relationship Manager' limit 1].Id;
        User usr = TestUtilities.createTestUser(1,profileId,true);      
        System.runAs(usr){
            test.startTest();
            CreateTestData('Suffix','Middle','Suffix Custom ','Middle Custom '); 
            List<Contact> cList2 = GetContact();
            createTaskRecord('New',cList2);
            for(Contact objContact : cList2){
                objContact.Contact_Status__c='Inactive';
            }
            List<Database.SaveResult> results =Database.update(cList2,false);
            for(Database.SaveResult result : results){
                System.assert(!result.isSuccess());
                if (!result.isSuccess()){
                    for (Database.Error err : result.getErrors()){
                        System.assertEquals('Contact with open cases or tasks can not be inactive.', err.getMessage());
                    }
                }
            }
            test.stopTest();
        }
    }
    //****************************************************************************
    // Method to check error message when contact status changed to inactive which has close task
    // @return void
    //****************************************************************************  
    static testMethod void testContactInactiveStatusWithCloseTask(){
        String profileId = [select id from Profile where name ='Custom: Relationship Manager' limit 1].Id;
        User usr = TestUtilities.createTestUser(1,profileId,true);      
        System.runAs(usr){
            test.startTest();
            CreateTestData('Suffix','Middle','Suffix Custom ','Middle Custom '); 
            List<Contact> cList2 = GetContact();
            createTaskRecord('Complete',cList2);
            for(Contact objContact : cList2){
                objContact.Contact_Status__c='Inactive';
            }
            List<Database.SaveResult> results =Database.update(cList2,false);
            for(Database.SaveResult result : results){
                System.assert(result.isSuccess());
            }
            test.stopTest();
        }
    }
    //****************************************************************************
    // Method to create task on provide contact list
    // @param status: status of task
    // @param contactList: list of Contact
    // @return void
    //**************************************************************************** 
    private static  void createTaskRecord(String status,List<Contact> contactList){
        List<Task> taskList=TestUtilities.createTask(counter,false);
        for(Integer i=0; i<counter; i++){
            taskList[i].WhoId=contactList.get(i).Id;
            taskList[i].Status=status;
        }
        insert taskList;
    }
}