/***************************************************************************
// (c) 2015 Appirio, Inc.
//
// Description    : Schedulable Hourly Batch Class - Article Audit
//                : 
//                  
// 6 May 2016 Gaurav Dudani Original (S-442186)
//***************************************************************************/
global  class ScheduleFLoDReviewBatchClass implements Schedulable {
   global void execute(SchedulableContext sc) {
      FLoDReviewBatchClass flodReviewObj= new FLoDReviewBatchClass();
      database.executeBatch(flodReviewObj);
   }
}