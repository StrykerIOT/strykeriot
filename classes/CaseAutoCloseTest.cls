/*********************************************************************************************************************
* Author        :  Appirio (Neeraj Kumawat)
* Date          :  Sep 9th, 2016
* Purpose       :  Test class for CaseAutoClose.cls
* Updated Date  : Nov 11th, 2016
* Purpose       : Test class coverage for CaseAutoClose.cls
*
* Date Modified                Modified By                  Description of the update
*
**********************************************************************************************************************/
@isTest

//****************************************************************************
// Test Class to test the CaseAutoClose.cls
//****************************************************************************

public class CaseAutoCloseTest {
    
    public static Integer numberofRecords=1;
    /** Test Data Without Contact **/
    public static List<Case> createTestData(String caseStatus){
        
        //****************************************************************************
        // List of records created using createCase method from TestUtilities class
        //****************************************************************************
        Id UserId = UserInfo.getUserId();
        //Creating Account record
        List<Account> accountList=TestUtilities.createAccount(1,true);
        //Creating case record
        List<Case> caseList = TestUtilities.createCase(numberofRecords,UserId,false);
        for(Case objCase:caseList){
            objCase.Status=caseStatus;
            objCase.AccountId=accountList.get(0).Id;
            objCase.Future_Action_Date__c=System.today()+2;
        }
        insert caseList;
        //Creating Case Custom setting record
        Case_Custom_Setting__c caseSetting=TestUtilities.createCaseCustomSetting(-1,true);
        //Creating Provided DAte Custom setting record
        Provided_Date__c providedDateSetting=TestUtilities.createProvidedDateCustomSetting(true);
         return caseList;
    }
    /** Test Data With Active Contact **/
     public static List<Case> createTestDataWithActiveContact(String caseStatus){
        
        //****************************************************************************
        // List of records created using createCase method from TestUtilities class
        //****************************************************************************
        Id UserId = UserInfo.getUserId();
        //Creating Account record
        List<Account> accountList=TestUtilities.createAccount(1,true);
        //Creating Contact record
        List<Contact> contactList=TestUtilities.createContact(1,accountList.get(0).Id,false);
        for(Contact c:contactList){
        c.Contact_Status__c='Active';
        }
        insert contactList;
        //Creating case record
        List<Case> caseList = TestUtilities.createCase(numberofRecords,UserId,false);
        for(Case objCase:caseList){
            objCase.Status=caseStatus;
            objCase.ContactId=contactList.get(0).Id;
            objCase.AccountId=accountList.get(0).Id;
            objCase.Future_Action_Date__c=System.today()+2;
        }
        insert caseList;
        //Creating Case Custom setting record
        Case_Custom_Setting__c caseSetting=TestUtilities.createCaseCustomSetting(-1,true);
        //Creating Provided DAte Custom setting record
        Provided_Date__c providedDateSetting=TestUtilities.createProvidedDateCustomSetting(true);
         return caseList;
    }
    //********************************************************************************************************
    // Test Method for calling default construtor of  batch class and executing it without Contact Test Data
   //*********************************************************************************************************
    static testmethod void testWitoutContactTestData() {
        createTestData('Awaiting Information');
        Test.startTest();    
        CaseAutoClose objCaseAutoClose = new CaseAutoClose();
        Database.executeBatch(objCaseAutoClose);      
        Test.stopTest(); 
        Case_Custom_Setting__c objCaseCustomSetting=  Case_Custom_Setting__c.getValues('Case Last Modify Number of Days');
        Decimal caseLastModifyDays=objCaseCustomSetting.Number_of_Days__c;
        System.debug('test Case Last Modify days='+caseLastModifyDays);
        List<Case> caseList=  [SELECT Id,Reason,Status,Internal_Comments__c,lastModiFieddate,Number_of_Days_LastModified__c,subject FROM Case WHERE  Status= 'Closed' and (Number_of_Days_LastModified__c>:caseLastModifyDays) and  ((Contact.Contact_Status__c='Active') or (ContactId=null))];
        system.debug('Test Case Data list='+caseList);
        if(caseList.size()>0){
            Case objCase=caseList.get(0);
            System.assertEquals(objCase.Status, 'Closed');
            System.assertEquals(objCase.Reason, 'No Customer Response.');
            System.assertEquals(objCase.Internal_Comments__c, 'Auto-closed due to no customer response.');    
        }
    } 
    //****************************************************************************************************
    // Test Method for calling default construtor of  batch class and executing it with Contact Test Data
   //*****************************************************************************************************
    static testmethod void testWithContactTestData() {
        createTestDataWithActiveContact('Awaiting Information');
        Test.startTest();    
        CaseAutoClose objCaseAutoClose = new CaseAutoClose();
        Database.executeBatch(objCaseAutoClose);      
        Test.stopTest(); 
        Case_Custom_Setting__c objCaseCustomSetting=  Case_Custom_Setting__c.getValues('Case Last Modify Number of Days');
        Decimal caseLastModifyDays=objCaseCustomSetting.Number_of_Days__c;
        System.debug('test Case Last Modify days='+caseLastModifyDays);
        List<Case> caseList=  [SELECT Id,Reason,Status,Internal_Comments__c,lastModiFieddate,Number_of_Days_LastModified__c,subject FROM Case WHERE  Status= 'Closed' and (Number_of_Days_LastModified__c>:caseLastModifyDays) and  ((Contact.Contact_Status__c='Active') or (ContactId=null))];
        system.debug('Test Case Data list='+caseList);
        if(caseList.size()>0){
            Case objCase=caseList.get(0);
            System.assertEquals(objCase.Status, 'Closed');
            System.assertEquals(objCase.Reason, 'No Customer Response.');
            System.assertEquals(objCase.Internal_Comments__c, 'Auto-closed due to no customer response.');    
        }
    } 
     //********************************************************************************
     // Test method for calling ScheduleClass with case id without Contact Test Data
     //********************************************************************************
    static testmethod void testScheduleClassWithCaseIdWithoutContact() {
        Date providedDate=system.today();
        List<Case> testCaseList= createTestData('New');
        
        id caseId=testCaseList.get(0).Id;
        Test.startTest();    
        CaseAutoClose objCaseAutoClose = new CaseAutoClose();
        objCaseAutoClose.scheduleClass(system.today(), testCaseList.get(0).Id);
        Test.stopTest(); 
        Case_Custom_Setting__c objCaseCustomSetting=  Case_Custom_Setting__c.getValues('Case Last Modify Number of Days');
        Decimal caseLastModifyDays=objCaseCustomSetting.Number_of_Days__c;
        System.debug('test Case Last Modify days='+caseLastModifyDays);
        List<Case> caseList=  [SELECT Id,Reason,Status,Internal_Comments__c,lastModiFieddate,Number_of_Days_LastModified__c,subject FROM Case WHERE (Status<= 'Closed' and (Number_of_Days_LastModified__c>:caseLastModifyDays) and  ((Contact.Contact_Status__c='Active') or (ContactId=null))) or ((id =:caseId) and  ((Contact.Contact_Status__c='Active') or (ContactId=null)))];
        system.debug('Test Case Data list with Case Id='+caseList);
        if(caseList.size()>0){
            Case objCase=caseList.get(0);
            System.assertEquals(objCase.Status, 'Closed');
            System.assertEquals(objCase.Reason, 'No Customer Response.');
            System.assertEquals(objCase.Internal_Comments__c, 'Auto-closed due to no customer response.');    
        }
    } 
    //****************************************************************************
     // Test method for calling ScheduleClass with case id with Contact Test Data
     //****************************************************************************
    static testmethod void testScheduleClassWithCaseIdWithContact() {
        Date providedDate=system.today();
        List<Case> testCaseList= createTestDataWithActiveContact('New');
        
        id caseId=testCaseList.get(0).Id;
        Test.startTest();    
        CaseAutoClose objCaseAutoClose = new CaseAutoClose();
        objCaseAutoClose.scheduleClass(system.today(), testCaseList.get(0).Id);
        Test.stopTest(); 
        Case_Custom_Setting__c objCaseCustomSetting=  Case_Custom_Setting__c.getValues('Case Last Modify Number of Days');
        Decimal caseLastModifyDays=objCaseCustomSetting.Number_of_Days__c;
        System.debug('test Case Last Modify days='+caseLastModifyDays);
        List<Case> caseList=  [SELECT Id,Reason,Status,Internal_Comments__c,lastModiFieddate,Number_of_Days_LastModified__c,subject FROM Case WHERE (Status<= 'Closed' and (Number_of_Days_LastModified__c>:caseLastModifyDays) and  ((Contact.Contact_Status__c='Active') or (ContactId=null))) or ((id =:caseId) and  ((Contact.Contact_Status__c='Active') or (ContactId=null)))];
        system.debug('Test Case Data list with Case Id='+caseList);
        if(caseList.size()>0){
            Case objCase=caseList.get(0);
            System.assertEquals(objCase.Status, 'Closed');
            System.assertEquals(objCase.Reason, 'No Customer Response.');
            System.assertEquals(objCase.Internal_Comments__c, 'Auto-closed due to no customer response.');    
        }
    } 
     //***************************************************************************************
    // Test method for calling ScheduleClass with out case id and without Contact Test Data
    //****************************************************************************************
     static testmethod void testScheduleClassWithoutCaseIdWithoutContactTestData() {
        Date providedDate=system.today();
        createTestData('Awaiting Information');
        Test.startTest();    
        CaseAutoClose objCaseAutoClose = new CaseAutoClose();
        objCaseAutoClose.scheduleClass(system.today(), null);
        Test.stopTest(); 
        Case_Custom_Setting__c objCaseCustomSetting=  Case_Custom_Setting__c.getValues('Case Last Modify Number of Days');
        Decimal caseLastModifyDays=objCaseCustomSetting.Number_of_Days__c;
        System.debug('test Case Last Modify days='+caseLastModifyDays);
        List<Case> caseList=  [SELECT Id,Reason,Status,Internal_Comments__c,lastModiFieddate,Number_of_Days_LastModified__c,subject FROM Case WHERE (Status<= 'Closed' and (Number_of_Days_LastModified__c>:caseLastModifyDays) and  ((Contact.Contact_Status__c='Active') or (ContactId=null)))];
        system.debug('Test Case Data list without Case Id='+caseList);
        if(caseList.size()>0){
            Case objCase=caseList.get(0);
            System.assertEquals(objCase.Status, 'Closed');
            System.assertEquals(objCase.Reason, 'No Customer Response.');
            System.assertEquals(objCase.Internal_Comments__c, 'Auto-closed due to no customer response.');    
        }
    }
    //********************************************************************************************
    // Test method for calling ScheduleClass with out case id and with Contact Test Data
    //********************************************************************************************
     static testmethod void testScheduleClassWithoutCaseIdWithContactTestData() {
        Date providedDate=system.today();
        createTestDataWithActiveContact('Awaiting Information');
        Test.startTest();    
        CaseAutoClose objCaseAutoClose = new CaseAutoClose();
        objCaseAutoClose.scheduleClass(system.today(), null);
        Test.stopTest(); 
        Case_Custom_Setting__c objCaseCustomSetting=  Case_Custom_Setting__c.getValues('Case Last Modify Number of Days');
        Decimal caseLastModifyDays=objCaseCustomSetting.Number_of_Days__c;
        System.debug('test Case Last Modify days='+caseLastModifyDays);
        List<Case> caseList=  [SELECT Id,Reason,Status,Internal_Comments__c,lastModiFieddate,Number_of_Days_LastModified__c,subject FROM Case WHERE (Status<= 'Closed' and (Number_of_Days_LastModified__c>:caseLastModifyDays) and  ((Contact.Contact_Status__c='Active') or (ContactId=null)))];
        system.debug('Test Case Data list without Case Id='+caseList);
        if(caseList.size()>0){
            Case objCase=caseList.get(0);
            System.assertEquals(objCase.Status, 'Closed');
            System.assertEquals(objCase.Reason, 'No Customer Response.');
            System.assertEquals(objCase.Internal_Comments__c, 'Auto-closed due to no customer response.');    
        }
    }
    //************************************************************************************************************
    // Test method for SchedulableAutoCloseCaseBatchTestWithoutContactTestData class without Contact Test Data
    //************************************************************************************************************
   static testmethod void SchedulableAutoCloseCaseBatchTestWithoutContactTestData() {
    Test.startTest(); 
     createTestData('New');
     SchedulableAutoCloseCaseBatch obj = new SchedulableAutoCloseCaseBatch();
        obj.execute(null);     
    test.stopTest();
    } 
    //*********************************************************************************************************
    // Test method for SchedulableAutoCloseCaseBatchTestWithContactTestData class with Contact Test Data 
    //*******************************************************************************************************
   static testmethod void SchedulableAutoCloseCaseBatchTestWithContactTestData() {
    Test.startTest(); 
     createTestDataWithActiveContact('New');
     SchedulableAutoCloseCaseBatch obj = new SchedulableAutoCloseCaseBatch();
        obj.execute(null);     
    test.stopTest();
    } 
}