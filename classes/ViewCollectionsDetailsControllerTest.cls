/*********************************************************************
* Appirio, Inc
* Name: ViewCollectionsDetailsControllerTest
* Description: [Task# T-606470]
* Created Date: [05/29/2017]
* Created By: [Poornima Bhardwaj] (Appirio) 
* Date Modified                Modified By              Description of the update
* [07-June-2017]               [Poornima Bhardwaj]      [#T-608156 Added methods testCollectionsDetailsMXUser and testCollectionsDetailsNAUser ]
**********************************************************************/
@isTest
public class ViewCollectionsDetailsControllerTest{
    static user MXUser;
    static user agentUser;
     //****************************************************************************
    // Method to test the functionality when user is MX
    // @param : None
    // @return void
    //****************************************************************************
    public testmethod static void testCollectionsDetailsMXUser() { 
        List<Profile> profileInfo=[select id,name from Profile where name='Custom: MX Agent'];
        List<UserRole> agentRole=[select id,name from UserRole where name='Service Mexico Manager'];
        //Creating Test User
        MXUser=TestUtilities.createTestUser(1,profileInfo[0].id,false);
        MXUser.UserRole=agentRole[0];
        //Calling redirectToAccount() Method
        TestRedirectToAccount();
        //Setting mock response class MockHttpResponseGenerator
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator('CollectionDetails'));
        //Running test as MXUser
        System.runAs(MXUser){
            Test.StartTest();
            System.debug('Running as MX Profile'+profileInfo[0].name);
            //Object Creation
            ViewCollectionsDetailsController obj=new ViewCollectionsDetailsController();
            List<UtilityCls.Value> valueL=obj.valueList;
            System.debug('ValueListValueMX'+valueL);
            Test.StopTest(); 
        }
    }
     //****************************************************************************
    // Method to test the functionality when user is NA
    // @param : None
    // @return void
    //****************************************************************************
     public testmethod static void testCollectionsDetailsNAUser() { 
        List<UserRole> agentRole=[select id,name from UserRole where name='Service NA Manager'];
        List<Profile> agentProId=[select id,name from Profile where name='Custom: NA Agent'];
        //Creating Test User
        agentUser=TestUtilities.createTestUser(1,agentProId[0].id,false);
        System.debug('Agent User:'+agentUser);
        insert agentUser;
        //Setting mock response class MockHttpResponseGenerator
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator('CollectionDetails'));
        Test.StartTest();
        //Running test as agentUser
        System.runAs(agentUser){
            System.debug('Running as NA profile'+agentProId[0].name);
            //Object Creation
            ViewCollectionsDetailsController obj=new ViewCollectionsDetailsController();
            List<UtilityCls.Value> valueL=obj.valueList;
            System.debug('ValueListValueNA'+valueL);
            System.AssertEquals('9068362',valueL[0].mid);
            System.AssertEquals(55.47,valueL[0].incomingAmount);
            System.AssertEquals(null,valueL[0].clientGroup);
            System.AssertEquals(55.47,valueL[0].BalanceDueAmount); 
        }
        Test.StopTest();
    }
    //Calling Redirect To Account Method and Testing Result
    static void TestRedirectToAccount()
    {
        //Object Creation
        ViewCollectionsDetailsController obj=new ViewCollectionsDetailsController();
        //Calling redirectToAccount method
        obj.redirectToAccount();
        System.assert(obj.AccountId!=null);
        System.debug('Check Account Id: '+obj.AccountId);
    }
    
}