/*********************************************************************************************************************
Author        :  Appirio (Gaurav Dudani)
Date          :  Sep 9th, 2016
Purpose       :  Test class for EmailScheduleDeleteAttachments.cls

Updated By    : Gaurav Dudani
Updated Date  : Sep 30th, 2016
Purpose       : Test class coverage for EmailScheduleDeleteAttachments.cls and SchedulableNightlyBatch.cls
**********************************************************************************************************************/
// 23 Sep 2016 Gaurav Dudani (T-539936)
// 03 Oct 2016 Gaurav Dudani (T-542464)

@istest

//****************************************************************************
// Test Class to test the EmailScheduleDeleteAttachments.cls
//****************************************************************************

public class EmailScheduleDeleteAttachmentsTEST {
    
    public static void createTestData(){
        
        //****************************************************************************
        // List of records created using createCase method from TestUtilities class
        //****************************************************************************
       
        Id UserId = UserInfo.getUserId();
        List<Case> caseList = TestUtilities.createCase(1,UserId,false);
        insert caseList;
        Test.setCreatedDate(caseList.get(0).Id, DateTime.newInstance(2016,06,24));
        Case c1 = [select id,CreatedDate from Case LIMIT 1];
        System.debug('created date is '+c1.CreatedDate);
        System.assertEquals(c1.CreatedDate, DateTime.newInstance(2016,06,24));
        
        //**********************************************************************************
        // List of records created using createEmailMessage method from TestUtilities class
        //**********************************************************************************
        
        List<EmailMessage> emailMessageList = TestUtilities.createEmailMessage(1,c1.Id,true);
        
        //********************************************************************************
        // List of records created using createAttachment method from TestUtilities class
        //********************************************************************************
        
        List<Attachment> attList = TestUtilities.createAttachment(3,emailMessageList[0].id,false);
        attList[0].Name='Note1' + '.imf';
        attList[1].Name='Note2' + '.msg';
        insert attList;
        System.debug('Sending Inserted Attachments ' + attList);
        
    } 
    static testmethod void test() {
       
        //****************************************************************************
        // Calling the batch class and executing it.
        //****************************************************************************
        createTestData();
        Test.startTest();    
        EmailScheduleDeleteAttachments attachmentDeleteBatch = new EmailScheduleDeleteAttachments();
        Database.executeBatch(attachmentDeleteBatch);      
        Test.stopTest(); 
        
        //********************************************************************************************
        // Query to check the attachemnts deleted in Deleted_Case_Email_Attachment__c custom object.
        //********************************************************************************************
        
        List<Deleted_Case_Email_Attachment__c> daList = [select name from Deleted_Case_Email_Attachment__c];
        System.debug('Deleted Attachment Are'+ daList.size());
        System.assertEquals(daList.size(), 2, 'List size are equal' );
        System.assertEquals(daList[0].Name, 'Note1' + '.imf', 'name are same');
        System.assertEquals(daList[1].Name, 'Note2' + '.msg', 'name are same');
        

    } 
        //****************************************************************************
        // Calling the Scheduler class and executing it.
        //****************************************************************************
        static testmethod void testNightlyScheduler() {
        
        createTestData();
        Test.startTest();   
        SchedulableNightlyBatch obj = new SchedulableNightlyBatch();
        obj.execute(null);    
        Test.stopTest();
        
        //********************************************************************************************
        // Query to check the attachemnts deleted in Deleted_Case_Email_Attachment__c custom object.
        //********************************************************************************************
        
        List<Deleted_Case_Email_Attachment__c> daList = [select name from Deleted_Case_Email_Attachment__c];
        System.debug('Deleted Attachment Are'+ daList.size());
        System.assertEquals(daList.size(), 2, 'List size are equal' );
        System.assertEquals(daList[0].Name, 'Note1' + '.imf', 'name are same');
        System.assertEquals(daList[1].Name, 'Note2' + '.msg', 'name are same');
        

    }
}