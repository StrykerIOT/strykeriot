/*****************************************************************************************************************************
// (c) 2015 Appirio, Inc.
//
// Description : Batch Class - Delete Old Attachments
// Created On  : 5th September 2016 By Gaurav Dudani Original (T-532536)
// Updated On  : 28th September 2016 By Gaurav Dudani for updating variable names.
//                            
//****************************************************************************************************************************/

// 23 Sep 2016 Gaurav Dudani (T-539936)

//****************************************************************************
// Batch Class declaration.
//****************************************************************************

global class EmailScheduleDeleteAttachments implements Database.Batchable<sObject>,Database.AllowsCallouts, Database.Stateful {
    
    //********************************************************************* 
    // Querying List of cases who's created date is 90 days old.
    //*********************************************************************
    List<Deleted_Case_Email_Attachment__c> originalAttachment = New List <Deleted_Case_Email_Attachment__c>();
    //temporarily changed to 1 day for demo purposes
    date dt = system.today().addDays(-90); 
    global final String query= 'SELECT Id' +
                                ' FROM Case' + 
                                ' WHERE CreatedDate <= :dt';
                               
    global final String s_object;   
    global Database.QueryLocator start(Database.BatchableContext bc) {
        system.debug('Query for getting records '+query);
        return Database.getQueryLocator(query);
    }
    
    //******************************************************************************************
    // Execute Method to query the list of email messages and attachments for the queried cases. 
    //******************************************************************************************
    
    global void execute(Database.BatchableContext BC, list<sObject> scope){
        Set<Id> caseIdSet = new Set<Id>();  
        List<Id> emailMessageList = New List<ID>();
        List<Attachment> emailAttachList = New List<Attachment>();
        Map<Id,Id> mapOfEmailIdAndCaseId = New Map<ID,ID>();
        Map<Id,Id> mapOfAttachmentIdAndEmailId = New Map<ID,ID>();
        for(Sobject obj : scope){
            Id idVar = (Id)obj.Id;
            caseIdSet.add(idVar);
        }
           
        if(caseIdSet.Size() > 0){
        for(EmailMessage eMessage : [SELECT ID, ParentID 
                                    FROM EmailMessage 
                                    WHERE ParentId IN: caseIdSet]){
          if(eMessage!=null){
                emailMessageList.add(eMessage.Id);
                mapOfEmailIdAndCaseId.put(eMessage.Id,eMessage.ParentId);
                system.debug('** Outbound Email Message Are: **'+ eMessage);
          }
        }       
        for(Attachment attach : [SELECT Id, ParentId,Name,CreatedDate
                                FROM Attachment 
                                WHERE ParentId IN: emailMessageList
                                AND (Name LIKE '%.imf' 
                                OR Name LIKE '%.msg')]){  
              if(attach!=null){
                emailAttachList.add(attach);
                Deleted_Case_Email_Attachment__c deleteAttach = new Deleted_Case_Email_Attachment__c();
                deleteAttach.name = attach.name;
                deleteAttach.related_case__c = mapOfEmailIdAndCaseId.get(attach.ParentId);
                deleteAttach.attachment_created_date__c = Date.valueOf(attach.createddate);
                originalAttachment.add(deleteAttach);
                system.debug('** Inserted Record: **'+ deleteAttach);
                system.debug('** Attachmens Are: **'+ attach);
            }        
          }
        }
        system.debug('** Emails Attched Are: **'+ emailAttachList);
        If(emailAttachList.Size() > 0){
         Delete emailAttachList;
        }
    }
    //****************************************************************************
    // Finish Method
    //****************************************************************************
    
    global void finish(Database.BatchableContext BC) {
       system.debug('originalAttachment values' +originalAttachment);
       system.debug('originalAttachment size' +originalAttachment.size());
        if(originalAttachment.Size() > 0) {
         Insert originalAttachment;
         } 
        system.debug('finish Method Calling');
    }  
}