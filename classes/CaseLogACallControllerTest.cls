/****************************************************************************************
* Appirio, Inc
* Test Class Name: CaseLogACallControllerTest
* Class Name: [CaseLogACallController]
* Description: [T-563838 Test Class for CaseLogACallController]
* Created Date: [22/12/2016]
* Created By: [Neeraj Kumawat] (Appirio)
* 
* Date Modified                Modified By                  Description of the update
* [22/12/2016]               [Gaurav Dudani]               [Added Records and comments] 
* [27/02/2017]               [Neeraj Kumawat]              [Optimize code and added test method to cover request from console page] 
*****************************************************************************************/

@IsTest
public class CaseLogACallControllerTest{
    
    /****************************************************************************
* Test Method for creating Test Data.
*****************************************************************************/
    static List<Contact> createTestData(){
        List<Account> accountList = new List<Account>();
        //loop for iterating accounts
        for(Account acc: TestUtilities.createAccount(1,false)){
            acc.Name = 'Test Account';
            accountList.add(acc);
        }
        insert accountList;
        List<Contact> contactList = new List<Contact>();
        //loop for iterating contacts
        for(Contact con: TestUtilities.createContact(1,null,false)){
            con.AccountID = accountList[0].Id;
            con.LastName = 'TestUser';
            contactList.add(con);
        }
        insert contactList;
        return contactList;
    }
    /******************************************************************************************************
* Test Method for creating Task using log a call on Click of save when request is from regular page
*******************************************************************************************************/
    static testMethod void CaseLogACallControllerRequestFromNonConsolePageTestMethod(){
        List<Contact> contactList= createTestData();
        Test.startTest();
        List<Case> caseList = new List<Case>();
        for(Case cse: TestUtilities.createCase(1,null,false)){
            cse.status = 'New';
            cse.subject = 'test subject';
            cse.origin= 'Email';
            cse.ContactId = contactList[0].Id; 
            caseList.add(cse);
        }
        insert caseList;
        Apexpages.currentpage().getparameters().put('what_id' , caseList[0].Id);
        Apexpages.currentpage().getparameters().put('who_id' , contactList[0].Id);
        Apexpages.currentpage().getparameters().put('tsk5' ,'Call');
        CaseLogACallController Obj =  new CaseLogACallController(null);
        obj.isFromConsole = false;
        PageReference caseDetailPageLink = Obj.save();
        List<Task> objTaskList=[select subject,Status, Id from Task where WhatId=:caseList[0].Id];
        System.debug('Create Task List using Save method='+objTaskList);
        System.debug('caseDetailPageLink='+caseDetailPageLink.getUrl());
        System.assertEquals(objTaskList.size(), 2);
        System.assertEquals('/'+caseList[0].Id, caseDetailPageLink.getUrl());
        Test.stopTest();
    } 
    /******************************************************************************************************
* Test Method for creating Task using log a call on Click of save when request is from console page
*******************************************************************************************************/   
    static testMethod void CaseLogACallControllerRequestFromConsolePageTestMethod(){
        List<Contact> contactList= createTestData();
        Test.startTest();
        List<Case> caseList = new List<Case>();
        for(Case cse: TestUtilities.createCase(1,null,false)){
            cse.status = 'New';
            cse.subject = 'test subject';
            cse.origin= 'Email';
            cse.ContactId = contactList[0].Id; 
            caseList.add(cse);
        }
        insert caseList;
        Apexpages.currentpage().getparameters().put('what_id' , caseList[0].Id);
        Apexpages.currentpage().getparameters().put('who_id' , contactList[0].Id);
        Apexpages.currentpage().getparameters().put('tsk5' ,'TestTaskFromConsole');
        CaseLogACallController Obj =  new CaseLogACallController(null);
        obj.isFromConsole = true;
        PageReference taskDetailPageLink = Obj.save();
        List<Task> objTaskList=[select subject,Status, Id from Task where WhatId=:caseList[0].Id and subject ='TestTaskFromConsole' and status = 'Complete'];
        System.debug('Create Task List when request is from console method='+objTaskList);
        System.debug('taskDetailPageLink='+taskDetailPageLink.getUrl());
        System.assertEquals(objTaskList.size(), 1);
        System.assertEquals('/'+objTaskList[0].Id, taskDetailPageLink.getUrl());
        Test.stopTest();
    } 
    
    /**********************************************************************************************
* Test Method for creating Task using log a call  on click of save and close Case button.
***********************************************************************************************/
    static testMethod void CaseLogACallControllerTestMethod2(){
        List<Contact> contactList= createTestData();
        Test.startTest();
        List<Case> caseList = new List<Case>();
        for(Case cse: TestUtilities.createCase(1,null,false)){
            cse.status = 'New';
            cse.subject = 'test subject';
            cse.origin= 'Email';
            cse.ContactId = contactList[0].Id;
            caseList.add(cse);
        }
        insert caseList;
        Apexpages.currentpage().getparameters().put('what_id' , caseList[0].Id);
        Apexpages.currentpage().getparameters().put('who_id' , contactList[0].Id);
        Apexpages.currentpage().getparameters().put('tsk5' ,'Others');
        CaseLogACallController Obj =  new CaseLogACallController(null);
        PageReference caseClosePageLink = Obj.saveAndCloseCase();
        List<Task> objTaskList=[select subject,Status, Id from Task where WhatId=:caseList[0].Id];   
        System.debug('Create Task List using saveAndCloseCase ='+objTaskList);
        System.debug('caseClosePageLink='+caseClosePageLink.getUrl());
        System.assertEquals(objTaskList.size(), 2);
        System.assert(caseClosePageLink.getUrl().contains('retURL='+caseList[0].Id));
        Test.stopTest();
    }
}