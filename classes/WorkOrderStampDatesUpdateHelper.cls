/*********************************************************************
* Appirio a WiPro Company
* Name: WorkOrderStampDatesUpdateHelper 
* Description: Batch Helper class called by the WorkOrderStampDatesUpdatehHelper batch. 
* Created Date: 06/06/2017
* Created By: Aashish Sajwan
* 
* Date Modified                Modified By                  Description of the update
* [8-Jun-2017]                 [Aashish Sajwan]             [Update Batch Process logic]
**********************************************************************/
public with sharing class WorkOrderStampDatesUpdateHelper {

  private final String CLASSNAME = '\n\n*** WorkOrderStampDatesUpdateHelper .';
  
  public Database.Querylocator GetQueryLocator(String pQuery){
    return Database.getQueryLocator(pQuery);
  }  
  
  /*  author : Appirio
    date : 06/06/2017
    description :  Builds a SOQL query for WorkOrder records with child Cases
    paramaters : none
    returns : a String representing a SOQL query to retrieve a bach of records to be processed
  */
  public String GetQuery(){
    
    final String METHODNAME = CLASSNAME + 'GetQuery()';
    system.debug(LoggingLevel.INFO, METHODNAME);
    
    datetime minus24DateTime = datetime.now().addHours(-24);
    system.debug(LoggingLevel.DEBUG, '\n\n*** minus24DateTime :: '+ minus24DateTime);
    set<String> ApprovalStatus = new set<String>{'Approved for Processing','Rejected','Recalled'};
    String dateString = minus24DateTime.format('yyyy-MM-dd\'T\'hh:mm:ss\'z\'');
    String SOQL='';
        if(Test.isRunningTest()){
           SOQL = 'SELECT  Id, MID__c, Last_Stamped_Collections_Balance__c, Last_Stamped_Write_Off_Balance__c FROM WorkOrder  WHERE MID__c!=null AND Work_Order_Approval_Status__c not in (\'Approved for Processing\',\'Rejected\',\'Recalled\')' ;      
        }else{
           SOQL = 'SELECT  Id, MID__c, Last_Stamped_Collections_Balance__c, Last_Stamped_Write_Off_Balance__c FROM WorkOrder WHERE MID__c!=null AND  Work_Order_Approval_Status__c not in (\'Approved for Processing\',\'Rejected\',\'Recalled\') and Last_Batch_Execute_Date__c< TODAY limit 75' ;    
        }    
    system.debug(LoggingLevel.DEBUG, '\n\n*** SOQL :: '+ SOQL); 
    return SOQL;
  }  
  
  /*  author : Appirio
    date : 06/06/2017
    description : updates/sets the Account and Tax Information lookup relationship 
    paramaters : list of sObjects
    returns : nothing
  */
  public void ProcessBatch(list<SObject> scope){
    
        list<WorkOrder> workOrderList = new list<WorkOrder>();
        workOrderList.addall((List<WorkOrder>)scope);    
        //Call UtilityCls UpdateStamedFields
        UtilityCls.UpdateStampedFields(workOrderList);
        /*
        if(!workOrderList .isEmpty() || workOrderList.size() != 0){
           update workOrderList ;    
        } 
        */
     
  }  
}