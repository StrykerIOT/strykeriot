@isTest
public class RunApexControllerTest
{
    static Account Acc;
    static Run_Apex__c objRA;
    public testmethod static void testCallout1()
    {
        //********************* Positive Use Case ****************************
        RunApexControllerTest.TestData();
        //Data Check
        List<Execute_Batch_Job__c> objData=[select Name from Execute_Batch_Job__c where Class_Name__c=:objRA.Apex_Class_Name__c AND Record_Id__c=:Acc.id];
        System.assertEquals(0,objData.size());
        Test.StartTest();
        RunApexControllerTest.TestPositiveCase();
        RunApexControllerTest.TestIFBlockRecordId();
        
        //Data Check
        objData=[select Name from Execute_Batch_Job__c where Class_Name__c=:objRA.Apex_Class_Name__c AND Record_Id__c=:Acc.id];
        System.assertEquals(1,objData.size());
        Test.StopTest();
    }
    public testmethod static void testCallout2()
    {
        //********************* Nagative Use Case ****************************
        RunApexControllerTest.TestData();
        Test.StartTest();
        RunApexControllerTest.TestCatchBlock();
        Test.StopTest();
    }
    //Creating Test Data for Customo Setting and Account for Record ID
    static void TestData()
    {    
        //Test Data for Custom Settings
        objRA=TestUtilities.createCustomSetting('ExternalDocumentRecordsDeletion','ExternalDocumentRecordsDeletion',true);
        //Test Data For Account
        Acc=TestUtilities.createTestAccount(true);
    }
    //Testing Catch Block 
    static void TestCatchBlock()
    {
        RunApexController obj=new RunApexController();
        obj.SelectedClass=objRA.Apex_Class_Name__c;
        obj.recordId='JVBERi0xLjQKJeLjz9MKNCAwIG9iagpbMSAwIFIvWFlaIDAgNzE0IDBdCmVuZG9ia';        //Invalid ID
        try{
        obj.executeBatch();
        }
        catch(DMLException ex){
            System.debug('Expection Caught at Testing Side');
        }
    }
    //Test if Block
    static void TestIFBlockRecordId()
    {
        RunApexController obj=new RunApexController();
        obj.SelectedClass=objRA.Apex_Class_Name__c;
        obj.recordId='';
        obj.executeBatch();
    }
    //Test Positive case
    static void TestPositiveCase()
    {
        RunApexController obj=new RunApexController();
        obj.SelectedClass=objRA.Apex_Class_Name__c;
        obj.recordId=Acc.id;
        obj.executeBatch();
    }
}