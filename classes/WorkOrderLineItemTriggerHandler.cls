/**************************************************************************************************************
* Appirio, Inc
* Name: WorkOrderLineItemTriggerHandler
* Description: [T-603559 WorkOrderLineItemTriggerHandler Trigger Handler for WorkOrderLineItem Trigger]
* Created Date: [24/05/2017]
* Created By: [Neeraj Kumawat] (Appirio)
* 
* Date Modified      Modified By           Description of the update
* [26-May-2017]     [Neeraj Kumawat]       [Added method updateWorkOrderStatus to update workorder status. #T-605293/S-470343]
* [6-June-2017]     [Neeraj Kumawat]       [Added method updateWorkOrderStatusToCanceled to update workorder status canceled.#T-605293/S-470343]
* [7-June-2017]     [Neeraj Kumawat]       [Updated to close the work order if Status of all WOLIs are Closed and/or Cancelled (possible combo of both).T-608155/I-277751]
* [21-June-2017]    [Neeraj Kumawat]       [T-610373 I-280180 Set Is_Updated_From_Work_Order_Line_Item flag to true when work order status is changes via Work order line item.]
***************************************************************************************************************/
public class WorkOrderLineItemTriggerHandler {
    //****************************************************************************
    // Method to be call on Before Insert of WorkOrderLineItem
    // @param newWorkOrderLineItems: List of new Work Order Line Items
    // @return void
    // 24 May 2017 Neeraj Kumawat T-603559
    //****************************************************************************
    public static void OnBeforeInsert(List<WorkOrderLineItem> newWorkOrderLineItems) {
        updateWorkOrderLineItem(newWorkOrderLineItems);
        findDuplicates(newWorkOrderLineItems);
    }
    //****************************************************************************
    // Method to be call on Before update of WorkOrderLineItem
    // @param newWorkOrderLineItems: List of new Work Order Line Items
    // @return void
    // 24 May 2017 Neeraj Kumawat T-605293
    //****************************************************************************
    public static void OnBeforeUpdate(List<WorkOrderLineItem> newWorkOrderLineItems) {
        updateWorkOrderLineItem(newWorkOrderLineItems);
        findDuplicates(newWorkOrderLineItems);
    }
    //****************************************************************************
    // Method to be call on After Insert of WorkOrderLineItem
    // @param newWorkOrderLineItems: List of new Work Order Line Items
    // @return void
    // 24 May 2017 Neeraj Kumawat T-605293
    //****************************************************************************
    public static void OnAfterInsert(List<WorkOrderLineItem> newWorkOrderLineItems) {
        updateWorkOrderStatus(newWorkOrderLineItems);
    }
    //****************************************************************************
    // Method to be call on After update of WorkOrderLineItem
    // @param newWorkOrderLineItems: List of new Work Order Line Items
    // @return void
    // 24 May 2017 Neeraj Kumawat T-603559
    //****************************************************************************
    public static void OnAfterUpdate(List<WorkOrderLineItem> newWorkOrderLineItems) {
        updateWorkOrderStatus(newWorkOrderLineItems);
    }
    //****************************************************************************
    // Method To update work order line item from reference object base on specified key in task #T-603559
    // @param newWorkOrderLineItems: List of new Work Order Line Items
    // @return void
    // 24 May 2017 Neeraj Kumawat T-603559
    //****************************************************************************
    public static void updateWorkOrderLineItem(List<WorkOrderLineItem> newWorkOrderLineItems){
        //Set for WorkOrdeLineitem keys
        Set<String> departmentSet=new Set<String>();
        Set<String> processingCenterIdSet=new Set<String>();
        Set<String> categorySet=new Set<String>();
        Set<String> adjustmentTypeSet=new Set<String>();
        Set<String> adjustmentReasonSet=new Set<String>();
        //Map of WorkOrderLineItems based on key
        Map<String,List<WorkOrderLineItem>> workLineItemMap=new Map<String,List<WorkOrderLineItem>>();
        for(WorkOrderLineItem wrkLineItem: newWorkOrderLineItems){
            //Getting key value field from workOrderLineItem
            String department=wrkLineItem.Department__c	;
            String processingCenterId=wrkLineItem.Processing_Center_ID__c;
            String category=wrkLineItem.Adjustment_Reason_Category__c;
            String adjustmentType=wrkLineItem.Adjustment_Type__c;
            String adjustmentReason=wrkLineItem.Adjustment_Reason__c;
            //Initializing set values
            departmentSet.add(department);
            processingCenterIdSet.add(processingCenterId);
            categorySet.add(category);
            adjustmentTypeSet.add(adjustmentType);
            adjustmentReasonSet.add(adjustmentReason);
            //Setting Transaction_Code__c based on adjustment type and show on statement
            if(adjustmentType=='Customer Credit' && wrkLineItem.Show_on_Statement__c=='Yes'){
                wrkLineItem.Transaction_Code__c='138';
            }else if(adjustmentType=='Customer Credit' && wrkLineItem.Show_on_Statement__c=='No'){
                wrkLineItem.Transaction_Code__c='149';
            }else if(adjustmentType=='Customer Debit' && wrkLineItem.Show_on_Statement__c=='Yes'){
                wrkLineItem.Transaction_Code__c='238';
            }else if(adjustmentType=='Customer Debit' && wrkLineItem.Show_on_Statement__c=='No'){
                wrkLineItem.Transaction_Code__c='249';
            }
            //Creating key to initialize Work order Line Item Map
            String key=department+'-'+processingCenterId+'-'+category+'-'+adjustmentType+'-'+adjustmentReason;
            //Setting Map value
            if(workLineItemMap.containsKey(key)){
                workLineItemMap.get(key).add(wrkLineItem);
            }else{
                workLineItemMap.put(key, new List<WorkOrderLineItem>{wrkLineItem});
            }
            
        }
        //Featching value form Reference object based on workorderlineitem fields
        List<Reference__c> refList=[Select Id,Reportable_1099__c,CGT_NUM__c,Department_Code__c,Description_Code__c,Adjustment_Reason_Category__c,
                                    GL_Account__c,Department__c,Processor_ID__c,Category__c,Description__c,TRANS_CR__c,
                                    TRANS_DB__c  From Reference__c
                                    Where Department__c IN :departmentSet AND Processor_ID__c IN :processingCenterIdSet
                                    AND Category__c ='WorkorderLineItem' AND Description__c IN :adjustmentReasonSet AND Adjustment_Reason_Category__c IN:categorySet AND 
                                    (TRANS_CR__c=TRUE OR TRANS_DB__c=TRUE)];
        for(Reference__c ref: refList){
            String department=ref.Department__c	;
            String processingCenterId=ref.Processor_ID__c;
            String category=ref.Adjustment_Reason_Category__c;
            String adjustmentReason=ref.Description__c; 
            String adjustmentType='';
            String key='';
            //When transaction credit is true than set adjustment type is Customer Credit
            if(ref.TRANS_CR__c==True){
                adjustmentType='Customer Credit';
                key=department+'-'+processingCenterId+'-'+category+'-'+adjustmentType+'-'+adjustmentReason;
                setWorkOrderLineItemField(workLineItemMap, key, ref);
            }
            //When transaction credit is true than set adjustment type is Customer Debit
            if(ref.TRANS_DB__c==True){
                adjustmentType='Customer Debit';
                key=department+'-'+processingCenterId+'-'+category+'-'+adjustmentType+'-'+adjustmentReason;
                setWorkOrderLineItemField(workLineItemMap, key, ref);
            }
        }
    }
    //****************************************************************************
    // Method To update work order line item fields from reference fields #T-603559
    // @param workLineItemMap: MAp of List of new Work Order Line Items
    // @param Key: Unique key for getting value form workLineItemMap
    // @param ref: reference object
    // @return void
    // 24 May 2017 Neeraj Kumawat T-603559
    //****************************************************************************
    private static void setWorkOrderLineItemField(Map<String,List<WorkOrderLineItem>> workLineItemMap,String key,Reference__c ref){
        List<WorkOrderLineItem> wrkLineItems=workLineItemMap.get(key);
        if(wrkLineItems!=null){
            for(WorkOrderLineItem wrkLineItem: wrkLineItems){
                //If Reportable_1099__c is true than set X1099 flg to yes otherwise no
                if(ref.Reportable_1099__c==True){
                    wrkLineItem.X1099_Tax_Report_Flag__c='Yes';
                }else{
                    wrkLineItem.X1099_Tax_Report_Flag__c='NO';
                }
                //set department code to department code
                wrkLineItem.Department_Code__c=ref.Department_Code__c;
                //set general ledger to deGL account
                wrkLineItem.General_Ledger__c=ref.GL_Account__c;
                //set cgt num to charge type
                wrkLineItem.Charge_Type__c=ref.CGT_NUM__c;
                //construct offset comments like descriptioncode+depatmentcode+'-'+GL account+'-'
                wrkLineItem.Offset_Comment__c=(ref.Description_Code__c!=null? ref.Description_Code__c : '')+
                    (ref.Department_Code__c!=null? ref.Department_Code__c : '')+'-'+
                    (ref.GL_Account__c!=null?ref.GL_Account__c : '')+'-';
            }
        }
    }
    //****************************************************************************
    // Method To update work order Status based on WorkOrderLineItem #T-605293
    // @param newWorkOrderLineItems: List of new Work Order Line Items
    // @return void
    // 29 May 2017 Neeraj Kumawat T-605293/S-470343
    //****************************************************************************
    public static void updateWorkOrderStatus(List<WorkOrderLineItem> newWorkOrderLineItems){
        List<WorkOrder> wrkOrderList=new List<WorkOrder>();
        //Updating Work Order status based on Work order line item status
        wrkOrderList.addAll(updateWorkOrderStatusToInProgress(newWorkOrderLineItems));
        wrkOrderList.addAll(updateWorkOrderStatusToOnHold(newWorkOrderLineItems));
        wrkOrderList.addAll(updateWorkOrderStatusToClosed(newWorkOrderLineItems));
        wrkOrderList.addAll(updateWorkOrderStatusToCanceled(newWorkOrderLineItems));
        //Updating work Order Status
        update wrkOrderList;
    }
    //**************************************************************************************
    // Method To update work order Status to InProgress based on WorkOrderLineItem #T-605293
    // @param newWorkOrderLineItems: List of new Work Order Line Items
    // @return wrkOrderListInProgress: List of WorkOrders In progress
    // 29 May 2017 Neeraj Kumawat T-605293/S-470343
    // 21 June Neeraj Kumawat Updated to set Is_Updated_From_Work_Order_Line_Item flag to true when work order 
    //status is changes via Work order line item
    //**************************************************************************************
    public static List<WorkOrder> updateWorkOrderStatusToInProgress(List<WorkOrderLineItem> newWorkOrderLineItems){
        Set<Id> inProgressWorkOrderIds=new Set<Id>();
        for(WorkOrderLineItem wrkLineItem: newWorkOrderLineItems){
            if(wrkLineItem.status=='Sent for Processing' || wrkLineItem.status=='In Progress'){
                inProgressWorkOrderIds.add(wrkLineItem.WorkOrderId);
            }
        }
        List<WorkOrder> wrkOrderListInProgress=[Select Id,Status From WorkOrder Where Id IN:inProgressWorkOrderIds];
        for(WorkOrder wrkOrder: wrkOrderListInProgress){
            wrkOrder.Status='In Progress';
            //T-610373 Neeraj Kumawat Setting flag Is_Updated_From_Work_Order_Line_Item to check that work order status is updated 
            // From Work order line item or from work order
            wrkOrder.Is_Updated_From_Work_Order_Line_Item__c=True;
        }
        return  wrkOrderListInProgress;
    }
    //**************************************************************************************
    // Method To update work order Status to on Hold when WorkOrderLineItem  status is on Hold
    // @param newWorkOrderLineItems: List of new Work Order Line Items
    // @return wrkOrderListInHold: List of WorkOrders which status is going to change in to OnHold
    // 7 June 2017 Neeraj Kumawat T-608155/I-277751
    //**************************************************************************************
    public static List<WorkOrder> updateWorkOrderStatusToOnHold(List<WorkOrderLineItem> newWorkOrderLineItems){
        Set<Id> onHoldWorkOrderIds=new Set<Id>();
        for(WorkOrderLineItem wrkLineItem: newWorkOrderLineItems){
            if(wrkLineItem.status=='On Hold'){
                onHoldWorkOrderIds.add(wrkLineItem.WorkOrderId);
            }
        }
        List<WorkOrder> wrkOrderListInHold=[Select Id,Status From WorkOrder Where Id IN:onHoldWorkOrderIds];
        for(WorkOrder wrkOrder: wrkOrderListInHold){
            wrkOrder.Status='On Hold';
            //T-610373 Neeraj Kumawat Setting flag Is_Updated_From_Work_Order_Line_Item to check that work order status is updated 
            // From Work order line item or from work order
            wrkOrder.Is_Updated_From_Work_Order_Line_Item__c=True;
        }
        return  wrkOrderListInHold;
    }
    //**************************************************************************************************************************
    // Method To update work order Status to Closed based on WorkOrderLineItem #T-605293
    // @param newWorkOrderLineItems: List of new Work Order Line Items
    // @return wrkOrderListClosed: List of CLosed WorkOrders
    // 29 May 2017 Neeraj Kumawat T-605293/S-470343
    //7 June 2017 Neeraj Kumawat #I-277751/#T-608155 Close the work order if 
    //Status of all WOLIs are Closed and/or Cancelled (possible combo of both)
    //**************************************************************************************************************************
    public static List<WorkOrder> updateWorkOrderStatusToClosed(List<WorkOrderLineItem> newWorkOrderLineItems){
        List<WorkOrderLineItem> workOrderLineItems= [Select Id,WorkOrderId,Status From WorkOrderLineItem 
                                                     Where Id IN: newWorkOrderLineItems 
                                                     AND (Status='Adjustment Succeeded'  OR Status='Adjustment Failed' 
                                                          OR Status='Canceled' OR Status='Closed' OR Status='Manual Process')];
        Set<Id> WorkOrderIds=new Set<Id>();
        for(WorkOrderLineItem wrkOrderLI: workOrderLineItems){
            WorkOrderIds.add(wrkOrderLI.WorkOrderId);
        }
        //Fetching WorkOrder reocrd based on workorderLineItem associated to WorkOrder
        List<WorkOrder> wrkOrderListClosed=new List<WorkOrder>();
        List<WorkOrder> wrkOrderList= [Select Id,Status, (Select Id,Status,RecordTypeId From WorkOrderLineItems) 
                                       From WorkOrder Where Id IN :WorkOrderIds];
        for(WorkOrder wrk: wrkOrderList)
        {
            //isWorkOrderClosed:Boolean variable to check workorderLineItems are closed or not.
            Boolean isWorkOrderClosed=null;
            List<WorkOrderLineItem> WOLIList=wrk.WorkOrderLineItems;
            if(WOLIList.size()>0){
                isWorkOrderClosed=true;
            }
            //checking whether previously existing WorkOrderLineItems are not canceled,Adjustment Succedded,Adjustment Failed,
            //and Manual Process and Closed
            
            //Variable to count the work order line item size
            Integer WOLISize=0;
            for(WorkOrderLineItem wrkLI: WOLIList)
            {
                if(wrkLI.Status!='Adjustment Succeeded' && wrkLI.Status!='Adjustment Failed' && wrkLI.Status!='Manual Process' && wrkLI.Status!='Closed'){
                    //7 June 2017 #T-608155/#I-277751 Neeraj Kumawat Update the code to closed the work order 
                    //if Status of all WOLIs are Closed and/or Cancelled (possible combo of both). 
                    //increasing the cound when work order line item status is canceled
                    if(wrkLI.Status=='Canceled'){
                        WOLISize+=1;
                    }
                    if(wrkLI.Status=='Canceled' && WOLISize==WOLIList.size()){
                        //Not Closing the work order if all work order line item status is canceled
                        isWorkOrderClosed=false;
                    }else if(wrkLI.Status!='Canceled'){
                        //Not Closing the work order if any of the work order line item status is not canceled, adjustment succeedd, 
                        //adjustment Failed or Manual Process
                        isWorkOrderClosed=false;
                    }
                }
            }
            //Auto Close workOrder when all the work order Line Item associated to workOrder are closed
            if(isWorkOrderClosed!=null && isWorkOrderClosed){
                wrk.Status='Closed';
                //T-610373 Neeraj Kumawat Setting flag Is_Updated_From_Work_Order_Line_Item to check that work order status is updated 
                // From Work order line item or from work order
                wrk.Is_Updated_From_Work_Order_Line_Item__c=True;
                wrkOrderListClosed.add(wrk);
            }
        }
        return wrkOrderListClosed;        
    }
    //**************************************************************************************************************************
    // Method To update work order Status to Canceled when all workorderlineitems are canceled #T-607554
    // @param newWorkOrderLineItems: List of new Work Order Line Items
    // @return wrkOrderListClosed: List of Canceled WorkOrders
    // 6 June 2017 Neeraj Kumawat T-607554/S-470343
    //**************************************************************************************************************************
    public static List<WorkOrder> updateWorkOrderStatusToCanceled(List<WorkOrderLineItem> newWorkOrderLineItems){
        List<WorkOrderLineItem> workOrderLineItems= [Select Id,WorkOrderId,Status From WorkOrderLineItem 
                                                     Where Id IN: newWorkOrderLineItems 
                                                     AND Status='Canceled'];
        Set<Id> WorkOrderIds=new Set<Id>();
        for(WorkOrderLineItem wrkOrderLI: workOrderLineItems){
            WorkOrderIds.add(wrkOrderLI.WorkOrderId);
        }
        //Fetching WorkOrder reocrd based on workorderLineItem associated to WorkOrder
        List<WorkOrder> wrkOrderListCanceled=new List<WorkOrder>();
        List<WorkOrder> wrkOrderList= [Select Id,Status, (Select Id,Status From WorkOrderLineItems) 
                                       From WorkOrder Where Id IN :WorkOrderIds];
        for(WorkOrder wrk: wrkOrderList)
        {
            //isWorkOrderCanceled:Boolean variable to check workorderLineItems are canceled or not.
            Boolean isWorkOrderCanceled=null;
            List<WorkOrderLineItem> WOLIList=wrk.WorkOrderLineItems;
            if(WOLIList.size()>0){
                isWorkOrderCanceled=true;
            }
            //checking whether previously existing WorkOrderLineItems are not canceled
            for(WorkOrderLineItem wrkLI: WOLIList)
            {
                if(wrkLI.Status!='Canceled'){
                    isWorkOrderCanceled=false;
                }
            }
            //Auto Canceled workOrder when all the work order Line Item associated to workOrder are Canceled
            if(isWorkOrderCanceled!=null && isWorkOrderCanceled){
                wrk.Status='Canceled';
                //T-610373 Neeraj Kumawat Setting flag Is_Updated_From_Work_Order_Line_Item to check that work order status is updated 
                // From Work order line item or from work order
                wrk.Is_Updated_From_Work_Order_Line_Item__c=True;
                wrkOrderListCanceled.add(wrk);
            }
        }
        return wrkOrderListCanceled;        
    }
    //**************************************************************************************************************************
    // Method To Find duplicate work order by MPS MID, Line Item Total, and Adjustment Reason
    //@param exisitingWOLI: List of all exisiting WorkOrderLineItems in the system
    //@param duplicatesWOLI: list that will returning all duplicates in the system by MPS MID, Line Item Total, and Adjustment Reason
    
    // 31 May 2017 Kate Jovanovic
    //**************************************************************************************************************************
    
    
    public static List<WorkOrderLineItem> findDuplicates(List<WorkOrderLineItem> newWorkOrderLineItems){
        List<WorkOrderLineItem> exisiting = [Select MPS_MID__c, Account_Name__c,Line_Item_Total__c,Adjustment_Reason__c from WorkOrderLineItem ];
        System.debug(exisiting + 'this is exisiting');
        List<WorkOrderLineItem> results = new List<WorkOrderLineItem>();   
        Map<String,WorkOrderLineItem> newMap = new Map<String,WorkOrderLineItem>();
        for(WorkOrderLineItem x: exisiting){
            if(x.MPS_MID__c !=null){
                newMap.put(x.MPS_MID__c,x);
            }
        }
        
        for(WorkOrderLineItem y: newWorkOrderLineItems){
            if(newMap.containsKey(y.MPS_MID__c)){
                results.add(y);
            }
            
        }
        System.debug(results + 'this is it');
        return results;
        
    }
}