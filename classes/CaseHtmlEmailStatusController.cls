/*********************************************************************************************************************
* Author        :  Appirio (Neeraj Kumawat) (T-549511)
* Date          :  Oct 27th, 2016 
* Purpose       :  Class for getting Contact Html Email Status record of respective Case.
*
* Date Modified                Modified By           Description of the update
*[02/06/2017]                  Bobby Cheek           Ensure that Sharing rules are enforced in CaseHtmlEmailStatusController.cls files 
*                                                    by adding "with sharing" keyword to the class declaration
**********************************************************************************************************************/
public with sharing class CaseHtmlEmailStatusController {
    public List<HtmlEmailDetail> htmlEmailDetailList {get;set;}
    //Parameterized constructor for getting contactId and caseId of the case
    public CaseHtmlEmailStatusController(ApexPages.StandardController stdController){
        htmlEmailDetailList= new List<HtmlEmailDetail>();
        if(!Test.isRunningTest()){
            stdController.addFields(new List<String> {'Id','ContactId'});
        }
        Case cse = (Case)stdController.getRecord();
        List<Contact> contList = [Select name, (Select id,WhatId, Subject, Status From Tasks where whatId=:cse.Id),
                                  (SELECT TaskId,CreatedDate,TimesOpened,FirstOpenDate, LastOpenDate,EmailTemplateName FROM EmailStatuses) 
                                  From Contact where id=:cse.ContactId];
        system.debug('Case detail ='+cse);
        system.debug('Case Contact Id ='+cse.ContactId);
        if(contList.size()>0){
            Contact con=contList.get(0);          
            system.debug('contact name : '+con.Name);
            system.debug('Task list : '+con.tasks);
            system.debug('Email Status list : '+con.emailStatuses);
            //Getting Tasks and Email from Contact
            for(Task tsk : con.tasks){
                HtmlEmailDetail htmlemail= new HtmlEmailDetail();              
                htmlemail.subject=tsk.Subject;
                Id tskId=tsk.Id;
                system.debug('task subject : '+tsk.Subject);
               for(EmailStatus email : con.emailStatuses){
                    Id  emailTaskId= email.TaskId;
                   //Getting email of same task Id
                   if(tskId==emailTaskId){
                    htmlemail.status='Sent';
                    system.debug('Email Status Detail : '+email);
                   
                    if(email.CreatedDate!=null){
                      htmlemail.dateSent=email.CreatedDate.format(); 
                    }
                   if(email.LastOpenDate!=null){
                            htmlemail.lastOpened= email.LastOpenDate.format();
                   }
                   if(email.FirstOpenDate!=null){
                       htmlemail.dateOpened=email.FirstOpenDate.format();
                   }
                   htmlemail.timesOpened=email.TimesOpened;
                   // Updating the email status to read if it's open by the recipient
                   if(email.TimesOpened!=null){
                       integer timeOpen=Integer.valueOf(email.TimesOpened);
                       if(timeOpen>0){
                           htmlemail.status='Read';             
                       }
                   }
                    // Adding to the htmlEmail list
                    htmlEmailDetailList.add(htmlemail);
                   }
               }
            }
        }
        System.debug('List of html EmailDetail ='+htmlEmailDetailList);
    }
}