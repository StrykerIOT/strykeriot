/*********************************************************************
* Appirio a WiPro Company
* Name: CaseWithUnreadEmailBatchHelper
* Description: Batch Helper class called by the CaseWithUnreadEmailBatch batch. 
* Created Date: 06/08/2017
* Created By: Bobby Cheek
* 
* Date Modified                Modified By                  Description of the update
* [09-06-2017]               [Gaurav Dudani]                [updated all messageRead criteria as per task # T-608727.]
**********************************************************************/
public with sharing class CaseWithUnreadEmailBatchHelper {

  private final String CLASSNAME = '\n\n*** CaseWithUnreadEmailBatchHelper.';
  
  public Database.Querylocator GetQueryLocator(String pQuery){
    return Database.getQueryLocator(pQuery);
  }  
  
  /*author : Appirio
    date : 06/08/2017
    description :  Builds a SOQL query for Case records in the "Closed Case Queue With New/Unread Emails" Queue 
    paramaters : none
    returns : a String representing a SOQL query to retrieve a bach of records to be processed
  */
  public String GetQuery(){
    
    final String METHODNAME = CLASSNAME + 'GetQuery()';
    system.debug(LoggingLevel.INFO, METHODNAME);   
   
    String SOQL='';
        if(Test.isRunningTest()){
           SOQL = 'SELECT Id, Owner.Name FROM Case WHERE Owner.Name =\'Closed Case Queue With New/Unread Emails\'';      
        }else{
           SOQL = 'SELECT Id, Owner.Name FROM Case WHERE Owner.Name =\'Closed Case Queue With New/Unread Emails\'';  
        }    
    system.debug(LoggingLevel.DEBUG, '\n\n*** SOQL :: '+ SOQL);
    return SOQL;
  }  
  
  /*author : Appirio
    date : 06/08/2017
    description : removes Cases from the "Closed Case Queue With New/Unread Emails" Queue setting Ownership to Prior owner
                  if ALL emails for the Case have been read.  
    paramaters : list of sObjects
    returns : nothing
  */
  public void ProcessBatch(list<SObject> scope){
    
    list<Case> caseList = new list<Case>();
    caseList.addall((list<Case>)scope); 
    
    set<Id> caseIdSet = new set<Id>();
    for(Case cs : caseList){
        caseIdSet.add(cs.Id);
    }
    
    // create a list of Cases with related EmailMessages
    list<Case> caseWithEmailMessagesList = [SELECT Id, OwnerId, Prior_Owner__c, (SELECT Id, Status FROM EmailMessages) FROM Case WHERE Id IN : caseIdSet];
    
    // create a list of Case to update Ownership to Prior Owner
    list<Case> caseToSetToPriorOwnerList = new list<Case>();
    boolean allMessagesRead = true;
    for(Case cs : caseWithEmailMessagesList){
        
        allMessagesRead = true;
        if(cs.EmailMessages.Size() > 0){
            for(EmailMessage eMessage : cs.EmailMessages){
                // Updated Criteria for sent and replied status by Gaurav Dudani on 9th June 2017 as per task # T-608727.
                if(eMessage.Status == '1' || eMessage.Status == '2' || eMessage.Status == '3'){
                    allMessagesRead = true;
                    break;
                }else{
                    allMessagesRead = false;
                }
            }
        } 
        
        if(allMessagesRead){
            cs.OwnerId = cs.Prior_Owner__c;
            caseToSetToPriorOwnerList.add(cs);
        }       
    }
    system.debug('PriorOwner is'+caseToSetToPriorOwnerList);
    update caseToSetToPriorOwnerList;
  }     
}