/**********************************************************************
* Appirio, Inc
* Test Class Name: TaskTriggerHandlerTest
* Class Name: [TaskTriggerHandler]
* Description: [T-561973 Test Class for TaskTriggerHandler]
* Created Date: [15/12/2016]
* Created By: [Gaurav Dudani] (Appirio)
* 
* Date Modified                Modified By                  Description of the update
* [MON DD, YYYY]               [FirstName LastName]     [Short description for changes]
* [02/17/2017]                 [Vikash Agarwal]         [Fixing test cases which failed due to workflow chanegs - task id : T-578362]
* [03/03/2017]                 [Neeraj Kumawat]         [Removed test method regarding case status update as updateCaseStatus method is removed 
														 from TaskTriggerHandler by Kate according to this T-580692 task. Task Id T-580072 ]
* [03/03/2017]                 [Shannon Howe]           [Fix test class errors (remove bulk tests for now)]
* [29/03/2017]                 [Neeraj Kumawat]         [Added Test.StartTest()and Test.stopTest() in  updateCaseStatusTestMethod2 
														as sync from dev4 to dev5]
***********************************************************************/
@isTest
public class TaskTriggerHandlerTest {
/***************************************************************************************
// Test method to delete and validate Tasks 
// Profile is System Administrator
// case status is Pending Future Action.
// and case origin is Internal Task, to check Internal Task Counter.
****************************************************************************************/
    static testMethod void updateCaseStatusTestMethod3(){
        String profileId = [select id from Profile where name ='System Administrator' limit 1].Id;
        User usr = TestUtilities.createTestUser(1,profileId,true);
        Integer numberOfRecords = 20;
        System.runAs(usr){
            List<Case> caseList = new List<Case>();
            List<Account> acc=TestUtilities.createAccount(1,true);
            for(Case cse: TestUtilities.createCase(numberOfRecords,null,false)){
                cse.Future_Action_Date__c = System.Today()+2;
                cse.status = 'In Progress';
                cse.subject = 'test subject';
                cse.origin= 'Internal Task';
                cse.Account=acc.get(0);
                caseList.add(cse);
            }
            insert caseList;
            for(Case c: [select ID,Internal_Task__c from Case where id in :caseList]){
                System.assertEquals(null,c.Internal_Task__c);
            }
            List<Task> taskList = new List<Task>();
            for(Task tsk: TestUtilities.createTask(numberOfRecords,false)){
                tsk.whatId = caseList[0].Id;
                tsk.Case_Status__c = 'Pending Future Action';
                tsk.Type='Internal Task';
                taskList.add(tsk);
            }
            List<Database.SaveResult> result= Database.insert(taskList,false) ;
            System.debug('Result of updateCaseStatusTestMethod 200='+result);
            List<Task> objTsk=[select Id from Task where id in :taskList];
            System.assertEquals(numberOfRecords,objTsk.size());
            for(Case c: [select ID,Internal_Task__c from Case where id =:caseList[0].id]){
                System.assertEquals(numberOfRecords,c.Internal_Task__c);
            }
            delete taskList;
            system.debug('Deleted Successfully');
            objTsk=[select Id from Task where id in :taskList];
            System.assertEquals(0,objTsk.size());
        }
    }
    /***************************************************************************************
// Test method to delete and validate Tasks 
// Profile is System Administrator
// case status is Pending Future Action.
// case origin is Internal Task, to check Internal Task Counter.
// and Event List Test Data with type Internal Task
****************************************************************************************/
    static testMethod void updateCaseStatusTestMethod2(){
        Test.startTest();
        String profileId = [select id from Profile where name ='System Administrator' limit 1].Id;
        User usr = TestUtilities.createTestUser(1,profileId,true);
        Integer numberOfRecords = 20;
        System.runAs(usr){
            List<Case> caseList = new List<Case>();
            List<Account> acc=TestUtilities.createAccount(1,true);
            for(Case cse: TestUtilities.createCase(numberOfRecords,null,false)){
                cse.Future_Action_Date__c = System.Today()+2;
                cse.status = 'In Progress';
                cse.subject = 'test subject';
                cse.origin= 'Internal Task';
                cse.Account=acc.get(0);
                caseList.add(cse);
            }
            insert caseList;
            for(Case c: [select ID,Internal_Task__c from Case where id in :caseList]){
                System.assertEquals(null,c.Internal_Task__c);
            }
            List<Task> taskList = new List<Task>();
            for(Task tsk: TestUtilities.createTask(numberOfRecords,false)){
                tsk.whatId = caseList[0].Id;
                tsk.Case_Status__c = 'Pending Future Action';
                tsk.Type='Internal Task';
                taskList.add(tsk);
            }
            List<Event> evtList = new List<Event>();
            for(Event evt: TestUtilities.createEvent(numberOfRecords,false)){
                evt.Subject ='Test';
                evt.whatId = caseList[0].Id;
                evt.Case_Status__c = 'Pending Future Action';
                evt.StartDateTime=System.Today();
                evt.EndDateTime=System.Today()+2;
                evt.CurrencyIsoCode='USD';
                evt.Type='Internal Task';
                evt.ActivityDateTime=System.Today();
                evtList.add(evt);
            }
            List<Database.SaveResult> result1= Database.insert(evtList,false) ;
            System.debug('Result of updateCaseStatusTestMethod 0='+result1);
            System.assertEquals(numberOfRecords,Database.countQuery('Select count() from Event where id in :evtList'));
            for(Case c: [select ID,Internal_Task__c from Case where id =:caseList[0].id]){
                System.assertEquals(numberOfRecords,c.Internal_Task__c);
            }
            
            List<Database.SaveResult> result= Database.insert(taskList,false) ;
            System.debug('Result of updateCaseStatusTestMethod 0='+result);
            System.assertEquals(numberOfRecords,Database.countQuery('Select count() from Task where id in :taskList'));
            for(Case c: [select ID,Internal_Task__c from Case where id =:caseList[0].id]){
            	
                System.assertEquals((numberOfRecords+numberOfRecords),c.Internal_Task__c);
            }
        }
        Test.stopTest();
    }
    /***************************************************************************************
// Test method to delete and validate Tasks 
// Profile is System Administrator
// case status is Pending Future Action.
// and case origin is Internal Task, to check Internal Task Counter.
****************************************************************************************/
    static testMethod void updateCaseStatusTestMethod1(){
        String profileId = [select id from Profile where name ='System Administrator' limit 1].Id;
        User usr = TestUtilities.createTestUser(1,profileId,true);
        Integer numberOfRecords = 20;
        System.runAs(usr){
            List<Case> caseList = new List<Case>();
            List<Account> acc=TestUtilities.createAccount(1,true);
            for(Case cse: TestUtilities.createCase(numberOfRecords,null,false)){
                cse.Future_Action_Date__c = System.Today()+2;
                cse.status = 'In Progress';
                cse.subject = 'test subject';
                cse.origin= 'Internal Task';
                cse.Account=acc.get(0);
                caseList.add(cse);
            }
            insert caseList;
            for(Case c: [select ID,Internal_Task__c from Case where id in :caseList]){
                System.assertEquals(null,c.Internal_Task__c);
            }
            List<Task> taskList = new List<Task>();
            for(Task tsk: TestUtilities.createTask(numberOfRecords,false)){
                tsk.whatId = caseList[0].Id;
                tsk.Case_Status__c = 'Pending Future Action';
                tsk.Type='Internal Task';
                taskList.add(tsk);
            }
            List<Event> evtList = new List<Event>();
            for(Event evt: TestUtilities.createEvent(1,false)){
                evt.Subject ='Test';
                evt.whatId = caseList[0].Id;
                evt.Case_Status__c = 'Pending Future Action';
                evt.StartDateTime=System.Today();
                evt.EndDateTime=System.Today()+2;
                evt.CurrencyIsoCode='USD';
                evt.Type='Email';
                evt.ActivityDateTime=System.Today();
                evtList.add(evt);
            }
            System.debug('Event List:'+evtList);
            Database.SaveResult result1= Database.insert(evtList.get(0),false) ;
            System.debug('Result of updateCaseStatusTestMethod 0='+result1);
            System.assertEquals(1,Database.countQuery('Select count() from Event where id in :evtList'));
            for(Case c: [select ID,Customer_Interactions__c from Case where id =:caseList[0].id]){
                System.assertEquals(1,c.Customer_Interactions__c);
            }
            
            Database.SaveResult result= Database.insert(taskList.get(0),false) ;
            System.debug('Result of updateCaseStatusTestMethod 0='+result);
            System.assertEquals(1,Database.countQuery('Select count() from Task where id in :taskList'));
            for(Case c: [select ID,Internal_Task__c from Case where id =:caseList[0].id]){
                System.assertEquals(1,c.Internal_Task__c); 
            }
        }
    }
    /***************************************************************************************
// Test method to update and validate Tasks 
// Profile is System Administrator
// case status is Pending Future Action.
// and case origin is Internal Task, to check Internal Task Counter.
****************************************************************************************/
    static testMethod void updateCaseStatusTestMethodMinus0(){
        String profileId = [select id from Profile where name ='System Administrator' limit 1].Id;
        User usr = TestUtilities.createTestUser(1,profileId,true);
        Integer numberOfRecords = 20;
        System.runAs(usr){
            List<Case> caseList = new List<Case>();
            List<Account> acc=TestUtilities.createAccount(1,true);
            for(Case cse: TestUtilities.createCase(numberOfRecords,null,false)){
                cse.Future_Action_Date__c = System.Today()+2;
                cse.status = 'In Progress';
                cse.subject = 'test subject';
                cse.origin= 'Internal Task';
                cse.Account=acc.get(0);
                caseList.add(cse);
            }
            insert caseList;
            for(Case c: [select ID,Internal_Task__c from Case where id in :caseList]){
            	System.debug('@@@@ '+c);
                System.assertEquals(null,c.Internal_Task__c);
            }
            List<Task> taskList = new List<Task>();
            for(Task tsk: TestUtilities.createTask(numberOfRecords,false)){
                tsk.whatId = caseList[0].Id;
                tsk.Case_Status__c = 'Pending Future Action';
                tsk.Type='Internal Task';
                taskList.add(tsk);
            }
            Database.SaveResult result= Database.insert(taskList.get(0),false) ;
            System.debug('Result of updateCaseStatusTestMethod 0='+result);
            List<Task> objTsk=[select Id from Task where id in :taskList];
            System.assertEquals(1,objTsk.size());
            for(Case c: [select ID,Internal_Task__c from Case where id =: caseList[0].id]){
                System.assertEquals(1,c.Internal_Task__c);
            }
            
			taskList[0].Case_Status__c = 'Awaiting Information';
			taskList[0].Type='Email';
           
            update taskList[0];
             for(Case c: [select ID,Internal_Task__c,Customer_Interactions__c from Case where id =: caseList[0].id]){
             	System.debug('Case detail ='+c);
                System.assertEquals(1,c.Customer_Interactions__c);
                System.assertEquals(0,c.Internal_Task__c);
            }
        }
    }
}