/************************************************************************
* Appirio, Inc
* Class Name: WorkOrderLineItemCloneController 
* Description: [#T-609056 #I-278782]
* Created Date: [19/06/2017]
* Created By: [Neeraj Kumawat] (Appirio)
***********************************************************************/

public with sharing class WorkOrderLineItemCloneController {
    public String rtType{get;set;}
    //Flag to check that page is already redirected
    public String isRedirected='';
    public  ApexPages.StandardController standardCont;
    public String duplicateMessage {get;set;}
    public  List<WorkOrderLineItem> duplicateWorkOrderList {get;set;}
    public Integer saveCounter {get;set;}
    //WorkOrderLineItem to prepopulate workOrder Id in Work Order lookup
    public WorkOrderLineItem wrkOrderLineItem{get;set;}
    public String WorkOrderLineItemId='';
    public WorkOrderLineItemCloneController(ApexPages.StandardController controller) {
        standardCont=controller;
        duplicateMessage='';
        rtType = ApexPages.currentPage().getParameters().get('RecordType');
        isRedirected=ApexPages.currentPage().getParameters().get('isRedirected');
        saveCounter =0;
        WorkOrderLineItemId=ApexPages.currentPage().getParameters().get('id');
        System.debug('WorkOrderLineItemId'+WorkOrderLineItemId);
        List<WorkOrderLineItem> wrkOrderLineItemList=new List<WorkOrderLineItem>();
        wrkOrderLineItem=new WorkOrderLineItem();
            wrkOrderLineItemList=[SELECT ID,RecordTypeId,WorkOrderId From WorkOrderLineItem Where Id=:WorkOrderLineItemId];
            if(wrkOrderLineItemList.size()>0){
                wrkOrderLineItem.WorkOrderId=wrkOrderLineItemList.get(0).WorkOrderId;
                rtType=wrkOrderLineItemList.get(0).RecordTypeId;
            }
    }
    //******************************************************************************************
    // Method to override Standard Work Order Line Item page if Record Type =Adjustments
    // 19 June 2017 Neeraj Kumawat
    //******************************************************************************************
    public PageReference checkRecordType(){
        //Not going to redirect the page if checkDuplicateRecord flag is set to true.
        if(isRedirected!='true'){
            List<RecordType> recordTypeList = [Select Id, Name,DeveloperName FROM RecordType where Id =:rtType];
            for(RecordType rt: recordTypeList){
                if(rt.DeveloperName =='NA_Adjustment_Work_Order_Line_Item' || rt.DeveloperName  =='MX_Adjustment_Work_Order_Line_Item'){
                    PageReference page = new PageReference('/apex/WorkOrderLineItemClone');
                    page.getParameters().put('RecordType', rtType);
                    page.getParameters().put('isRedirected', 'true');
                    //To prepopulate workOrder Id in workorder lookup
                    page.getParameters().put('id', WorkOrderLineItemId);
                    page.setRedirect(true);
                    return page;
                }
                else{
                    //Showing standard page of work order when record type is not equal to adjustment
                    PageReference standardpage = new PageReference('/'+WorkOrderLineItemId+'/e?clone=1&nooverride=1');
                    standardpage.getParameters().put('isRedirected', 'true');
                    standardpage.setRedirect(true);
                    return standardpage;
                }
            }
        }
        return null;
    }

    //************************************************************************************************************
    // Method to override Save method to show duplicate matches and restrict creation of WOLI on Closed WorkOrders
    // 19 June 2017 Neeraj Kumawat
    //************************************************************************************************************
      public PageReference save(){
        WorkOrderLineItem workOrderLineItemObj = (WorkOrderLineItem)standardCont.getRecord();
        System.debug('workOrderLineItemObj'+workOrderLineItemObj);
        WorkOrderLineItem workOrderLineItemClone=workOrderLineItemObj.clone();
        workOrderLineItemClone.WorkOrderId= wrkOrderLineItem.WorkOrderId;
        String workOrderId= workOrderLineItemClone.WorkOrderId;
        System.debug('workOrderLineItemClone'+workOrderLineItemClone);
        System.debug('workOrderLineItemClone.Adjustment_Type__c'+workOrderLineItemClone.Adjustment_Type__c);
        duplicateWorkOrderList= checkDuplicateRecord(workOrderLineItemClone);
        saveCounter++;
        WorkOrder wrkOrderObj=[Select Id,Status From WorkOrder Where Id =:workOrderId];
        String status=wrkOrderObj.Status;
        if ((status=='Closed' || status=='Canceled') && saveCounter >= 1)
        {
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,System.Label.Restrict_WOLI_Creation_ON_Closed_OR_Canceled_WorkOrder));
        }
        else if(status!='Closed' && status!='Canceled' && saveCounter >= 2)
        {   
            insert workOrderLineItemClone;
            PageReference page = new PageReference('/'+workOrderLineItemClone.Id);
            return page;
        }
        //End
        return null;
    }

    //******************************************************************************************
    // Method to override cancel button to bring the user back to the Work Order.
    // 19 June 2017 Neeraj Kumawat
    //******************************************************************************************
    public PageReference cancel() {
        WorkOrderLineItem workOrderLineItemObj = (WorkOrderLineItem)standardCont.getRecord();
        PageReference page = new PageReference('/'+WorkOrderLineItemId);
        return page;
        
    }
    //******************************************************************************************
    // Method to check Duplicate work order line ApexTestQueueItem
    // @param workOrderLineItemObj:workOrderLineItem record 
    // @return List of duplicate WorkOrderLineItem
    // 19 June 2017 Neeraj Kumawat
    //******************************************************************************************
    public List<WorkOrderLineItem> checkDuplicateRecord(WorkOrderLineItem workOrderLineItemClone){
        List<WorkOrderLineItem> duplicateWorkOrderList=new List<WorkOrderLineItem>();
        Decimal lineItemTotal;
        String adjustmentType = workOrderLineItemClone.Adjustment_Type__c;
        System.debug('adjustmentType'+adjustmentType);
        Decimal lineItemQuantity = workOrderLineItemClone.Line_Item_Quantity__c;
        Decimal actualFeeBilled = workOrderLineItemClone.Actual_Fee_Billed__c;
        Decimal lineItemAmount = workOrderLineItemClone.Line_Item_Amount__c;
        String adjustmentReason = workOrderLineItemClone.Adjustment_Reason__c;
        //Set WorkOrder Id from user input work order lookup
        workOrderLineItemClone.WorkOrderId=wrkOrderLineItem.WorkOrderId;
        String workOrderId= workOrderLineItemClone.WorkOrderId;
        String workOrderAccountName=null;
        //set Line Item Total
        if(adjustmentType == 'Customer Credit'){
            lineItemTotal = ((lineItemQuantity*actualFeeBilled) - (lineItemQuantity*lineItemAmount)) * -1;
            
        }
        if((lineItemQuantity * actualFeeBilled) > (lineItemQuantity* lineItemAmount)){
            lineItemTotal = ((lineItemQuantity *actualFeeBilled) - (lineItemQuantity * lineItemAmount));
        }
        
        workOrderLineItemClone.Line_Item_Total__c = lineItemTotal;
        //Get created workOrderLineItem account Name from Work Order Account
        WorkOrder wrkOrderObj=[Select Id, Account.Name From WorkOrder Where Id =:workOrderId];
        if(wrkOrderObj.Account!=null){
            workOrderAccountName=wrkOrderObj.Account.Name;
        }
        //Fetch dulicate Work order line item records
        duplicateWorkOrderList=[Select Id, WorkOrderId, Account_Name__c,MID__c,Adjustment_Reason_Category__c,
                                Adjustment_Reason__c,Subject, Line_Item_Total__c,LastModifiedDate, Processing_Date__c
                                From WorkOrderLineItem Where Line_Item_Total__c=:lineItemTotal AND 
                                Adjustment_Reason__c=:adjustmentReason AND Account_Name__c=:workOrderAccountName AND recordTypeId =:rtType];
        if(duplicateWorkOrderList.isEmpty()){
            duplicateMessage = System.Label.No_Duplicate_WorkOrderLineItem ;
        }
        else{
            duplicateMessage = System.Label.Duplicate_WorkOrderLineItems ;
        }   
        return duplicateWorkOrderList;
    }   
}