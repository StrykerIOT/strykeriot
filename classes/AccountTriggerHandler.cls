/*********************************************************************
* Appirio a WiPro Company
* Name: AccountTriggerHandler
* Description: [T-563821-HandlerClass - For updating Contact Tax Id from Account Tax Id]
* Created Date: [23/12/2016]
* Created By: [Neeraj Kumawat] (Appirio)
* 
* Date Modified                Modified By                  Description of the update
* [02/03/2017]                 [Aashish Sajwan]             [Optimize code of TriggerHandler] 
* [04/19/2017]                 [Bobby Cheek]                [Code Clean-up, added code to replace Account Process PB]
* [05/02/2017]                 [Aashish Sajwan]             [Added Condition to check key in map or not]
* [05/30/2017]                 [Poornima Bhardwaj]          [Added method updateWorkOrderCurrencyISOCode]
* [06/16/2017]                 [Poornima Bhardwaj]          [#T-609950 Added updateAccountRegionByProcessingCenterID method]
* [06/16/2017]                 [Poornima Bhardwaj]          [#T-609690 Deleted updateContactTaxId and updatingContactTaxIdFromAccountTaxId method]
**********************************************************************/
public class AccountTriggerHandler {
    
    static String CLASSNAME = '\n\n**** AccountTriggerHandler.METHODNAME()';
    /* static variables */ 
    static Map<Id,List<Contact>> mapAccntContacts = new Map<Id,List<Contact>>();
    static Map<Id,Account> mapAccounts = new Map<Id,Account>();
    
    
    /*  author : Appirio, Inc
        date : 04/19/2017
        description :  this method handles the 'before insert' events for the trigger
        paramaters : List of new ServiceContract objects
        returns : nothing
    */
    public static void onBeforeInsert(list<Account> newAccountList){
        
        string METHODNAME = CLASSNAME.replace('METHODNAME','onBeforeInsert');
        system.debug(LoggingLevel.INFO, 'INSIDE :: ' + METHODNAME);
        
        setOrUpdateCurrencyISOCodeOnAccount(newAccountList); 
        updateAccountRegionByProcessingCenterID(newAccountList,null);
    }
    
    /*  author : Appirio, Inc
        date : 04/19/2017
        description :  this method handles the 'before update' event for the trigger.
        paramaters : List of updated Account objects, List of old Account objects, Updated Account Id to Account Map, Old Account Id to Account Map
        returns : nothing
    */
    public static void onBeforeUpdate(list<Account> updatedAccountList, list<Account> oldAccountList, map<Id,Account> updatedAccountMap, map<Id,Account> oldAccountMap){
        
        string METHODNAME = CLASSNAME.replace('METHODNAME','onBeforeUpdate');
        system.debug(LoggingLevel.INFO, 'INSIDE :: ' + METHODNAME);
        
        setOrUpdateCurrencyISOCodeOnAccount(updatedAccountList);
        updateAccountRegionByProcessingCenterID(updatedAccountList,oldAccountMap);
    }
    
    /*  author : Appirio, Inc
        date : 04/19/2017
        description :  this method handles the 'after update' event for the trigger.
        paramaters : List of updated Account objects, List of old Account objects, Updated Account Id to Account Map, Old Account Id to Account Map
        returns : nothing
    */
    public static void onAfterUpdate(list<Account> updatedAccountList, list<Account> oldAccountList, map<Id,Account> updatedAccountMap, map<Id,Account> oldAccountMap){
        
        string METHODNAME = CLASSNAME.replace('METHODNAME','onAfterUpdate');
        system.debug(LoggingLevel.INFO, 'INSIDE :: ' + METHODNAME);
        
        updatingContactAndServiceContractCurrencyISOCode(updatedAccountList, updatedAccountMap);    
        updateWorkOrderCurrencyISOCode(updatedAccountList, oldAccountMap);
    } 
    
    
    
    /*  author : Appirio, Inc
        date : 04/19/2017
        description :  set or udate CurrencyISOCode on Account record
        paramaters : list of updated Account objects
        returns : nothing
    */
    private static void setOrUpdateCurrencyISOCodeOnAccount(list<Account> accountList){
        
        string METHODNAME = CLASSNAME.replace('METHODNAME','setOrUpdateCurrencyISOCodeOnAccount');
        system.debug(LoggingLevel.INFO, 'INSIDE :: ' + METHODNAME);
        
        for(Account acc : accountList){
            
            if(acc.Funding_Currency_Code__c !=''){
                acc.CurrencyIsoCode = acc.Funding_Currency_Code__c;
            }           
        }    
    }
    
    /*  author : Appirio, Inc
        date : 04/19/2017
        description :  updates Contact and Service Contract Currency ISO Codes to match Account
        paramaters : list of updated Account objects, AccountId to Account Map for updated Accounts
        returns : nothing
    */
    private static void updatingContactAndServiceContractCurrencyISOCode(list<Account> updatedAccountList, map<Id,Account> updatedAccountMap){      
        
        string METHODNAME = CLASSNAME.replace('METHODNAME','updatingContactAndServiceContractCurrencyISOCode');
        system.debug(LoggingLevel.INFO, 'INSIDE :: ' + METHODNAME);
        
        list<Contact> contactList = [SELECT Id, CurrencyIsoCode, AccountId FROM Contact WHERE AccountId IN :updatedAccountMap.keySet()];
        list<ServiceContract> serviceContractList = [SELECT Id, CurrencyIsoCode, AccountId FROM ServiceContract WHERE AccountId IN :updatedAccountMap.keySet()];
        
        map<Id, List<Contact>> accountIdToContactListMap = new map<Id, List<Contact>>();
        map<Id, List<ServiceContract>> accountIdToServiceContractListMap = new map<Id, List<ServiceContract>>();
        
        for(Contact con : contactList){
            if(accountIdToContactListMap.ContainsKey(con.AccountId)){
                accountIdToContactListMap.get(con.AccountId).add(con);
            }else{
                accountIdToContactListMap.put(con.AccountId, new list<Contact>{con});
            }           
        }
        
        for(ServiceContract ser : serviceContractList){
            if(accountIdToServiceContractListMap.ContainsKey(ser.AccountId)){
                accountIdToServiceContractListMap.get(ser.AccountId).add(ser);
            }else{
                accountIdToServiceContractListMap.put(ser.AccountId, new list<ServiceContract>{ser});
            }           
        }
        
        list<Contact> contactsToUpdate = new list<Contact>();
        list<ServiceContract> serviceContractsToUpdate = new list<ServiceContract>(); 
        
        for(Account acc : updatedAccountList){
            
            if(acc.Funding_Currency_Code__c == 'USD'){
                if(accountIdToContactListMap.ContainsKey(acc.Id)){
                    for(Contact con : accountIdToContactListMap.get(acc.Id)){
                        con.CurrencyIsoCode = 'USD';
                    }
                    contactsToUpdate.addAll(accountIdToContactListMap.get(acc.Id));
                }
                
                if(accountIdToServiceContractListMap.ContainsKey(acc.Id)){
                    for(ServiceContract ser : accountIdToServiceContractListMap.get(acc.Id)){
                        ser.CurrencyIsoCode = 'USD';
                    }
                    serviceContractsToUpdate.addAll(accountIdToServiceContractListMap.get(acc.Id));                 
                }               
            }else if(acc.Funding_Currency_Code__c == 'MXN'){
                if(accountIdToContactListMap.ContainsKey(acc.Id)){
                    for(Contact con : accountIdToContactListMap.get(acc.Id)){
                        con.CurrencyIsoCode = 'MXN';
                    }
                    contactsToUpdate.addAll(accountIdToContactListMap.get(acc.Id));
                }
                
                if(accountIdToServiceContractListMap.ContainsKey(acc.Id)){
                    for(ServiceContract ser : accountIdToServiceContractListMap.get(acc.Id)){
                        ser.CurrencyIsoCode = 'MXN';
                    }
                    serviceContractsToUpdate.addAll(accountIdToServiceContractListMap.get(acc.Id));                 
                }               
            }else if(acc.Funding_Currency_Code__c == 'CAD'){
                if(accountIdToContactListMap.ContainsKey(acc.Id)){
                    for(Contact con : accountIdToContactListMap.get(acc.Id)){
                        con.CurrencyIsoCode = 'CAD';
                    }
                    contactsToUpdate.addAll(accountIdToContactListMap.get(acc.Id));
                }
                
                if(accountIdToServiceContractListMap.ContainsKey(acc.Id)){
                    for(ServiceContract ser : accountIdToServiceContractListMap.get(acc.Id)){
                        ser.CurrencyIsoCode = 'CAD';
                    }
                    serviceContractsToUpdate.addAll(accountIdToServiceContractListMap.get(acc.Id));                 
                }               
            }
        }
        
        if(!contactsToUpdate.isEmpty() || contactsToUpdate.size() != 0) {
            update contactsToUpdate;
        } 
        
        if(!serviceContractsToUpdate.isEmpty() || serviceContractsToUpdate.size() != 0) {
            update serviceContractsToUpdate;
        }           
    }
    
     
    /*  Date : 05/30/2017
        Description :Updates WorkOrder Currency ISO Codes to match Account
        Paramaters : List of updated Account objects, AccountId to Account Map for updated Accounts
        Returns : Void
    */
    private static void updateWorkOrderCurrencyISOCode(list<Account> updatedAccountList, map<Id,Account> oldAccountMap){      
        
        list<workOrder> workOrderList = [SELECT Id, CurrencyIsoCode, AccountId FROM workOrder WHERE AccountId IN :oldAccountMap.keySet()];
        
        map<Id, List<workOrder>> accountIdToWorkOrderListMap = new map<Id, List<workOrder>>();
        
        for(workOrder wk : workOrderList){
            if(accountIdToWorkOrderListMap.ContainsKey(wk.AccountId)){
                accountIdToWorkOrderListMap.get(wk.AccountId).add(wk);
            }else{
                accountIdToWorkOrderListMap.put(wk.AccountId, new list<workOrder>{wk});
            }           
        }
        list<workOrder> workOrderToUpdate = new list<workOrder>();
        for(Account acc : updatedAccountList){
            if(acc.Funding_Currency_Code__c == 'USD' && oldAccountMap.get(acc.Id).Funding_Currency_Code__c!='USD'){
                if(accountIdToWorkOrderListMap.ContainsKey(acc.Id)){
                    System.debug('Inside if condition');
                    for(workOrder wk : accountIdToWorkOrderListMap.get(acc.Id)){
                        wk.CurrencyIsoCode = 'USD';
                    }
                    workOrderToUpdate.addAll(accountIdToWorkOrderListMap.get(acc.Id));
                }
                
            }else if(acc.Funding_Currency_Code__c == 'MXN' && oldAccountMap.get(acc.Id).Funding_Currency_Code__c!='MXN'){
                if(accountIdToWorkOrderListMap.ContainsKey(acc.Id)){
                    for(workOrder wk : accountIdToWorkOrderListMap.get(acc.Id)){
                        wk.CurrencyIsoCode = 'MXN';
                    }
                    workOrderToUpdate.addAll(accountIdToWorkOrderListMap.get(acc.Id));
                }
                
            }else if(acc.Funding_Currency_Code__c == 'CAD' && oldAccountMap.get(acc.Id).Funding_Currency_Code__c!='CAD'){
                if(accountIdToWorkOrderListMap.ContainsKey(acc.Id)){
                    for(workOrder wk : accountIdToWorkOrderListMap.get(acc.Id)){
                        wk.CurrencyIsoCode = 'CAD';
                    }
                    workOrderToUpdate.addAll(accountIdToWorkOrderListMap.get(acc.Id));
                }
                
            }
        }
        
        if(!workOrderToUpdate.isEmpty() || workOrderToUpdate.size() != 0) {
            update workOrderToUpdate;
        } 
        
        
    }  
    
 
     /*  author : Appirio, Inc
        date : 06/16/2017
        description :  updating Region form Processing Center Id 
        paramaters : list of updated Account objects, AccountId to Account Map for updated Accounts
        returns : nothing
        Added by Poornima Bhardwaj on 16-June-2017 as per Task #T-609950
    */
    private static void updateAccountRegionByProcessingCenterID(List<Account> newAccountList, Map<Id,Account> oldAccountMap){
         for(Account acc:newAccountList){
         //Execute when oldMap is null
         if(oldAccountMap==null){
             //Setting account region as Processing Center ID
             acc.Region__c=acc.Processing_Center_ID__c;
             
         }else if(oldAccountMap!=null && acc.Processing_Center_ID__c!=oldAccountMap.get(acc.Id).Processing_Center_ID__c){
                //Setting account region as Processing Center ID
                acc.Region__c=acc.Processing_Center_ID__c;
            }
         }  
    } 
    
    
/**************************************** DEPRICATED *****************************************
* 
* Method for calling action on After Insert Update of Task
* */
   //static Map<Id,List<Contact>> mapAccntContacts = new Map<Id,List<Contact>>();
   //static Map<Id,Account> mapAccounts = new Map<Id,Account>();
   /* public static void onAfterUpdate(List<Account> newAccountList, List<Account> oldAccountList, Map<Id,Account> newAccountMap, Map<Id,Account> oldAccountMap){
        //Checking when Account Tax Id is changed
        //
        List<Account> objAccountList  = new List<Account>();
        List<Contact> contList = new List<Contact>();
       
        if(newAccountList!=null && oldAccountMap!=null){
            mapAccounts= new Map<Id,Account>([SELECT Id, Tax_Id_Encrypted__c, Name FROM Account WHERE Id in: newAccountList ]);
            
            contList = [SELECT Id, AccountId,Tax_Id_Encrypted__c, Name 
                        FROM Contact 
                        where AccountId in: newAccountList];
                        
             for(Contact objCon : contList){
               if(!mapAccntContacts.containsKey(objCon.AccountId)){
                  mapAccntContacts.put(objCon.AccountId,new List<Contact>{objCon});
               }else{
                mapAccntContacts.get(objCon.AccountId).add(objCon);
               }
              
             }   
             system.debug('** Account of contacts map'+mapAccntContacts.values());
             system.debug('** Account of contacts map'+mapAccntContacts.keyset());                 
        }
        //create a map
        Map<Id,Contact> mapContacts = new Map<Id,Contact>();
        List<Contact> ConList = new List<Contact>();
        if(newAccountList!=null && oldAccountMap!=null){    
            for(Account newAcc: newAccountList){
                Account oldAcc = oldAccountMap.get(newAcc.Id);
                system.debug('newAcc.Tax_Id_Encrypted__c='+newAcc.Tax_Id_Encrypted__c);
                system.debug('OldAcc.Tax_Id_Encrypted__c='+oldAcc.Tax_Id_Encrypted__c);
                if(newAcc.Tax_Id_Encrypted__c != oldAcc.Tax_Id_Encrypted__c){
                    //Calling updateContactTaxId method for updating 
                    //Contact Tax Id when Account Tax Id changed
                  
                    ConList = updateContactTaxId(mapAccntContacts);
                    if(conList != null && conList.size()>0){
                        //add this contact to the map 
                        for(Contact objCon: conList){
                            if(!mapContacts.containsKey(objCon.Id)){
                                mapContacts.put(objCon.Id,objCon);
                            }
                        }                        
                    }                    
                }
            }
            //update map
            ////Aashish is the bomb diggity
            if(mapContacts.size()>0){
                update mapContacts.values();
            }
        }
    }*/   
}