/*****************************************************************************************************
* Appirio, Inc
* Test Class Name:FeeChargeTriggerHandlerTriggerHandlerTest
* Class Name: FeeChargeTriggerHandler
* Description:(#T-607016) Handler test for FeeChargeTriggerHandler Trigger bulk testing
* Created Date: [June 1, 2017]
* Created By: [Neeraj Kumawat] (Appirio)
* 
* Date Modified          Modified By              Description of the update
  [04-June-2017]         [Poornima Bhardwaj]      [#T-607016 Updated createReferenceListWOLI for creation of reference record
                                                  ,applied asserts,created method insertFeeChargeDescriptionNotRelativeToReference]
******************************************************************************************************/
@isTest
private class FeeChargeTypeControllerTest {
    static List<Fee_Charge__c> feeChargeList;
    static List<Reference__c> refList;
    static Integer numberOfRecords=1;
    static user agentUser;
    //************************************************************************************************
    // Method to create test data for FreeChargeTriggerHandler 
    // @return void
    // @param refExternalId-External Id For Reference
    // @param feeExternalId-External Id For Fee Charge
    // @param LanguageCode-Language Code For Reference
    // @param ChargeType-Charge Type For Fee Charge
    // 1-June-2017 Neeraj Kumawat #T-607016
    //****************************************************************************
    static void CreateTestData(String language,String refExternalId,String feeExternalId,String LanguageCode,String Category)
    {   //Custom Agent User Data
        List<UserRole> agentRole=[select id,name from UserRole where name='Service NA Manager'];
        List<Profile> agentProId=[select id,name from Profile where name='Custom: NA Agent'];
        agentUser=TestUtilities.createTestUser(1,agentProId[0].id,false);
        agentUser.UserRole=agentRole[0];
        agentUser.LanguageLocaleKey=language;
        insert agentUser;
        //Case Creation from Agent User
        System.runAs(agentUser)
        {
        //creating reference record
        refList=TestUtilities.createReferenceListWOLI(numberOfRecords,false);
        for(Integer i=0; i<refList.size(); i++){
            refList[i].Description__c='Test Description--Fee Charge';
            refList[i].External_Id__c=refExternalId+i;
            refList[i].Language_Code__c=LanguageCode;
            refList[i].Category__c=Category;
        }
        insert refList;
        //creating FeeCharge record
        feeChargeList=TestUtilities.createFeeCharge(numberOfRecords,false);
        for(Integer i=0;i<feeChargeList.size();i++){
            feeChargeList[i].External_Id__c=feeExternalId+i;
        }
        insert feeChargeList;
    }
    }
    //******************************************************************************************
    // Method to verify Description of FeeCharge relative to the reference Charge Type On Insert
    // @return void
    // 1-June-2017 Neeraj Kumawat #T-607016
    //******************************************************************************************
    static testMethod void insertFeeChargeDescriptionRelativeToReference()
    {
        createTestData('en_US','NA-3-333','NA-3-333','en_US','Charge Type');
        System.runAs(agentUser)
        {
        Test.startTest();
        Apexpages.currentpage().getparameters().put('id', feeChargeList[0].Id);
        ApexPages.StandardController fees = new ApexPages.StandardController(feeChargeList[0]);
        //Calling Constructor of FeeChargeTypeController
        FeeChargeTypeController feeChargeObj= new FeeChargeTypeController(fees);    
        //fetching External Id of FeeCharge to verify the functionality of FeeTriggerHandler
        List<Fee_Charge__c> newFreeChargeList=[Select Id,External_Id__c FROM Fee_Charge__c 
                                               WHERE Id IN:feeChargeList]; 
        for(Fee_Charge__c fee:newFreeChargeList){
            //Assert for verifying the Description of FeeCharge and the Expected Description
            System.assertEquals('Test Description--Fee Charge', feeChargeObj.descriptionValue);  
        }
        test.stopTest();
    }
    }
    //******************************************************************************************
    // Method to verify Description of FeeCharge not relative to the reference Charge Type
    // @return void
    // 4-June-2017 Poornima Bhardwaj #T-607016
    //******************************************************************************************
    static testMethod void insertFeeChargeDescriptionNotRelativeToReference()
    {
        createTestData('en_US','NA-3-333','NA-3-333','en_US','Statement Portfolio');
        System.runAs(agentUser)
        {
        Test.startTest();
        Apexpages.currentpage().getparameters().put('id', feeChargeList[0].Id);
        ApexPages.StandardController fees = new ApexPages.StandardController(feeChargeList[0]);
        //Calling Constructor of FeeChargeTypeController
        FeeChargeTypeController feeChargeObj= new FeeChargeTypeController(fees);    
        //fetching External Id of FeeCharge to verify the functionality of FeeTriggerHandler
        List<Fee_Charge__c> newFreeChargeList=[Select Id,External_Id__c FROM Fee_Charge__c 
                                               WHERE Id IN:feeChargeList]; 
        for(Fee_Charge__c fee:newFreeChargeList){
            //Assert for verifying the Description of FeeCharge and the Expected Description
            System.assertNotEquals('Test Description--Fee Charge', feeChargeObj.descriptionValue);  
        }
        test.stopTest();
    }
}
}