/*********************************************************************
* Appirio a WiPro Company
* Name: AccountTaxInfoRelationshipBatch
* Description: Batch class that runs daily to update Account.Tax_Info_link__c field with 
*                Related Tax_Information__c record See the 'AccountTaxInfoRelationshipBatchHelper' class 
*                for more details regarding specific functionality of individual methods
* Created Date: 04/26/2017
* Created By: Bobby Cheek
* 
* Date Modified                Modified By                  Description of the update
*
**********************************************************************/
global class AccountTaxInfoRelationshipBatch implements Database.Batchable<Sobject>{

  AccountTaxInfoRelationshipBatchHelper helper = new AccountTaxInfoRelationshipBatchHelper();

  global void AccountTaxInfoRelationshipBatch() {}
  
  global Database.Querylocator start(Database.Batchablecontext BC)
  {
    //system.debug(LoggingLevel.INFO, '\n\n AccountTaxInfoRelationshipBatch.start()\n\n');
    return helper.GetQueryLocator(helper.GetQuery());
  }

  global void execute(Database.Batchablecontext BC, list<SObject> scope)
  {
    //system.debug(LoggingLevel.INFO, '\n\n AccountTaxInfoRelationshipBatch.execute()\n\n');
    helper.ProcessBatch(scope);
  }

  global void finish (Database.Batchablecontext BC)
  {
    //system.debug(LoggingLevel.INFO, '\n\n AccountTaxInfoRelationshipBatch.finish()\n\n');
  }
}