/*********************************************************************
* Appirio, Inc
* Name: AllComplaintExtractionController
* Description: [Story# S-470712]
* Created Date: [05/09/2017]
* Created By: [Aashish Sajwan] (Appirio)
* 
* Date Modified                Modified By              Description of the update
**********************************************************************/
public class AllComplaintExtractionController{
    public List<ComplaintWrapper> ComplaintWrapperList{set;get;}
    public AllComplaintExtractionController(){
        ComplaintWrapperList=new List<ComplaintWrapper>();
        
    }
    //Complaint Wrapper to Bind on Page Block Table
    public class ComplaintWrapper{
        public string Request_Id{set;get;}  
        public string Mid{set;get;} 
        public string Dba{set;get;} 
        public string Clt_Grp{set;get;} 
        public string Portfolio{set;get;}  
        public string Iso_Name{set;get;}  
        public string Report_Group{set;get;}  
        public string Letter_Type{set;get;}
        public string Options{set;get;}
        public string Type_Of_Issue{set;get;} 
        public string Disputed_Amount{set;get;}
        public string Pulled_By{set;get;} 
        public string Received_On{set;get;}
        public string Assigned_To{set;get;} 
        public string Addressed_To{set;get;}   
        public string Created_On{set;get;} 
        public string Created_By{set;get;}
        public string Last_Update_On{set;get;} 
        public string Last_Update_By{set;get;} 
        public string Site{set;get;}
        public string Completed_On{set;get;} 
        public string Comments{set;get;} 
        public string Response_On{set;get;}  
        public string Log_In_Saber{set;get;} 
        public string Saber_Case_Number{set;get;} 
        public string Issue{set;get;} 
        public string Fee_Type{set;get;} 
        public string Invalid{set;get;}  
        public string User_Group{set;get;}   
        public string Eclipse_Status{set;get;}   
        public string Category{set;get;} 
        public string Sub_Category{set;get;} 
        public string Address_To_Manager{set;get;}  
        public string Address_To_Group{set;get;} 
        public string Auth_Vendor{set;get;}  
        public string Draft_Vendor{set;get;} 
        public string Foreign_Network{set;get;}  
        public string Par_Enty{set;get;} 
        public string Gateway{set;get;}  
        public string Gateway_Name{set;get;}  
        public string Coaching_Opportunity{set;get;}   
        public string Department{set;get;}  
        public string Reimbursement_Category{set;get;}   
        public string Reimbursement_Comments{set;get;} 
    }
}