/*********************************************************************
* Appirio, Inc
* Name: ArticleDetailsComponentController 
* Description: [S-470753-Controller for ArticleDetials Component]
* Created Date: [21/04/2017]
* Created By: [Aashish Sajwan] (Appirio)
* 
* Date Modified                Modified By                  Description of the update
[26/04/2017]                   [Aashish Sajwan]             [Update Soql for retriver Data Category on basis of article type #S-470753]
**********************************************************************/
public without sharing class ArticleDetailsComponentController {    
    
    public string articleId{get;set;}
    public ArticleDetails artDetails{get;set;}
    public string articleType{set;get;}
    public ArticleDetailsComponentController (){     
        
    }
    public void getArticleDetails(){
        system.debug('@@artDetails  value @@'+artDetails );
        system.debug('@@articleIdValue @@'+articleId);
        
        artDetails = new ArticleDetails();
        string QueueId ='';
        for(ProcessInstanceWorkItem  processItem : [SELECT Id,ActorId,OriginalActorId 
                                                    FROM ProcessInstanceWorkItem 
                                                    WHERE ProcessInstance.TargetObjectId =: articleId]){
                                                        QueueId  =  processItem.ActorId;
                                                    }
        if(QueueId!=''){
            for(Group grp : [SELECT Name, Id From Group 
                             WHERE Type = 'Queue' 
                             AND Id=:QueueId]){
                                 artDetails.QueueDetails = grp.Name;
                             }
        }
        system.debug('@@articleTypeValue @@'+articleType);
        String Query = 'SELECT id,DataCategoryName,ParentId,DataCategoryGroupName '+ 
            ' FROM '+articleType+' '+
            ' WHERE parentId=:articleId';
        List<sObject> sobjList=database.query(Query); 
        for(sObject obj : Database.Query(Query)){
            string DataCategoryName = (String)obj.get('DataCategoryName');
            string DataCategoryGroupName = (String)obj.get('DataCategoryGroupName');
            if(!string.isBlank(DataCategoryGroupName) && DataCategoryGroupName == 'Region'){
                artDetails.RegionName += DataCategoryName +',';
            }else if(!string.isBlank(DataCategoryGroupName) && DataCategoryGroupName == 'Intended_Audience'){
                artDetails.IntendedAudienceValue += DataCategoryName +',';
            }
            
        } 
        artDetails.RegionName = artDetails.RegionName.removeEnd(',');
        artDetails.IntendedAudienceValue  = artDetails.IntendedAudienceValue.removeEnd(',');
    }
    //Wrapper Class for Article Details
    public class ArticleDetails{
        
        public string RegionName {get;set;}
        public string IntendedAudienceValue {get;set;} 
        public string QueueDetails{get;set;}       
        public ArticleDetails(){
            RegionName ='';
            IntendedAudienceValue ='';
            QueueDetails = '';
        }
    }
}