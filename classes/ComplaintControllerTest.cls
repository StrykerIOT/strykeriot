/*********************************************************************
* Appirio, Inc
* Name: ComplaintControllerTest
* Description: [Task # T-593301 Test Class for ComplaintController]
* Created Date: [11/04/2017]
* Created By: [Gaurav Dudani] (Appirio)
* 
* Date Modified                Modified By              Description of the update
* [17/05/2017]               [Aashish Sajwan]     		[Added required field for Complaint object record]
* [23-June-2017]             [Poornima Bhardwaj]        [Added new method saveAndNewTestMethodNAUser and created Users]
**********************************************************************/
@isTest
public class ComplaintControllerTest {
     static user agentUser;
    //Test setup method to create test data for other methods.
    @testSetup
    static void setupTestData(){
      Complaint__c comp = new Complaint__c();
      comp.What_is_the_customer_s_complaint__c = 'test customer component';
      comp.Elavon_Channels__c = 'Bank of the West';
      comp.Communication_Method__c = 'Written';
      comp.Issue_Resolved__c = 'Yes';
      comp.Resolution_communicated_via__c = 'Verbal';
      insert comp;
    }
  
  
    //****************************************************************************************
    // Test Method to save Complaint record when save button is clicked on record.
    // @return void
    //*****************************************************************************************
    public static testmethod void saveAndNewTestMethodMXUser(){
        //Query data for Complaint object.
        List<Profile> agentProId=[select id,name from Profile where name='Custom: MX Agent'];
        agentUser=TestUtilities.createTestUser(1,agentProId[0].id,true);
        Complaint__c comp = [SELECT Name FROM Complaint__c];
        System.runAs(agentUser){
        ApexPages.StandardController stdController = new ApexPages.StandardController(comp);
        ComplaintController ctrl = new ComplaintController(stdController);
        Test.startTest();
        ctrl.saveAndNew();
        System.assertNotEquals(null,comp,'Record not created');
        Test.stopTest();
        }    
    }
    //****************************************************************************************
    // Test Method to save Complaint record when save button is clicked on record.
    // @return void
    //*****************************************************************************************
    public static testmethod void saveAndNewTestMethodNAUser(){
        //Query data for Complaint object.
        List<Profile> agentProId=[select id,name from Profile where name='Custom: NA Agent'];
        agentUser=TestUtilities.createTestUser(1,agentProId[0].id,true);
        Complaint__c comp = [SELECT Name FROM Complaint__c];
        System.runAs(agentUser){
        ApexPages.StandardController stdController = new ApexPages.StandardController(comp);
        ComplaintController ctrl = new ComplaintController(stdController);
        Test.startTest();
        ctrl.saveAndNew();
        System.assertNotEquals(null,comp,'Record not created');
        Test.stopTest();
        }    
    }
}