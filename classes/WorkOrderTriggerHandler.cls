/***********************************************************************************************************************
* Appirio, Inc
* Name: WorkOrderTriggerHandler
* Description: [T-601144 WorkOrder Trigger Handler for workOrder Trigger]
* Created Date: [12/05/2017]
* Created By: [Neeraj Kumawat] (Appirio)
* 
* Date Modified                Modified By               Description of the update
* 18-May-2017                 [Neeraj Kumawat]           (#T-602715) Updated case closed comments message.
* 19-May-2017                 [Neeraj Kumawat]           (#T-602715) Updated Error message on General Release
workorder status update.
* 24-May-2017                 [Gaurav Dudani]            (#T-605132) Updated to close case when Work order Status
are in (Canceled, Completed and Closed Status on a Case.)
* 29-May-2017                 [Aashish Sajwan]           [#T-606597 Upate  Last_Stamped_Write_Off_Balance__c and Last_Stamped_Collections_Balance__c  via Web service on Field update Work_Order_Approval_Status__c]
* 30-May-2017                 [Aashish Sajwan]           [#T-606597 Upate  Calling future methods]
* 2-Jun-2017                  [Aashish Sajwan]           [#T-607211 Update Approver value ]
* 6-Jun-2017                  [Aashish Sajwan]           [#T-607211 Update  in Logic Call Elavon API and update  Last_Stamped_Write_Off_Balance__c and Last_Stamped_Collections_Balance__c  via Web service on Field update on before insert]
* 6-Jun-2017                  [Neeraj Kumawat]           [#T-607753  Restrict general release work order status update based on adjustment work order status
*                                                        and update adjsutment work order approval status when GR work order status closed or canceled. and added method updateAdjsutmentWrkOrderApprovalStatus.]
* 7-Jun-2017                  [Aashish Sajwan]           [#T-607211 Update Approver value method]
* 9-June-2017                 [Neeraj Kumawat]           [#T-608730/I-278559 Added assignCETQueueToGeneralReleaseWorkOrder to assign workorderid to CET queue.]
* 12-June-2017                [Neeraj Kumawat]           [#T-608975/#I-278203 Allow  General Release work order status change when Adjsutment work order approval status is Approved for Processing]
* 14-June-2017                [Poornima Bhardwaj]        [#T-606531 Added workorderCurrencyISOCode method to set workOrder CurrencyISOCode as Account's Funding Currency Code]
* 15 June 2017                [Bobby Cheek]              [Enforcing CRUD and FLS Security in the Apex Class]
* 15 June 2017                [Neeraj Kumawat]           [Created method restrictWorkOrderStatusUpdate and Added null check for for FundingCurrencyCode__c field T-609308]
* 16 June 2017                [Neeraj Kumawat]           [T-610217 Added Try catch to show  proper error message when case status is not changed to closed due to unread email message on case.]
* 21 June 2017                [Neeraj Kumawat]           [T-610373 I-280180 Added check to show error msg on changing this work order status and Is_Updated_From_Work_Order_Line_Item flag is not true.]
* 22 June 2017                [Neeraj Kumawat]           [T-608730/I-278559 Updated assignCETQueueToGeneralReleaseWorkOrder to get the value of queue id from common method od utilitycls class.]
* [23 June 2017]              [Neeraj Kumawat]           [T-610968 Getting record type Id from UtilityCls.getRecordTypeId and fetching record type name from Custom Label.]
***************************************************************************************************************/
public class WorkOrderTriggerHandler {
    
    //****************************************************************************
    // Method to be call on OnBeforeInsert of WorkOrder
    // @param newWorkOrders: List of new Work Order 
    // @return void
    // 6-Jun-2017 Aashish Sajwan T-607211 Upate  Last_Stamped_Write_Off_Balance__c and Last_Stamped_Collections_Balance__c  via Web service on Field update Work_Order_Approval_Status__c
    public static void OnAfterInsert(List<WorkOrder> newWorkOrders) {
        //Calling method to update StampedDatesonApprovalStatus
        system.debug('** Calling SetStampedDates methods ***');
        SetStampedDates(newWorkOrders);
    }
    //****************************************************************************
    // Method to be call on OnBeforeInsert of WorkOrder
    // @param newWorkOrders: List of new Work Order 
    // @return void
    //Neeraj Kumawat T-608730/I-278559  Calling assignCETQueueToGeneralReleaseWorkOrder 
    public static void OnBeforeInsert(List<WorkOrder> newWorkOrders) {
        assignCETQueueToGeneralReleaseWorkOrder(newWorkOrders);
        workorderCurrencyISOCode(newWorkOrders);
    }
    //****************************************************************************
    // Method to be call on After update of WorkOrder
    //@param oldWorkOrderMap: Map of Old work Order
    // @param newWorkOrders: List of new Work Order 
    // @return void
    // 29 May 2017 Aashish Sajwan T-606597 Upate  Last_Stamped_Write_Off_Balance__c and Last_Stamped_Collections_Balance__c  via Web service on Field update Work_Order_Approval_Status__c
    //****************************************************************************
    public static void OnAfterUpdate(Map<Id,WorkOrder> oldWorkOrderMap,List<WorkOrder> newWorkOrders) {
        //Calling method to auto close case when all work order associated to case closed
        closeCaseOnWorkOrderclose(newWorkOrders);
        
        
    }
    //****************************************************************************
    // Method to be call on Before update of WorkOrder
    // @param oldWorkOrderMap: Map of Old work Order
    // @param newWorkOrders: List of new Work Order 
    // @return void
    // 12 May 2017 Neeraj Kumawat #T-601144
    // 6 June 2017 Neeraj Kumawat #T-607753 Added OnBeforeUpdate to restrict general release work order status update
    // and Update the Adjustment work order approval status to 'Approved for Processing' when GR status is closed or Canceled
    //****************************************************************************
    public static void OnBeforeUpdate(Map<Id,WorkOrder> oldWorkOrderMap,List<WorkOrder> newWorkOrders) {
        //Calling method to restict general release work order status update and update the adjsutment work order 
        //approval status to 'Approved for Processing' when GR status is closed or Canceled
        restrictGeneralReleaseWorkOrderStatusUpdate(oldWorkOrderMap,newWorkOrders);
        //#T-607753 Calling method to update adjsutmnet work order approval status to 'Approved for Processing' when adjustment WO 
        // approval status is changed to 'Approved - Pending' and  associated case don't have any General release Work order
        updateAdjsutmentWrkOrderApprovalStatus(oldWorkOrderMap,newWorkOrders);
        workorderCurrencyISOCode(newWorkOrders);
        //T-609308 Neeraj Kumawat 15 June 2017
        //Skip work order status update restriction for System Administrator and Custom: Integration profile user
        Id profileId=Userinfo.getProfileid();
        String profileName=null;
        if(profileId!=null){
            Profile profileObj = [Select Name from Profile where Id =: profileId];
            profileName = profileObj.name;
        }
        
        if(profileName!=null && profileName!='System Administrator' && profileName!='Custom: Integration'){
            //calling restrictWorkOrderStatusUpdate method to restrict WO status update except New or canceled
            restrictWorkOrderStatusUpdate(oldWorkOrderMap,newWorkOrders);
        }
    }
    //****************************************************************************
    // Method to restrict General Relase work order status update when there is pending adjustment available to case
    // @param oldWorkOrderMap: Map of Old work Order
    // @param newWorkOrders: List of new Work Order 
    // @return void
    // 12 May 2017 Neeraj Kumawat T-601144
    //****************************************************************************
    public static void restrictGeneralReleaseWorkOrderStatusUpdate(Map<Id,WorkOrder> oldWorkOrderMap,List<WorkOrder> newWorkOrders){
        //Adjustment work order list to update adjustment status                     
        List<WorkOrder> adjustmentWorkOrderList= new List<WorkOrder>(); 
        //General Relase Work Order Record Type Id
        //T-610968 Getting record type Id from UtilityCls.getRecordTypeId
        String genReleaseWrkOrderTypeId=UtilityCls.getRecordTypeId('WorkOrder',System.Label.General_Release);
        //Adjustment Work Order Record Type Id
        //T-610968 Getting record type Id from UtilityCls.getRecordTypeId
        String adjustmentWrkOrderTypeId=UtilityCls.getRecordTypeId('WorkOrder',System.Label.Adjustment);
        List<Id> caseIds=new List<Id>();
        //Map of Case Id and associated General Relase Work Order
        Map<Id,WorkOrder> caseWorkOrderMap=new Map<Id,WorkOrder>();
        for(WorkOrder newWrkOrder: newWorkOrders){
            //Check status update validation for General Release type work Order
            if(newWrkOrder.recordTypeId==genReleaseWrkOrderTypeId){
                //Getting old Work order to check the status change
                WorkOrder oldWrkOrder=oldWorkOrderMap.get(newWrkOrder.Id);
                //Checking if Status is changed for General Release work order
                if(oldWrkOrder.Status!=newWrkOrder.Status){
                    caseIds.add(newWrkOrder.CaseId);
                    caseWorkOrderMap.put(newWrkOrder.CaseId, newWrkOrder);
                }
            }
        }
        //Fetching Adjustment type Work Order from case
        List<Case> caseList=[Select Id, Subject,Status, (Select Id, Subject,RecordTypeId,Status,Work_Order_Approval_Status__c
                                                         From WorkOrders Where recordTypeId=: adjustmentWrkOrderTypeId)
                             From Case Where Id IN :caseIds];
        for(Case caseObj: caseList){
            List<WorkOrder> caseWorkOrderList=caseObj.WorkOrders;
            //Checking work order status
            for(WorkOrder adjustmentWrkOrder: caseWorkOrderList){
                WorkOrder genReleaseWrkOrder= caseWorkOrderMap.get(caseObj.Id);
                //#T-608975/#I-278203 Neeraj Kumawat  Added Approved for Processing condition to provide updation in 
                //General release work order.
                if(adjustmentWrkOrder.Status=='Canceled' || (adjustmentWrkOrder.Status!='Canceled' && (adjustmentWrkOrder.Work_Order_Approval_Status__c=='Approved - Pending' || adjustmentWrkOrder.Work_Order_Approval_Status__c=='Approved for Processing'))){
                    //No need to show the error
                }else{
                    //Showing error on General Relase status change if adjustment work order status is  canceled 
                    //OR if status is other than canceled and approval status is not approved-Pending
                    // #T-607753  Neeraj Kumawat 6-June-2017
                    //19-May-2017 (#T-602715) Updated Error message. NK
                    genReleaseWrkOrder.addError(System.Label.Restrict_Closing_of_Work_Order);
                }
                //Updating Adjustment Work order status to 'Approved for Processing' when general release work order status is
                // changed to closed or canceled and adjustmentWrkOrder status is not canceled
                // #T-607753  Neeraj Kumawat 6-June-2017
                if((genReleaseWrkOrder.Status=='Closed' || genReleaseWrkOrder.Status=='Canceled') && adjustmentWrkOrder.Status!='Canceled'){
                    adjustmentWrkOrder.Work_Order_Approval_Status__c='Approved for Processing';
                    adjustmentWorkOrderList.add(adjustmentWrkOrder);
                }
            }
        }
        update adjustmentWorkOrderList;
    }
    //****************************************************************************
    // Method to restrict work order status update except New or canceled  once there is not WOLI on the WO
    // @param oldWorkOrderMap: Map of Old work Order
    // @param newWorkOrders: List of new Work Order 
    // @return void
    // 15 May 2017 Neeraj Kumawat T-609308
    //21-June 2017 Neeraj Kumawat T-610373 I-280180
    //Added check for Is_Updated_From_Work_Order_Line_Item flag. showing error when this flag is not checked
    //****************************************************************************
    public static void restrictWorkOrderStatusUpdate(Map<Id,WorkOrder> oldWorkOrderMap,List<WorkOrder> newWorkOrders){
        //Fetching General release work Order record type id
        //T-610968 Getting record type Id from UtilityCls.getRecordTypeId
        String genReleaseWrkOrderTypeId=UtilityCls.getRecordTypeId('WorkOrder',System.Label.General_Release);
        Map<Id,WorkOrder> workOrderMapWithOutCanceledWOLI=new Map<Id,WorkOrder>([Select Id,Status, (Select Id,Status From WorkOrderLineItems) 
                                                                                 From WorkOrder Where Id IN :newWorkOrders]);
        for(WorkOrder newWrkOrder: newWorkOrders){
            //Check status update validation if work order record type is not General Release work Order
            if(newWrkOrder.recordTypeId!=genReleaseWrkOrderTypeId){
                //Getting old Work order to check the status change
                WorkOrder oldWrkOrder=oldWorkOrderMap.get(newWrkOrder.Id);
                //Checking if Status is changed for work order
                if(oldWrkOrder.Status!=newWrkOrder.Status){
                    //T-610373 I-280180 Neeraj Kumawat 21-June 2017
                    //Added check to show error msg when Is_Updated_From_Work_Order_Line_Item flag is not true
                    if(newWrkOrder.Is_Updated_From_Work_Order_Line_Item__c){
                        newWrkOrder.Is_Updated_From_Work_Order_Line_Item__c=false;
                    }else{
                        // showing error when work order is changed to any other status except New or Canceled
                        if(newWrkOrder.Status!='New' && newWrkOrder.Status!='Canceled'){
                            //Showing error if work order status is changes to any status except New or Canceled
                            newWrkOrder.addError(System.Label.Restrict_New_OR_Canceled_Work_Order_Status);  
                        }
                        // showing error when work order is changed to  New or Canceled and work order has WOLI 
                        if(newWrkOrder.Status=='Canceled' || newWrkOrder.Status=='New'){
                            //if status is canceled or New then check is there any open WOLI on WO if yes then  show error message
                            List<WorkOrderLineItem> wrkOrderLineItemsWithOutCanceled=workOrderMapWithOutCanceledWOLI.get(newWrkOrder.Id).WorkOrderLineItems;
                            //Showing error if work order status is changed to canceled and there is already work order line item
                            if(wrkOrderLineItemsWithOutCanceled.size()>0){
                                newWrkOrder.addError(System.Label.Restrict_New_OR_Canceled_Work_Order_Status);
                            }
                        }
                    }
                    
                }
            }
        }
    }
    //****************************************************************************
    // Method to update adjustment work order approval status to 'Approved for Processing' when adjustment WO 
    // approval status is changed to 'Approved - Pending' associated case don't have any General release Work order
    // @param oldWorkOrderMap: Map of Old work Order
    // @param newWorkOrders: List of new Work Order 
    // @return void
    // 6 June 2017 Neeraj Kumawat T-607753
    //****************************************************************************
    public static void updateAdjsutmentWrkOrderApprovalStatus(Map<Id,WorkOrder> oldWorkOrderMap,List<WorkOrder> newWorkOrders){
        //General Relase Work Order Record Type Id
        //T-610968 Getting record type Id from UtilityCls.getRecordTypeId
        String genReleaseWrkOrderTypeId=UtilityCls.getRecordTypeId('WorkOrder',System.Label.General_Release);
        //Adjustment Work Order Record Type Id
        //T-610968 Getting record type Id from UtilityCls.getRecordTypeId
        String adjustmentWrkOrderTypeId=UtilityCls.getRecordTypeId('WorkOrder',System.Label.Adjustment);
        List<Id> caseIds=new List<Id>();
        //Map of Case Id and associated Adjustment Work Order
        Map<Id,WorkOrder> caseWorkOrderMap=new Map<Id,WorkOrder>();
        for(WorkOrder newWrkOrder: newWorkOrders){
            //Check status update Adjsutment type work Order
            if(newWrkOrder.recordTypeId==adjustmentWrkOrderTypeId){
                //Getting old Work order to check the status change
                WorkOrder oldWrkOrder=oldWorkOrderMap.get(newWrkOrder.Id);
                //Checking if Status is changed for Adjustment  work order and it is Approved - Pending
                if(oldWrkOrder.Work_Order_Approval_Status__c!=newWrkOrder.Work_Order_Approval_Status__c && newWrkOrder.Work_Order_Approval_Status__c=='Approved - Pending'){
                    caseIds.add(newWrkOrder.CaseId);
                    caseWorkOrderMap.put(newWrkOrder.CaseId, newWrkOrder);
                }
            }
        }
        //Fetching General Release Work Order from case
        List<Case> caseList=[Select Id, Subject,Status, (Select Id, Subject
                                                         From WorkOrders Where recordTypeId=: genReleaseWrkOrderTypeId)
                             From Case Where Id IN :caseIds];
        //looping all case list on which adjsutment work order aprroval status updated
        for(Case caseObj: caseList){
            List<WorkOrder> caseGenWorkOrderList=caseObj.WorkOrders;
            //Updating adjustment work order approval status to 'Approved for Processing' when case don't have any 
            //General release work order
            if(caseGenWorkOrderList.size()==0){
                WorkOrder adjustmentWrkOrder= caseWorkOrderMap.get(caseObj.Id);
                adjustmentWrkOrder.Work_Order_Approval_Status__c='Approved for Processing';
            }
        }
    }
    //**************************************************************************************************************
    // Method to close the case when all work order related to case are either closed,Completed or Canceled status.
    // @param newCases: List of Case
    // @return void
    // 12 May 2017 Neeraj Kumawat T-601144
    // 24 May 2017 Gaurav Dudani T-605132
    //**************************************************************************************************************
    public static void closeCaseOnWorkOrderclose(List<WorkOrder> newWorkOrders){
        // Start
        // Updated the Query to fetch workorders with Completed and canceled statuses as well.
        // By Gaurav Dudani on 24 May 2017 as per task # T-605132.
        List<WorkOrder> workOrderList=  [Select Id, CaseId,Status,Subject From WorkOrder Where Id IN: newWorkOrders 
                                         AND (Status='Closed' OR Status='Completed' OR Status='Canceled')];
        //End
        Set<Id> caseIds=new Set<Id>();
        for(WorkOrder wrkOrder: workOrderList){
            caseIds.add(wrkOrder.CaseId);
        }
        List<Case> closeCaseList=new List<Case>();
        //Fetching Case reocrd based on workorder associated case
        List<Case> caseList= [Select Id, Subject,Status, (Select Id, Subject,RecordTypeId,Status From WorkOrders) 
                              From Case Where Id IN :caseIds];
        for(Case caseObj: caseList){
            //isWorkOrderClosed:Boolean variable to check workorder are closed or not.
            Boolean isWorkOrderClosed=null;
            //wrkOrderSubject: to store all the workorder subject associated to the case
            String wrkOrderSubject='';
            List<WorkOrder> caseWorkOrderList=caseObj.WorkOrders;
            if(workOrderList.size()>0){
                isWorkOrderClosed=true;
            }
            //Checking work order status
            for(WorkOrder wrk: caseWorkOrderList){
                // Start 
                // Updated to verify workorders with Completed and canceled statuses. 
                // By Gaurav Dudani on 24 May 2017 as per task # T-605132.
                if(wrk.Status!='Closed' && wrk.Status!='Completed' && wrk.Status!='Canceled'){
                    //End       
                    isWorkOrderClosed=false;
                }
                wrkOrderSubject+=wrk.Subject+',';
            }
            //Auto Close case when all the work order associated to case are closed
            if(isWorkOrderClosed!=null && isWorkOrderClosed){
                
                // 15 June 2017 Bobby Cheek checking FLS on Case fields
                if(Schema.sObjectType.Case.fields.Status.isUpdateable())
                    caseObj.Status='Closed';
                
                if(wrkOrderSubject!=null){
                    wrkOrderSubject=wrkOrderSubject.removeEnd(',');
                    
                    //(#T-602715) Updated case closed comments message.NK
                    // 15 June 2017 Bobby Cheek checking FLS on Case fields
                    if(Schema.sObjectType.Case.fields.Internal_Comments__c.isUpdateable())
                        caseObj.Internal_Comments__c=System.Label.Work_Orders+' '+wrkOrderSubject
                        +' '+System.Label.Case_Internal_Comments_On_WorkOrder_Close;
                    
                    // 15 June 2017 Bobby Cheek checking FLS on Case fields
                    if(Schema.sObjectType.Case.fields.Reason.isUpdateable())
                        caseObj.Reason='Suggestion/Solution Provided';
                }
                closeCaseList.add(caseObj);
            }
        }
        //Closing the case when all work Order are closed
        //T-610217 Neeraj Kumawat 19-June 2017
        // Added Try catch to show  proper error message when case status is not changed to closed 
        //due to unread email message on case.
        try{
            update closeCaseList;
        }catch(Exception ex){
            for(WorkOrder wrkOrder: newWorkOrders){
                //If error message contains case closed error due to unread email message 
                //Then showing custom error message
                if(ex.getMessage().contains(System.Label.CaseClosureOnUnreadEmail)){
                    ex.setMessage(System.Label.WorkOrderCloseOnCaseUnreadEmail);
                    wrkOrder.addError(ex.getMessage());
                }
            }
        }
    }
    
    
    //****************************************************************************
    // Method to UpdateStampedDates as Per Approval Status Field value
    // @param newWorkOrders: List of new Work Order 
    // @return void
    // 29 May 2017 Aashish Sajwan T-606597
    //****************************************************************************
    private  static void SetStampedDates(List<WorkOrder> newWorkOrders){
        system.debug('Inside UpdateStampedDatesOnApprovalStatus method');
        try{
            //Check Adjustment Recrod Type 
            //T-610968 Getting record type Id from UtilityCls.getRecordTypeId
            Id adjustmentWrkOrderTypeId= UtilityCls.getRecordTypeId('WorkOrder',System.Label.Adjustment);
            system.debug('** adjustmentWrkOrderTypeId values ***'+ adjustmentWrkOrderTypeId);
            List<WorkOrder> newWorkOrderList = new List<WorkOrder>();
            for(WorkOrder objWorkOrder : newWorkOrders){
                if(objWorkOrder.RecordTypeId == adjustmentWrkOrderTypeId){
                    newWorkOrderList.add(objWorkOrder);
                }
            }
            
            if(newWorkOrderList.size()>0){
                system.debug('** newWorkOrderList values ***'+ newWorkOrderList);
                UtilityCls.UpdateStampedFields(newWorkOrderList);
            }
            
        }catch(Exception ex){
            system.debug('Exception ='+ex);
        }
    }
    
    
    
    //****************************************************************************
    // Method to assign work order to CET queue based on work Order Account Processing center id
    // @param newWorkOrders: List of new Work Order 
    // @return void
    // 9 June 2017 Neeraj Kumawat T-608730/I-278559
    //****************************************************************************
    public static void assignCETQueueToGeneralReleaseWorkOrder(List<WorkOrder> newWorkOrders){
        //calling getCETQueueMap to get queueName with Id
        Map<String,String> queueNameMap= UtilityCls.getCETQueueMap();
        String  naCetQueueId=queueNameMap.get('NA_CET_Team_General_Release');
        String mxCetQueueId=queueNameMap.get('MX_CET_Team_General_Release');
        //General Relase Work Order Record Type Id
        //T-610968 Getting record type Id from UtilityCls.getRecordTypeId
        String genReleaseWrkOrderTypeId=UtilityCls.getRecordTypeId('WorkOrder',System.Label.General_Release);
        for(WorkOrder wrkOrder: newWorkOrders){
            //Assign owner to queue only for general release work order
            if(wrkOrder.recordTypeId==genReleaseWrkOrderTypeId){
                //Assigning owner of work order to CET queue based on account processing center id
                if(naCetQueueId!=null && wrkOrder.Account_Processing_Center_ID__c!=null  && wrkOrder.Account_Processing_Center_ID__c=='NA'){
                    if(Schema.sObjectType.WorkOrder.fields.OwnerId.isCreateable()){
                        wrkOrder.OwnerId=naCetQueueId;
                    }
                }else if(mxCetQueueId!=null && wrkOrder.Account_Processing_Center_ID__c!=null  && wrkOrder.Account_Processing_Center_ID__c=='QUERE'){
                    if(Schema.sObjectType.WorkOrder.fields.OwnerId.isCreateable()){
                        wrkOrder.OwnerId=mxCetQueueId;
                    }
                }
            }
        }
    }
    
    //****************************************************************************
    // Method to set workOrder's CurrencyISOCode as Account's Funding Currency Code
    // @param newWorkOrders: List of new Work Order 
    // @return void
    // 14 June 2017 Poornima Bhardwaj T-606531
    //****************************************************************************
    public static void workorderCurrencyISOCode(List<WorkOrder> newWorkOrders){
        for(WorkOrder wk:newWorkOrders){
            //T-609308 Neeraj Kumawat 15 June 2017
            //Null check for FundingCurrencyCode__c field
            if(wk.FundingCurrencyCode__c!=null && wk.FundingCurrencyCode__c!=wk.CurrencyIsoCode){
                wk.CurrencyIsoCode=wk.FundingCurrencyCode__c;
            }
        }
    }
}