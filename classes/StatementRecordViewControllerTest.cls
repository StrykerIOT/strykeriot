/*********************************************************************
* Appirio, Inc
* Name: StatementRecordViewControllerTest
* Description: [Task# T-557407]
* Created Date: [12/07/2016]
* Created By: [Aashish Sajwan] (Appirio)
* 
* Date Modified                Modified By              Description of the update
* [Wed 07, 2016]               [Aashish Sajwan]         [T:557407 ]
* [Fri 17,March 2017]          [Aashish Sajwan]         
**********************************************************************
//****************************************************************************
// Test Class to test the StatementRecordViewController.cls
//****************************************************************************/
@istest
public class StatementRecordViewControllerTest {
    public static String AccountId;
    public static String MID;
    public static String StatementId;
    public static String StatementDate;
 public static testmethod void testCallout1() {  
        //Creating Test Data
        //Creating External Doc, ContentVersion and FeedItem records 
        TestData();
        
        //Sending Parameters
        Test.setCurrentPageReference(new PageReference('Page.ViewStatement'));         
        System.currentPageReference().getParameters().put('StatementId', StatementId);
        System.currentPageReference().getParameters().put('AccountId', AccountId);
        System.currentPageReference().getParameters().put('MID', MID);
        System.currentPageReference().getParameters().put('StatementDate', StatementDate);
        
        //Calling Constructor
        StatementRecordViewController obj=new StatementRecordViewController();
        
        Test.StartTest();
        //***********************Positive Use Cases***********************
        
        //For Process Block IF
        TestProcessMethodIF();
        
        //For Process Block Else + Save Data Block 
        TestProcessMethodElse();
        
        
        Test.StopTest();
    }
    public static testmethod void testCallout2() {  
        //Creating Test Data
        //Creating External Doc, ContentVersion and FeedItem records 
        TestData();
        
        //Sending Parameters
        Test.setCurrentPageReference(new PageReference('Page.ViewStatement'));         
        System.currentPageReference().getParameters().put('StatementId', StatementId);
        System.currentPageReference().getParameters().put('AccountId', AccountId);
        System.currentPageReference().getParameters().put('MID', MID);
        System.currentPageReference().getParameters().put('StatementDate', StatementDate);
        
        //Calling Constructor
        StatementRecordViewController obj=new StatementRecordViewController();
        
        Test.StartTest();
        
        //************************Negative Use Cases**********************
        
        //For Process Block 
        TestProcessMethodCatchBlock();
        
        //For Save Data
        
        Test.StopTest();
    }
    //Creating Test Data of Account,External Doc, FeedItem and Content Version
    static void TestData()
    {
        //Create Test Account                                                 
        Account Acc=TestUtilities.createTestAccount(true);                                   
        AccountId=Acc.Id;                                                      
        MID=Acc.MID__c;  
        StatementId='2759115';                                           //2759115
        //Creating Test External Document Data               
        External_Document__c objExtDoc=TestUtilities.SaveExternalDoc(Acc,StatementId,MID,false,true);
       
        StatementDate=String.valueof(Date.Today().addDays(2));
        
        //Creating ContentVersion & feedItem
        ContentVersion con=TestUtilities.SaveContentVersion('pdf','hbjhbjhjbb',MID,true);
        
        FeedItem fd=TestUtilities.SaveFeedItems(MID,objExtDoc,con,true); 
    }
    //For Creating and Calling Mock Class
    static void TestMockProcessRecord()
    {
        //Creating Mockup Object
        ElavonServicesImagingserviceUMock objMock=new ElavonServicesImagingserviceUMock();
        objMock.setVariable('ProcessRecord');
        String Check=objMock.getVariable();
        Test.setMock(WebServiceMock.class,objMock);
    }
    //For Testing IF Block of Process Record Block
    static void TestProcessMethodIF()
    {
         StatementRecordViewController obj=new StatementRecordViewController();
         obj.ProcessRecord();
         System.assert(obj.contentId!=null);
    }
    //For Testing Catch Block of Process Record Method
    static void TestProcessMethodCatchBlock()
    {
         StatementRecordViewController obj=new StatementRecordViewController();
         obj.mapExternalDocs.keySet().clear();
         obj.ProcessRecord();
    }
    //For Testing Else Part of Process Method
    static void TestProcessMethodElse()
    {
        StatementRecordViewControllerTEST.TestMockProcessRecord();
        StatementRecordViewController obj=new StatementRecordViewController();
        //Clearing mapExternalDocs
        obj.mapExternalDocs.clear();
        //obj.StatementDate=null;
        obj.ProcessRecord();     
        //Test Else Part of Process Method
        //obj.StatementDate=Date.Today();
        obj.ProcessRecord();
        //Check Data in Else part
        System.assert(obj.contentId!=null);
    }
}