/**********************************************************************
* Appirio, Inc
* Test Class Name: caseTriggerHandlerTest
* Class Name: caseTriggerHandler
* Description: Handler test for case; bulk testing
* Created Date: [March 7, 2017]
* Created By: [Kate Jovanovic] (Appirio)
* 
* Date Modified          Modified By              Description of the update
* [23-Mar2017]          [Aashish Sajwan]        [Optimization of code]
* [12-April-2017]       [Neeraj Kumawat]        [T-590411 Added test method for case internal comments]
* [24-April-2017]       [Neeraj Kumawat]        [T-590411 Updated code to pass failed test method]
* [16-May-2017]         [Neeraj Kumawat]        [T-602715 Added test methods for Case General Release creation work order 
And Update the user creation for new Profile.]
* [18-May-2017]         [Gaurav Dudani]         [T-592212 Added test method to test Complaint creation if complaint applicability is "yes"]
* [18-May-2017]         [Gaurav Dudani]         [T-603030 Added test method to test preventing case from getting closed if it contains unread/new emails]
* [23 May 2017]         [Neeraj Kumawat]        [Updated Test method to Assigning Work order based on Processing center id. T-601144]
* [24-May-2017]         [Poornima Bhardwaj]     [T-603987 Added test Methods to test creation of work order testCreateWorkOrderAdjustment]
* [24-May-2017]         [Poornima Bhardwaj]     [T-592161 Added test Methods to test creation of work order testCreateWorkOrderFeeMaintenance]
* [31 May 2017]         [Neeraj Kumawat]        [Added test method testPreventCaseClosureOnOpenWorkOrdersOnCase for prevent 
case case closure when there are open work order on case.T-606953] 
***********************************************************************/
@isTest
public class caseTriggerHandlerTest{
    static List<Case> caseList;
    static List<Account> acc;
    static user agentUser;
    static user MXUser;
    static user managerUser;
    static user realtionshipUser;
    static Integer numberOfRecords = 1;
    //****************************************************************************
    // Method to create test data for relationship manager
    // @return void
    // 23-May-2017 Neeraj Kumawat T-602715
    //****************************************************************************
    static void createTestDataForRelationshipManager(String processingCenterId){
        List<UserRole> globalOpsRole=[select id,name from UserRole where name='Global Ops'];
        List<Profile> relationshipProdileId=[select id,name from Profile where name='Custom: Relationship Manager'];
        realtionshipUser=TestUtilities.createTestUser(1,relationshipProdileId[0].id,false);
        realtionshipUser.Username='testrelationship@testdomain.comtestuser';
        realtionshipUser.CommunityNickname='test';
        realtionshipUser.UserRole=globalOpsRole[0];
        insert realtionshipUser;
        System.runAs(realtionshipUser){ 
            //Creating account with USD currency Iso Code
            acc= TestUtilities.createAccount(1,false);
            for(Account a:acc){
                a.CurrencyIsoCode='USD';
                a.MID__c='12345678';
                a.Processing_Center_ID__c = processingCenterId;
            }
            insert acc;
        }
    }
    static void CreateTestData(){
        //Custom Manager User Data
        List<UserRole> managerRole=[select id,name from UserRole where name='Global Ops'];
        // Updated By Neeraj Kumawat as per new Profile for Agent. 22-May-2017
        List<Profile> managerProId=[select id,name from Profile where name='Custom: NA Manager'];
        managerUser=TestUtilities.createTestUser(1,managerProId[0].id,false);
        managerUser.Username='test100@testdomain.comtestuser';
        managerUser.CommunityNickname='kjbjhvbhj';
        managerUser.UserRole=managerRole[0];
        insert managerUser;
        System.debug('Manager User:'+managerUser.UserRole);
        //end
        //Custom Agent User Data
        List<UserRole> agentRole=[select id,name from UserRole where name='Service NA Manager'];
        // Updated By Neeraj Kumawat as per new Profile for Agent. 22-May-2017
        List<Profile> agentProId=[select id,name from Profile where name='Custom: NA Agent'];
        agentUser=TestUtilities.createTestUser(1,agentProId[0].id,false);
        System.debug('Agent User:'+agentUser);
        agentUser.UserRole=agentRole[0];
        agentUser.ManagerId = managerUser.Id;
        insert agentUser;
        
        System.debug('Agent User:'+agentUser);
        //end
        //
        //Case Account Data
        acc=TestUtilities.createAccount(1,false);
        for(Account a:acc){
            a.CurrencyIsoCode='CAD';
            a.MID__c='12345678';
            a.Processing_Center_ID__c = 'NA';
        }
        insert acc;
        System.debug('Account Inserted'+acc);
    }
    
    /*
This method is to check that when a user selects "return to previous owner" that the prior owner is set 
and the new ownerid is set correctly. 
*/
    static testMethod void callout1(){
        //Create Test Data
        CreateTestData();
        //end
        
        System.runAs(agentUser){
            
            System.debug('**Account Id:'+acc[0].id+' Account MID:'+acc[0].MID__c);
            //Case Creation from Agent User
            caseList=TestUtilities.createCase(1,agentUser.id,false);
            for(Case c: caseList){
                c.subject='Testing';
                //c.MID_text__c=acc[0].MID__c;
                //c.AccountId=acc[0].id;
            }
            insert caseList;
            System.debug('Case Created:'+caseList);
            
            List<Case> caseRecordOwner=[select id,OwnerId,CurrencyIsoCode, Prior_Owner__c from Case 
                                        where id=:caseList[0].id];
            System.debug('Case Owner'+caseRecordOwner);
            
            //update the Owner to Manager User
            for(Case c: caseList){
                c.OwnerId=managerUser.id;
            }
            update caseList;
            System.debug('**Updated Case Lst:'+caseList);
            System.assertEquals(caseList[0].OwnerId, managerUser.id);
            System.debug('Case Id:'+caseList[0].id);
            
        }
        
        //Test that the prior owner is the Manager and current owner is Agent by returning case to previous user
        System.runAs(managerUser)
        {
            List<Case> caseLstNewOwner=[select Id,Return_Case_to_Previous_Owner__c,OwnerId from Case 
                                        where id=:caseList[0].id];
            System.debug('caseLstNewOwner'+caseLstNewOwner);
            for(Case c: caseLstNewOwner)
            {
                c.Return_Case_to_Previous_Owner__c=true;
            }
            update caseLstNewOwner;
        }
        System.runAs(agentUser)
        {
            List<Case> caseLstPrevOwner=[select Id,Return_Case_to_Previous_Owner__c,OwnerId, Prior_Owner__c from Case 
                                         where id=:caseList[0].id];
            System.debug('caseLstPrevOwner'+caseLstPrevOwner);
            System.assert(caseLstPrevOwner[0].OwnerId==agentUser.id);
            System.assertEquals(caseLstPrevOwner[0].Prior_Owner__c, managerUser.id);
        }
    }
    
    
    /*
This testMethod is to check to see if a task was created for each case when an Agent and Manager create a Case.
*/
    static testMethod void callout2() {        
        //Create Test Data
        CreateTestData();
        //end     
        ///Test Method for Agent
        System.runAs(agentUser){
            
            //Case Creation
            caseList=TestUtilities.createCase(1,agentUser.id,false);
            for(Case c: caseList){
                c.subject='Testing';
                //c.MID_text__c=acc[0].MID__c;
                //c.AccountId=acc[0].id;
            }
            insert caseList;
            System.debug('Case Created:'+caseList);
            
            //List<Case> taskOnCase = [Select Id, (Select id, ActivityDate, LastModifiedDate FROM OpenActivities 
            //ORDER BY ActivityDate ASC, LastModifiedDate DESC LIMIT 1) FROM case where id=:caseList[0].id LIMIT 1];
            Id testcase;
            //Checks to see if task was created when Case was created and the task is related to the Case
            List<Case> caseList2 = [Select id from Case where id=:caseList[0].id LIMIT 1];
            for(Case x: caseList2){
                testcase = x.id;
            }
            List<Task> taskList2 = [Select id, whatid from Task where whatid=:testcase LIMIT 1];
            ID taskwhatId;
            for(Task tsk : taskList2){
                taskwhatId = tsk.whatId;
            }
            System.assertEquals(testcase, taskwhatId);
            //System.assertEquals(taskOnCase[0].whatid, caseList[0].id);            
            
        }
        ///Test Method for Manager
        System.runAs(managerUser){
            
            //Case Creation
            caseList=TestUtilities.createCase(1,agentUser.id,false);
            for(Case c: caseList){
                c.subject='Testing';
                //c.MID_text__c=acc[0].MID__c;
                //c.AccountId=acc[0].id;
            }
            insert caseList;
            System.debug('Case Created:'+caseList);
            
            //List<Case> taskOnCase = [Select Id, (Select id, ActivityDate, LastModifiedDate FROM OpenActivities 
            //ORDER BY ActivityDate ASC, LastModifiedDate DESC LIMIT 1) FROM case where id=:caseList[0].id LIMIT 1];
            Id testcase;
            //Checks to see if task was created when Case was created and the task is related to the Case
            
            List<Task> taskList2 = [Select id, whatid from Task where whatid=:caseList[0].id LIMIT 1];
            ID taskwhatId;
            for(Task tsk : taskList2){
                taskwhatId = tsk.whatId;
            }
            //asserting equal case id equals task whatid
            System.assertEquals(caseList[0].id, taskwhatId);
            
            
            
        }
    }
    //****************************************************************************
    // Method to test Case  Internal Comments on Create
    // @return void
    //****************************************************************************
    static testMethod void testCaseCommentonInsert(){
        //Create Test Data
        CreateTestData();
        //end
        Test.StartTest();
        //System.runAs(agentUser){
        //Case Creation from Agent User
        caseList=TestUtilities.createCase(numberOfRecords,agentUser.id,false);
        for(Case c: caseList){
            c.subject='Testing Case Internal Comments on Insert';
            c.Internal_Comments__c='Test internal comments on Insert';
        }
        insert caseList;
        List<CaseComment> caseCommentList=[Select Id, CommentBody From CaseComment where ParentId IN : caseList];
        if(caseCommentList.size()>0){
            for(CaseComment  caseCommentObj: caseCommentList)
                System.assertEquals(caseCommentObj.CommentBody, 'Test internal comments on Insert');
        }
        // }
        Test.stopTest();
    }
    //****************************************************************************
    // Method to test Case  Internal Comments on Update
    // @return void
    //****************************************************************************
    static testMethod void testCaseCommentonUpdate(){
        //Create Test Data
        CreateTestData();
        //end
        Test.StartTest();
        //System.runAs(agentUser){
        //Case Creation from Agent User
        caseList=TestUtilities.createCase(numberOfRecords,agentUser.id,false);
        for(Case c: caseList){
            c.subject='Testing Case Internal Comments on Update';
        }
        insert caseList;
        for(Case caseObj:caseList){
            caseObj.Internal_Comments__c='Test internal comments on Update';
        }
        update caseList;
        List<CaseComment> caseCommentList=[Select Id, CommentBody From CaseComment where ParentId IN : caseList];
        if(caseCommentList.size()>0){
            for(CaseComment  caseCommentObj: caseCommentList)
                System.assertEquals(caseCommentObj.CommentBody, 'Test internal comments on Update');
        }
        // }
        Test.stopTest();
    }
    //****************************************************************************
    // Method to test Case Currency Iso Code with Account currency
    // @return void
    //****************************************************************************
    static testMethod void testAccountCurrencyIsocode(){
        createTestDataForRelationshipManager('NA');
        Test.StartTest();
        System.runAs(realtionshipUser){
            caseList=TestUtilities.createCase(numberOfRecords,realtionshipUser.id,false);
            //creating case with MXN currency Iso Code
            for(Case c: caseList){
                c.subject='Testing Case for Currency';
                c.Type='Account Detail';
                c.MID_text__c='12345678';
                c.accountId = acc[0].Id;
                c.CurrencyIsoCode='MXN';
            }
            insert caseList;
            List<Case> updatedCaseList=[Select Id,CurrencyIsoCode,accountId From Case 
                                        Where subject='Testing Case for Currency'];
            for(Case caseObj: updatedCaseList){
                System.assertEquals('USD',caseObj.CurrencyIsoCode);
            }
        }
        Test.stopTest();
    }
    //****************************************************************************
    // Method to test Creation of WorkOrder
    // @return void
    // 16-May-2017 Neeraj Kumawat T-602715
    //****************************************************************************
    static testMethod void testCreateWorkOrder(){
        //Create Test Data
        CreateTestData();
        Test.startTest();
        System.runAs(agentUser){
            System.debug('**Account Id:'+acc[0].id+' Account MID:'+acc[0].MID__c);
            //Case Creation from Agent User
            caseList=TestUtilities.createCase(1,agentUser.id,false);
            for(Case c: caseList){
                c.subject='Testing Case for WorkOrderCreation';
                c.Work_Items__c='General Release, Fee Maintenance';
                // c.accountId=acc.get(0).Id;
            }
            insert caseList;
        }
        List<Case> newCaseList=[Select Id, Subject,Status, (Select Id, Subject,RecordTypeId,Status 
                                                            From WorkOrders)
                                From Case Where Id IN :caseList];
        Map<String, Schema.Recordtypeinfo> workOrderRecordTypes = WorkOrder.sObjectType.getDescribe().
            getRecordtypeinfosByName(); 
        //General Relase Work Order Record Type Id
        String genReleaseWekOrderTypeId=workOrderRecordTypes.get('General Release').getRecordTypeId();
        for(Case cse: newCaseList){
            List<WorkOrder> caseWorkOrderList=cse.WorkOrders;
            //Checking work order status
            for(WorkOrder wrk: caseWorkOrderList){
                if(wrk.RecordTypeId==genReleaseWekOrderTypeId){
                    System.assertEquals(wrk.Subject,'General Release');
                }
            }
        }
        Test.stopTest();
    }
    //****************************************************************************
    // Method to test Creation of WorkOrder when value of WorkLine Item is Fee Maintenance
    // @return void
    // 24-May-2017 Poornima Bhardwaj T-592161
    //****************************************************************************
    static testMethod void testCreateWorkOrderFeeMaintenance(){
        //Create Test Data
        CreateTestData();
        Test.startTest();
        System.runAs(agentUser){
            System.debug('**Account Id:'+acc[0].id+' Account MID:'+acc[0].MID__c);
            //Case Creation from Agent User
            caseList=TestUtilities.createCase(1,agentUser.id,false);
            for(Case c: caseList){
                //Populating Subject of Case
                c.subject='Testing Case for WorkOrderCreation';
                c.Work_Items__c='Fee Maintenance';
                }
            insert caseList;
        }
        List<Case> newCaseList=[Select Id, Subject,Status, (Select Id, Subject,RecordTypeId,Status 
                                                            From WorkOrders)
                                From Case Where Id IN :caseList];  
        //Map of String and RecordTypeInfo                                              
        Map<String, Schema.Recordtypeinfo> workOrderRecordTypes = WorkOrder.sObjectType.getDescribe().
            getRecordtypeinfosByName(); 
        //Fee Maintenance Work Order Record Type Id
        String genReleaseWekOrderTypeId=workOrderRecordTypes.get('Fee Maintenance').getRecordTypeId();
        for(Case cse: newCaseList){
            List<WorkOrder> caseWorkOrderList=cse.WorkOrders;
            //Checking work order status
            for(WorkOrder wrk: caseWorkOrderList){
                if(wrk.RecordTypeId==genReleaseWekOrderTypeId){
                    System.assertEquals(wrk.Subject,'Fee Maintenance');
                }
            }
        }
        Test.stopTest();
    }
    //****************************************************************************
    // Method to test Creation of WorkOrder when value WorkLine Item is Adjustment
    // @return void
    // 24-May-2017 Poornima Bhardwaj T-603987
    //****************************************************************************
    static testMethod void testCreateWorkOrderAdjustment(){
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator('CollectionTotal'));
        //Create Test Data
        CreateTestData();
        Test.startTest();
        System.runAs(agentUser){
            System.debug('**Account Id:'+acc[0].id+' Account MID:'+acc[0].MID__c);
            //Case Creation from Agent User
            caseList=TestUtilities.createCase(1,agentUser.id,false);
            for(Case c: caseList){
                //Populating Subject of Case
                c.subject='Testing Case for WorkOrderCreation';
                c.Work_Items__c='Adjustment';
                }
            insert caseList;
        }
        List<Case> newCaseList=[Select Id, Subject,Status, (Select Id, Subject,RecordTypeId,Status 
                                                            From WorkOrders)
                                From Case Where Id IN :caseList];
        //Map of String and RecordTypeInfo                        
        Map<String, Schema.Recordtypeinfo> workOrderRecordTypes = WorkOrder.sObjectType.getDescribe().
            getRecordtypeinfosByName(); 
        //Adjustment Work Order Record Type Id
        String genReleaseWekOrderTypeId=workOrderRecordTypes.get('Adjustment').getRecordTypeId();
        for(Case cse: newCaseList){
            List<WorkOrder> caseWorkOrderList=cse.WorkOrders;
            //Checking work order status
            for(WorkOrder wrk: caseWorkOrderList){
                if(wrk.RecordTypeId==genReleaseWekOrderTypeId){
                    System.assertEquals(wrk.Subject,'Adjustment');
                }
            }
        }
        Test.stopTest();
    }
    //****************************************************************************
    // Method to test Creation of WorkOrder with NA account
    // @return void
    // 23-May-2017 Neeraj Kumawat T-602715
    //****************************************************************************
    static testMethod void testCreateWorkOrderWithNAAccount(){
        //Create Test Data
        createTestDataForRelationshipManager('NA');
        Test.startTest();
        System.runAs(realtionshipUser){
            //Case Creation from Agent User
            caseList=TestUtilities.createCase(1,realtionshipUser.id,false);
            for(Case c: caseList){
                c.subject='Testing Case for WorkOrderCreation';
                c.Work_Items__c='General Release, Fee Maintenance';
                c.accountId=acc.get(0).Id;
            }
            insert caseList;
        }
        List<Case> newCaseList=[Select Id, Subject,Status, (Select Id, Subject,RecordTypeId,Status 
                                                            From WorkOrders)
                                From Case Where Id IN :caseList];
        Map<String, Schema.Recordtypeinfo> workOrderRecordTypes = WorkOrder.sObjectType.getDescribe().
            getRecordtypeinfosByName(); 
        //General Relase Work Order Record Type Id
        String genReleaseWekOrderTypeId=workOrderRecordTypes.get('General Release').getRecordTypeId();
        for(Case cse: newCaseList){
            List<WorkOrder> caseWorkOrderList=cse.WorkOrders;
            //Checking work order status
            for(WorkOrder wrk: caseWorkOrderList){
                if(wrk.RecordTypeId==genReleaseWekOrderTypeId){
                    System.assertEquals(wrk.Subject,'General Release');
                }
            }
        }
        Test.stopTest();
    }
    //****************************************************************************
    // Method to test Creation of WorkOrder with MX account
    // @return void
    //23-May-2017 Neeraj Kumawat T-602715
    //****************************************************************************
    static testMethod void testCreateWorkOrderWithMXAccount(){
        //Create Test Data
        createTestDataForRelationshipManager('QUERE');
        Test.startTest();
        System.runAs(realtionshipUser){
            //Case Creation from Agent User
            caseList=TestUtilities.createCase(1,realtionshipUser.id,false);
            for(Case c: caseList){
                c.subject='Testing Case for WorkOrderCreation';
                c.Work_Items__c='General Release';
                c.accountId=acc.get(0).Id;
            }
            insert caseList;
        }
        List<Case> newCaseList=[Select Id, Subject,Status, (Select Id, Subject,RecordTypeId,Status 
                                                            From WorkOrders)
                                From Case Where Id IN :caseList];
        Map<String, Schema.Recordtypeinfo> workOrderRecordTypes = WorkOrder.sObjectType.getDescribe().
            getRecordtypeinfosByName(); 
        //General Relase Work Order Record Type Id
        String genReleaseWekOrderTypeId=workOrderRecordTypes.get('General Release').getRecordTypeId();
        for(Case cse: newCaseList){
            List<WorkOrder> caseWorkOrderList=cse.WorkOrders;
            //Checking work order status
            for(WorkOrder wrk: caseWorkOrderList){
                if(wrk.RecordTypeId==genReleaseWekOrderTypeId){
                    System.assertEquals(wrk.Subject,'General Release');
                }
            }
        }
        Test.stopTest();
    }
    //****************************************************************************
    // Method to test Work Order creation when there is already work order created on case
    // @return void
    // 16-May-2017 Neeraj Kumawat T-602715
    //****************************************************************************
    static testMethod void testWhenGeneralReleaseWorkOrderExists(){
        //Create Test Data
        CreateTestData();
        Test.startTest();
        System.runAs(agentUser){
            //Case Creation from Agent User
            caseList=TestUtilities.createCase(1,agentUser.id,false);
            for(Case c: caseList){
                c.subject='Testing Case for WorkOrderCreation';
            }
            insert caseList;
        }
        Map<String, Schema.Recordtypeinfo> workOrderRecordTypes = WorkOrder.sObjectType.getDescribe().
            getRecordtypeinfosByName(); 
        //General Relase Work Order Record Type Id
        String genReleaseWekOrderTypeId=workOrderRecordTypes.get('General Release').getRecordTypeId();
        List<WorkOrder> workOrderList=new List<WorkOrder>();
        // Creating General Release work Order on Case
        for(Case cse: caseList){
            WorkOrder wrkOrder= new WorkOrder();
            wrkOrder.Subject='Custom General Release';
            wrkOrder.caseId=cse.Id;
            wrkOrder.recordTypeId=genReleaseWekOrderTypeId;
            workOrderList.add(wrkOrder);
            cse.Work_Items__c='General Release';
        }
        insert workOrderList;
        //Updating work item in case
        update caseList;
        List<Case> newCaseList=[Select Id, Subject,Status, (Select Id, Subject,RecordTypeId,Status 
                                                            From WorkOrders)
                                From Case Where Id IN :caseList];
        for(Case cse: newCaseList){
            List<WorkOrder> caseWorkOrderList=cse.WorkOrders;
            //Checking work order status
            for(WorkOrder wrk: caseWorkOrderList){
                if(wrk.RecordTypeId==genReleaseWekOrderTypeId){
                    System.assertEquals(wrk.Subject,'Custom General Release');
                }
            }
        }
        Test.stopTest();
    }
    //****************************************************************************
    // Method to test Work Order creation on Case update
    // @return void
    // 16-May-2017 Neeraj Kumawat T-602715
    //****************************************************************************
    static testMethod void testGeneralRelaseWorkOrderCreationOnCaseUpdate(){
        //Create Test Data
        CreateTestData();
        Test.startTest();
        System.runAs(agentUser){
            //Case Creation from Agent User
            caseList=TestUtilities.createCase(1,agentUser.id,false);
            for(Case c: caseList){
                c.subject='Testing Case for WorkOrderCreation';
            }
            insert caseList;
        }
        Map<String, Schema.Recordtypeinfo> workOrderRecordTypes = WorkOrder.sObjectType.getDescribe().
            getRecordtypeinfosByName(); 
        //General Relase Work Order Record Type Id
        String genReleaseWekOrderTypeId=workOrderRecordTypes.get('General Release').getRecordTypeId();
        // Creating General Release work Order on Case
        for(Case cse: caseList){
            cse.Work_Items__c='General Release';
        }
        //Updating work item in case
        update caseList;
        List<Case> newCaseList=[Select Id, Subject,Status, (Select Id, Subject,RecordTypeId,Status 
                                                            From WorkOrders)
                                From Case Where Id IN :caseList];
        for(Case cse: newCaseList){
            List<WorkOrder> caseWorkOrderList=cse.WorkOrders;
            //Checking work order status
            for(WorkOrder wrk: caseWorkOrderList){
                if(wrk.RecordTypeId==genReleaseWekOrderTypeId){
                    System.assertEquals(wrk.Subject,'General Release');
                }
            }
        }
        Test.stopTest();
    }
    //****************************************************************************
    // Method to test Complaint creation if complaint applicability is "yes"
    // 18-May-2017 Gaurav Dudani T-592212.
    //Removing test case as we are no longer user Complaint - June 26 2017, Kate Jovanovic
    //**************************************************************************** 
    /* static testMethod void testComplaintCreationWhenCaseComplaintApplicablityIsYes(){
        CreateTestData();
        Test.startTest();
        // Case Creation
        caseList=TestUtilities.createCase(1,UserInfo.getUserId(),false);
        for(Case c: caseList){
            c.subject='Testing complaint creation if complaint applicabilty is selected as "Yes" on case';
            c.Complaint_Applicability__c = 'No';
        }
        insert caseList;
        
        caseList[0].Complaint_Applicability__c = 'Yes';
        Update caseList;
        List<Complaint__c> complntList = [SELECT Id FROM Complaint__c WHERE Case__c IN: caseList];
        System.assert(complntList.size()>0);
        Test.stopTest();
    }
    */
    
    //****************************************************************************
    // Method to test preventing case from getting closed if it contains unread/new emails
    // 18-May-2017 Gaurav Dudani T-603030.
    //****************************************************************************
    static testMethod void testPreventCaseCloureIfUnreadEmailsOnCase(){
        CreateTestData();
        Test.startTest();
        // Case Creation
        caseList=TestUtilities.createCase(1,UserInfo.getUserId(),false);
        for(Case c: caseList){
            c.subject='Testing Case for prevention of Case closure if contains unread/New emails';
        }
        insert caseList;
        
        List<EmailMessage> emailMessageList=TestUtilities.createEmailMessage(1,caseList[0].id,false);
        emailMessageList[0].Status = '0';
        insert emailMessageList;
        
        // Case Closure
        Try{
            caseList[0].status = 'Closed';
            Update caseList;
        } catch(exception e){
            System.assertEquals(true, e.getMessage().contains('You cannot Close the case since you have some unread emails on this case.'));
        }
        Test.stopTest();
    }
    //***************************************************************************************************
    // Method to test preventing case from getting closed if it contains New,InProgress,OnHold WorkOrders
    // 31 May 2017 Neeraj Kumawat as per task #T-606953
    //***************************************************************************************************
    static testMethod void testPreventCaseClosureOnOpenWorkOrdersOnCase(){
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator('CollectionTotal'));
        CreateTestData();
        Test.startTest();
        List<WorkOrder> workOrderOpenList=new List<WorkOrder>();
        List<WorkOrder> workOrderList=new List<WorkOrder>();
        // Case Creation
        caseList=TestUtilities.createCase(numberOfRecords,UserInfo.getUserId(),false);
        for(Case c:caseList){   
            c.subject='Testing Case';
        }
        insert CaseList;
        //Work Order creation on case
        for(Case c:CaseList){
            workOrderList=TestUtilities.createWorkOrder(1,false);
            for(WorkOrder wrk:workOrderList){
                wrk.caseId=c.id;
                wrk.status='New';
                workOrderOpenList.add(wrk);
            }
        }
        insert workOrderOpenList;
        // Case Closure
        Try{
            for(Case cse: caseList){
                cse.status ='Closed';
                cse.Reason ='Cancelled';
                cse.Internal_Comments__c='Test Case';
            }
            Update caseList;        
        }
        catch(exception e){
            System.assertEquals(true,e.getMessage().contains(Label.Case_Close_When_Open_WorkOrder));
        }
        Test.stopTest();
    }
}