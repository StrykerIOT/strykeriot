/*********************************************************************************************
* Appirio, Inc
* Name: CaseLogACallController
* Description: [T-563284- Controller Class to Create Activity history task and new task 
*                         and to save and close the related case]
* Created Date: [21/12/2016]
* Created By: [Gaurav Dudani] (Appirio)
*
* Date Modified                Modified By                  Description of the update
* [07/02/2017]               [Gaurav Dudani]      [Updated to handle exception errors on custom Vf Page]
* [27/02/2017]               [Neeraj Kumawat]     [Updated Save method to redirect the response based on request comes from (console page or normal page)]
* [03/05/2017]               [Gaurav Dudani]      [Added createLogACallComplaint method to create complaint record as per Story # S-470695.]
* [06/05/2017]               [Gaurav Dudani]      [Added Compaliant applicability field mapping on case and Log a call VF page as per Story # S-470695.]
* [17/05/2017]               [Aashish Sajwan]     [Added required field for Complaint object record]
* [18/05/2017]               [Gaurav Dudani]      [Updated By Gaurav Dudani on 18th May 2017 for Story # S-470695.]
* [31/05/2017]               [Bobby Cheek]        [Enforcing CRUD and FLS Security in the Apex Class - T-600183] 
* [01/06/2017]               [Gaurav Dudani]      [Added to create a task on Complaint creation from case (Log a Call) when Complaint Applicability is "Yes" as per S-470695.]
* [05/06/2017]               [Bobby Cheek]        [Ensure that Sharing rules are enforced in CaseLogACallController.cls files 
*                                                  by adding "with sharing" keyword to the class declaration. - T-600207]
* [05/06/2017]               [Gaurav Dudani]      [Added comments to the code]
* [13/06/2017]               [Gaurav Dudani]      [Added required fields in createLogACallComplaint method as per issue # I-278864]
*[26/06/2017]                [Kate Jovanovic]      [Commenting out all methods/lines that reference the complaint object]
*********************************************************************************************/

/*********************************************************************************************
* Creating Controller class for CaseLogACall VF Page.
*********************************************************************************************/

public with sharing class CaseLogACallController{
    public Task task {get; set;}
    public Task activityTask {get;set;}
    //Boolean variable to check is request from console or from regular page
    public Boolean isFromConsole {get;set;}
    List<Task> taskList=new List<Task>();
    // Start
    // Added by Gaurav Dudani on 03rd and 6th May 2017 as per Story # S-470695.
  //  private List<Complaint__c> compList;
    private String profileId;
    private String profileName;
    private List<Case> caseList;        
    private Case cas {get;set;}      
    String caseId;
    // End
    
/*********************************************************************************************    
* Constructor for CaseLogACallController.
*********************************************************************************************/
    
    public CaseLogACallController(ApexPages.StandardController stdController){
        
/*********************************************************************************************
* Creating record for New task.  
*********************************************************************************************/
        // Start
        // Added by Gaurav Dudani on 6th May 2017 as per Story # S-470695.
        caseList = new List<Case>();        
        cas = new Case();       
        caseId = '';    
        this.task = new task();       
        caseId = ApexPages.currentPage().getParameters().get('what_id');
        //June 26-2017 Deactivating this method. As we are removing Complaint object- Kate Jovanovic
      //  compList = new List<Complaint__c>();
        profileId = userinfo.getProfileId();
        profileName = '';
        profileName = [Select Name from Profile where Id=:profileId].Name;
        system.debug('** task value **'+task);
        
        // 07 June 2017 Bobby Cheek checking FLS on Task fields
        if(Schema.sObjectType.Task.fields.whatId.isCreateable())
        	this.task.whatId = ApexPages.currentPage().getParameters().get('what_id');
    	if(Schema.sObjectType.Task.fields.subject.isCreateable())
        	this.task.subject = ApexPages.currentPage().getParameters().get('tsk5');
         
        String contactId=ApexPages.currentPage().getParameters().get('who_id');
        
        if(contactId!=null && contactId!='' && contactId.substring(0, 3)=='003'){
        	
        	if(Schema.sObjectType.Task.fields.WhoId.isCreateable())
            	this.task.WhoId = ApexPages.currentPage().getParameters().get('who_id');
        }
        
        if(Schema.sObjectType.Task.fields.Priority.isCreateable())        
        	this.task.Priority = 'Normal';
    	if(Schema.sObjectType.Task.fields.OwnerId.isCreateable())
        	this.task.OwnerId = UserInfo.getUserId();
    	if(Schema.sObjectType.Task.fields.status.isCreateable())
        	this.task.status = 'New';
        if(Schema.sObjectType.Task.fields.activitydate.isCreateable()) 
        	this.task.activitydate = Date.today();
        
/*********************************************************************************************        
* Creating Record for activityTask record.
**********************************************************************************************/
        
        this.activityTask = new task();
        
        // 07 June 2017 Bobby Cheek checking FLS on Task fields
    	if(Schema.sObjectType.Task.fields.whatId.isCreateable())
        	this.activityTask.whatId = ApexPages.currentPage().getParameters().get('what_id');
    	if(Schema.sObjectType.Task.fields.subject.isCreateable())
        	this.activityTask.subject = ApexPages.currentPage().getParameters().get('tsk5');
        	
        if(contactId!=null && contactId!='' && contactId.substring(0, 3)=='003'){
        	
        	if(Schema.sObjectType.Task.fields.WhoId.isCreateable())
            	this.activityTask.WhoId  = ApexPages.currentPage().getParameters().get('who_id');
        }
        
        System.debug('WhatId='+this.activityTask.whatId);
        System.debug('WhoId='+this.activityTask.WhoId);
        System.debug('Subject='+this.activityTask.subject);
        
        if(Schema.sObjectType.Task.fields.type.isCreateable())
        	this.activityTask.type = 'Call';
        if(Schema.sObjectType.Task.fields.OwnerId.isCreateable())
        	this.activityTask.OwnerId = UserInfo.getUserId();
        if(Schema.sObjectType.Task.fields.Priority.isCreateable())
        	this.activityTask.Priority = 'Normal';
        if(Schema.sObjectType.Task.fields.status.isCreateable())
        	this.activityTask.status = 'Complete';
        if(Schema.sObjectType.Task.fields.activitydate.isCreateable())
        	this.activityTask.activitydate = Date.today();
    }
    
/*********************************************************************************************    
* Method to be performed On click of Save and Close Case button.
* Updated By Gaurav Dudani as per Task#: T-576695 on 7th February 2017.
**********************************************************************************************/  
 
    public PageReference saveAndCloseCase()
    {   
        try{
        	//if(Schema.sObjectType.Task.fields.Complaint_Applicability__c.isCreateable())
            //	this.task.Complaint_Applicability__c = this.activityTask.Complaint_Applicability__c;
            // Inserting Both Task and activity Task on Case on save and close button click.
            // Added By Gaurav Dudani on 5th June 2017 for Story # S-470695.
            //taskList.add(this.task);
            //taskList.add(this.activityTask);
        	//insert taskList;
        	// Start
        	// Added by Gaurav Dudani on 03rd May 2017 as per Story # S-470695.
          //June 26-2017 Deactivating this method. As we are removing Complaint object- Kate Jovanovic
        	//createLogACallComplaint(taskList);
        	// End
       	 	String urlStr=task.WhatId+'/s?retURL='+task.WhatId+'&cas7=Closed&cas6=Suggestion/Solution%20Provided';
        	PageReference pageRef = new PageReference('/'+urlStr);
        	return pageRef;            
        } catch (System.DmlException ex) {
            return null;
        }   
    }
    
/*********************************************************************************************        
* Method to be performed On click of Save button.
* Updated By Gaurav Dudani as per Task#: T-576695 on 7th February 2017.
**********************************************************************************************/ 
   
    public PageReference save()
    {
        try{
        	//if(Schema.sObjectType.Task.fields.Complaint_Applicability__c.isCreateable())
            //	this.task.Complaint_Applicability__c = this.activityTask.Complaint_Applicability__c;
            //system.debug('Activity History Tasks '+this.activityTask.Complaint_Applicability__c);
            // Inserting Both Task and activity Task on Case on click of save button.
            // Added By Gaurav Dudani on 5th June 2017 for Story # S-470695.
            //taskList.add(this.task);
            //taskList.add(this.activityTask);
            //insert taskList;
            // Start
            // Added by Gaurav Dudani on 03rd May 2017 as per Story # S-470695.
            //June 26-2017 Deactivating this method. As we are removing Complaint object- Kate Jovanovic
         //   createLogACallComplaint(taskList);
            // End
            PageReference pageRef;
            system.debug('Is request from Console='+isFromConsole);
            //Redirecting logacall response to respective page according to the request comes
            if(isFromConsole){
                 //when request comes from console page than redirecting to the task detail page
                 String urlStr=activityTask.Id;
                 pageRef = new PageReference('/'+urlStr);
            }else{
                //when request comes from normal page than redirecting to the case detail page
                 String urlStr=task.WhatId;
                 pageRef = new PageReference('/'+urlStr);
            }
            return pageRef;
        } catch (System.DmlException ex) {
            return null;
        }
    }
    
    //*********************************************************************************************
    // Added by Gaurav Dudani on 03rd May 2017 as per Story # S-470695.
    // Method to create systemically activated complaint record when user logs a call on a case and 
    // when Complaint Applicability is selected as "Yes" or updated to "Yes".
    //June 26-2017 Deactivating this method. As we are removing Complaint object- Kate Jovanovic
    //*********************************************************************************************
  /*  public void createLogACallComplaint(List<Task> tempList){
      List<Task> taskList = new List<Task>();
      Set<Id> taskIdSet = new Set<Id>(); 
      for(Task t: tempList){
        taskIdSet.add(t.Id);
      }
      if(!taskIdSet.isEmpty()){
        //Updated By Gaurav Dudani on 18th May 2017 for Story # S-470695.
        if(!string.isBlank(profileName) && (profileName == 'Custom: NA Manager' || profileName == 'Custom: MX Manager' || profileName == 'Custom: NA Agent' || profileName == 'Custom: MX Agent' || profileName == 'System Administrator')){
          for(Task t: [select Complaint_Applicability__c,whatId from Task where status = 'New' AND ID IN: taskIdSet]){
            if(t.Complaint_Applicability__c == 'Yes' &&  Schema.sObjectType.Complaint__c.isCreateable()){
                // 17 May 2017 Aashish Sajwan - Added Required Communication_Method__c field for creation Compalaint object               
               compList.add(new Complaint__c(What_is_the_customer_s_complaint__c = Label.New_Complaint_From_Case, 
                                               Communication_Method__c = 'Verbal', 
                                               Elavon_Channels__c = 'City National',
                                               Issue_Resolved__c = 'Yes',
                                               Resolution_communicated_via__c = 'Verbal',
                                               Date_complaint_received__c=Date.today(),
                                               Case__c = t.WhatId));
                  
                                            
          	    Complaint__c taskComplaint = new Complaint__c();
          	    
          	    // 07 June 2017 Bobby Cheek checking FLS on Complaint__c fields          	   
				if(Schema.sObjectType.Complaint__c.fields.What_is_the_customer_s_complaint__c.isCreateable())
          	   		taskComplaint.What_is_the_customer_s_complaint__c = Label.New_Complaint_From_Case; 
          	   	if(Schema.sObjectType.Complaint__c.fields.Communication_Method__c.isCreateable())
               		taskComplaint.Communication_Method__c = 'Verbal'; 
               	if(Schema.sObjectType.Complaint__c.fields.Elavon_Channels__c.isCreateable())
               		taskComplaint.Elavon_Channels__c = 'City National';
               	if(Schema.sObjectType.Complaint__c.fields.Issue_Resolved__c.isCreateable())
               		taskComplaint.Issue_Resolved__c = 'Yes';
               	if(Schema.sObjectType.Complaint__c.fields.Resolution_communicated_via__c.isCreateable())
               		taskComplaint.Resolution_communicated_via__c = 'Verbal';
               	if(Schema.sObjectType.Complaint__c.fields.Date_complaint_received__c.isCreateable())
               		taskComplaint.Date_complaint_received__c=Date.today();
                // Start
                // Added the required fields when Complaint is created using log a call on case.
                // By Gaurav Dudani on 13 June 2017 as per Issue # I-278864.
                if(Schema.sObjectType.Complaint__c.fields.How_and_when_did_you_resolve_the_issue__c.isCreateable())
               		taskComplaint.How_and_when_did_you_resolve_the_issue__c='Test Complaint';
                if(Schema.sObjectType.Complaint__c.fields.Your_Department__c.isCreateable())
               		taskComplaint.Your_Department__c='CAM';
               	// End	
               	if(Schema.sObjectType.Complaint__c.fields.Case__c.isCreateable())
               		taskComplaint.Case__c = t.WhatId;
               
               compList.add(taskComplaint);
            }
          }  
        }  
      }
      if(compList.size() >0 ){
        insert compList;
        // Calling createTaskOnComplaintCreation method on complaint creation with Complaint Applicability selected as "Yes".
        // Added on 1st June 2017 Gaurav Dudani S-470695.
        createTaskOnComplaintCreation(compList);
      }
      
    }
    */
    
    //****************************************************************************
    // Method to Create task when an Complaint gets created and display the reminder pop-up.
    // 1st June 2017 Gaurav Dudani S-470695.
    //June 26-2017 Deactivating this method. As we are removing Complaint object- Kate Jovanovic
    //****************************************************************************
 /*   public static void createTaskOnComplaintCreation(List<Complaint__c> newComplaintList) {
      List<Task> taskListToInsert = new List<Task>();
      for(Complaint__c comp: newComplaintList){
      	
        taskListToInsert.add(new Task(whatId = comp.Id, 
                                      ActivityDate = System.today(),
                                      Status = 'New',
                                      Priority =  'Normal',
                                      Subject = 'Complaint Informaion Incomplete:Please Furnish the necessary details.',
                                      isreminderset = true, 
                                      ReminderDateTime = System.now().addMinutes(1)));
        
                                      
		Task complaintTask = new Task();
		
		// 07 June 2017 Bobby Cheek checking FLS on Task fields
		if(Schema.sObjectType.Task.fields.whatId.isCreateable())
			complaintTask.whatId = comp.Id;
		if(Schema.sObjectType.Task.fields.ActivityDate.isCreateable()) 
         	complaintTask.ActivityDate = System.today();
     	if(Schema.sObjectType.Task.fields.Status.isCreateable())
          	complaintTask.Status = 'New';
      	if(Schema.sObjectType.Task.fields.Priority.isCreateable())
          	complaintTask.Priority =  'Normal';
      	if(Schema.sObjectType.Task.fields.Subject.isCreateable())
      		complaintTask.Subject = 'Complaint Informaion Incomplete:Please Furnish the necessary details.';
  		if(Schema.sObjectType.Task.fields.isreminderset.isCreateable())
          	complaintTask.isreminderset = true;
      	if(Schema.sObjectType.Task.fields.ReminderDateTime.isCreateable())
          	complaintTask.ReminderDateTime = System.now().addMinutes(1);
          	
        taskListToInsert.add(complaintTask);
      	
      }
      
      if(taskListToInsert.size() > 0){
        insert taskListToInsert;
      }
    }   
*/

}