/**********************************************************************
* Appirio, Inc
* Test Class Name: AccountTaxInfoRelationshipBatchTEST
* Class Name: [AccountTaxInfoRelationshipBatch]
* Description: [T-598844 Test class for AccountTaxInfoRelationshipBatch]
* Created Date: [28/04/2017]
* Created By: [Neeraj Kumawat] (Appirio)
* 
* Date Modified                Modified By                  Description of the update
***********************************************************************/
@isTest
public class AccountTaxInfoRelationshipBatchTEST {
    //Specified number of records to be created
    public static Integer numberOfRecord=2;
    //***************************************************************************
    // Method to create test record
    // @param taxExternalId : Tax external Id for Account record
    // @return List of Account Object
    //***************************************************************************
    public static List<Account> createTestData(String taxExternalId){
        List<UserRole> globalOpsRole=[select id,name from UserRole where name='Global Ops'];
        List<Profile> relationshipProdileId=[select id,name from Profile where name='Custom: Relationship Manager'];
        User realtionshipUser=TestUtilities.createTestUser(1,relationshipProdileId[0].id,false);
        realtionshipUser.Username='testrelationship@acctRelationbatch.comtestuser';
        realtionshipUser.CommunityNickname='testcommunity';
        realtionshipUser.UserRole=globalOpsRole[0];
        insert realtionshipUser;
        List<Account> accList=new List<Account>();
        //Run as relationship Manager
        System.runAs(realtionshipUser){
            //Getting Account RecordTypes Map
            Map<String, Schema.Recordtypeinfo> AccountRecordTypes = Account.sObjectType.getDescribe().getRecordtypeinfosByName();
            //Creating account
            accList= TestUtilities.createAccount(1,false);
            for(Integer i=0; i<accList.size(); i++){
                accList[i].MID__c='12345';
                if(taxExternalId!=null){
                    accList[i].Tax_External_Id__c=taxExternalId +i;
                }
                accList[i].Processing_Center_ID__c = 'NA';
                accList[i].recordTypeId=AccountRecordTypes.get('General').getRecordTypeId();
            }
            insert accList;
        }
        return accList;
    }
    //*********************************************************************************************
    // Test method to test AccountTaxInfoRelationshipBatch job with account having tax External Id
    // @return void
    //*********************************************************************************************
    static testMethod void testWithTaxExternalId() {
        List<Account> acctList=createTestData('12345');
        List<Tax_Information__c> taxInfoList=TestUtilities.createTaxInformation(numberOfRecord,false);
        for(Integer i=0; i<taxInfoList.size(); i++){
            taxInfoList[i].External_Id__c='12345'+i;
        }
        insert taxInfoList;
        Test.startTest();
        AccountTaxInfoRelationshipBatch objAcctTaxInfoBatch = new AccountTaxInfoRelationshipBatch();
        Database.executeBatch(objAcctTaxInfoBatch);  
        Test.stopTest();
        List<Account> accountList=[SELECT Id, Tax_External_Id__c, Tax_Info_link__c, RecordType.Name FROM Account 
                                   WHERE RecordType.Name ='General' 
                                   AND Tax_External_Id__c != null];
        Map<String,Tax_Information__c> taxInfoMap = new map<String,Tax_Information__c>();
        for(Tax_Information__c taxInfo : taxInfoList){
            if(!taxInfoMap.ContainsKey(taxInfo.External_Id__c)){
                taxInfoMap.put(taxInfo.External_Id__c, taxInfo);
            }
        }
        for(Account acc : accountList){
            if(taxInfoMap.ContainsKey(acc.Tax_External_Id__c)){
                //check account Tax_Info_link__c should be equal to Tax information id
                System.assertEquals(acc.Tax_Info_link__c, taxInfoMap.get(acc.Tax_External_Id__c).Id);
            }
        }
        
    }
    //*********************************************************************************************
    // Test method to test AccountTaxInfoRelationshipBatch job with account not haing tax external id
    // @return void
    //*********************************************************************************************
    static testMethod void testWithEmptyTaxExternalId() {
        List<Account> acctList=createTestData(null);
        List<Tax_Information__c> taxInfoList=TestUtilities.createTaxInformation(numberOfRecord,false);
        for(Integer i=0; i<taxInfoList.size(); i++){
            taxInfoList[i].External_Id__c='12345'+i;
        }
        insert taxInfoList;
        Test.startTest();
        AccountTaxInfoRelationshipBatch objAcctTaxInfoBatch = new AccountTaxInfoRelationshipBatch();
        Database.executeBatch(objAcctTaxInfoBatch);  
        Test.stopTest();
        List<Account> accountList=[SELECT Id, Tax_External_Id__c, Tax_Info_link__c, RecordType.Name FROM Account 
                                   WHERE RecordType.Name ='General' 
                                   AND Tax_External_Id__c != null];
        for(Account acc : accountList){
            //check account Tax_Info_link__c should be null when account don't have tax external id
            System.assertEquals(acc.Tax_Info_link__c, null);
        } 
    }
}