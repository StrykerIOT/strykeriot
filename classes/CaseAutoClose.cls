/****************************************************************************************
* Appirio, Inc
* Test Class Name: CaseLogACallControllerTest
* Class Name: [CaseAutoClose]
* Description: [Create as per Task T-554439. Batch Class - Closing old Cases]
* Created Date: [10/11/2016]
* Created By: [Neeraj Kumawat] (Appirio)
* 
* Date Modified                Modified By                  Description of the update
* [03/02/2016]               [Neeraj Kumawat]               [Added extra condition for fetch case which is having contact Status Active or ContactId=null]
* [4 May 2017]               [Neeraj Kumawat]               [Modify to add case auto close comments in mexican language T-599785]
* [14 June 2017]             [Neeraj Kumawat]               [Added condition to close the case which don't have open work orders and unread email message #T-609575 ]
*****************************************************************************************/
//****************************************************************************
// Batch Class declaration.
//****************************************************************************

global class CaseAutoClose implements Database.Batchable<sObject>,Database.AllowsCallouts, Database.Stateful, IScheduler {
    Id caseId=null;
    // Getting number of days value from custom setting
    Case_Custom_Setting__c objCaseCustomSetting=  Case_Custom_Setting__c.getValues('Case Last Modify Number of Days');
    Decimal caseLastModifyDays=objCaseCustomSetting.Number_of_Days__c;
    // Querying List of cases who's lastModified date is 90 days old.
    global  String query= 'SELECT Id,User_Language__c' +
        ' FROM Case' + 
        ' WHERE  Status= \'Awaiting Information\' and (Number_of_Days_LastModified__c>:caseLastModifyDays) and ((Contact.Contact_Status__c=\'Active\')  or (ContactId=null))';
    
    global final String s_object;   
    //*********************************************************************
    //Implement start method of Database.Batchable Interface
    //*********************************************************************
    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }
    //*********************************************************************
    //Non Parameterized Constructor for Batch Class
    //*********************************************************************
    public CaseAutoClose(){
        
    }
    //*********************************************************************
    //Parameterized Constructor for Batch Class
    //*********************************************************************
    global CaseAutoClose(Date providedDate, Id recordId) {
        caseId = recordId;
        if(caseId==null){
            query=  ' SELECT Id,User_Language__c' +
                ' FROM Case' + 
                ' WHERE  Status= \'Awaiting Information\' and (Number_of_Days_LastModified__c>:caseLastModifyDays) and ((Contact.Contact_Status__c=\'Active\') or (ContactId=null))';
        }else{
            query='SELECT Id,User_Language__c' +
                ' FROM Case' + 
                ' WHERE  (Status= \'Awaiting Information\' and (Number_of_Days_LastModified__c>:caseLastModifyDays) and (Contact.Contact_Status__c=\'Active\' or ContactId=null)) OR (id =:caseId and ((Contact.Contact_Status__c=\'Active\') or (ContactId=null)))';
        }
    }
    //*********************************************************************
    //Implementing method of Interface(IScheduler)
    //Calling Parameterized Constructor of CaseAutoClose class in scheduleClass Method
    //*********************************************************************
    global void scheduleClass (Date providedDate, Id recortId) {
        // Adding one more day to get current day's Cases
        //Updated provided date custom setting value
        Provided_Date__c objProvidedDate=Provided_Date__c.getOrgDefaults();
        objProvidedDate.CaseRunAsDate__c=providedDate;
        update objProvidedDate;
        Database.executeBatch(new CaseAutoClose(providedDate,recortId));
    }
    
    //******************************************************************************************
    // Execute Method for update the case status to close
    //******************************************************************************************
    
    global void execute(Database.BatchableContext BC, list<sObject> scope){
        //#T-609575 Neeraj Kumawat  
        //Added check for closing the case which don't have open work orders
        List<String> workOrderOpenStatus=new List<String>{'New','In Progress','On Hold'};
            List<Case> caseList = new List<Case>();
        //Type cast the scope to case list
        List<Case> newCaseList=(List<Case>)scope;
        //Query to fetch cases with open work orders
        List<Case> caseListWithWorkOrder=[Select Id,User_Language__c, Subject,Status, (Select Id, Subject,Status 
                                                                                       From WorkOrders Where Status IN :workOrderOpenStatus)
                                          From Case Where Id IN :newCaseList];
        //Query to fetch  unread email on cases
        List<EmailMessage> emailMessageList=[SELECT Id, RelatedToID, ParentID
                                             FROM EmailMessage
                                             WHERE RelatedToID IN :newCaseList 
                                             AND Status = '0'];
        Map<Id,EmailMessage> caseWithEmailMessageMap=new Map<Id,EmailMessage>();
        //Looping to set caseWithEmailMessageMap this map is used to check unread email on case
        for(EmailMessage emailMsg: emailMessageList){
            caseWithEmailMessageMap.put(emailMsg.RelatedToID,emailMsg);
        }
        for(Case caseObj : caseListWithWorkOrder){
            List<WorkOrder> openWorkOrderList=caseObj.WorkOrders;
            EmailMessage emailMsg=caseWithEmailMessageMap.get(caseObj.Id);
            //closing the case if they don't have open work order and unread email message
            if(openWorkOrderList.size()==0 && emailMsg==null){
                if(caseObj.User_Language__c=='en_US'){
                    caseObj.Reason = 'No Customer Response.';
                    caseObj.Internal_Comments__c = 'Auto-closed due to no customer response.';
                }else{
                    //Modify to add case auto close comments in mexican language
                    //4 May 2017 Neeraj Kumawat T-599785
                    caseObj.Reason = 'Sin respuesta del Comercio.';
                    caseObj.Internal_Comments__c = 'Cierre automático debido a la falta de respuesta del Comercio.';
                }
                caseObj.Status= 'Closed';
                caseList.add(caseObj);
            }
        }
        //Check CaseList size before update list in database
        if(caseList.size()>0){
            //Update Case list in database
            update caseList;
        }
        
    }
    //****************************************************************************
    // Finish Method
    //****************************************************************************
    
    global void finish(Database.BatchableContext BC) {
        //Set provided date custom setting to null after completion of batch job
        Provided_Date__c objProvidedDate=Provided_Date__c.getOrgDefaults();
        objProvidedDate.CaseRunAsDate__c=null;
        update objProvidedDate;
    }  
}