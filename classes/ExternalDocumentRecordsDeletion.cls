/*****************************************************************************************************************************
// (c) 2015 Appirio, Inc.
//
// Description : Batch Class - Delete Old External Document Records
// Created On  : 26th September 2016 By Aashish Singh Sajwan Original (T-563913)
//                            
//****************************************************************************************************************************/

//****************************************************************************
// Batch Class declaration.
//****************************************************************************

global class ExternalDocumentRecordsDeletion implements Database.Batchable<sObject>,Database.AllowsCallouts, Database.Stateful, IScheduler {
    
    //********************************************************************* 
    // Querying List of cases who's created date is today days old.
    //*********************************************************************
    List<External_Document__c> originalExternalDocs = New List <External_Document__c>();
    //temporarily changed to 1 day for demo purposes
    External_Doc_Number_Days__c objCaseCustomSetting=  External_Doc_Number_Days__c.getValues('Days');
    date createdDate = (objCaseCustomSetting!=null && objCaseCustomSetting.Number_of_Days__c!=null)?system.today().addDays(Integer.valueOf(objCaseCustomSetting.Number_of_Days__c)) : system.today(); //system.today().addDays(3); 
    Id externalDocId = null;
    global  String query= 'SELECT Id' +
                ' FROM External_Document__c' + 
                ' WHERE Expiry_Date__c< :createdDate';
    
    global final String s_object;   
    //*********************************************************************
    //Implement start method of Database.Batchable Interface
    //*********************************************************************
    global Database.QueryLocator start(Database.BatchableContext bc) {
        system.debug('Query for getting records '+query);
        return Database.getQueryLocator(query);
    }
    //*********************************************************************
    //Non Parameterized Constructor for Batch Class
    //*********************************************************************
    public ExternalDocumentRecordsDeletion(){
         system.debug('** Default constructor call ***');
         system.debug('** Date Values in default constructor ***'+createdDate);
         system.debug('** Query Values in default constructor ***'+query); 
    }
    //*********************************************************************
    //Parameterized Constructor for Batch Class
    //*********************************************************************
    global ExternalDocumentRecordsDeletion (Date providedDate, Id recordId) {
        system.debug('** parameterized constructor call ***');
        createdDate = providedDate;
        externalDocId = recordId;
        system.debug('**Date Values ***'+createdDate);
        system.debug('** externalDocId Values ***'+externalDocId );
        if(externalDocId ==null){
            query=  'SELECT Id,Name ' +
                    'FROM External_Document__c  ' + 
                    'WHERE Expiry_Date__c < :createdDate ';
            system.debug('**Query Values Without Record Id***'+query);
        }else if(recordId!=null && createdDate == null){
           query='SELECT Id,Name ' +
                'FROM External_Document__c ' + 
                'WHERE id =:externalDocId'; 
        }
        else{
          query='SELECT Id,Name ' +
                'FROM External_Document__c ' + 
                'WHERE Expiry_Date__c < :createdDate OR id =:externalDocId';       
          system.debug('** Query Values With Record Id***'+query);
        }
    }
    //*********************************************************************
    //Implementing method of Interface(IScheduler)
    //Calling Parameterized Constructor of EmailScheduleDeleteAttachments class in scheduleClass Method
    //*********************************************************************
    global void scheduleClass (Date providedDate, Id recordId) {
        System.debug(':::Inside scheduleClass Method recordId Id value:::'+recordId);
        System.debug(':::Provided Date='+providedDate);
        // Adding one more day to get current day's Cases 
        if(providedDate!=null){
         providedDate= providedDate.addDays(1);
         System.debug(':::Next Day Date ='+providedDate);
        }
       
        Database.executeBatch(new ExternalDocumentRecordsDeletion(providedDate,recordId));
        System.debug('Called Database.executeBatch Method');
    }
    
    //******************************************************************************************
    // Execute Method to query the list of email messages and attachments for the queried cases. 
    //******************************************************************************************
    
    global void execute(Database.BatchableContext BC, list<sObject> scope){
        System.debug(':::Inside execute Method:::');
        System.debug(':::Scope Value:::'+scope);
        Set<Id> externalDocIdSet = new Set<Id>();  
        List<External_Document__c> externalDocRecordList = new  List<External_Document__c>();
        List<FeedItem> FeedItemList = New List<FeedItem>();
        List<ContentDocument> ContentDocList = New List<ContentDocument>();
        set<Id> contentIdSet = New set<Id>();
        Map<Id,Id> mapOfEmailIdAndCaseId = New Map<ID,ID>();
        Map<Id,Id> mapOfAttachmentIdAndEmailId = New Map<ID,ID>();
        for(Sobject obj : scope){
            External_Document__c objExternalDoc = (External_Document__c)obj;
            if(objExternalDoc != null){
                externalDocIdSet.add(objExternalDoc.Id);
                externalDocRecordList.add(objExternalDoc);
            }
            
        }
           
        if(externalDocIdSet.Size() > 0){
            for(FeedItem objFeedItem : [Select Id,RelatedRecordId,ParentId  
                                        From FeedItem                                    
                                        WHERE ParentId IN: externalDocIdSet]){
              if(objFeedItem !=null){
                    FeedItemList.add(objFeedItem);
                    contentIdSet.add(objFeedItem.RelatedRecordId);
                    system.debug('** objFeedItem Are: **'+ objFeedItem);
              }
            }       
           
           if(contentIdSet.size()>0){
            for(ContentDocument objContentDoc : [Select Id 
                                                     from  ContentDocument 
                                                     Where Id in (Select ContentDocumentId 
                                                                  From ContentVersion 
                                                                  WHERE Id IN: contentIdSet)]){
                  ContentDocList.add(objContentDoc);             
              }
           }
           
           system.debug('** FeedItemList Are: **'+ FeedItemList);
           system.debug('** ContentDocList Are: **'+ ContentDocList);
           system.debug('** externalDocRecordList Are: **'+ externalDocRecordList);
           //Check Feed Item list size before delete them
           If(FeedItemList.Size() > 0){
               //Delete Feed Items Records which are linkup with External Documents
               Delete FeedItemList;
           }
           
           //Check Content Version list size before delete them
           if(ContentDocList.Size()>0){
               //Delete Content Verison Records which are linkup with External Documents
               Delete ContentDocList;
           }
           
           //Check External Document list size before delete them
           if(externalDocRecordList.Size()>0){
             //Delete External Documents Records
             Delete externalDocRecordList;
           }
           
           
        }
        
    }
    //****************************************************************************
    // Finish Method
    //****************************************************************************
    
    global void finish(Database.BatchableContext BC) {
        system.debug('finish Method Calling');
    }  
}