/*********************************************************************************************************************
* Author        :  Appirio 
* Description: [T-558200 Test Class for EmailMessageTriggerHandler]
* Created Date: [02/12/2016]
* Created By: [Neeraj Kumawat] (Appirio)
*
* Date Modified                Modified By                  Description of the update
*[22 May 2017]              [Poornima Bhardwaj]             [T-603553 Added method assignCaseToQueueTest]
**********************************************************************************************************************/
@isTest
//****************************************************************************
// Test Class to test the AccountTriggerHandler.cls
//****************************************************************************
public class EmailMessageTriggerHandlerTest
{
     static List<Case> caseList;
     static Case tmpCase;
    /**
     * Method for creating Test Data
     **/
    public static void createTestData(User tmpUser){
        List<Contact> tmpContactList=TestUtilities.createContact(1, null, true);
        Contact tmpContact=tmpContactList.get(0);
        List<Case> tmpCaseList=TestUtilities.createCase(1, tmpUser.Id, false);
        for(Case cse: tmpCaseList){
            cse.ContactId=tmpContact.Id;
        }
        insert tmpCaseList;
        tmpCase=tmpCaseList.get(0);
    }
    /**
     * Test Method for prevent Deletion of Email Message with Admin
     * */
    static testMethod void preventDeletionEmailMessageWithAdminUserTest(){
        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
         User tmpAdminUser=TestUtilities.createTestUser(1, p.Id, true);
        Test.startTest();
        System.runAs(tmpAdminUser){
            createTestData(tmpAdminUser);
            EmailMessage tmpEmailMessage=new EmailMessage();
            tmpEmailMessage.ParentID=tmpCase.id;
            tmpEmailMessage.TextBody='Test';
            insert tmpEmailMessage;
            Database.DeleteResult result=Database.delete(tmpEmailMessage,false);
            System.debug('Result of preventDeletionEmailMessageWithAdminUserTest='+result);
            System.assert(result.isSuccess());
            System.assert(result.getErrors().size()==0);
        }
        Test.stopTest();
    }
     /**
     * Test Method for prevent Deletion of Email Message with Standard User
     * */
    static testMethod void preventDeletionEmailMessageWithUserTest(){
        //A non administrator user should not be able to delete an Email Message
        Profile p=[SELECT Id FROM Profile WHERE Name='Custom: NA Agent'];
        User tmpUser=TestUtilities.createTestUser(1, p.Id, false);
        List<UserRole> tmpRole=[SELECT Id,Name FROM UserRole where DeveloperName='Global_Ops'];
        System.debug('System Roles='+tmpRole);
        if(tmpRole.size()>0){
          tmpUser.UserRoleId=tmpRole.get(0).Id;
        }
        insert tmpUser;
        Test.startTest();
        System.runAs(tmpUser){
            createTestData(tmpUser);
            EmailMessage tmpEmailMessage=new EmailMessage();
            tmpEmailMessage.ParentID=tmpCase.id;
            tmpEmailMessage.TextBody='Test';
            insert tmpEmailMessage;
            Database.DeleteResult result=Database.delete(tmpEmailMessage,false);
            System.debug('Result of preventDeletionEmailMessageWithUserTest='+result);
            System.assert(!result.isSuccess());
            System.assert(result.getErrors().size()>0);          
        }
        Test.stopTest();
    }
    
    //Added 22 May 2017 Poornima Bhardwaj for #T-603553
    
    
    static testMethod void assignCaseToQueueTest(){
        Test.startTest();
        // Creating Test Case
        caseList=TestUtilities.createCase(1,UserInfo.getUserId(),false);
        for(Case c: caseList){
            //Populating subject of case
            c.subject='Testing Case for prevention of Case closure if contains unread/New emails';
            //Populating status of case
            c.Status='Closed';
        }
        //Inserting Case
        insert caseList;
        //Creating Test Email Message
        List<EmailMessage> emailMessageList=TestUtilities.createEmailMessage(1,caseList[0].id,false);
        //Setting status of email message
        emailMessageList[0].Status = '0';
        insert emailMessageList;
        
        // Case Closure
        Try{
          caseList[0].status = 'Closed';
          Update caseList;
        } catch(exception e){
          System.assertEquals(true, e.getMessage().contains('You Cannot Close the case since you have some unread emails on this case.'));
        }
        Test.stopTest();
    }
   }