//
// (c) 2012 Appirio, Inc.
//
// Apex class for Run Batch Annonymously
//
// 26 Dec 2016 Aashish Singh Sajwan (T-563917)

public class RunApexController{

public Date runAsDate{get;set;}
public string recordId{get;set;}
public List<SelectOption> classList {get;set;}
public string SelectedClass{get;set;}

public RunApexController(){
     init();
 }
private void init(){
classList =  new List<SelectOption>();
classList.add(new SelectOption('','--None--'));
 for(Run_Apex__c clsName: Run_Apex__c.getall().values()){
          classList.add(new SelectOption(clsName.Name,clsName.Name));
 }
system.debug('** ClassList **'+classList);
}

// Exectue Batch Job and Create object to Execute_Batch_Job__c
   public void executeBatch(){
        // Getting interface reference and calling the implemented method of class selected by user in VF page
        Type t =Type.forName(SelectedClass);
        IScheduler batchobj = (IScheduler) t.newInstance();
        try{
           System.debug('Inside executeBatch Method');
           System.debug('Value of Record Id='+recordId);
            System.debug('Value of runAsDate='+runAsDate);
            //Set recordId to null when it's coming empty from client
            if(recordId == ''){
                recordId = null;
            }
           //  batchobj.scheduleClass(null ,recordId);
            batchobj.scheduleClass(runAsDate,recordId);
         }
        catch(Exception ex){
            System.debug('Exception='+ex);
             ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,+ex.getMessage()));  
        }
        Execute_Batch_Job__c excecuteJob = new Execute_Batch_Job__c();
        excecuteJob.Class_Name__c = SelectedClass;
        excecuteJob.Run_As_Date__c= runAsDate;
        excecuteJob.Record_Id__c= recordId;
        excecuteJob.Date_Submitted__c= Date.today();
        excecuteJob.Submitter__c = UserInfo.getUserId();
        insert excecuteJob;
    }
}