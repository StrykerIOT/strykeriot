/**********************************************************************
* Appirio, Inc
* Test Class Name: ServiceContractTriggerHandlerTest
* Class Name: [ServiceContractTriggerHandler]
* Description: [T-598844 Test class for ServiceContractTriggerHandler]
* Created Date: [28/04/2017]
* Created By: [Neeraj Kumawat] (Appirio)
* 
* Date Modified                Modified By                  Description of the update
***********************************************************************/
@isTest
public class ServiceContractTriggerHandlerTest {
    public static Integer numberOfRecord=2;
    public static List<ServiceContract> createTestData(String currencyISOCode){
        List<UserRole> globalOpsRole=[select id,name from UserRole where name='Global Ops'];
        List<Profile> relationshipProdileId=[select id,name from Profile where name='Custom: Relationship Manager'];
        User realtionshipUser=TestUtilities.createTestUser(1,relationshipProdileId[0].id,false);
        realtionshipUser.Username='testrelationship@serviceContract.comtestuser';
        realtionshipUser.CommunityNickname='testcommunity';
        realtionshipUser.UserRole=globalOpsRole[0];
        insert realtionshipUser;
        List<Account> accList=new List<Account>();
        System.runAs(realtionshipUser){
            //Creating account with Funding currency Iso Code
            accList= TestUtilities.createAccount(1,false);
            for(Account a:accList){
                a.Funding_Currency_Code__c=currencyISOCode;
                a.MID__c='12345';
                a.Processing_Center_ID__c = 'NA';
            }
            insert accList;
        }
        List<ServiceContract> serviceContractList=TestUtilities.createServiceContract(numberOfRecord,accList[0].Id,true);
        return serviceContractList;
    }
    //****************************************************************************************
    // Test Method to Create ServiceContract and delete it and then Undelete it
    // @return void
    //*****************************************************************************************
    static testMethod void testDelete_Undelete(){
        List<Account> acc=TestUtilities.createAccount(1, true);
        List<ServiceContract> serviceContractList=TestUtilities.createServiceContract(1, acc[0].id, true);
        delete serviceContractList;
        undelete serviceContractList;
    }
    //****************************************************************************************
    // Test Method to check Service Contract with USD ISO Code
    // @return void
    //*****************************************************************************************
    static testMethod void testServiceContractISOCode(){
      List<ServiceContract> serviceContractList=createTestData('USD');
        Test.startTest();
        serviceContractList=[Select Id,CurrencyISOCode, Name from ServiceContract where Id IN : serviceContractList];
        for(ServiceContract serContract: serviceContractList){
            system.debug('Service Contract value='+serContract);
            system.assertEquals('USD',serContract.CurrencyISOCode);
        }
        Test.stopTest();
    }
    //****************************************************************************************
    // Test Method to check Service Contract with MXN ISO Code
    // @return void
    //*****************************************************************************************
    static testMethod void testServiceContractWithMXNISOCode(){
      List<ServiceContract> serviceContractList=createTestData('MXN');
        Test.startTest();
        serviceContractList=[Select Id,CurrencyISOCode, Name from ServiceContract where Id IN : serviceContractList];
        for(ServiceContract serContract: serviceContractList){
            system.debug('Service Contract value='+serContract);
            system.assertEquals('MXN',serContract.CurrencyISOCode);
        }
        Test.stopTest();
    }
    //****************************************************************************************
    // Test Method to check Service Contract with CAD ISO Code
    // @return void
    //*****************************************************************************************
    static testMethod void testServiceContractWithCADISOCode(){
      List<ServiceContract> serviceContractList=createTestData('CAD');
        Test.startTest();
        serviceContractList=[Select Id,CurrencyISOCode, Name from ServiceContract where Id IN : serviceContractList];
        for(ServiceContract serContract: serviceContractList){
            system.debug('Service Contract value='+serContract);
            system.assertEquals('CAD',serContract.CurrencyISOCode);
        }
        Test.stopTest();
    }
}