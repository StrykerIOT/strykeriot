/*********************************************************************
* Appirio, Inc
* Name: RefreshDateStampsControllerTest
* Description: [Task# T-606470]
* Created Date: [05/29/2017]
* Created By: [Poornima Bhardwaj] (Appirio)
* Date Modified                Modified By              Description of the update

**********************************************************************/
@isTest
public class RefreshDateStampsControllerTest{
    static user MXUser;
    static user agentUser;
    //****************************************************************************
    // Method to test the functionality when user is MX
    // @param : None
    // @return void
    //****************************************************************************
    public testmethod static void testRefreshDateStampsMXUser() { 
        List<Profile> profileInfo=[select id,name from Profile where name='Custom: MX Agent'];
        List<UserRole> agentRole=[select id,name from UserRole where name='Service Mexico Manager'];
        //Creating Test User
        MXUser=TestUtilities.createTestUser(1,profileInfo[0].id,false);
        MXUser.UserRole=agentRole[0];
        //Calling redirectToAccount() Method
        TestRedirectToAccount();
        //Setting mock response class MockHttpResponseGenerator
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator(''));
        //Running Test as MX User
        System.runAs(MXUser){
            Test.StartTest();
            System.debug('Running as MX Profile'+profileInfo[0].name);
            //Creating Object of RefreshDateStampsController 
            RefreshDateStampsController obj=new RefreshDateStampsController();
            List<UtilityCls.Value> valueL=obj.valueList;
            System.debug('ValueListValueMX'+valueL);
            Test.StopTest(); 
        }
    }
    //****************************************************************************
    // Method to test the functionality when user is NA
    // @param : None
    // @return void
    //****************************************************************************
    public testmethod static void testRefreshDateStampsNAUser() { 
        List<UserRole> agentRole=[select id,name from UserRole where name='Service NA Manager'];
        List<Profile> agentProId=[select id,name from Profile where name='Custom: NA Agent'];
        //Creating Test User
        agentUser=TestUtilities.createTestUser(1,agentProId[0].id,false);
        System.debug('Agent User:'+agentUser);
        //Inserting Test User
        insert agentUser;
        //Setting mock response class MockHttpResponseGenerator
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator(''));
        Test.StartTest();
        //Running Test as NA User
        System.runAs(agentUser){
            System.debug('Running as NA profile'+agentProId[0].name);
            //Creating Object of RefreshDateStampsController
            RefreshDateStampsController obj=new RefreshDateStampsController(); 
            List<UtilityCls.Value> valueL=obj.valueList;
            System.debug('ValueListValueNA'+valueL);
            System.AssertEquals('30081368',valueL[0].mid);
            System.AssertEquals(479.7,valueL[0].totIncomingAmount);
            System.AssertEquals(null,valueL[0].clientGroup);
            System.AssertEquals(10,valueL[0].totBalanceDueAmount); 
        }
        Test.StopTest();
    }
    //****************************************************************************
    // Method to Redirect to Account
    // @param : None
    // @return void
    //**************************************************************************** 
    static void TestRedirectToAccount()
    {   
        //Creating Object of RefreshDateStampsController
        RefreshDateStampsController obj=new RefreshDateStampsController();
        //Calling redirectToAccount method
        obj.redirectToAccount();
        System.assert(obj.AccountId!=null);
        System.debug('Check Account Id: '+obj.AccountId);
    }
    
}