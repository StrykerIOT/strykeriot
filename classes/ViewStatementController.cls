/*********************************************************************
* Appirio, Inc
* Name: ViewStatement
* Description: [Task# T-557407]
* Created Date: [11/30/2016]
* Created By: [Aashish Sajwan] (Appirio)
* 
* Date Modified                Modified By              Description of the update
* [Tue 06, 2016]               [Aashish Sajwan]         [T-557407]
* [Tue 27, 2016]               [Aashish Sajwan]         [T-557407]
* [Wed 04, 2017]               [Aashish Sajwan]         [T-557407]
* [Thu 05, 2017]               [Aashish Sajwan]         [T-557407]
* [Fri 06, 2017]               [Aashish Sajwan]         [T-557407] 
* [Wed 01-Mar, 2017]           [Aashish Sajwan]         [T-578828]
* [Wed 08-Mar, 2017]           [Aashish Sajwan]         [T-578828]
* [Fri 10-Mar, 2017]           [Aashish Sajwan]         [T-584311]
* [Thr 16-Mar, 2017]           [Aashish Sajwan]         [T-580331]
* [Fri 17-Mar, 2017]           [Aashish Sajwan]         [T-580331]
* [Mon 20-Mar, 2017]           [Aashish Sajwan]         [S-470288]
* [Fri 31-Mar, 2017]           [Aashish Sajwan]         [S-470288]
* [Mon 3-Apr, 2017]            [Aashish Sajwan]         [Optimation and Revert back code of Statement Email Request functionality:  T-590827]
* [Tue 18-Apr, 2017]           [Aashish Sajwan]         [Default to show all statements: T-595187/S-471447]
* [Tue 21-Apr, 2017]           [Aashish Sajwan]         [Default to show all statements: T-595187/S-471447]
* [Thu 11-May, 2017]           [Aashish Sajwan]         [New Enhancements : S-471447]
* [Mon 22-May, 2017]           [Aashish Sajwan]         [Resolve Record limit via query result:    T-603941/I-275448]
* [Wed 21-Jun, 2017]           [Aashish Sajwan]         [T-610961/I-280040 Added ReadyOnly on Apex Page to show more than 1000 records on page]
*********************************************************************/
public class ViewStatementController {
    //Process Block variables
    private static final String CONTENT_POST = 'ContentPost';
    public string StatementId;
    public Map<string,External_Document__c> mapExternalDocs;
    
    
    
    // variables  
    private string stmntType = 'NA';
    public String FromDate{get;set;}
    public string selectedType {get;set;}
    public String ToDate {get;set;}
    public string AccountId {get;set;}
    public string DBAName {get;set;}
    public string MID {get;set;}
    public String statmentIdValue;
    public string clientGroupId {get;set;}
    public List<SelectOption> statementType{get;set;}
    // 21 Jun 2017 add Transient  in variable of list and collection -T-610961/I-280040  Aashish Sajwan Start
    public  List<UtilityCls.StatmentWrapper> statmentList {get;set;}
   
    public  List<UtilityCls.StatmentWrapper> selectedStatmentList {get;set;}                                    //Added By AS
    //End
    public boolean showMessage {get;set;}
    public string AccountLink {get;set;}
    public string fromDateValue {get;set;}
    public string toDateValue {get;set;}
    wwwElavonComServicesImagingserviceU.UAT01 request {get;set;}
    public Boolean isSendingEmail{set;get;}                                                  //Added by AS
    public Boolean isPageLoad {get;set;}
    //23-Feb-17 Aashish Sajwan #T-578828
    public Account accountRecord {get;set;}
    public String getstatmentIdValue(){
        return statmentIdValue;
    }
    
    public void setstatmentIdValue(String statmentIdValue){
        this.statmentIdValue= statmentIdValue;
    } 
   
    // 21 Jun 2017 add Transient  in variable of list and collection -T-610961/I-280040  Aashish Sajwan Start
    // Create Statement Response  reference
   wwwElavonComSchemaImagingresourcesV.StatementListResponse_element statementlistresponse =null;
    
    //28-Feb-2017 - For Maxico Region Get all MID associated with TaxId - Aashish Singh Sajwan # T-580331 
    //Start
    public string accountTaxId {get;set;}
    //End
    
    //Added by Aashish Singh Sajwan
    // Constructor of ViewStatement Class
    public ViewStatementController(){
        //Set isPageLoad variable as per #T-595187/#S-471447
        isPageLoad = true;
        
        //Process Block Constructor
        mapExternalDocs = new Map<string, External_Document__c >();
        
        
        //Added by AS
        
        //Statement Email Constructor
        AccountId =ApexPages.currentPage().getParameters().get('AccountId'); //Need to check once all dev complete
        isSendingEmail=false;                                    //Added by AS
        showMessage = false;
        statmentList = new List<UtilityCls.StatmentWrapper>();
        selectedStatmentList = new List<UtilityCls.StatmentWrapper>();              //Added by AS
        FromDate = String.valueof(Date.Today());
        ToDate = String.valueof(Date.Today().addYears(1).addDays(-1));
        Map<String, String> urlParams = Apexpages.currentPage().getParameters();  
        stmntType = urlParams.containsKey('StatementType') ? EncodingUtil.urlDecode(urlParams.get('StatementType'),'UTF-8') : 'NA';
        DBAName = urlParams.containsKey('AccountName') ? EncodingUtil.urlDecode(urlParams.get('AccountName'),'UTF-8') : '';
        AccountId = urlParams.containsKey('AccountId') ? EncodingUtil.urlDecode(urlParams.get('AccountId'),'UTF-8') : '';
        MID = urlParams.containsKey('MID') ? EncodingUtil.urlDecode(urlParams.get('MID'),'UTF-8') : '';
        clientGroupId = urlParams.containsKey('ClientGroupId') ? EncodingUtil.urlDecode(urlParams.get('ClientGroupId'),'UTF-8') : '';
        system.debug('** stmntType value **'+stmntType);
        statementType = populateStatementType(stmntType);
        system.debug('** statementType **'+statementType);
        AccountLink  = 'http';
        //28-Feb-2017 - For Maxico Region Get all MID associated with TaxId - Aashish Singh Sajwan # T-580331   
        //Start 
        accountTaxId  = urlParams.containsKey('AccountTaxId') ? EncodingUtil.urlDecode(urlParams.get('AccountTaxId'),'UTF-8') : '';
        //End
        
        if(AccountId!=''){
            //Get Account Record value on basis of account Id.
            accountRecord  = [SELECT Id, Name, Parent_Chain__c,MPS_MID__c,MID__c,Client_Group__c,
                              Processing_Center_ID__c, Owner.Name, Account_Owner_Role__c 
                              FROM Account
                              WHERE Id =: AccountId];  
        }      
    }
    
    
    //Popluate StatementType Picklist
    
    private list<SelectOption> populateStatementType(string type) {
        list<SelectOption> listStatement = new list<SelectOption>();
        //Added New Enhancement as per #S-471447 Aashish Singh Sajwan 11 May 2017 -->
        // Start 
        listStatement.add(new SelectOption('All','All'));
        // End
        if(stmntType =='NA'){
            
            listStatement.add(new SelectOption('MER_BILL','Merchant Billing Statement'));
            //20-Mar 2017 Change MER_PSS to SIMPLE after disucssion with Devi #S470288
            //31-Mar 2017 Change Simple Statement to Simplified Statement after disucssion with Devi #S470288
            listStatement.add(new SelectOption('SIMPLE','Simplified Statement')); 
            listStatement.add(new SelectOption('WMER_BILL','What-If Statements'));
            listStatement.add(new SelectOption('MER_TRAN','Merchant Transaction Detail Statement'));
            listStatement.add(new SelectOption('CHAIN_BAT','Chain Batch Summary Statement'));
            //20-Mar 2017 Change CHAIN_DEP to CHAIN_FUND after disucssion with Devi #S470288
            listStatement.add(new SelectOption('CHAIN_FUND','Chain Deposit Statement'));
            listStatement.add(new SelectOption('WCHAIN_MER','Chain-level What-if Statement'));
        }else if(stmntType =='EU'){
            
            listStatement.add(new SelectOption('MER_FUND','Merchant Funding Statement' ));
            listStatement.add(new SelectOption('CHAIN_FUND','Chain Funding Statement'));
            listStatement.add(new SelectOption('MER_BILL','Merchant Billing Statement'));
            listStatement.add(new SelectOption('CHAIN_BILL','Chain Billing Statement'));
            listStatement.add(new SelectOption('VPOLISH','Merchant VAT Invoice'));
            listStatement.add(new SelectOption('VPOLISHC','Chain VAT Invoice'));
            
        }else{
            
            listStatement.add(new SelectOption('MER_BILL','Merchant Billing Statement'));
            listStatement.add(new SelectOption('CHAIN_BILL','Chain Billing Statement'));
            listStatement.add(new SelectOption('VAT','Customer Tax Invoice at the TAXID/RFC level'));
            //17-Mar-2017 Updated After Discsusion with Devi Elavon not need them now
            // listStatement.add(new SelectOption('MER_FUND','Merchant Funding Statement'));
            // listStatement.add(new SelectOption('CHAIN_FUND','Chain Funding Statement '));
            
        }
        
        //retrun listStatement
        return listStatement; 
    }
    
   
    
    //Redirect To Account 
    public PageReference redirectToAccount(){
        // Create Page Reference for Account
        PageReference accountPage = new PageReference('/'+AccountId);
        //return account page reference
        return accountPage;
        
    }
    
    //Search Statement Method
    public void searchStatement() {
        statmentList =  new List<UtilityCls.StatmentWrapper>();
        System.debug(' ** searchStatement **');
        System.debug(' ** FromDate value **'+ FromDate);
        System.debug(' ** ToDate value **'+ ToDate);
        System.debug(' ** selectedType value **'+ selectedType);
        
        system.debug(' ** IsPageLoad Value **'+isPageLoad);
        
        //Clear Statement List 
        //statmentList.clear();
        
        
        try{   
            //Create Request Object
            request =  new  wwwElavonComServicesImagingserviceU.UAT01();
            
            //Crate StatementList Header Object
            wwwElavonComSchemaImagingresourcesV.StatementListHeader requeststatements = new wwwElavonComSchemaImagingresourcesV.StatementListHeader();
            //Assign From Date value in mm/dd/yyyy format
            fromDateValue  =  (Date.valueof(FromDate).month()<10)? '0'+ string.valueOf(Date.valueof(FromDate).month()) : string.valueOf(Date.valueof(FromDate).month());
            fromDateValue =  fromDateValue +'/'+ ( (Date.valueof(FromDate).day()<10)? '0'+ string.valueOf(Date.valueof(FromDate).day()) : string.valueOf(Date.valueof(FromDate).day()))  +'/'+Date.valueof(FromDate).year(); //FromDate.format();
            //Assign To Date value in mm/dd/yyyy format
            toDateValue = (Date.valueof(ToDate).month()<10)? '0'+ string.valueOf(Date.valueof(ToDate).month()) : string.valueOf(Date.valueof(ToDate).month());
            toDateValue = toDateValue  +'/'+( (Date.valueof(ToDate).day()<10)? '0'+ string.valueOf(Date.valueof(ToDate).day()) : string.valueOf(Date.valueof(ToDate).day())) +'/'+Date.valueof(ToDate).year();// ToDate.format();
            
            
            system.debug('&& From Date Value in request && '+fromDateValue);
            system.debug('&& To Date Value in request && '+toDateValue);
            
            //Assign from date in request object
            requeststatements.FromDate =  fromDateValue;//'05/01/2016';
            //Assign to date in request object
            requeststatements.ToDate = toDateValue;//'10/31/2016';
            //Assign Statement type in request
            requeststatements.Type_x = (isPageLoad)?'All':selectedType;// 'MER_BILL';
            
            system.debug('&&  requeststatements Type_x value'+requeststatements.Type_x);
            
            system.debug('&& From Date Value in request && '+fromDateValue);
            system.debug('&& To Date Value in request && '+toDateValue);
            
            
            
            //28-Feb-2017 -For Maxico Region Get all MID associated with TaxId - Aashish Singh Sajwan # T-580331    
            //Start
            List<Account> accntList = new  List<Account>();
            
            if(accountTaxId!='' && stmntType =='QUERE'  && selectedType=='VAT' && !isPageLoad){
                // Aashish Sajwan 22 May 2017: Apply SoSl to find Tax Encrypted Id from account  #T-603941/I-275448
                //execute SOSL to reterive Tax Id Encrypted field from Account based on Processing Center Id 
                String searchQuery = 'FIND \'' + accountTaxId + '\' IN ALL FIELDS RETURNING  Account (Id,MID__c,Client_Group__c,Tax_Id_Encrypted__c, Processing_Center_ID__c WHERE Processing_Center_ID__c =\'' + stmntType + '\' )';
                list<list <sObject>> searchList = search.query(searchQuery);
                //create accountSearchList to get result of find sosl
                list<Account> accountSearchList = ((List<Account>)searchList[0]);
                
                System.Debug(LoggingLevel.DEBUG, '\n\nACCOUNT SEARCH LIST Size : ' + accountSearchList.Size() + '\n\n');
                
                System.Debug(LoggingLevel.DEBUG, '\n\nACCOUNT SEARCH LIST : ' + accountSearchList + '\n\n');
                //loop through accountSearchList to find tax encrypted id with current account record
                for(Account objAccount : accountSearchList){
                    //Compare Tax_Id_Encrypted__c with current account record
                    if(objAccount.Tax_Id_Encrypted__c !=null && objAccount.Tax_Id_Encrypted__c == accountTaxId){                    
                        //Add account object to accntList
                        accntList.add(objAccount);
                    }                       
                }                   
                System.Debug(LoggingLevel.DEBUG, '\n\nACCOUNT LIST : ' + accntList + '\n\n');
             //END   
                
                
                
            }else{
                accntList.add(accountRecord);
            }
            
            system.debug('&& accntList values &&'+accntList);
            
            //End
            wwwElavonComSchemaImagingresourcesV.AccountNumberType[] accountnumbers = CreateAccountNum(accntList);
            //End 
            
            
            
            //Assign Accountnumber array to request's AccountNumber property
            requeststatements.AccountNumber = accountnumbers;
            
            //Get Response of Request
            statementlistresponse = request.retrieveStatementList(requeststatements,System.Label.Elavon_Integration_End_User_ID);
            System.debug('@@@@ statementlistresponse value :: '+statementlistresponse );
            
            //Check Response is not null and Statementlist of respinse  is not null
            if(statementlistresponse!=null && statementlistresponse.StatementList!=null && statementlistresponse.StatementList.size()>0){
                System.debug('@@@@@ response statmentList  size' + statementlistresponse.StatementList.size());
                //Itterate StatementList of response
                for(wwwElavonComSchemaImagingresourcesV.StatementData  stmndtData :statementlistresponse.StatementList){
                    //Create Wrapper Object 
                    UtilityCls.StatmentWrapper objWrapper = new UtilityCls.StatmentWrapper();
                    objWrapper.ID = stmndtData.ID; //Assign Id
                    objWrapper.MID = MID ;//Assign MID
                    objWrapper.AccountName = DBAName; //Assign DBA Name
                    objWrapper.MediaType = stmndtData.MediaType; //Assign Media Type
                    objWrapper.Date_x = stmndtData.Date_x; //Assign Date
                    objWrapper.Description = stmndtData.Description; //Assign Description
                    objWrapper.AccountNumber = stmndtData.AccountNumber; //Assign AccountNumber
                    objWrapper.Attributes= stmndtData.Attributes; //Assign Attribute
                    objWrapper.Type_x = stmndtData.Type_x ; //Assign Type
                    
                    //Add Wrapper object to Statement List
                    statmentList.add(objWrapper); 
                    
                }
                
            }
            System.debug('@@@@@ response statmentList  value ' + statmentList );
            System.debug('@@@@@ response statmentList  count ' + statmentList.size() );
            System.debug('@@@@@ response is from date' + statementlistresponse.Request.FromDate);
            System.debug('@@@@@ type is ' + statementlistresponse.Request.Type_x);                    
            
            //Check Statementresponse Error property is not null
            if(statementlistresponse.Error!=null){
                System.debug('@@@@@ error code  is ' + statementlistresponse.Error[0].ErrorCode);
                System.debug('@@@@@ error message is ' + statementlistresponse.Error[0].ErrorMessage);
            }
            
            isPageLoad = false;
            
        }catch(Exception e){
            System.debug('@@@@@ error code  is ' + statementlistresponse);
            
            System.debug('@@@@ FAILURE'+e);
        }
        //Check Statement List Size
        if(statmentList.size()==0){
            //Assign Showmessage value as true
            showMessage = true;
        }else{
            showMessage = false;
        }
    }
    
    public wwwElavonComSchemaImagingresourcesV.AccountNumberType[] CreateAccountNum(List<Account> accntList){
        //Create AccountNumberType[] Object
        system.debug('** CreateAccountNum Calling **');
        system.debug('** accntList value **'+accntList);
        wwwElavonComSchemaImagingresourcesV.AccountNumberType[] accountnums = new wwwElavonComSchemaImagingresourcesV.AccountNumberType[]{};
            
            //28-Feb-2017 -For Maxico Region Get all MID associated with TaxId - Aashish Singh Sajwan # T-580331   
            for(Account accObj : accntList){
                //Create AccountNumberType Object
                wwwElavonComSchemaImagingresourcesV.AccountNumberType accountnum = new wwwElavonComSchemaImagingresourcesV.AccountNumberType();
                //Assign Client Id to Account Number 
                accountnum.ClientGroup = (isPageLoad) ? Integer.valueOf(clientGroupId) : Integer.valueOf(accObj.Client_Group__c) ;//3 ;       
                
                //10-Mar 2017 Aashish Sajwan  #S-470288/#T-584311 Statement Types for NA, MX
                //Check Statement Type is not 'EU' 
                if(stmntType !='EU' && !isPageLoad) {
                    //IF Statement Type NA
                    if(stmntType =='NA'){
                        string MIDValue = accObj.MID__c; 
                        //Check MID Lenght and Statement Type
                        if(MIDValue.length()<16){
                            System.debug(' ** MID less thatn 16 **'+ MIDValue);
                            integer count = 16- MIDValue.length();
                            string appendValue = '';
                            for(integer n=0; n<count; n++){
                                //Append '0' to appendvalue
                                appendValue +='0';
                            }
                            System.debug(' ** appendValue  **'+ appendValue );
                            System.debug(' ** selectedType  **'+ selectedType );
                            // Append Value to MID
                            MIDValue = appendValue + MIDValue;
                            System.debug(' ** MID final value**'+ MID);
                        }
                        
                        if(selectedType == 'CHAIN_BAT' || selectedType == 'CHAIN_FUND'  || selectedType == 'WCHAIN_MER' ){
                            system.debug('&& selectedType value: CHAIN_BAT&&');
                            accountnum.MerchantNumber = MIDValue;
                            accountnum.ChainNumber = accObj.Parent_Chain__c;
                        }else{
                            accountnum.MerchantNumber = MIDValue;//'0000008010255837';  
                          
                        }  
                        
                        
                    }else{//Else Statement Type MX
                        if(selectedType == 'MER_BILL'){
                            system.debug('&& selectedType value: MER_BILL &&');
                            accountnum.MerchantNumber = accObj.MPS_MID__c;
                        }else if(selectedType == 'CHAIN_BILL'){
                            system.debug('&& selectedType value : CHAIN_BILL &&');
                            accountnum.MerchantNumber = accObj.MPS_MID__c;
                            accountnum.ChainNumber = accObj.Parent_Chain__c;
                        }else{
                            accountnum.MerchantNumber = accObj.MID__c; // # T-580331 
                        }
                    }
                    
                }else if(!isPageLoad){
                    //Assign MPSMID to accountnumber
                    accountnum.MPSMID = accObj.MID__c; 
                }else{
                    accountnum.MerchantNumber = MID; 
                    accountnum.ChainNumber = accObj.Parent_Chain__c;   
                }
                
                //Add account number object to accountnums list
                accountnums.add(accountnum);
                
            }
        system.debug('** Account Num values **'+accountnums);
        return accountnums;
    }
    
    //****************************************************************************
    // Method to openRecord
    // Crated by : Aashish Singh Sajwan 06 Dec 2016 #T-557407
    // @param null
    // @return PageReference of content Record
    //****************************************************************************  
    public PageReference openRecord(){
        //webservice callout using the retrievestatement method.
        System.debug('@@@@ openRecord:: ');
        
        // PageReference pageRef = new PageReference('/apex/StatementRecordView?StatementId='+statmentIdValue+'&AccountId='+AccountId+'&MID='+MID+'');
        // return pageRef ;
        
        //8-March 2017 Code Optimization 
        UtilityCls.ExternalDocumentWrapper objExternalDocument = null;  
        string contentId = '';
        for(External_Document__c externalObj : ([SELECT Id, Expiry_Date__c,Account__c,
                                                 MPSMID__c, ExternalID__c
                                                 FROM External_Document__c
                                                 Where MPSMID__c =: MID])){
                                                     mapExternalDocs.put(externalObj.ExternalID__c,externalObj);
                                                 }
        
        if(mapExternalDocs.size()>0 && mapExternalDocs.containsKey(statmentIdValue)){
            system.debug('** mapExternalDocs Keys **'+mapExternalDocs.keySet());
            system.debug('** mapExternalDocs Values **'+mapExternalDocs.values());
            system.debug('** StatementId Values **'+statmentIdValue);
            system.debug('** Key Contains in mapExternalDocs **'+mapExternalDocs.containsKey(statmentIdValue));
            system.debug('** UtilityCls.CheckStatementExits Start **');
            objExternalDocument = UtilityCls.CheckStatementExits(mapExternalDocs,statmentIdValue);
            system.debug('** UtilityCls.CheckStatementExits End **'+objExternalDocument);
            if(objExternalDocument!=null){
                contentId = objExternalDocument.cv.Id;
            } 
            
        }else{
            System.debug('**For Testing**');
            System.debug('**Testing accountRecord'+accountRecord);
            System.debug('**Testing MID'+MID);
            System.debug('**Testing StatementId'+StatementId);
            objExternalDocument =  UtilityCls.ProcessStatments(accountRecord,MID,statmentIdValue,false,false);
            system.debug('** objExternalDocument values **'+objExternalDocument);
            if(objExternalDocument!=null){
                contentId = objExternalDocument.cv.Id;
            } 
        }
        PageReference pageRef = new PageReference('/'+contentId);
        return pageRef;
    }
    
}