/*********************************************************************
* Appirio a WiPro Company
* Name: ScheduleCaseWithUnreadEmail
* Description: Scheduleable class to execute Batch class. See the 'CaseWithUnreadEmailBatch' and 'CaseWithUnreadEmailBatchHelper' 
*              classes for more details.
* Created Date: 6/8/2017
* Created By: Bobby Cheek
*/
global class ScheduleCaseWithUnreadEmail implements Schedulable{

   global void execute(SchedulableContext sc){
        Id batchProcessId = Database.ExecuteBatch(new CaseWithUnreadEmailBatch());
   }

}
/************* DO NOT DELETE *****************
        CaseWithUnreadEmailBatch stcba = new CaseWithUnreadEmailBatch();
        // Seconds Minutes Hours Day_of_monthMonth Day_of_week optional_year
        String Sched = '0 0 0 0 MMM DDD YYYY';  // houely 24/7 365
        System.Schedule('Remove Case From Unread Email Queue', Sched, stcba);
*/