/*********************************************************************
* Appirio a WiPro Company
* Name: AccountTaxInfoRelationshipBatch
* Description: Batch Helper class called by the AccountTaxInfoRelationshipBatch batch. 
* Created Date: 04/26/2017
* Created By: Bobby Cheek
* 
* Date Modified                Modified By                  Description of the update
*
**********************************************************************/
public with sharing class AccountTaxInfoRelationshipBatchHelper {

  private final String CLASSNAME = '\n\n*** AccountTaxInfoRelationshipBatchHelper.';
  
  public Database.Querylocator GetQueryLocator(String pQuery){
    return Database.getQueryLocator(pQuery);
  }  
  
  /*  author : Appirio
    date : 04/26/2017
    description :  Builds a SOQL query for Account records with child Cases
    paramaters : none
    returns : a String representing a SOQL query to retrieve a bach of records to be processed
  */
  public String GetQuery(){
    
    final String METHODNAME = CLASSNAME + 'GetQuery()';
    system.debug(LoggingLevel.INFO, METHODNAME);
    
    datetime minus24DateTime = datetime.now().addHours(-24);
    system.debug(LoggingLevel.DEBUG, '\n\n*** minus24DateTime :: '+ minus24DateTime);
    
    String dateString = minus24DateTime.format('yyyy-MM-dd\'T\'hh:mm:ss\'z\'');
    String SOQL='';
        if(Test.isRunningTest()){
           SOQL = 'SELECT Id, Tax_External_Id__c, Tax_Info_link__c, RecordType.Name FROM Account WHERE RecordType.Name =\'General\' AND Tax_External_Id__c != null';      
        }else{
           SOQL = 'SELECT Id, Tax_External_Id__c, Tax_Info_link__c, RecordType.Name FROM Account WHERE RecordType.Name =\'General\' AND Tax_External_Id__c != null AND SystemModStamp >= ' + dateString;    
        }    
    system.debug(LoggingLevel.DEBUG, '\n\n*** SOQL :: '+ SOQL);
    return SOQL;
  }  
  
  /*  author : Appirio
    date : 04/26/2017
    description : updates/sets the Account and Tax Information lookup relationship 
    paramaters : list of sObjects
    returns : nothing
  */
  public void ProcessBatch(list<SObject> scope){
    
    list<Account> accountList = new list<Account>();
    accountList.addall((List<Account>)scope);    
    
    set<String> taxExternalIdSet = new set<String>();    
    for(Account acc : accountList){        
      taxExternalIdSet.add(acc.Tax_External_Id__c);        
    }  
    
    list<Tax_Information__c> taxInformationList = [SELECT Id, External_Id__c, US_Tax_ID_Number__c FROM Tax_Information__c WHERE External_Id__c IN : taxExternalIdSet];
    
    map<String,Tax_Information__c> taxInformationExternalIdToTaxInformationMap = new map<String,Tax_Information__c>();
    
    for(Tax_Information__c taxInfo : taxInformationList){
      if(!taxInformationExternalIdToTaxInformationMap.ContainsKey(taxInfo.External_Id__c)){
        taxInformationExternalIdToTaxInformationMap.put(taxInfo.External_Id__c, taxInfo);
      }
    }
    
    for(Account acc : accountList){
      if(taxInformationExternalIdToTaxInformationMap.ContainsKey(acc.Tax_External_Id__c)){
        acc.Tax_Info_link__c = taxInformationExternalIdToTaxInformationMap.get(acc.Tax_External_Id__c).Id;
      }
    }
    
    if(!accountList.isEmpty() || accountList.size() != 0){
      update accountList;    
    }  
  }  
}