/***********************************************************************************************
* Appirio, Inc
* Name: AssetTriggerHandlerTest
* Description: [T-586882 -Test Class for AssetTriggerHandler]
* Created Date: [21/03/2017]
* Created By: [Neeraj Kumawat] (Appirio)
* 
* Date Modified                Modified By                  Description of the update
[03/04/2017]                 Neeraj Kumawat                [T-586882 Updated Test Case to uncheck Multi-Mid]
*************************************************************************************************/
@isTest
public class AssetTriggerHandlerTest {
    public static Integer numberOfRecords = 200;
     //****************************************************************************
    // Method for Creating and updating Na Asset
    // @param assetList: List of Asset
    //@param accountId: Id of Account
    //@param contactId: Id of Contact
    //@param MultiMid:  boolean flag of multi mid
    //@param TerminalNumber: Terminal Number
    //@param assetName: Name of Asset
    //@param isInsert: Flag to check asset insert or update
    // @return assetList: List of Asset
    //****************************************************************************
    private static List<Asset> createNAAccountAsset(List<Asset>assetList,Id accountId,Id contactId,Boolean MultiMid,
                                                    String TerminalNumber, String assetName,Boolean isInsert){
                                                     for(Asset asst:assetList){
                        if(TerminalNumber!=null){
                         asst.Terminal_Management_Number_TMS__c=TerminalNumber;
                        }
                        if(accountId!=null){
                        asst.AccountId=accountId;
                        }
                        if(contactId!=null){
                        asst.ContactId=contactId;
                        }
                        if(MultiMid!=null){
                            asst.Multi_MID__c=MultiMid;
                        }
                        if(assetName!=null){
                         asst.Name=assetName;
                        }
                    }
                    if(isInsert){
                      insert assetList;
                    }else{
                      update assetList;
                    }
                    return assetList;                                        
    }
    //****************************************************************************
    // Method for Creating and updating MX Asset
    // @param assetList: List of Asset
    //@param accountId: Id of Account
    //@param MultiMid:  boolean flag of multi mid
    //@param LegacyEquipId: Legacy Equip Id
    //@param assetName: Name of Asset
    //@param EquipmentStatusCode: Equipment Status Code
    //@param MerchantEquipStatusCode: Merchant Equip Status Code
    //@param assetName: Name of Asset
    //@param isInsert: Flag to check asset insert or update
    // @return assetList: List of Asset
    //****************************************************************************
    private static List<Asset> createMXAccountAsset(List<Asset>assetList,Id accountId,Boolean MultiMid,String LegacyEquipId,
                                                    String EquipmentStatusCode,String MerchantEquipStatusCode,
                                                    String assetName,Boolean isInsert){
                                                   for(Asset asst:assetList){
                    if(LegacyEquipId!=null){
                     asst.Legacy_Equip_ID__c=LegacyEquipId;
                    }
                    if(EquipmentStatusCode!=null){
                    asst.Equipment_Status_Code__c=EquipmentStatusCode;
                    }
                    if(MerchantEquipStatusCode!=null){
                    asst.Merchant_Equipment_Status_Code__c=MerchantEquipStatusCode;
                    }
                    if(accountId!=null){
                    asst.AccountId=accountId;
                    }
                    if(MultiMid!=null){
                        asst.Multi_MID__c=MultiMid;
                    }
                    if(assetName!=null){
                     asst.Name=assetName;
                    }
                }
                if(isInsert){
                  insert assetList;
                }else{
                  update assetList;
                }
                return assetList;
     }
    //****************************************************************************************
    // Test Method to check Asset Creation with NA  Account
    // @return void
    //*****************************************************************************************
    public static testmethod void testCreationWithNAAccount(){
        List<Account> accountList=TestUtilities.createAccountWithProcessingCenterId(1,'NA',true);
        Test.startTest();
        List<asset> assetList=TestUtilities.createAsset(numberOfRecords,false);
        assetList=createNAAccountAsset(assetList,accountList[0].Id,null,null,'100',null,true);
        List<Asset> createAssetList=[Select Id, Multi_MID__c,Terminal_Management_Number_TMS__c 
                                     From Asset
                                     Where Terminal_Management_Number_TMS__c='100'];
        for(Asset asst:createAssetList){
            System.assertEquals(true, asst.Multi_MID__c);
        }
        Test.stopTest();
    }
    //****************************************************************************************
    // Test Method to check Asset updation with NA Account
    // @return void
    //*****************************************************************************************
    public static testmethod void testUpdationWithNAAccount(){
        List<Account> accountList=TestUtilities.createAccountWithProcessingCenterId(1,'NA',true);
        Test.startTest();
        List<asset> assetList=TestUtilities.createAsset(numberOfRecords,false);
        assetList=createNAAccountAsset(assetList,accountList[0].Id,null,null,null,'Test Update Asset With Na Account', true);
        List<Asset> createAssetList=[Select Id,Name,Multi_MID__c,Terminal_Management_Number_TMS__c 
                                     From Asset where Name='Test Update Asset With Na Account'];
        for(Asset asst:createAssetList){
            System.assertEquals(false, asst.Multi_MID__c);
        }
        for(Asset asst:createAssetList){
            asst.Terminal_Management_Number_TMS__c='101';
            asst.AccountId=accountList[0].Id;
        }
        update assetList;
        List<Asset> updateAssetList=[Select Id,Name, Multi_MID__c,Terminal_Management_Number_TMS__c 
                                     From Asset
                                     Where Terminal_Management_Number_TMS__c='101'];
        for(Asset asst:updateAssetList){
            System.assertEquals(true, asst.Multi_MID__c);
        }
        Test.stopTest();
    }
    //****************************************************************************************
    // Test Method to check Asset Creation with MX Account
    // @return void
    //*****************************************************************************************
    public static testmethod void testCreationWithMXAccount(){
        List<Account> accountList=TestUtilities.createAccountWithProcessingCenterId(1,'QUERE',true);
        Test.startTest();
        List<asset> assetList=TestUtilities.createAsset(numberOfRecords,false);
        assetList=createMXAccountAsset(assetList,accountList[0].Id,null,'100','A','A',null,true);
        List<Asset> createAssetList=[Select Id, Multi_MID__c,Terminal_Management_Number_TMS__c 
                                     From Asset
                                     Where Legacy_Equip_ID__c='100'];
        for(Asset asst:createAssetList){
            System.assertEquals(true, asst.Multi_MID__c);
        }
        Test.stopTest();
    }
    //****************************************************************************************
    // Test Method to check Asset updation with MX Account
    // @return void
    //*****************************************************************************************
    public static testmethod void testUpdationWithMXAccount(){
        List<Account> accountList=TestUtilities.createAccountWithProcessingCenterId(1,'QUERE',true);
        Test.startTest();
        List<asset> assetList=TestUtilities.createAsset(numberOfRecords,false);
        for(Asset asst:assetList){
            asst.AccountId=accountList[0].Id;
        }
        insert assetList;
        List<Asset> createAssetList=[Select Id,Name,Multi_MID__c,Legacy_Equip_ID__c 
                                     From Asset];
        system.debug('Asset Create List='+createAssetList);
        for(Asset asst:createAssetList){
            System.assertEquals(false, asst.Multi_MID__c);
        }
        assetList=createMXAccountAsset(assetList,accountList[0].Id,null,'101','A','A',null,false);
        List<Asset> updateAssetList=[Select Id,Name, Multi_MID__c,Terminal_Management_Number_TMS__c 
                                     From Asset
                                     Where Legacy_Equip_ID__c='101'];
        System.debug('Update Asset List='+updateAssetList);
        for(Asset asst:updateAssetList){
            System.assertEquals(true, asst.Multi_MID__c);
        }
        Test.stopTest();
    }
    //****************************************************************************************
    // Test Method to check Asset Creation without  Account
    // @return void
    //*****************************************************************************************
    public static testmethod void testCreationWithoutAccount(){
        List<Account> accountList=TestUtilities.createAccount(1, true);
        List<Contact> contactList=TestUtilities.createContact(1,accountList[0].Id, true);
        Test.startTest();
        List<asset> assetList=TestUtilities.createAsset(numberOfRecords,false);
        assetList=createNAAccountAsset(assetList,null,contactList[0].Id,true,'100','Test Asset Without Account',true);
        List<Asset> createAssetList=[Select Id, Multi_MID__c,Terminal_Management_Number_TMS__c 
                                     From Asset
                                     Where Name='Test Asset Without Account'];
        for(Asset asst:createAssetList){
            System.assertEquals(false, asst.Multi_MID__c);
        }
        Test.stopTest();
    }
    //****************************************************************************************
    // Test Method to check Asset Creation with out Terminal Number Account
    // @return void
    //*****************************************************************************************
    public static testmethod void testCreationWithoutTerminalNumber(){
        List<Account> accountList=TestUtilities.createAccount(1, false);
        for(Account acct:accountList){
            acct.Processing_Center_ID__c='NA';
        }
        insert accountList;
        Test.startTest();
        List<asset> assetList=TestUtilities.createAsset(numberOfRecords,false);
        assetList=createNAAccountAsset(assetList,accountList[0].Id,null,true,null,
                                       'Test Asset Without Terminal Management Number and Multi Mid checked',true);
        List<Asset> createAssetList=[Select Id, Multi_MID__c,Terminal_Management_Number_TMS__c 
                                     From Asset
                                     Where Name='Test Asset Without Terminal Management Number and Multi Mid checked'];
        for(Asset asst:createAssetList){
            System.assertEquals(false, asst.Multi_MID__c);
        }
        Test.stopTest();
    }
    //****************************************************************************************
    // Test Method to check Asset Creation with out Legacy Equip Id
    // @return void
    //*****************************************************************************************
    public static testmethod void testCreationWithoutLegacyEquipId(){
        List<Account> accountList=TestUtilities.createAccount(1, false);
        for(Account acct:accountList){
            acct.Processing_Center_ID__c='QUERE';
        }
        insert accountList;
        Test.startTest();
        List<asset> assetList=TestUtilities.createAsset(numberOfRecords,false);
        assetList=createMXAccountAsset(assetList,accountList[0].Id,true,null,'A','A',
                                       'Test Asset Without LegacyEquipId and Multi Mid checked',true);
        List<Asset> createAssetList=[Select Id, Multi_MID__c,Terminal_Management_Number_TMS__c 
                                     From Asset
                                     Where Name='Test Asset Without LegacyEquipId and Multi Mid checked'];
        for(Asset asst:createAssetList){
            System.assertEquals(false, asst.Multi_MID__c);
        }
        Test.stopTest();
    }
    //****************************************************************************************
    // Test Method to check Asset update with Terminal number
    // @return void
    //*****************************************************************************************
    public static testmethod void testResetMultiMidtoUnchecked(){
        List<Account> accountList=TestUtilities.createAccount(1, false);
        for(Account acct:accountList){
            acct.Processing_Center_ID__c='NA';
        }
        insert accountList;
        Test.startTest();
        List<asset> assetList=TestUtilities.createAsset(2,false);
        assetList=createNAAccountAsset(assetList,accountList[0].Id,null,null,'103',
                                       'Test Asset with Reset MultiMid to unchecked',true);
        List<Asset> createAssetList=[Select Id, Multi_MID__c,Terminal_Management_Number_TMS__c 
                                     From Asset
                                     Where Name='Test Asset with Reset MultiMid to unchecked'];
        System.debug('createAssetList='+createAssetList);
        for(Asset asst:createAssetList){
            System.assertEquals(true, asst.Multi_MID__c);
        }
        //Test.stopTest();
        if(createAssetList.size()>0){
            createAssetList[0].Terminal_Management_Number_TMS__c='104';
            createAssetList[0].IsAssetUpdate__c=0.424242424;
            createAssetList[1].Terminal_Management_Number_TMS__c='107';
            createAssetList[1].IsAssetUpdate__c=0.424242444;
            
        }
        update createAssetList;
        List<Asset> updateAssetList=[Select Id, Multi_MID__c,Terminal_Management_Number_TMS__c 
                                     From Asset
                                     Where Name='Test Asset with Reset MultiMid to unchecked'];
        System.debug('updateAssetList='+updateAssetList);
        for(Asset asst:updateAssetList){
            System.assertEquals(false, asst.Multi_MID__c);
        }
        Test.stopTest();
    }
}