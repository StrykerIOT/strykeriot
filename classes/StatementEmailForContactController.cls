/*********************************************************************
 * Appirio, Inc
 * Name: StatementEmailForContactController
 * Description: [Task# T-581911]
 * Created Date: [06/3/2017]
 * Created By: [Aashish Sajwan] (Appirio)
 * 
 * Date Modified                Modified By              Description of the update
 [3-Apr-2017]                   [Aashish Sajwan]         [Functionality no longer for use for any release]
 **********************************************************************/
public class StatementEmailForContactController{
    private string contactId;
    public List<Statement_Email_Request__c> statementLst{set;get;}
    public List<StatementEmailRequestWrapper> StatementEmailRequestWrapperLst{set;get;}
    public StatementEmailForContactController(ApexPages.StandardController controller) {
    //for getting contact Id from Standard Controller
        contactId = ((Contact)controller.getRecord()).id;
        statementEmailRequestWrapperLst=new List<StatementEmailRequestWrapper>();
    //query for getting contact related Statement Request Emails
        statementLst=[SELECT id,name,CreatedDate,Statements_Emailed__c 
                      FROM Statement_Email_Request__c
                      WHERE Stmt_Recipient_Contact1__c=:contactId or Stmt_Recipient_Contact2__c=:contactId
                          or Stmt_Recipient_Contact3__c=:contactId or Stmt_Recipient_Contact4__c=:contactId 
                          or Stmt_Recipient_Contact5__c=:contactId or Stmt_Recipient_Contact6__c=:contactId
                          or Stmt_Recipient_Contact7__c=:contactId or Stmt_Recipient_Contact8__c=:contactId
                          or Stmt_Recipient_Contact9__c=:contactId or Stmt_Recipient_Contact10__c=:contactId 
                          order by CreatedDate desc Limit 5 offset 0];
        system.debug('&& statementLst && '+statementLst);
        
        //wrapping and manupulating values 
        for(Statement_Email_Request__c objStementRequest: statementLst){
            system.debug('&& object statement value && '+objStementRequest);
            StatementEmailRequestWrapper objStatmentWrap=new StatementEmailRequestWrapper();
            objStatmentWrap.Id = objStementRequest.id;
            objStatmentWrap.Name = objStementRequest.name;
            List<String> CreatedDateWoTime = (String.valueof(objStementRequest.CreatedDate)).split(' ');
            objStatmentWrap.CreatedDate = CreatedDateWoTime[0];                       
            objStatmentWrap.Statements_Emailed = getEmailDescription(objStementRequest.Statements_Emailed__c);
            StatementEmailRequestWrapperLst.add(objStatmentWrap);
        }
    }
    
    public string getEmailDescription(string EmailData){
            String target_To_Change = 'Statement Type: ';            
            String base_string = EmailData;           
            String replace_string = '';
            String returnString = base_string.replace(target_To_Change , replace_string);
            target_To_Change  = 'Statement Date';
            returnString  = returnString .replace(target_To_Change ,replace_string );
            return returnString;
    }
    //Wrapper for Object Statement_Email_Request__c
    public class StatementEmailRequestWrapper{
        public string Id{set;get;}
        public string Name{set;get;}
        public string CreatedDate{set;get;}
        public string Statements_Emailed{set;get;}
    }
}