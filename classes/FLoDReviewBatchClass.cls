/****************************************************************************************
* Appirio, Inc
* Test Class Name: FLoDReviewBatchClassTest
* Class Name: [FLoDReviewBatchClass]
* Description: [Create as per Story# S-442186 ]
* Created Date: [04/05/2017]
* Created By: [Gaurav Dudani] (Appirio)
* 
* Date Modified                Modified By                  Description of the update
*****************************************************************************************/
//****************************************************************************
// Batch Class declaration.
//****************************************************************************

global class FLoDReviewBatchClass implements Database.Batchable<sObject>,Database.AllowsCallouts, Database.Stateful, IScheduler {
    string KnowledgeArticleId =null;
    string PublishStatus='Online';
    string Language='en_US'; 
    Datetime dt = datetime.now().addhours(-1);
    
    global String query= 'SELECT ID,createdById,KnowledgeArticleId,ArticleType,FLoD_Review__c,Language,PublishStatus,Summary,Title,LastPublishedDate,Regulatory_Content__c FROM External_Link__kav WHERE PublishStatus=:PublishStatus AND Language = :Language AND FLoD_Review__c=true AND createdDate > :dt';
     public FLoDReviewBatchClass(){
        //system.debug('** Default constructor call ***');
        //system.debug('** Query Values in default constructor ***'+query);  
    }
    global FLoDReviewBatchClass(Date providedDate, Id recordId) {
        
    }
    
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
        //system.debug('Query for getting records '+query);
        return Database.getQueryLocator(query);
    }
    global void scheduleClass (Date providedDate, Id recordId) {
    //system.debug('** parameterized constructor call ***');
        KnowledgeArticleId = recordId;
        //system.debug('** KnowledgeArticleId  Values ***'+KnowledgeArticleId );
        if(KnowledgeArticleId !=null){
            query=  'SELECT ID,KnowledgeArticleId,ArticleType,Title,FLoD_Review__c FROM External_Link__kav WHERE PublishStatus=:PublishStatus AND Language = :Language AND FLoD_Review__c=true';
            //system.debug('**Query Values Without Record Id***'+query);
        }
    }
    global void execute(Database.BatchableContext BC, list<sObject> scope){
    Group grp = new Group();
    List<Group> grpList = [SELECT Id FROM Group WHERE Name = 'FLoD Queue' AND Type = 'Queue' LIMIT 1];
    if(grpList.size()>0){
      grp = grpList[0];
    }
    
    //For External_Link__kav
    List<External_Link__kav> ExternalLinkList=[SELECT ID,createdById,KnowledgeArticleId,VersionNumber,FLoD_Review__c,Language,ArticleType,PublishStatus,Summary,Title,LastPublishedDate,Regulatory_Content__c FROM External_Link__kav WHERE PublishStatus='Online' AND Language='en_US' AND FLoD_Review__c=true AND createdDate > :dt];
    List<FLoD_Review__c> FLoD_Review_List_ExternalLink=new List<FLoD_Review__c>();
        for(External_Link__kav obj : ExternalLinkList){
            External_Link__kav External_Link_Obj = (External_Link__kav)obj;
            System.debug('External_Link__kav List:'+External_Link_Obj);
            FLoD_Review__c FLodobj=new FLoD_Review__c();
                FLodobj.Article_Link__c=System.URL.getSalesforceBaseUrl().toExternalForm()+'/'+External_Link_Obj.Id;
                FLodobj.Article_Language__c=External_Link_Obj.Language;
                FLodobj.Article_Publishing_Status__c=External_Link_Obj.PublishStatus;
                FLodobj.Article_Summary__c=External_Link_Obj.Summary;
                FLodobj.Article_Title__c=External_Link_Obj.Title;
                FLodobj.Date_Time_Article_Published__c=External_Link_Obj.LastPublishedDate;
                Flodobj.Regulatory_Content__c=External_Link_Obj.Regulatory_Content__c;
                Flodobj.Creator_s_Name__c = External_Link_Obj.createdById;
                Flodobj.ArticleType__c = External_Link_Obj.ArticleType;
                Flodobj.Master_Article_Id__c= External_Link_Obj.KnowledgeArticleId;
                Flodobj.Version_Number__c= String.Valueof(External_Link_Obj.VersionNumber);
                if(grp != null){
                  Flodobj.ownerId = grp.Id;
                }
                FLoD_Review_List_ExternalLink.add(FLodobj);
            }
            //System.debug('FLoD_Review_List size is '+FLoD_Review_List_ExternalLink.size());
            if(FLoD_Review_List_ExternalLink.size()>0){
              insert FLoD_Review_List_ExternalLink;  
            }
    
    //For FAQ__kav
    List<FAQ__kav> FAQList=[SELECT ID,createdById,KnowledgeArticleId,VersionNumber,FLoD_Review__c,Language,ArticleType,PublishStatus,Summary,Title,LastPublishedDate,Regulatory_Content__c FROM FAQ__kav WHERE PublishStatus='Online' AND Language='en_US' AND FLoD_Review__c=true AND createdDate > :dt];
    List<FLoD_Review__c> FLoD_Review_List_FAQ=new List<FLoD_Review__c>();
        for(FAQ__kav obj : FAQList){
            FAQ__kav FAQ_Obj = (FAQ__kav)obj;
            System.debug('FAQ__kav List:'+FAQ_Obj);
            FLoD_Review__c FLodobj=new FLoD_Review__c();
                FLodobj.Article_Link__c=System.URL.getSalesforceBaseUrl().toExternalForm()+'/'+FAQ_Obj.Id;
                FLodobj.Article_Language__c=FAQ_Obj.Language;
                FLodobj.Article_Publishing_Status__c=FAQ_Obj.PublishStatus;
                FLodobj.Article_Summary__c=FAQ_Obj.Summary;
                FLodobj.Article_Title__c=FAQ_Obj.Title;
                FLodobj.Date_Time_Article_Published__c=FAQ_Obj.LastPublishedDate;
                Flodobj.Regulatory_Content__c=FAQ_Obj.Regulatory_Content__c;
                Flodobj.Creator_s_Name__c = FAQ_Obj.createdById;
                Flodobj.ArticleType__c = FAQ_Obj.ArticleType;
                Flodobj.Master_Article_Id__c= FAQ_Obj.KnowledgeArticleId;
                Flodobj.Version_Number__c= String.Valueof(FAQ_Obj.VersionNumber);
                if(grp != null){
                  Flodobj.ownerId = grp.Id;
                }
                FLoD_Review_List_FAQ.add(FLodobj);
            }
            //System.debug('FLoD_Review_List_FAQ size is '+FLoD_Review_List_FAQ.size());
            if(FLoD_Review_List_FAQ.size()>0){
              insert FLoD_Review_List_FAQ;  
            }
    
    //For General__kav
    List<General__kav> GeneralList=[SELECT ID,createdById,KnowledgeArticleId,VersionNumber,FLoD_Review__c,Language,ArticleType,PublishStatus,Summary,Title,LastPublishedDate,Regulatory_Content__c FROM General__kav WHERE PublishStatus='Online' AND Language ='en_US' AND FLoD_Review__c=true AND createdDate > :dt];
    List<FLoD_Review__c> FLoD_Review_List_General=new List<FLoD_Review__c>();
        for(General__kav obj : GeneralList){
            General__kav General_Obj = (General__kav)obj;
            System.debug('General__kav List:'+General_Obj);
            FLoD_Review__c FLodobj=new FLoD_Review__c();
                FLodobj.Article_Link__c=System.URL.getSalesforceBaseUrl().toExternalForm()+'/'+General_Obj.Id;
                FLodobj.Article_Language__c=General_Obj.Language;
                FLodobj.Article_Publishing_Status__c=General_Obj.PublishStatus;
                FLodobj.Article_Summary__c=General_Obj.Summary;
                FLodobj.Article_Title__c=General_Obj.Title;
                FLodobj.Date_Time_Article_Published__c=General_Obj.LastPublishedDate;
                Flodobj.Regulatory_Content__c=General_Obj.Regulatory_Content__c;
                Flodobj.Creator_s_Name__c = General_Obj.createdById;
                Flodobj.ArticleType__c = General_Obj.ArticleType;
                Flodobj.Master_Article_Id__c= General_Obj.KnowledgeArticleId;
                Flodobj.Version_Number__c= String.Valueof(General_Obj.VersionNumber);
                if(grp != null){
                  Flodobj.ownerId = grp.Id;
                }
                FLoD_Review_List_General.add(FLodobj);
            }
            //System.debug('FLoD_Review_List_General size is '+FLoD_Review_List_General.size());
            if(FLoD_Review_List_General.size()>0){
              insert FLoD_Review_List_General;  
            }

    //For Marketing__kav
    List<Marketing__kav> MarketingList=[SELECT ID,createdById,KnowledgeArticleId,VersionNumber,FLoD_Review__c,Language,ArticleType,PublishStatus,Summary,Title,LastPublishedDate,Regulatory_Content__c FROM Marketing__kav WHERE PublishStatus='Online' AND Language='en_US' AND FLoD_Review__c=true AND createdDate > :dt];
    List<FLoD_Review__c> FLoD_Review_List_Marketing=new List<FLoD_Review__c>();
        for(Marketing__kav obj : MarketingList){
            Marketing__kav Marketing_Obj = (Marketing__kav)obj;
            System.debug('Marketing__kav List:'+Marketing_Obj);
            FLoD_Review__c FLodobj=new FLoD_Review__c();
                FLodobj.Article_Link__c=System.URL.getSalesforceBaseUrl().toExternalForm()+'/'+Marketing_Obj.Id;
                FLodobj.Article_Language__c=Marketing_Obj.Language;
                FLodobj.Article_Publishing_Status__c=Marketing_Obj.PublishStatus;
                FLodobj.Article_Summary__c=Marketing_Obj.Summary;
                FLodobj.Article_Title__c=Marketing_Obj.Title;
                FLodobj.Date_Time_Article_Published__c=Marketing_Obj.LastPublishedDate;
                Flodobj.Regulatory_Content__c=Marketing_Obj.Regulatory_Content__c;
                Flodobj.Creator_s_Name__c = Marketing_Obj.createdById;
                Flodobj.ArticleType__c = Marketing_Obj.ArticleType;
                Flodobj.Master_Article_Id__c= Marketing_Obj.KnowledgeArticleId;
                Flodobj.Version_Number__c= String.Valueof(Marketing_Obj.VersionNumber);
                if(grp != null){
                  Flodobj.ownerId = grp.Id;
                }
                FLoD_Review_List_Marketing.add(FLodobj);
            }
            //System.debug('FLoD_Review_List_Marketing size is '+FLoD_Review_List_Marketing.size());
            if(FLoD_Review_List_Marketing.size()>0){
              insert FLoD_Review_List_Marketing;  
            }
            
    //For Policy__kav
    List<Policy__kav> PolicyList=[SELECT ID,createdById,KnowledgeArticleId,VersionNumber,FLoD_Review__c,Language,ArticleType,PublishStatus,Summary,Title,LastPublishedDate,Regulatory_Content__c FROM Policy__kav WHERE PublishStatus='Online' AND Language='en_US' AND FLoD_Review__c=true AND createdDate > :dt];
    List<FLoD_Review__c> FLoD_Review_List_Policy=new List<FLoD_Review__c>();
        for(Policy__kav obj : PolicyList){
            Policy__kav Policy_Obj = (Policy__kav)obj;
            System.debug('Policy__kav List:'+Policy_Obj);
            FLoD_Review__c FLodobj=new FLoD_Review__c();
                FLodobj.Article_Link__c=System.URL.getSalesforceBaseUrl().toExternalForm()+'/'+Policy_Obj.Id;
                FLodobj.Article_Language__c=Policy_Obj.Language;
                FLodobj.Article_Publishing_Status__c=Policy_Obj.PublishStatus;
                FLodobj.Article_Summary__c=Policy_Obj.Summary;
                FLodobj.Article_Title__c=Policy_Obj.Title;
                FLodobj.Date_Time_Article_Published__c=Policy_Obj.LastPublishedDate;
                Flodobj.Regulatory_Content__c=Policy_Obj.Regulatory_Content__c;
                Flodobj.Creator_s_Name__c = Policy_Obj.createdById;
                Flodobj.ArticleType__c = Policy_Obj.ArticleType;
                Flodobj.Master_Article_Id__c= Policy_Obj.KnowledgeArticleId;
                Flodobj.Version_Number__c= String.Valueof(Policy_Obj.VersionNumber);
                if(grp != null){
                  Flodobj.ownerId = grp.Id;
                }
                FLoD_Review_List_Policy.add(FLodobj);
            }
            //System.debug('FLoD_Review_List_Policy size is '+FLoD_Review_List_Policy.size());
            if(FLoD_Review_List_Policy.size()>0){
              insert FLoD_Review_List_Policy;  
            }

    //For Procedure__kav
    List<Procedure__kav> ProcedureList=[SELECT ID,createdById,KnowledgeArticleId,VersionNumber,FLoD_Review__c,Language,ArticleType,PublishStatus,Summary,Title,LastPublishedDate,Regulatory_Content__c FROM Procedure__kav WHERE PublishStatus='Online' AND Language='en_US' AND FLoD_Review__c=true AND createdDate > :dt];
    List<FLoD_Review__c> FLoD_Review_List_Procedure=new List<FLoD_Review__c>();
        for(Procedure__kav obj : ProcedureList){
            Procedure__kav Procedure_Obj = (Procedure__kav)obj;
            System.debug('Procedure__kav List:'+Procedure_Obj);
            FLoD_Review__c FLodobj=new FLoD_Review__c();
                FLodobj.Article_Link__c=System.URL.getSalesforceBaseUrl().toExternalForm()+'/'+Procedure_Obj.Id;
                FLodobj.Article_Language__c=Procedure_Obj.Language;
                FLodobj.Article_Publishing_Status__c=Procedure_Obj.PublishStatus;
                FLodobj.Article_Summary__c=Procedure_Obj.Summary;
                FLodobj.Article_Title__c=Procedure_Obj.Title;
                FLodobj.Date_Time_Article_Published__c=Procedure_Obj.LastPublishedDate;
                Flodobj.Regulatory_Content__c=Procedure_Obj.Regulatory_Content__c;
                Flodobj.Creator_s_Name__c = Procedure_Obj.createdById;
                Flodobj.ArticleType__c = Procedure_Obj.ArticleType;
                Flodobj.Master_Article_Id__c= Procedure_Obj.KnowledgeArticleId;
                Flodobj.Version_Number__c= String.Valueof(Procedure_Obj.VersionNumber);
                if(grp != null){
                  Flodobj.ownerId = grp.Id;
                }
                FLoD_Review_List_Procedure.add(FLodobj);
            }
            //System.debug('FLoD_Review_List_Procedure size is '+FLoD_Review_List_Procedure.size());
            if(FLoD_Review_List_Procedure.size()>0){
              insert FLoD_Review_List_Procedure;  
            }

    //For Training__kav
    List<Training__kav> TrainingList=[SELECT ID,createdById,KnowledgeArticleId,VersionNumber,FLoD_Review__c,Language,ArticleType,PublishStatus,Summary,Title,LastPublishedDate,Regulatory_Content__c FROM Training__kav WHERE PublishStatus='Online' AND Language='en_US' AND FLoD_Review__c=true AND createdDate > :dt];
    List<FLoD_Review__c> FLoD_Review_List_Training=new List<FLoD_Review__c>();
        for(Training__kav obj : TrainingList){
            Training__kav Training_Obj = (Training__kav)obj;
            System.debug('Training__kav List:'+Training_Obj);
            FLoD_Review__c FLodobj=new FLoD_Review__c();
                FLodobj.Article_Link__c=System.URL.getSalesforceBaseUrl().toExternalForm()+'/'+Training_Obj.Id;
                FLodobj.Article_Language__c=Training_Obj.Language;
                FLodobj.Article_Publishing_Status__c=Training_Obj.PublishStatus;
                FLodobj.Article_Summary__c=Training_Obj.Summary;
                FLodobj.Article_Title__c=Training_Obj.Title;
                FLodobj.Date_Time_Article_Published__c=Training_Obj.LastPublishedDate;
                Flodobj.Regulatory_Content__c=Training_Obj.Regulatory_Content__c;
                Flodobj.Creator_s_Name__c = Training_Obj.createdById;
                Flodobj.ArticleType__c = Training_Obj.ArticleType;
                Flodobj.Master_Article_Id__c= Training_Obj.KnowledgeArticleId;
                Flodobj.Version_Number__c= String.Valueof(Training_Obj.VersionNumber);
                if(grp != null){
                  Flodobj.ownerId = grp.Id;
                }
                FLoD_Review_List_Training.add(FLodobj);
            }
            //System.debug('FLoD_Review_List_Training size is '+FLoD_Review_List_Training.size());
            if(FLoD_Review_List_Training.size()>0){
              insert FLoD_Review_List_Training;  
            }
    
    //For Troubleshooting__kav
    List<Troubleshooting__kav> TroubleshootingList=[SELECT ID,createdById,KnowledgeArticleId,VersionNumber,FLoD_Review__c,Language,ArticleType,PublishStatus,Summary,Title,LastPublishedDate,Regulatory_Content__c FROM Troubleshooting__kav WHERE PublishStatus='Online' AND Language='en_US' AND FLoD_Review__c=true AND createdDate > :dt];
    List<FLoD_Review__c> FLoD_Review_List_Troubleshooting=new List<FLoD_Review__c>();
        for(Troubleshooting__kav obj : TroubleshootingList){
            Troubleshooting__kav Troubleshooting_Obj = (Troubleshooting__kav)obj;
            System.debug('Troubleshooting__kav List:'+Troubleshooting_Obj);
            FLoD_Review__c FLodobj=new FLoD_Review__c();
                FLodobj.Article_Link__c=System.URL.getSalesforceBaseUrl().toExternalForm()+'/'+Troubleshooting_Obj.Id;
                FLodobj.Article_Language__c=Troubleshooting_Obj.Language;
                FLodobj.Article_Publishing_Status__c=Troubleshooting_Obj.PublishStatus;
                FLodobj.Article_Summary__c=Troubleshooting_Obj.Summary;
                FLodobj.Article_Title__c=Troubleshooting_Obj.Title;
                FLodobj.Date_Time_Article_Published__c=Troubleshooting_Obj.LastPublishedDate;
                Flodobj.Regulatory_Content__c=Troubleshooting_Obj.Regulatory_Content__c;
                Flodobj.Creator_s_Name__c = Troubleshooting_Obj.createdById;
                Flodobj.ArticleType__c = Troubleshooting_Obj.ArticleType;
                Flodobj.Master_Article_Id__c= Troubleshooting_Obj.KnowledgeArticleId;
                Flodobj.Version_Number__c= String.Valueof(Troubleshooting_Obj.VersionNumber);
                if(grp != null){
                  Flodobj.ownerId = grp.Id;
                }
                FLoD_Review_List_Troubleshooting.add(FLodobj);
            }
            //System.debug('FLoD_Review_List_Troubleshooting size is '+FLoD_Review_List_Troubleshooting.size());
            if(FLoD_Review_List_Troubleshooting.size()>0){
              insert FLoD_Review_List_Troubleshooting;  
            }
    }
    global void finish(Database.BatchableContext BC) {
        system.debug('finish Method Calling');
        
    }
}