/*********************************************************************
* Appirio a Wipro Company 
* Name: ComplaintTriggerHandler
* Description: Populate queue as ownerID based on user DB Region T-601115 / S-470559
* Created Date: 05-11-2017
* Created By: Gaurav Dudani
* 
* Date Modified                Modified By                  Description of the update
* [16 May 2017]                Aashish Sajwan                [Populate custom field to show in Report T-601582 / S-470712]
* [15 Jun 2017]                Aashish Sajwan                [I-279578 Fix remove null value error in complaint partner account ]
**********************************************************************/
public class ComplaintTriggerHandler {
    public static void beforeInsert(List<Complaint__c> newComplaintList) {
        Group NAQueue = [SELECT Id FROM Group WHERE DeveloperName = 'NA_Complaints' AND Type = 'Queue' LIMIT 1];
        Group MXQueue = [SELECT Id FROM Group WHERE DeveloperName = 'MX_Complaint_Work_Item_Queue' AND Type = 'Queue' LIMIT 1];
        User currentUser = [SELECT Id, DB_Region__c 
                            FROM User 
                            WHERE Id = :UserInfo.getUserId()];
        for (Complaint__c complaint : newComplaintList) {
            if (currentUser.DB_Region__c == 'NA') {
                complaint.OwnerId = NAQueue.Id;
            } else if (currentUser.DB_Region__c == 'LA') {
                complaint.OwnerId = MXQueue.Id;
            }
        }
    }
    //****************************************************************************
    // Method to populate Relationship_Manager_First_Name__c and Relationship_Manager_Last_Name__c of Complaint object
    // @param newComplaint: List of Complaint
    // @return void
    // 12 May 2017 Poornima Bhardwaj T-601582
    //****************************************************************************
    
    public static void updateAccoutFields(List<Complaint__c> newComplaint){
        set<Id> caseId=new set<Id>();
        Map<Id,List<String>> accountMap = new Map<Id,List<String>>();
        map<Id,Case> caseMap=new map<Id,case>();
        for(Complaint__c cl:newComplaint){
            if(cl.case__c!=null){
                caseId.add(cl.case__c);
            }
        } 
        
        for(Case caseList:[Select AccountId, Account.Relationship__c,Account.Relationship__r.Owner.FirstName,
                           Account.Relationship__r.Owner.LastName from Case where id IN:caseId
                           AND AccountId!= null AND Account.Relationship__c != null]){
                               caseMap.put(caseList.Id,caseList);
                           }
        for(Complaint__c cm:newComplaint){
            if(cm.Case__c != null && caseMap.containsKey(cm.Case__c)) {
                cm.Relationship_Manager_First_Name__c = caseMap.get(cm.case__c).Account.Relationship__r.Owner.FirstName;
                cm.Relationship_Manager_Last_Name__c = caseMap.get(cm.case__c).Account.Relationship__r.Owner.LastName;
                accountMap.put(caseMap.get(cm.case__c).AccountId,new list<String>());
            }
        }
        system.debug('&& account Map Value KeySet &&'+ accountMap.keySet());
        if(accountMap.size()>0){
            List<Partner> partnerList = [SELECT AccountTo.Name,AccountFromId  
                                         FROM Partner
                                         WHERE AccountFromId = :accountMap.KeySet()];
            for(Partner objPart: partnerList){
                if(accountMap.containsKey(objPart.AccountFromId)){
                    accountMap.get(objPart.AccountFromId).add(objPart.AccountTo.Name);
                }
                
            }
            system.debug('&& account Map Value KeySet2 &&'+ accountMap.keySet());
            system.debug('&& account Map Value values &&'+ accountMap.values());
            //15 Jun 2017 Fix for I-279578 Aashish Sajwan  
            string partnerAccountValue = '';
            for(Complaint__c cm:newComplaint){
                if(cm.Case__c != null && accountMap.containsKey(caseMap.get(cm.case__c).AccountId)) {
                    //15 Jun 2017 Fix for I-279578  assign  partnerAccountValue  blank  - Aashish Sajwan 
                    partnerAccountValue ='';
                    for(String accntName: accountMap.get(caseMap.get(cm.case__c).AccountId)){
                        //15 Jun 2017 Fix for I-279578  assing value in partnerAccountValue   -Aashish Sajwan 
                        partnerAccountValue  = (!string.isBlank(cm.Partner_Account__c)) ? cm.Partner_Account__c + accntName +',' : accntName +',' ;
                    }
                    //15 Jun 2017 Fix for assign partnerAccountValue value to complaint Partner Account I-279578 Aashish Sajwan 
                    cm.Partner_Account__c = partnerAccountValue.contains(',') ? partnerAccountValue.removeEnd(','): partnerAccountValue  ;
                    system.debug('&& cm.Partner_Account__c Values &&'+ cm.Partner_Account__c);
                }
            }     
        } 
    }
}