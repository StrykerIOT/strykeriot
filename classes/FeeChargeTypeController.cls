/*********************************************************************
* Appirio, Inc
* Name: FeeChargeTypeController
* Description: [T-607016-Controller for FeeChargeType Component]
* Created Date: [02/06/2017]
* Created By: [Poornima Bhardwaj] (Appirio)
* 
* Date Modified                Modified By                  Description of the update

**********************************************************************/
public without sharing class FeeChargeTypeController { 
    public Id feeChargeId;
    public String userLang;
    public Reference__c  referenceVal;
    public String descriptionValue{get;set;}
    public String feeChargeExternalId;
    public FeeChargeTypeController(ApexPages.StandardController controller) {
        //Current page Id
        feeChargeId=ApexPages.currentPage().getParameters().get('id');
        //External_ID of Fee Charge Record
        feeChargeExternalId=[Select External_ID__c from Fee_Charge__c where id=:feeChargeId].External_ID__c ;
        String userLanguage=UserInfo.getLanguage();
        //Fetching reference record when Category__c='Charge Type' and External_ID__c equals to Fee Charge External Id 
        List<Reference__c> referenceValList= [Select Id,External_ID__c, Description__c,Language_Code__c From Reference__c 
                                              Where   Category__c='Charge Type' AND External_ID__c=:feeChargeExternalId
                                              AND Language_Code__c=:userLanguage];                                        
        if(referenceValList!=null && referenceValList.size()>0 &&  referenceValList.get(0)!=null){
            referenceVal=referenceValList.get(0);
            descriptionValue=referenceVal.Description__c;
        }
        else{
            //Setting descriptionValue null
            descriptionValue='';
        }
    }
     
}