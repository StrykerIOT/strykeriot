/*********************************************************************
 * Appirio, Inc
 * Name: OasisOpenOrgWssSecuritySecext
 * Description: [#T-557407]
 * Created Date: [12/5/2016]
 * Created By: [Aashish Sajwan] (Appirio)
 * 
 * Date Modified                Modified By                  Description of the update
 * [Tue 27, 2016]                   [Aashish Sajwan]         [#T-557407]
 * [Sun 19-Feb, 2017]               [Aashish Sajwan]         [#I-261128]
 **********************************************************************/
 public class OasisOpenOrgWssSecuritySecext {

    // UserToken Class
    public class UsernameToken 
    {
        // Constructor for UsernameToken used to pass in username and password parameters
        public UsernameToken(String username, String password)
        {
            this.Username = username;
            this.Password = password;
            this.Nonce = generateNounce();
            this.Created = generateTimestamp();
            
            system.debug('@@ User Name @@'+this.Username);
            system.debug('@@ Password  Value @@'+this.Password );
            system.debug('@@ Created Value @@'+this.Created );
        }

        public String Username;
        public String Password;
        public String Nonce;
        public String Created;
        private String[] Username_type_info = new String[]{'Username','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] Password_type_info = new String[]{'Password','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] Nonce_type_info = new String[]{'Nonce','http://www.w3.org/2001/XMLSchema','string','0','1','false'};
        private String[] Created_type_info = new String[]{'wsu:Created','http://www.w3.org/2001/XMLSchema','string','0','1','false'};        
        private String[] apex_schema_type_info = new String[]{'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd','true','false'};
        private String[] field_order_type_info = new String[]{'Username','Password','Nonce','Created'};

        // Generate Nounce, random number base64 encoded
        public String generateNounce()
        {
            Long randomLong = Crypto.getRandomLong();
            return EncodingUtil.base64Encode(Blob.valueOf(String.valueOf(randomLong)));
        }

        // Generate timestamp in GMT
        public String generateTimestamp()
        {
            return Datetime.now().formatGmt('yyyy-MM-dd\'T\'hh:mm:ss\'Z\'');
        }
    }

    // SecurityHeaderType Class
    public class SecurityHeaderType 
    {       
        // Constructor for SecurityHeaderType used to pass in username and password parameters and instantiate the UsernameToken object     
        //public SecurityHeaderType(String UserName , string password)
        public SecurityHeaderType()
        {
            //19-feb-2017 Aashish Sajwan - Fix #I-261128
            
            Oasis_Open_Secuity_Header__c objSecuityHeaderSetting =  Oasis_Open_Secuity_Header__c.getValues('SecurityHeader');
            String UserName = objSecuityHeaderSetting!=null? objSecuityHeaderSetting.User_Name__c : ''; 
            String password= objSecuityHeaderSetting!=null? objSecuityHeaderSetting.Password__c : '';
            system.debug('&& UserName Value &&'+UserName );
            system.debug('&& Pwd Value  &&'+password);
            
            this.UsernameToken = new OasisOpenOrgWssSecuritySecext.UsernameToken(UserName , password);
            //End
        }

        public String wsuNamespace = 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd';              
        public OasisOpenOrgWssSecuritySecext.UsernameToken UsernameToken;
        private String[] UsernameToken_type_info = new String[]{'UsernameToken','http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd','UsernameToken','1','1','false'};
        private String[] wsuNamespace_att_info = new String[]{'xmlns:wsu'};               
        private String[] apex_schema_type_info = new String[]{'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd','true','false'};
        private String[] field_order_type_info = new String[]{'UsernameToken'};
    }
 }