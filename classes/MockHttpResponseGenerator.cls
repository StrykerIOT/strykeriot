@isTest
global class MockHttpResponseGenerator implements HttpCalloutMock 
{   
    string apiCall;
    public MockHttpResponseGenerator(string apiVal){
    apiCall=apiVal;
    }
    global HTTPResponse respond(HTTPRequest req) 
    {
        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res = new HTTPResponse();
            
        if(apiCall=='CollectionDetails'){
                //Response for CollectionDetails call
                res.setBody('{"@odata.context": "$metadata#details","value": [{"mid": "9068362","clientGroup": null,"originalDate": "2013-04-02","incomingAmount": 55.47,"balanceDueAmount": 55.47,"itemType": "BOULDER","description": "/BOULDER"}]}');
            }else if(apiCall=='WriteOffDetails'){
                //Response for WriteOffDetails call
                res.setBody('{"@odata.context": "$metadata#detail","value": [{"mid": "8026984123","caseDate": "2014-08-11","caseType": "RIS","dba": "PIZZA QUATRE FRERES","caseAmount": 237.93,"balance": 0,"status": "RESUB-DDA","lastDepositDate": "2014-06-20","settDate": "2014-08-11","effDate": "08/08/2014","tranCode": "DB"},{ "mid": "8026984123","caseDate": "2014-08-13","caseType": "RIS","dba": "PIZZA QUATRE FRERES","caseAmount": 237.93,"balance": 0,"status": "PAIDINFULL","lastDepositDate": "2014-06-20","settDate": "2014-08-13","effDate": "08/12/2014","tranCode": "DB"}]}'); 
            }else if(apiCall=='CollectionTotal'){
                //Response for CollectionTotal call
                res.setBody('{"@odata.context": "$metadata#detail","value": [{"mid": "8026984123","caseDate": "2014-08-11","caseType": "RIS","dba": "PIZZA QUATRE FRERES","caseAmount": 237.93,"balance": 0,"status": "RESUB-DDA","lastDepositDate": "2014-06-20","settDate": "2014-08-11","effDate": "08/08/2014","tranCode": "DB","totBalanceDueAmount":10},{ "mid": "8026984123","caseDate": "2014-08-13","caseType": "RIS","dba": "PIZZA QUATRE FRERES","caseAmount": 237.93,"balance": 0,"status": "PAIDINFULL","lastDepositDate": "2014-06-20","settDate": "2014-08-13","effDate": "08/12/2014","tranCode": "DB","totBalanceDueAmount":10}]}'); 
            }else if(apiCall=='WriteoffSummary'){
                //Response for WriteoffSummary call
                res.setBody('{"@odata.context": "$metadata#summary","value":[{"mid": "8026984123","caseType": "RIS","dbaName": "PIZZA QUATRE FRERES","balance": 0,"lastRejectDate": "2014-09-03","status": "PAIDINFULL","oldBalance": 608.37,"oldLastRejectDate": "2014-09-03","oldStatus": "RECOVERY"}]}'); 
            }else if(apiCall=='MXAccuity'){
                //Response for MXAccuity call
                res.setBody('{"@odata.context": "$metadata#financialInstitution","@odata.count": 1,"value": [   {   "CountryISOCode": "MX",   "NationalBankCode": "00320146",   "FormattedBankCode": null,   "NationalBankCodeType": "NIB",   "NationalBankCodeFlag": "Y",   "FinancialInstType": "01",   "FinancialInstDesc": "Commercial Bank",   "FinancialInstName": "Bkinter",   "FinancialInstFullName": "Bankinter, S.A.",   "BranchName": "Republica",   "OfficeTypeCode": "06",   "OfficeTypeName": "Branch",   "BranchAddrLine1": "Av da Republica, 50 A",   "BranchAddrLine2": null,   "BranchCity": "LISBON",   "BranchStateCode": null,   "BranchStateName": null,   "BranchPostalCode": "1050 196",   "BranchCountry": null,   "BranchPhone": "(351 21) 7911 285",   "BranchFax": null,   "SwiftBicCode": "BARCPTPCXXX",   "SwiftBicRefFlag": "N"}]}'); 
            }else if(apiCall=='NAAccuity'){
                //Response for NAAccuity call
                res.setBody('{"@odata.context": "$metadata#financialInstitution","@odata.count": 1,"value": [   {   "CountryISOCode": "IN",   "NationalBankCode": "SBIN0008157",   "FormattedBankCode": null,   "NationalBankCodeType": "IFSC",   "NationalBankCodeFlag": "Y",   "FinancialInstType": "01",   "FinancialInstDesc": "Commercial Bank",   "FinancialInstName": "State Bk of India",   "FinancialInstFullName": "State Bank of India",   "BranchName": "Sahugarh",   "OfficeTypeCode": "06",   "OfficeTypeName": "Branch",   "BranchAddrLine1": "Dist : Madhipura State: Bihar",   "BranchAddrLine2": null,   "BranchCity": "MADHEPURA",   "BranchStateCode": null,   "BranchStateName": null,   "BranchPostalCode": null,   "BranchCountry": null,   "BranchPhone": null,   "BranchFax": null,   "SwiftBicCode": "SBININBB521",   "SwiftBicRefFlag": "N"}]}'); 
            }
            else{
               //Response for RefreshDateStamps call
               res.setBody('{"@odata.context": "$metadata#total","value": [{"mid": "30081368","clientGroup": null,"totIncomingAmount": 479.7,"totBalanceDueAmount": 10}]}');  
            } 
            
        res.setStatus('OK');
        //Setting status code as 200
        res.setStatusCode(200);
        return res;
    }
}