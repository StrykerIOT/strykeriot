/***********************************************************************************************
* Appirio, Inc
* Name: AssetTriggerHandler
* Description: [T-585639 -HandlerClass - Updating Multi-Mid of Asset Assuming that user is not changing asset account and
Processing center Id of acccount]
* Created Date: [17/03/2017]
* Created By: [Neeraj Kumawat] (Appirio)
* 
* Date Modified                Modified By                  Description of the update
[20/03/2017]                 Neeraj Kumawat                [T-585639 Added Common method Update Asset]
[24/03/2017]                 Neeraj Kumawat                [T-585639 Handling scenario to uncheck Multi-Mid]
[02/06/2017]                 Bobby Cheek                   [T-600206 Enforcing CRUD and FLS Security in the Apex Class, AssetTriggerHandler.cls]
*************************************************************************************************/
public class AssetTriggerHandler {
    //Random number to stop trigger recursion and to fulfill upsert functionality
    public static Double randomNumber=Math.random();
    //****************************************************************************
    // Method for calling action on After Insert or Update
    // @param newAssetList: List of Asset
    //@param assetOldMap: Map of Old Asset
    // @return void
    //****************************************************************************
    public static void onAfterInsertUpdate(List<Asset> newAssetList, Map<Id,Asset> assetOldMap){
        updateAssetMultiMid(newAssetList,assetOldMap);
    }
    //****************************************************************************
    // Method to Update Multi Mid for Account
    // @param newAssetList: List of Asset
    //@param assetOldMap: Map of Old Asset
    // @return void
    //****************************************************************************
    private static void  updateAssetMultiMid(List<Asset> newAssetList,Map<Id,Asset> assetOldMap){
        List<Asset> assetList =[Select Id, Account.Processing_Center_ID__c,AccountId,Terminal_Management_Number_TMS__c,
                                Merchant_Equipment_Status_Code__c,Equipment_Status_Code__c,Legacy_Equip_ID__c,Multi_MID__c
                                From Asset 
                                Where Id IN:newAssetList];
        List<Asset> assetWithMultiMidChecked=new List<Asset>();
        //Set of teminal numbers
        Set<String> terminalNumberSet=new Set<String>();
        //Set of Legacy Equip Ids
        Set<String> LegacyEquipIdSet=new Set<String>();
        for(Asset asst:assetList){
              if((asst.accountId==null && asst.Multi_MID__c) || (asst.Account.Processing_Center_ID__c!='NA' 
                 && asst.Account.Processing_Center_ID__c!='QUERE' && asst.Multi_MID__c)){
                     //When Multi MID is true and Account Id is null or account is not NA and MX 
                     if(Schema.sObjectType.Asset.fields.Multi_MID__c.isUpdateable())
                     asst.Multi_MID__c=false;
                     if(Schema.sObjectType.Asset.fields.IsAssetUpdate__c.isUpdateable())
                     asst.IsAssetUpdate__c=randomNumber;
                     assetWithMultiMidChecked.add(asst);
              }else if(asst.Account.Processing_Center_ID__c=='QUERE' && asst.Multi_MID__c
                       && (asst.Merchant_Equipment_Status_Code__c!='A' || asst.Equipment_Status_Code__c!='A'
                           ||asst.Legacy_Equip_ID__c==null )){
                           //When Account is MX and Multi Mid true and 
                           //Merchant Equip Status is not A or Equip status code id not A or Legacy Equip Id is null 
                           if(Schema.sObjectType.Asset.fields.Multi_MID__c.isUpdateable())
                           asst.Multi_MID__c=false;
                           if(Schema.sObjectType.Asset.fields.IsAssetUpdate__c.isUpdateable())
                           asst.IsAssetUpdate__c=randomNumber;
                           assetWithMultiMidChecked.add(asst);
               }else if(asst.Account.Processing_Center_ID__c=='NA' && asst.Multi_MID__c 
                        && asst.Terminal_Management_Number_TMS__c==null ){
                           //When Account is NA and Multi Mid true and 
                           //Terminal System Number Id is null 
                           if(Schema.sObjectType.Asset.fields.Multi_MID__c.isUpdateable())
                            asst.Multi_MID__c=false;
                            if(Schema.sObjectType.Asset.fields.IsAssetUpdate__c.isUpdateable())
                            asst.IsAssetUpdate__c=randomNumber;
                            assetWithMultiMidChecked.add(asst);
               }else if(asst.accountId!=null && asst.Account.Processing_Center_ID__c=='NA' 
                        && asst.Terminal_Management_Number_TMS__c!=null){
                            //When Account is NA and Terminal System Number Id is not null 
                            terminalNumberSet.add(asst.Terminal_Management_Number_TMS__c);
               }else if(asst.accountId!=null && asst.Legacy_Equip_ID__c!=null && asst.Account.Processing_Center_ID__c=='QUERE' 
                        && asst.Merchant_Equipment_Status_Code__c=='A' && asst.Equipment_Status_Code__c=='A'){
                            //When Account is MX and Merchant Equip Status is  A and Equip status code id  A or Legacy Equip Id
                            // is not null
                            LegacyEquipIdSet.add(asst.Legacy_Equip_ID__c);
              }
            //When Asset is updated
            if(assetOldMap!=null && assetOldMap.size()>0){
                String oldAssetTerminalNumber=assetOldMap.get(asst.Id).Terminal_Management_Number_TMS__c;
                String oldAssetLegacyEquipId=assetOldMap.get(asst.Id).Legacy_Equip_ID__c;
                String oldAssetMerchantEquipStatusCode=assetOldMap.get(asst.Id).Merchant_Equipment_Status_Code__c;
                String oldAssetEquipStatusCode=assetOldMap.get(asst.Id).Equipment_Status_Code__c;
                if(oldAssetTerminalNumber!=null && asst.Terminal_Management_Number_TMS__c!=oldAssetTerminalNumber){
                    //When asse terminal number is updated 
                    terminalNumberSet.add(oldAssetTerminalNumber);
                }
                if((oldAssetLegacyEquipId!=null && asst.Legacy_Equip_ID__c!=oldAssetLegacyEquipId) 
                   || asst.Merchant_Equipment_Status_Code__c!=oldAssetMerchantEquipStatusCode
                   || asst.Equipment_Status_Code__c!=oldAssetEquipStatusCode){
                    //When asse Legacy Equip Id or Merchant Quipment code or Equipment status code is updated
                       LegacyEquipIdSet.add(oldAssetLegacyEquipId);
                   }
            }
        }
        //update asses to unchecked Multi Mid
        update assetWithMultiMidChecked; 
        //calling updateAssetMultiMidForNA method
        updateAssetMultiMidForNA(terminalNumberSet);
        //calling updateAssetMultiMidForMX method
        updateAssetMultiMidForMX(LegacyEquipIdSet);
    }
    //****************************************************************************
    // Method to Update Multi Mid for Account which has Processing Center Id NA
    // @param terminalNumberSet: Set of Terminal Number
    // @return void
    //****************************************************************************
    private static void updateAssetMultiMidForNA(Set<String> terminalNumberSet){
        if(terminalNumberSet.size()>0){
            //Fetching Assets records whihc have same terminal management number and account processing id is NA
            List<Asset> assetListSameTeminalNumList=[Select Id, Terminal_Management_Number_TMS__c 
                                                     From Asset 
                                                     Where Terminal_Management_Number_TMS__c IN:terminalNumberSet
                                                     AND Account.Processing_Center_ID__c='NA'];
            //Map of Asset list
            Map<String,List<Asset>> assetListMap= new Map<String,List<Asset>>();
            //Getting Assets Records basesd on Terminal Management Number 
            for(Asset asst :assetListSameTeminalNumList){
                if(assetListMap.containsKey(asst.Terminal_Management_Number_TMS__c)){
                    //when terminal number is already there in map than add asset to the assest list
                    assetListMap.get(asst.Terminal_Management_Number_TMS__c).add(asst);
                }else{
                    //when terminal number is not there in map than create list and add asset to it
                    assetListMap.put(asst.Terminal_Management_Number_TMS__c,new List<Asset>{asst});
                }
            }
            //Calling Update Assest Method to update assest
            updateAsset(assetListMap);
        }
    }
    //****************************************************************************
    // Method to Update Multi Mid for Account which has Processing Center Id QUERE(MX)
    // @param LegacyEquipIdSet: Set of Legacy Equip Id
    // @return void
    //****************************************************************************
    private static void updateAssetMultiMidForMX(Set<String> LegacyEquipIdSet){
        if(LegacyEquipIdSet.size()>0){
            //Fetching Assets records whihc have same terminal management number and account processing id is NA
            List<Asset> assetListSameLegacyIdList=[Select Id,Merchant_Equipment_Status_Code__c,Equipment_Status_Code__c,
                                                   Legacy_Equip_ID__c
                                                   From Asset 
                                                   Where Legacy_Equip_ID__c IN:LegacyEquipIdSet
                                                   AND Account.Processing_Center_ID__c='QUERE' 
                                                   AND Merchant_Equipment_Status_Code__c='A'
                                                   AND Equipment_Status_Code__c='A'];
            //Map of Asset list
            Map<String,List<Asset>> assetListMap= new Map<String,List<Asset>>();
            //Getting Assets Records basesd on Terminal Management Number 
            for(Asset asst :assetListSameLegacyIdList){
                if(assetListMap.containsKey(asst.Legacy_Equip_ID__c)){
                    //when Legacy Equip Id is already there in map than add asset to the assest list
                    assetListMap.get(asst.Legacy_Equip_ID__c).add(asst);
                }else{
                    //when Legacy Equip Id is not there in map than create list and add asset to it
                    assetListMap.put(asst.Legacy_Equip_ID__c,new List<Asset>{asst});
                }
            }
            //Calling Update Assest Method to update assest
            updateAsset(assetListMap);
        }
    }
    //****************************************************************************
    // Method to fetch asset from Assest List Map and Update Asset. 
    // @param assetListMap: Map of List of Asset
    // @return void
    //****************************************************************************
    private static void updateAsset(Map<String,List<Asset>> assetListMap){
        List<Asset> asstList=new List<Asset>();
        for(List<Asset> assestList : assetListMap.values()){
            //Updating Multi-Mid when Asset count having same Legacy Equip Id is greater than one
            if(assestList!=null && assestList.size()>1){
                for(Asset ast: assestList){
                	if(Schema.sObjectType.Asset.fields.Multi_MID__c.isUpdateable())
                    ast.Multi_MID__c=true;                    
                    if(Schema.sObjectType.Asset.fields.IsAssetUpdate__c.isUpdateable())
                    ast.IsAssetUpdate__c=randomNumber;
                    asstList.add(ast);
                }
            }else{
            	if(Schema.sObjectType.Asset.fields.Multi_MID__c.isUpdateable())
                assestList[0].Multi_MID__c=false;
                if(Schema.sObjectType.Asset.fields.IsAssetUpdate__c.isUpdateable())
                assestList[0].IsAssetUpdate__c=randomNumber;
                asstList.add(assestList[0]);
            }
        }
        //Updating asset List
        update asstList;
    }
    
}