/**********************************************************************
* Appirio, Inc
* Test Class Name: EventTriggerHandlerTest
* Class Name: [EventTriggerHandler]
* Description: [T-565229 Test Class for EventTriggerHandler]
* Created Date: [01/04/2017]
* Created By: [Gaurav Dudani] (Appirio)
* 
* Date Modified                Modified By                  Description of the update
***********************************************************************/

@isTest
public class EventTriggerHandlerTest{
    /***************************************************************************************
// Test method to validate future action date is not null or today or past date 
// and case status is Awaiting Information.
****************************************************************************************/
    
    static testMethod void updateCaseStatusTestMethod(){
        String profileId = [select id from Profile where name ='Custom: NA Agent' limit 1].Id;
        User usr = TestUtilities.createTestUser(1,profileId,true);
        System.runAs(usr){
            List<Case> caseList = new List<Case>();
            for(Case cse: TestUtilities.createCase(1,null,false)){
                cse.Future_Action_Date__c = System.Today()+2;
                cse.status = 'New';
                cse.subject = 'test subject';
                cse.origin= 'Email';
                caseList.add(cse);
            }
            insert caseList;
            List<Event> evtList = new List<Event>();
            for(Event evt: TestUtilities.createEvent(1,false)){
                evt.Subject ='Test';
                evt.whatId = caseList[0].Id;
                evt.Case_Status__c = 'Awaiting Information';
                evt.StartDateTime=System.Today();
                evt.EndDateTime=System.Today()+2;
                evt.CurrencyIsoCode='USD';
                evt.Type='Email';
                evt.ActivityDateTime=System.Today();
                evtList.add(evt); 
            }
            Database.SaveResult result= Database.insert(evtList.get(0),false) ;
            System.debug('Result of updateCaseStatusTestMethod='+result);
            System.assert(!result.isSuccess());
            System.assert(result.getErrors().size()>0);
        }
    }
    
    /*****************************************************************************************
// Test method to validate comments and case reason is not null and case status is Closed.
******************************************************************************************/
    
    static testMethod void updateCaseStatusTestMethod5(){
        String profileId = [select id from Profile where name ='Custom: NA Agent' limit 1].Id;
        User usr = TestUtilities.createTestUser(1,profileId,true);
        
        System.runAs(usr){
            List<Case> caseList = new List<Case>();
            for(Case cse: TestUtilities.createCase(1,null,false)){
                cse.status = 'New';
                cse.subject = 'test subject';
                cse.Reason = null;
                cse.Internal_Comments__c = null;
                cse.origin= 'Email';
                caseList.add(cse);
            }
            insert caseList;
            List<Event> evtList = new List<Event>();
            for(Event evt: TestUtilities.createEvent(1,false)){
                evt.Subject ='Test';
                evt.whatId = caseList[0].Id;
                evt.Case_Status__c = 'Closed';
                evt.StartDateTime=System.Today();
                evt.EndDateTime=System.Today()+2;
                evt.CurrencyIsoCode='USD';
                evt.Type='Email';
                evt.ActivityDateTime=System.Today();
                evtList.add(evt); 
            }
            Database.SaveResult result= Database.insert(evtList.get(0),false) ;
            System.debug('Result of updateCaseStatusTestMethod2='+result);
            System.assert(!result.isSuccess());
            System.assert(result.getErrors().size()>0);
        }
    }
    
    /***************************************************************************************
// Test method to validate future action date is not null or today or past date 
// and case status is Pending Future Action.
****************************************************************************************/
    
    static testMethod void updateCaseStatusTestMethod4(){
        String profileId = [select id from Profile where name ='Custom: NA Agent' limit 1].Id;
        User usr = TestUtilities.createTestUser(1,profileId,true);
        
        System.runAs(usr){
            List<Case> caseList = new List<Case>();
            for(Case cse: TestUtilities.createCase(1,null,false)){
                cse.Future_Action_Date__c = System.Today()+2;
                cse.status = 'New';
                cse.subject = 'test subject';
                cse.origin= 'Email';
                caseList.add(cse);
            }
            insert caseList;
            List<Event> evtList = new List<Event>();
            for(Event evt: TestUtilities.createEvent(1,false)){
                evt.Subject ='Test';
                evt.whatId = caseList[0].Id;
                evt.Case_Status__c = 'Pending Future Action';
                evt.StartDateTime=System.Today();
                evt.EndDateTime=System.Today()+2;
                evt.CurrencyIsoCode='USD';
                evt.Type='Email';
                evt.ActivityDateTime=System.Today();
                evtList.add(evt);
            }
            Database.SaveResult result= Database.insert(evtList.get(0),false) ;
            System.debug('Result of updateCaseStatusTestMethod1='+result);
            System.assert(!result.isSuccess());
            System.assert(result.getErrors().size()>0);
        }
    }
    /***************************************************************************************
// Test method to delete and validate Tasks 
// Profile is System Administrator
// case status is Pending Future Action.
// and case origin is Internal Task, to check Internal Task Counter.
****************************************************************************************/
    static testMethod void updateCaseStatusTestMethod3(){
        String profileId = [select id from Profile where name ='System Administrator' limit 1].Id;
        User usr = TestUtilities.createTestUser(1,profileId,true);
        System.runAs(usr){
            List<Case> caseList = new List<Case>();
            List<Account> acc=TestUtilities.createAccount(1,true);
            for(Case cse: TestUtilities.createCase(1,null,false)){
                cse.Future_Action_Date__c = System.Today()+2;
                cse.status = 'In Progress';
                cse.subject = 'test subject';
                cse.origin= 'Internal Task';
                cse.Account=acc.get(0);
                caseList.add(cse);
            }
            insert caseList;
            for(Case c: [select ID,Internal_Task__c from Case where id in :caseList]){
                System.assertEquals(null,c.Internal_Task__c);
            }
            List<Event> evtList = new List<Event>();
            for(Event evt: TestUtilities.createEvent(1,false)){
                evt.Subject ='Test';
                evt.whatId = caseList[0].Id;
                evt.Case_Status__c = 'Pending Future Action';
                evt.StartDateTime=System.Today();
                evt.EndDateTime=System.Today()+2;
                evt.CurrencyIsoCode='USD';
                evt.Type='Internal Task';
                evt.ActivityDateTime=System.Today();
                evtList.add(evt);
            }
            System.debug('Event List:'+evtList);
            Database.SaveResult result= Database.insert(evtList.get(0),false) ;
            System.debug('Result of updateCaseStatusTestMethod 0='+result);
            List<Event> objEvt=[select Id from Event where id in :evtList];
            System.assertEquals(1,objEvt.size());
            for(Case c: [select ID,Internal_Task__c from Case where id in :caseList]){
                System.assertEquals(1,c.Internal_Task__c);
            }
            delete evtList;
            system.debug('Deleted Successfully');
            objEvt=[select Id from Event where id in :evtList];
            System.assertEquals(0,objEvt.size());
        }
    }
    /***************************************************************************************
// Test method to delete and validate Tasks 
// Profile is System Administrator
// case status is Pending Future Action.
// and case origin is Internal Task, to check Internal Task Counter.
****************************************************************************************/
    static testMethod void updateCaseStatusTestMethod2(){
        String profileId = [select id from Profile where name ='System Administrator' limit 1].Id;
        User usr = TestUtilities.createTestUser(1,profileId,true);
        System.runAs(usr){
            List<Case> caseList = new List<Case>();
            List<Account> acc=TestUtilities.createAccount(1,true);
            for(Case cse: TestUtilities.createCase(1,null,false)){
                cse.Future_Action_Date__c = System.Today()+2;
                cse.status = 'In Progress';
                cse.subject = 'test subject';
                cse.origin= 'Internal Task';
                cse.Account=acc.get(0);
                caseList.add(cse);
            }
            insert caseList;
            for(Case c: [select ID,Internal_Task__c from Case where id in :caseList]){
                System.assertEquals(null,c.Internal_Task__c);
            }
            List<Event> evtList = new List<Event>();
            for(Event evt: TestUtilities.createEvent(1,false)){
                evt.Subject ='Test';
                evt.whatId = caseList[0].Id;
                evt.Case_Status__c = 'Pending Future Action';
                evt.StartDateTime=System.Today();
                evt.EndDateTime=System.Today()+2;
                evt.CurrencyIsoCode='USD';
                evt.Type='Email';
                evt.ActivityDateTime=System.Today();
                evtList.add(evt);
            }
            List<Task> taskList = new List<Task>();
            for(Task tsk: TestUtilities.createTask(1,false)){
                tsk.whatId = caseList[0].Id;
                tsk.Case_Status__c = 'Pending Future Action';
                tsk.Type='Internal Task';
                taskList.add(tsk);
            }
            System.debug('Task List:'+taskList);
            Database.SaveResult result1= Database.insert(taskList.get(0),false) ;
            System.debug('Result of updateCaseStatusTestMethod 0='+result1);
            System.assertEquals(1,Database.countQuery('Select count() from Task where id in :taskList'));
            for(Case c: [select ID,Internal_Task__c from Case where id in :caseList]){
                System.assertEquals(1,c.Internal_Task__c);
            }
            
            System.debug('Event List:'+evtList);
            Database.SaveResult result= Database.insert(evtList.get(0),false) ;
            System.debug('Result of updateCaseStatusTestMethod 0='+result);
            System.assertEquals(1,Database.countQuery('Select count() from Event where id in :evtList'));
            for(Case c: [select ID,Customer_Interactions__c from Case where id in :caseList]){
                System.assertEquals(1,c.Customer_Interactions__c);
            }
        }
    }
    /***************************************************************************************
// Test method to delete and validate Tasks 
// Profile is System Administrator
// case status is Pending Future Action.
// and case origin is Internal Task, to check Internal Task Counter.
****************************************************************************************/
    static testMethod void updateCaseStatusTestMethod1(){
        String profileId = [select id from Profile where name ='System Administrator' limit 1].Id;
        User usr = TestUtilities.createTestUser(1,profileId,true);
        System.runAs(usr){
            List<Case> caseList = new List<Case>();
            List<Account> acc=TestUtilities.createAccount(1,true);
            for(Case cse: TestUtilities.createCase(1,null,false)){
                cse.Future_Action_Date__c = System.Today()+2;
                cse.status = 'In Progress';
                cse.subject = 'test subject';
                cse.origin= 'Internal Task';
                cse.Account=acc.get(0);
                caseList.add(cse);
            }
            insert caseList;
            for(Case c: [select ID,Internal_Task__c from Case where id in :caseList]){
                System.assertEquals(null,c.Internal_Task__c);
            }
            List<Event> evtList = new List<Event>();
            for(Event evt: TestUtilities.createEvent(1,false)){
                evt.Subject ='Test';
                evt.whatId = caseList[0].Id;
                evt.Case_Status__c = 'Pending Future Action';
                evt.StartDateTime=System.Today();
                evt.EndDateTime=System.Today()+2;
                evt.CurrencyIsoCode='USD';
                evt.Type='Email';
                evt.ActivityDateTime=System.Today();
                evtList.add(evt);
            }
            List<Task> taskList = new List<Task>();
            for(Task tsk: TestUtilities.createTask(1,false)){
                tsk.whatId = caseList[0].Id;
                tsk.Case_Status__c = 'Pending Future Action';
                tsk.Type='Email';
                taskList.add(tsk);
            }
            System.debug('Task List:'+taskList);
            Database.SaveResult result1= Database.insert(taskList.get(0),false);
            System.debug('Result of updateCaseStatusTestMethod 0='+result1);
            System.assertEquals(1,Database.countQuery('Select count() from Task where id in :taskList'));
            for(Case c: [select ID,Customer_Interactions__c from Case where id in :caseList]){
                System.assertEquals(1,c.Customer_Interactions__c);
            }
            
            System.debug('Event List:'+evtList);
            Database.SaveResult result= Database.insert(evtList.get(0),false) ;
            System.debug('Result of updateCaseStatusTestMethod 0='+result);
            System.assertEquals(1,Database.countQuery('Select count() from Event where id in :evtList'));
            for(Case c: [select ID,Customer_Interactions__c from Case where id in :caseList]){
                System.assertEquals(2,c.Customer_Interactions__c);
            }
        }
    }
    /***************************************************************************************
// Test method to update and validate Tasks 
// Profile is System Administrator
// case status is Pending Future Action.
// and case origin is Internal Task, to check Internal Task Counter.
****************************************************************************************/
    static testMethod void updateCaseStatusTestMethod0(){
        String profileId = [select id from Profile where name ='System Administrator' limit 1].Id;
        User usr = TestUtilities.createTestUser(1,profileId,true);
        System.runAs(usr){
            List<Case> caseList = new List<Case>();
            List<Account> acc=TestUtilities.createAccount(1,true);
            for(Case cse: TestUtilities.createCase(1,null,false)){
                cse.Future_Action_Date__c = System.Today()+2;
                cse.status = 'In Progress';
                cse.subject = 'test subject';
                cse.origin= 'Internal Task';
                cse.Account=acc.get(0);
                caseList.add(cse);
            }
            insert caseList;
            for(Case c: [select ID,Internal_Task__c from Case where id in :caseList]){
                System.assertEquals(null,c.Internal_Task__c);
            }
            List<Event> evtList = new List<Event>();
            for(Event evt: TestUtilities.createEvent(1,false)){
                evt.Subject ='Test';
                evt.whatId = caseList[0].Id;
                evt.Case_Status__c = 'Pending Future Action';
                evt.StartDateTime=System.Today();
                evt.EndDateTime=System.Today()+2;
                evt.CurrencyIsoCode='USD';
                evt.Type='Internal Task';
                evt.ActivityDateTime=System.Today();
                evtList.add(evt);
            }
            System.debug('Event List:'+evtList);
            Database.SaveResult result= Database.insert(evtList.get(0),false) ;
            System.debug('Result of updateCaseStatusTestMethod 0='+result);
            List<Event> objEvt=[select Id from Event where id in :evtList];
            System.assertEquals(1,objEvt.size());
            for(Case c: [select ID,Internal_Task__c from Case where id in :caseList]){
                System.assertEquals(1,c.Internal_Task__c);
            }
            for(Event evt: evtList){
                evt.Case_Status__c = 'Awaiting Information';
                evt.Type='Email';
            }
            update evtList;
            for(Case c: [select ID,Internal_Task__c,Customer_Interactions__c from Case where id in :caseList]){
                System.assertEquals(1,c.Customer_Interactions__c);
                System.assertEquals(0,c.Internal_Task__c);
            }
        }
    }
}