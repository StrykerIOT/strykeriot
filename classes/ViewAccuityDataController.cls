/*********************************************************************
* Appirio a WiPro Company
* Name: ViewAccuityDataController
* Description: [T-607631 - ControllerClass- to get response recieved via services ]
* Created Date: [06/06/2017]
* Created By: [Aashish Sajwan] (Appirio)
* 
* Date Modified                Modified By                  Description of the update

*****************************************************************************************/

public class ViewAccuityDataController{
    public string Routing_Number{set;get;}
    public string Bank_Country{set;get;}
    public string BankInfoId{set;get;}
    public List<UtilityCls.Value> AccutiyDatalist {get;set;}
    public ViewAccuityDataController(){
        AccutiyDatalist = new List<UtilityCls.Value>();
        Routing_Number = apexpages.currentpage().getparameters().get('Routing_Number');
        Bank_Country=apexpages.currentpage().getparameters().get('Bank_Country');
        BankInfoId=apexpages.currentpage().getparameters().get('BankInfoId');
        GetAccuityData(Routing_Number ,Bank_Country);
    }
    
    // GetAccuityData 
    private void GetAccuityData(string Routing_Number ,string Bank_Country){
        if(Bank_Country == 'Mexico'){
         AccutiyDatalist = UtilityCls.CallElavonApi('MXAccuity','','','MX',Routing_Number  );
        }else{
           AccutiyDatalist = UtilityCls.CallElavonApi('NAAccuity','','','',Routing_Number  );
        }
        system.debug('&& AccutiyDatalist values &&'+AccutiyDatalist );
    }
    
    public pagereference returnToBankingInfo(){
        PageReference bankingPage = new PageReference('/'+BankInfoId);
       return bankingPage;
    }
}