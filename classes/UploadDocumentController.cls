/*********************************************************************
 * Appirio, Inc
 * Name: UploadDocumentController
 * Description: [Task# T-558156]
 * Created Date: [12/19/2016]
 * Created By: [Aashish Sajwan] (Appirio)
 * 
 * Date Modified                Modified By             Description of the update
 * [12 20, 2016]               [Aashish Sajwan]         [T-558156]
 * [12 23, 2016]               [Aashish Sajwan]         [T-558156]
 * [01 30, 2017]               [Aashish Sajwan]         [I-255622]
 * [02 07, 2017]               [Aashish Sajwan]         [T-576696]
 * [02 16, 2017]               [Aashish Sajwan]         [T-578644/I-256366]
 * [02 18, 2017]               [Aashish Sajwan]         [T-578644/I-256366]
 * [03 24, 2017]               [Aashish Sajwan]         [T-589057/S-470290]
 * [03 29, 2017]               [Aashish Sajwan]         [T-589057/S-470290]
 
 **********************************************************************/
  public class UploadDocumentController {
   //Variables
   string  accountId {get;set;}
   string caseId {get;set;}
   string  dBAName {get;set;}
   // Added new variable to get Processing Center Id from Query String of browser -- Aashish Sajwan - [02 07, 2017] - T-576696 
   //Start
   string  processingCenterId {set;get;} 
   //End
   
   
   //Added by Aashish Sajwan 18-Feb-2016 : Translation changes on Page  T-578644/I-256366
   //Start
   public boolean redirectToCase{get ;set;}
   public String Error_Message {get;set;}  
   //End
   public string mode {get ;set;}  //I-255622 
   public string  mID {get;set;}                            //Added - AS (Access Modifier)
   public String Message {get;set;}
   public string accountapplctnId {get;set;} 
         
   public List<SelectOption> statementType{get;set;}
   public string selectedType {get;set;}
   public Document document {
        get {
          if (document == null)
            document = new Document();
           return document;
        }
        set;
      }
      
   //Constructor    
   public UploadDocumentController(){ 
         //Added by Aashish Sajwan 18-Feb-2016 : Translation changes on Page  T-578644/I-256366
         Message = '';  
         Error_Message='';
        //End
        //Map for Get Url's Parameters 
        Map<String, String> urlParams = Apexpages.currentPage().getParameters();  
        //Get Account Name from Url Params
        dBAName = urlParams.containsKey('AccountName') ? EncodingUtil.urlDecode(urlParams.get('AccountName'),'UTF-8') : '';
        //Get Account Id from Url Params
        accountId = urlParams.containsKey('AccountId') ? EncodingUtil.urlDecode(urlParams.get('AccountId'),'UTF-8') : '';
        //Get Case Id from Url Params
        caseId = urlParams.containsKey('CaseId') ? EncodingUtil.urlDecode(urlParams.get('CaseId'),'UTF-8') : '';
        //Get MID from Url Params 
        mID = urlParams.containsKey('MID') ? EncodingUtil.urlDecode(urlParams.get('MID'),'UTF-8') : '';
        //Get Processesing Center Id from Url Paramas (7/02/2017 - #T-576696/S-449550)
        processingCenterId = urlParams.containsKey('PID') ? EncodingUtil.urlDecode(urlParams.get('PID'),'UTF-8') : '';
        
        //Set mode value as it for Case or Account
        mode = caseId!=''?'Case':'Account';
        //Set Button Lable on Upload Image Page on basis of Case or Account
        
        
        //Added by Aashish Sajwan 18-Feb-2016 : Translation changes on Page  T-578644/I-256366        
        redirectToCase = (mode =='Case')? true: false;
        
        //Get Application ID from Url Params (24-03-2017 -#S-470290)
        accountapplctnId = urlParams.containsKey('ApplicationID') ? EncodingUtil.urlDecode(urlParams.get('ApplicationID'),'UTF-8') : '';
        //End
        
        //Create StatementType SelectOption List
        statementType = new list<SelectOption>();
        //Add values in SelectOption  List for Statement Type
        statementType.add(new SelectOption('1099','1099'));
        statementType.add(new SelectOption('ACH','ACH'));
        statementType.add(new SelectOption('Add Equip Svcs','Add Equip Svcs'));
        statementType.add(new SelectOption('AML/EDD','AML/EDD'));
        statementType.add(new SelectOption('Applic','Applic'));
        statementType.add(new SelectOption('Assum Ag','Assum Ag'));
        statementType.add(new SelectOption('Bankruptcy','Bankruptcy'));
        statementType.add(new SelectOption('CB email note','CB email note'));
        statementType.add(new SelectOption('Closure','Closure'));
        statementType.add(new SelectOption('Contract','Contract'));
        statementType.add(new SelectOption('Convert','Convert'));
        statementType.add(new SelectOption('Correspondence','Correspondence'));
        statementType.add(new SelectOption('DBA/DDA Chg','DBA/DDA Chg'));
        statementType.add(new SelectOption('Debit Card C/B','Debit Card C/B'));
        statementType.add(new SelectOption('Decline/Ret App','Decline/Ret App'));
        statementType.add(new SelectOption('Financial Updat','Financial Updat'));
        statementType.add(new SelectOption('Financials','Financials'));
        statementType.add(new SelectOption('FMIS Quality','FMIS Quality'));
        statementType.add(new SelectOption('FN','FN'));
        statementType.add(new SelectOption('Hierarchy Chang','Hierarchy Chang'));
        statementType.add(new SelectOption('ID Theft Affida','ID Theft Affida'));
        statementType.add(new SelectOption('Ladco Bankruptc','Ladco Bankruptc'));
        statementType.add(new SelectOption('Ladco Buyback','Ladco Buyback'));
        statementType.add(new SelectOption('Ladco Check','Ladco Check'));
        statementType.add(new SelectOption('Ladco Collect','Ladco Collect'));
        statementType.add(new SelectOption('Ladco Corresp','Ladco Corresp'));
        statementType.add(new SelectOption('Ladco Insuran','Ladco Insuran'));
        statementType.add(new SelectOption('Ladco Insurance','Ladco Insurance'));
        statementType.add(new SelectOption('Ladco Lease Con','Ladco Lease Con'));
        statementType.add(new SelectOption('Ladco Legal','Ladco Legal'));
        statementType.add(new SelectOption('Ladco MFM Updat','Ladco MFM Updat'));
        statementType.add(new SelectOption('Ladco Novations','Ladco Novations'));
        statementType.add(new SelectOption('Ladco Other','Ladco Other'));
        statementType.add(new SelectOption('Ladco Sublease','Ladco Sublease'));
        statementType.add(new SelectOption('Legal','Legal'));
        statementType.add(new SelectOption('LGB','LGB'));
        statementType.add(new SelectOption('LMG Periodic','LMG Periodic'));
        statementType.add(new SelectOption('MRN/IVI Lotto C','MRN/IVI Lotto C'));
        statementType.add(new SelectOption('PCI','PCI'));
        statementType.add(new SelectOption('Q of C Updates','Q of C Updates'));
        statementType.add(new SelectOption('Quality','Quality'));
        statementType.add(new SelectOption('Quality of Comp','Quality of Comp'));
        statementType.add(new SelectOption('Recovery','Recovery'));
        statementType.add(new SelectOption('Reenroll/Transf','Reenroll/Transf'));
        statementType.add(new SelectOption('Re-Signs','Re-Signs'));
        statementType.add(new SelectOption('RISK REVIEW','RISK REVIEW'));
        statementType.add(new SelectOption('Rpt Setup/Updat','Rpt Setup/Updat'));
        statementType.add(new SelectOption('SMG REUN','SMG REUN'));
        statementType.add(new SelectOption('SMU Quality','SMU Quality'));
        statementType.add(new SelectOption('TEST','TEST'));
        statementType.add(new SelectOption('Update','Update'));
        statementType.add(new SelectOption('Validation','Validation'));
        statementType.add(new SelectOption('W9','W9'));
        statementType.add(new SelectOption('Welcome Letter','Welcome Letter'));
        
        // Fix for I-256366 (Translation Change) Aashish Sajwan - 16- Feb-2017  
        system.debug('** current locale value: '+UserInfo.getLanguage());        
   }
 
   //Upload Document 
  //Upload all Image and Tiff Document to FileNet 
  public void uploadDocuments(){
     //Create Request Object 
      wwwElavonComServicesImagingserviceU.UAT01  request = new wwwElavonComServicesImagingserviceU.UAT01();
      try{
            system.debug('** document value **'+document);
            system.debug('** MID value **'+mID);
            system.debug('** DBAName value **'+dBAName);
            system.debug('** AccountId value **'+accountId);
            //Set Application Id for Input Parameter of Upload Image request
            string ApplicationId = mID ;//'8024778006';
            //Set BusinessName  for Input Parameter of Upload Image request
            String BusinessName = dBAName ;//'CSPR NEW MID FILE1';
            //Set MimeType for Input Parameter of Upload Image request on basis of file type
            String MimeType = (document.Name.contains('.pdf'))?'PDFDocument':'TIFFImage';
            system.debug('** MimeType value **'+MimeType );
            //Set ScanDate for Input Parameter of Upload Image request
            //Fix Issue - I-259085 Aasish Sajwan - 9-Feb-2017: Pass System GMT date instead of Date.Today()
            DateTime ScanDate = System.now();// Date.Today();
            System.debug('@@@@ ScanDate value is during upload document '+ ScanDate);
            //Set DocumentType for Input Parameter of Upload Image request
            String DocumentType = selectedType;//'APPLI';
           // Blob b= Blob.valueof(document.Body);
            String ImageDocument = EncodingUtil.base64Encode(document.Body);//Blob.valueOf(document.Body); 
            //Updated Added Country Code for Request if Processing Center Id is QUERE -7-Feb-2017 Aashish Sajwan For Task T-576696
            //Assign Country code null for Input Parameter of Upload Image request 
            String CountryCode = null;
            //Added SSource for Max region 29-Mar-2017 AS #T-589057/S-470290 
            String SSource = null;
            //Check Process Center Id value.
            if(processingCenterId == 'QUERE'){
              //Assign Country code Null for Input Parameter of Upload Image request for Processing Center Id QUERE
              CountryCode = 'MEXICO';
              //As Application Id will be account's Application Id per Story #S-470290
              ApplicationId = accountapplctnId;
             //Added SSource for Max region 29-Mar-2017 AS #T-589057/S-470290 
              SSource = MID;
            }
            
            system.debug('** CountryCode value **'+CountryCode );
            system.debug('** ImageDocument value **'+ImageDocument );
            //Updated 7-Feb-2017 Aashish Sajwan For Task T-576696
            //Add Countrycode parameter value in request 
            //Added SSource Updated 29-Feb-2017 AS #T-589057/S-470290
            system.debug('** request value **'+request);
            wwwElavonComSchemaImagingresourcesV.UploadImageResponse_element  statementresponse = request.uploadImage(ApplicationID,CountryCode,BusinessName,MimeType,ScanDate,DocumentType,ImageDocument,null,null,SSource);
            //Statement Response is not null
            if(statementresponse!=null){
                System.debug('@@@@ ImageID value is '+ statementresponse.ImageID);
                System.debug('@@@@ statement Error is '+ statementresponse.Error);
                Message =statementresponse.ImageID;
            }else{
                Error_Message ='Image not Uploaded successfully on FileNet. Please try again';
            }
            //Set Document varible null to resolve view state error on visualforce page.
           document = null;
      }
      catch(Exception ex){
          //handle Exception on Page: Aashish Sajwan 18-Feb-2017 
          ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage()));
         
      }
                
   
   
  }
  //Method for Redirect to Account or Case Record
      public PageReference redirectToAccount(){
       system.debug('&& Account Id Value &&'+accountId);
       //Create Pagereference object on basis of case or account.
       PageReference accountPage = (mode =='Case')? new PageReference('/'+caseId) : new PageReference('/'+accountId);
       //return pagereference object
       return accountPage;
      
      }
 
 }