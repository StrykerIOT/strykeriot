/*********************************************************************
* Appirio a WiPro Company
* Name: KCSAttachtoCaseController 
* Description: [T-598751 - ControllerClass- For FAQArticlePage ]
* Created Date: [11/5/2017]
* Created By: [Poornima Bhardwaj] (Appirio)
* 
* Date Modified                Modified By                  Description of the update

**********************************************************************/
global class KCSAttachtoCaseController {
    
    public string caseID{get;set;}
    public string kaID{get;set;}
    //Costructor of KCSAttachtoCaseController
    public KCSAttachtoCaseController(ApexPages.StandardController stdController) {
        //Fetching caseId Value of current page
        caseID = ApexPages.currentPage().getParameters().get('caseid');
        //Fetching Id of Current Page
        kaID = ApexPages.currentPage().getParameters().get('id');
    }
    //****************************************************************************
    // Method to attach the Article to the Case
    // @param oldMap: None
    // @return void
    //**************************************************************************** 
    public void attachToCase(){
        //Creating object of CaseArticle
        CaseArticle caseArticleObj = new CaseArticle();
        //Assigning caseID to CaseArticle
        caseArticleObj.CaseID = caseID;
        caseArticleObj.KnowledgeArticleId = kaID;
        //Inserting caseArticle object
        insert caseArticleObj;
    }
}