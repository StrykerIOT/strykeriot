/*********************************************************************
* Appirio a WiPro Company
* Name: ViewCollectionsDetailsControllerr
* Description: [T-598751 - ControllerClass- to get response recieved via services ]
* Created Date: [25/5/2017]
* Created By: [Poornima Bhardwaj] (Appirio)
* 
* Date Modified                Modified By                  Description of the update
* [25-May-2017]                [Aashish Sajwan]             [handle Parameter]
* [26-May-2017]                [Aashish Sajwan]             [Create  GetCollectionDetails method  #T-606275]
*[31-May-2017]                 [Neeraj Kumawat]             [#T-607012 Added Return to WorkOrder link]
*[06-Jun-2017]                 [Aashish Sajwan]             [#T-607012 Update parameter in GetCollectionDetails]
*[07-June-2017]                [Poornima Bhardwaj]          [#T-608156 Update Constructor and GetCollectionDetails method]
**********************************************************************/
public without sharing class ViewCollectionsDetailsController{
    public string AccountId {get;set;}
    public string midValue {get;set;}
    public string workOrderId {get ;set;}
    public boolean redirectToWorkOrder{get ;set;}
    public List<UtilityCls.Value> valueList { get; set; }
    public boolean showMessage {get;set;}
    public ViewCollectionsDetailsController() {
        Map<String, String> urlParams = Apexpages.currentPage().getParameters();  
        AccountId = urlParams.containsKey('AccountId') ? EncodingUtil.urlDecode(urlParams.get('AccountId'),'UTF-8') : '';
        midValue = urlParams.containsKey('Mid') ? EncodingUtil.urlDecode(urlParams.get('Mid'),'UTF-8') : '';
        //Updated By Neeraj Kumawat as per task T-607012
        //Start
        workOrderId = urlParams.containsKey('workOrderId') ? EncodingUtil.urlDecode(urlParams.get('workOrderId'),'UTF-8') : '';
        redirectToWorkOrder=(workOrderId!='')? true : false;
        //End
        //Updated by Poornima Bhardwaj
        string ProfileId = userinfo.getProfileId();
        string ProfileName = [Select Name 
                              FROM Profile
                              WHERE Id =:ProfileId].Name;
        system.debug('&& Profile Name &&'+ProfileName);
        if(ProfileName=='Custom: MX Agent' || ProfileName =='Custom: MX Manager'){
          //Calling GetRefreshDateStamps method 
          GetCollectionDetails(True);
          showMessage= True;
        }else{
         GetCollectionDetails(False);
         showMessage= False;
        }
        
    }
    
    //****************************************************************************
    // Method to GetCollectionDetails
    // Crated by : Aashish Singh Sajwan 24 May 2017 #T-598751/#T-606275
    // @return void
    //****************************************************************************
    public void GetCollectionDetails(boolean isMaxRegion){
        try {
        if(isMaxRegion){
            valueList   = new List<UtilityCls.Value>();
         
        }else{
        //Hit web service when Profile is NA
        valueList  = UtilityCls.CallElavonApi('CollectionDetails',midValue,'','','');
        system.debug('&& valueList values &&'+valueList);
        
        }
           
        }catch(Exception ex){
            
        }
        
        
    }
    
    //Redirect To Account 
    public PageReference redirectToAccount(){
        // Create Page Reference for Account
        //Updated By Neeraj Kumawat as per task T-607012
        PageReference accountPage = (workOrderId !='')? new PageReference('/'+workOrderId) : new PageReference('/'+AccountId);
        //return account page reference
        return accountPage;
        
    }
}