'use strict';
var app = angular.module('semApp', [
					'ngForce',
                    'ngForce.config',
                    'ui.grid',   
                    'ui.grid.resizeColumns', 
                    'ui.grid.exporter',
					'SEM.config',
                    'nvd3',
                    'ngRoute',
                    'ngSanitize',
                    'ngAnimate',
                    'mgcrea.ngStrap',
					'semAppControllers',
					'semAppServices',
					'semAppDirectives',
                    'semAppProviders',
					'semAppFilters',
					'semAppControllerHelpers'
                ])

    .run(function (sfTemplate,sfLabels,$rootScope) {
        $rootScope.labels = sfLabels;
        sfTemplate.fromVf($rootScope.labels.sitePrefix + '/SEM_HomeView'); 
    })

    //assignment of VF label to JS variables
    .run(function ($rootScope,sfLabels,globalConstants, utility) {
        globalConstants.USER_TIMEZONE = utility.getTimeZoneName();
		$rootScope.GLOBALCONSTANTS = globalConstants;
    })


    //route

    .config(['$routeProvider', '$compileProvider','$locationProvider','sfLabels','insightsProvider','globalConstants', function ($routeProvider, $compileProvider,$locationProvider,sfLabels,insightsProvider, globalConstants) {

        $locationProvider.hashPrefix('');

        $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|file|javascript):/);
        $compileProvider.debugInfoEnabled(false);

		var resolveForQuickLink =  {
					quickLink: function(){
						return true;
					}
				};
        $routeProvider
                .when('/', {
                    controller: 'HomeCtrl',
                    templateUrl: sfLabels.sitePrefix + '/SEM_HomeView'
                })
                .when('/group/:name', {
                    controller: 'GroupCtrl',
                    templateUrl: sfLabels.sitePrefix + '/SEM_GroupView'
                })
                .when('/type/:name/:groupName', {
                    controller: 'TypeCtrl',
                    templateUrl: sfLabels.sitePrefix + '/SEM_TypeView'
                })
                .when('/settings', {
                    controller: 'SettingsCtrl',
                    templateUrl: sfLabels.sitePrefix + '/SEM_SettingsView',
					resolve: resolveForQuickLink
                })
                .when('/cart', {
                    controller: 'CartCtrl',
                    templateUrl: sfLabels.sitePrefix + '/SEM_CartView',
					resolve: resolveForQuickLink
                })
                .when('/datatable/:query/:previousPage', {
                    controller: 'DataTableCtrl',
                    templateUrl: sfLabels.sitePrefix + '/SEM_DatatableView'
                })
                .when('/loaners/:query/:previousPage', {
                    controller: 'LoanersCtrl',
                    templateUrl: sfLabels.sitePrefix + '/SEM_LoanersView'
                })
                .when('/help', {
                    controller: 'HelpCtrl',
                    templateUrl: sfLabels.sitePrefix + '/SEM_HelpView',
					resolve: resolveForQuickLink
                })
                .when('/about', {
                    templateUrl: sfLabels.sitePrefix + '/SEM_AboutView',
					resolve: resolveForQuickLink
                })
                .when('/contact', {
                    templateUrl: sfLabels.sitePrefix + '/SEM_ContactView',
					resolve: resolveForQuickLink
                })
				.when('/error', {
                    templateUrl: sfLabels.sitePrefix + '/SEM_Error',
					resolve: resolveForQuickLink
                })
                .when('/disclaimer', {
                    templateUrl: sfLabels.sitePrefix + '/SEM_DisclaimerView',
					resolve: resolveForQuickLink
                })
                .when('/search/:term', {
					controller: 'SearchCtrl',
                    templateUrl: sfLabels.sitePrefix + '/SEM_SearchView',
					resolve: resolveForQuickLink
                })
                .otherwise({redirectTo: "/"});

        insightsProvider.start({
            instrumentationKey: globalConstants.AI_APPLICATION_ID,
            disableAjaxTracking: true,
            autoTrackPageVisitTime: true
        });
    }])
.factory('$exceptionHandler', ['$log','insights', function($log,insights) {
    return function(exception, cause) {
      insights.trackException(exception);
      $log.error(exception, cause);
    };
  }])


    //fetch init global data
    .run(function ($rootScope, $route, $location, $window, dataServices,utility,globalConstants,insights, notificationService) {
        
        $rootScope.userName = localStorage.usrName;
		$rootScope.currentOrgsAndFacilities = [];
        $rootScope.dataTableParam = [];
        $rootScope.savedState = {};
        $rootScope.savedStateLoaner = {};
        $rootScope.gridData = [];
        $rootScope.gridDataLoaner = [];
        $rootScope.dataTableQuery = '';
        $rootScope.dataTableQueryLoaner = '';
        $rootScope.oldCart = [];
        $rootScope.orgsAndFacilities = [];
        $rootScope.allFacilities = 0;
        $rootScope.selectedFacilities = [];
		$rootScope.visibleModal = null;
		$rootScope.isBackButtonPressed = true;
		$rootScope.popupControl = {};
		$rootScope.chartSortOrder = [];
        $rootScope.isTypingForSearch = false;
		$rootScope.loadingData = true;
		$rootScope.tabControl = {
			tabCurrentPage : '/'
		};
		
		String.prototype.replaceAll = function(search, replacement) {
			var target = this;
			return target.split(search).join(replacement);
		};
        
		$rootScope.activeTabNew = "overview";
		
		$rootScope.showPrintPreview = function(elementId){
			$rootScope.lockEvent = true;
			utility.showPrintPreview(elementId === null ? false : true);
		}
		
		$rootScope.openDisclaimer = function () {
			utility.disclaimerModal.$promise.then(utility.disclaimerModal.show);
		};
		
		$rootScope.getPrintClass = function(index){
				if(index > 3){
					return 'sem-page-break';
				}else{
					return '';
				}
		}

        $rootScope.onKeyDown = function(e) {
            if(e.ctrlKey || e.metaKey){
                utility.onKeyDown(e);
            }  
        };
		
		$rootScope.isActiveTab = function(tabName){
			return $rootScope.activeTabNew === tabName;
		}
		
		$rootScope.getUserSettingOptions = function(callback){
	        	
	        	dataServices.getUsageTimeWeekAndDayOptions()
	            .then(function (data) {
	            
			            // this variable will be replaced when we have data from Azure
									$rootScope.defaultUsageSettings = utility.populateChartView(data);
									
									$rootScope.chartUsageData = angular.copy($rootScope.defaultUsageSettings);

                                    _.forEach($rootScope.defaultUsageSettings.daysOptions, function(dayOpt){
                                        if(dayOpt.value === $rootScope.GLOBALCONSTANTS.BDC_NUMBER_OF_DAYS_FOR_ALL){
                                            $rootScope.labels.LAST_ALL_DAYS = dayOpt.label;
                                        }
                                    });
                                    
                                    $rootScope.$broadcast('userSettingLoaded');
									callback();
									
							}).catch(function(e) {
		          	utility.redirectOnError(e); 
		            
		          });
					
        }
        
        $rootScope.view = {
            hasLocation: true,
            location: ""
        };
		
        $rootScope.notSnapshot = false;
        $rootScope.cart = [];
        $rootScope.cartSubmitResponse = [];
		
		$rootScope.$on("$routeChangeStart", function (event, current, previous) { 
		    if(current.originalPath.indexOf('SearchView') !== -1 || current.originalPath.indexOf('SettingsView') !== -1 
		        || current.originalPath.indexOf('ContactView') !== -1 || current.originalPath.indexOf('HelpView') !== -1 
		        || current.originalPath.indexOf('DisclaimerView') !== -1 || current.originalPath.indexOf('AboutView') !== -1) {
		        $rootScope.loadingData = false;
		    } else {
		        $rootScope.loadingData = true;
		    }
			
             $rootScope.$emit('startTrackPageView',{
				name:$location.url()
			});
			if(angular.element('.modal').length !== 0 && $rootScope.visibleModal !== null && $rootScope.isBackButtonPressed){
				$rootScope.visibleModal.hide();
				utility.locationModal.hide();
				$rootScope.visibleModal = null;
				event.preventDefault();
				$window.history.forward();
				$rootScope.loadingData = false;
			} 
			$rootScope.isBackButtonPressed = true;
		});
		
		$rootScope.go = function (path) {
			$rootScope.isBackButtonPressed = false;
			if(path.match(/datatable/gi) || path.match(/loaners/gi)){
				path = path + "/" + $location.path().replaceAll('/','*');
			}
            if ($location.path() !== path) {
                $location.path(path);
            }
        };

        $rootScope.$on('$routeChangeSuccess', function (e, current, previous) {
            var currentPath = (typeof current === 'undefined' || typeof current.originalPath === 'undefined') ? "" : current.originalPath;
			/*
            the Page Track load time is stopped in controllers after the async calls, and for the pages that does not have 
            any controller, the Page Load Tracking time needs to be stopped after the route has successfully changed
            */
            if(angular.isUndefined(current.controller)){
                $rootScope.$emit('stopTrackPageView', {
                    name: $location.url(), url: $location.absUrl()
                });
				$rootScope.loadingData = false;
            }
            $rootScope.tabControl.showTabs = true;
			
            if (!currentPath.match(/datatable/gi) && !currentPath.match(/loaners/gi)) {
                $rootScope.activeTabNew = 'overview';
            }else{
                $rootScope.activeTabNew = 'listview';
            }
            if (!currentPath.match(/search/gi)) {
                $rootScope.search.term = '';  
                $rootScope.search.currentTerm = ''; 
            }

            if (currentPath === '/cart') {
                $rootScope.cartSubmitResponse = [];
            }
             
            if (typeof current !== 'undefined' && typeof current.locals !== 'undefined' && current.locals.quickLink){
                $rootScope.tabControl.showTabs = false;
            }
            
        });
		
		$rootScope.$on('$routeChangeError', function () {
            utility.redirectOnError({message : globalConstants.SESSION_ERROR}); 
        });

         $rootScope.$on("startTrackPageView", function (event, data) {
             try {
                 insights.startTrackPage(data.name);
             }
             /*
             the exception may occur when a Page Track is started but due to some error, the page track view 
             is not stopped, so when next time the same page is loaded the exception is thrown since the 
             previous track page event has not stopped, so the previous event is stopped, logged with the 
             exception thrown and start the track page for the recent page load event
              */
             catch (e) {
                 insights.stopTrackPage(data.name, '', { 'exception': e });
                 insights.startTrackPage(data.name);
             }
        });

         $rootScope.$on("stopTrackPageView", function (event, data) {
           insights.stopTrackPage(data.name,data.url,data.customProp);
        });

        $rootScope.showDataTable = function (status,productGroup,productType) {
            if(status !== null){
                $rootScope.dataTableParam = [{
						paramName: 'productGroup',
						paramValue: productGroup
					},
                    {
                        paramName: 'productType',
                        paramValue: productType
                    },
                    {
                        paramName: 'status',
                        paramValue: status
                    }];
            }else{
				$rootScope.savedState = {};
			}
			$rootScope.savedStateLoaner = {};
            if(status === 'LOANER'){
                $rootScope.go('/loaners/' + JSON.stringify($rootScope.dataTableParam));
            }else{
                $rootScope.go('/datatable/' + JSON.stringify($rootScope.dataTableParam));
            }
        };

        
        $rootScope.openDeviceModal = function (deviceId, numOfDays) {
            $rootScope.$emit('startTrackPageView', {
				name: globalConstants.DEVICE_MODAL
			});
            if(!numOfDays){
            	numOfDays = $rootScope.chartUsageData.days;
            } 
            $rootScope.processingData = true;
            dataServices.getDeviceById(deviceId, numOfDays)
							.then(
								function(data){
								angular.element('#Home_SearchBox').blur();	
								$rootScope.selectedDevice	= data;
								if(data.isLoaner === true) {
    				                $rootScope.confirmationForLoaner = true;
                                    $rootScope.processingData = false;
    				                utility.loanerModal.$promise.then(utility.loanerModal.show);
				            } else {
				                $rootScope.confirmationForLoaner = false;
                                $rootScope.processingData = false;
				                utility.deviceModal.$promise.then(utility.deviceModal.show);
				            }	
								},				
								function(){
									$rootScope.selectedDevice = {};
							}).finally(function(){
                                $rootScope.$emit('stopTrackPageView', {
                                    name: globalConstants.DEVICE_MODAL,
                                    customProp:{deviceSerialNumber:$rootScope.selectedDevice.serialNumber}
                                });
                            });
        };

		$rootScope.addToCartButtonClick = function(device, isRecommended) {
        	if(!$rootScope.lockAction) {
                $rootScope.lockAction = true;
                $rootScope.selectedDevice = device;
                
	        	if(device.loaner === true || device.isLoaner === true) {
	        		$rootScope.selectedDevice.loanerReasonResponse = 
	                    isRecommended === true ? $rootScope.GLOBALCONSTANTS.LOANER_PRI_BTN_VAL : $rootScope.GLOBALCONSTANTS.LOANER_SEC_BTN_VAL;
	        		
	        		$rootScope.confirmationForLoaner = false;
	        		$rootScope.confirmAddToCart(device, $rootScope.selectedDevice.loanerReasonResponse);
	             } else {
	                $rootScope.selectedDevice.maintenanceReasonResponse = 
	                  isRecommended === true ? $rootScope.GLOBALCONSTANTS.DEVICE_PRI_BTN_VAL : $rootScope.GLOBALCONSTANTS.DEVICE_SEC_BTN_VAL;
	                
	                $rootScope.confirmationForLoaner = true;
	                $rootScope.confirmAddToCart(device, $rootScope.selectedDevice.maintenanceReasonResponse);
	              }
        	}
        }
        
        
        $rootScope.confirmAddToCart = function(device, isRecommended){
          $rootScope.selectedDevice = device;
            var reasonResponse = {
              'maintenanceReasonResponse' : '',
              'loanerReasonResponse' : '' 
            };
            if(device.isLoaner === true || device.loaner === true){
              reasonResponse.loanerReasonResponse = isRecommended;
            }else{
              reasonResponse.maintenanceReasonResponse = isRecommended;
            }
            $rootScope.processingData = true;
            dataServices.addToCart($rootScope.selectedDevice.deviceId, JSON.stringify(reasonResponse))
            .then(function() {
				$rootScope.processingData = false;
                  $rootScope.$broadcast('deviceAddedSuccess');
                  utility.confModal.$promise.then(utility.confModal.hide);
                  utility.deviceModal.hide();
                  utility.addAlert.show();
				  $rootScope.lockAction = false;
				  $rootScope.getCartDevices();
                }).catch(function(e) {
					$rootScope.processingData = false;
                    $rootScope.lockAction = false;
                    utility.confModal.hide();
                    utility.redirectOnError(e); 
                });
        }

        $rootScope.addToCartFromDataTableById = function(device){
        	$rootScope.selectedDevice = device;
            if(device.loaner === true || device.isLoaner === true) {
            	utility.populateConfPopupData(
					$rootScope.labels.SEM_Cart_Model_Title_Add, 
					$rootScope.labels.SEM_Cart_Model_body_Text_Loaner,
					$rootScope.labels.SEM_Cart_Model_Left_Button_Loaner,
					$rootScope.labels.SEM_Cart_Model_Right_Button_Loaner
				);
				utility.confModal.$promise.then(utility.confModal.show);
            } else {
            	dataServices.getDeviceById(device.deviceId, $rootScope.chartUsageData.days)
                .then(
                  function(data){
						if(typeof data.maintenancePercentRecommendation === 'undefined' && typeof data.usesUntilMaintenanceIsNow === 'undefined') {
							$rootScope.addToCartButtonClick(device, false);
						} else {
							utility.populateConfPopupData(
								$rootScope.labels.SEM_Cart_Model_Title_Add,
								$rootScope.labels.SEM_Cart_Model_body_Text_Standard,
								$rootScope.labels.SEM_Cart_Model_Left_Button_Standard,
								$rootScope.labels.SEM_Cart_Model_Right_Button_Standard
							);
							if(device.maintenanceRecommendation !== null && device.maintenanceRecommendation !== '' && typeof device.maintenanceRecommendation !== 'undefined'){
								utility.confModal.$promise.then(utility.confModal.show);
							}
						}
                  },        
                  function(){
                    $rootScope.selectedDevice = {};
                });
            }
            
        }

        $rootScope.addToCart = function (device) {
        	if(typeof device.maintenancePercentRecommendation === 'undefined' && typeof device.usesUntilMaintenanceIsNow === 'undefined' && (device.loaner === false || device.isLoaner === false)) {
                $rootScope.addToCartButtonClick(device, false);
            } else {
            	
            	if(device.loaner === true || device.isLoaner === true){
                	utility.populateConfPopupData(
                		$rootScope.labels.SEM_Cart_Model_Title_Add, 
                		$rootScope.labels.SEM_Cart_Model_body_Text_Loaner,
                		$rootScope.labels.SEM_Cart_Model_Left_Button_Loaner,
                		$rootScope.labels.SEM_Cart_Model_Right_Button_Loaner
                	);
                }else{
                	utility.populateConfPopupData(
                		$rootScope.labels.SEM_Cart_Model_Title_Add,
                		$rootScope.labels.SEM_Cart_Model_body_Text_Standard,
                		$rootScope.labels.SEM_Cart_Model_Left_Button_Standard,
                		$rootScope.labels.SEM_Cart_Model_Right_Button_Standard
                	);
                }
                if (utility.isAlreadyExistInCart(device) === -1){
                    device.numTimestamp = new Date().getTime() * -1;
                    utility.loanerModal.hide();
                    utility.deviceModal.hide();
                    utility.confModal.$promise.then(utility.confModal.show);
                }
            } 
        };
        
        $rootScope.updateLocation = function () {

            $rootScope.gridData = [];
            $rootScope.gridDataLoaner = [];

            if ($rootScope.selectedFacilities.length === 1) {
                $rootScope.view.location = $rootScope.labels.NUMBER_OF_FACILITY_SELECTED;
            } else {
                $rootScope.view.location = 
                	$rootScope.selectedFacilities.length + " " + $rootScope.labels.Facilities_Selected;
            }
        };

        $rootScope.getCartDevices = function(callbackFun){
			$rootScope.loadingDevicesInCart = true;
            dataServices.getCartDevices()
                .then(function (data) {
					$rootScope.loadingData = false;
                    $rootScope.loadingDevicesInCart = false;
                    $rootScope.cart = data;
					$rootScope.userData.countDevicesInMaintenanceCart = data.length;
                    if(callbackFun){
						callbackFun();
					}
                }).catch(function(e) {
					utility.redirectOnError(e); 
				});
        };
        
        $rootScope.logout = function(isGuestUser){
            var usrName = $rootScope.userData.currentUserName ? 
                $rootScope.userData.currentUserName : 
                $rootScope.labels.sf_username;
            var userId = $rootScope.labels.sf_RandomUserId;
            if(isGuestUser){
                timeoutAction(usrName, userId); 
            }else{
                dataServices.logout(usrName, userId)
                    .then(function (retUrl) {
                        $window.location.href = retUrl.logoutURL; 
                    }).catch(function(e) {
                        utility.redirectOnError(e); 
                    });
            }
        };

        function timeoutAction(usrName, userId){
            dataServices.timeout(usrName, userId)
                .then(function (retUrl) {
                    $window.location.href = retUrl.logoutURL;
                }).catch(function(e) {
                        utility.redirectOnError(e); 
                });
        }
        
        $rootScope.userData = {
            countDevicesInMaintenanceCart : 0
        };
        
        
        dataServices.getUserData()
            .then(function(userDataList){
                if (userDataList && userDataList.length > 0 && userDataList[0].isUserDisclaimerAccepted === true) {
                    $rootScope.userData = userDataList[0];
                    
                    $rootScope.loadUserOrgsAndFacilitiesModal();
                } else {
                    $rootScope.showDisclaimer = true;
                }
                $rootScope.processingData = false;
        }).catch(function(e) {
            $rootScope.processingData = false;
            utility.redirectOnError(e); 
        });  
             
        $rootScope.isNotBlank = function(field){
        	return (field || field === 0);
        }
           
        $rootScope.isStrBlank = function(field){
        	return (field === '')
        }
           
        $rootScope.isStrNull = function(field){
        	return (typeof field === 'undefined')
        }
        
        $rootScope.isDateNull = function(field){
        	return (typeof field === 'undefined' || (''+field).indexOf($rootScope.GLOBALCONSTANTS.NULL_DATE) !== -1)
        }
        
        
        
        notificationService.showNotification();
        $rootScope.$on("criticalAlertClosed",function(){
			if($rootScope.showDisclaimer){
				$rootScope.openDisclaimer();
			}
		}); 
        
        $rootScope.getUserSettingOptions(function(){
            var isDayPrefNotExist = true;
            var isWeekPrefNotExist = true;
            dataServices.getAllUserPreferences()
                .then(
                    function(data){
                        _.forEach(data, function(pref){
                            if(pref.userPreferenceName === $rootScope.GLOBALCONSTANTS.DAY_PREFERENCE){
                                $rootScope.chartUsageData.days = pref.userPreferenceValue;  
                                $rootScope.defaultUsageSettings.days = pref.userPreferenceValue;
                                isDayPrefNotExist = false;
                            }else if(pref.userPreferenceName === $rootScope.GLOBALCONSTANTS.WEEK_PREFERENCE){
                                $rootScope.chartUsageData.weeks = pref.userPreferenceValue;
                                $rootScope.defaultUsageSettings.weeks = pref.userPreferenceValue;
                                isWeekPrefNotExist = false;
                            }
                        });   
                        if(isDayPrefNotExist){
                            dataServices.addUserPreference($rootScope.GLOBALCONSTANTS.DAY_PREFERENCE, escape($rootScope.chartUsageData.days));
                        }
                        if(isWeekPrefNotExist){
                            dataServices.addUserPreference($rootScope.GLOBALCONSTANTS.WEEK_PREFERENCE, escape($rootScope.chartUsageData.weeks));    
                        }
                    }).catch(function(e) {
                utility.redirectOnError(e); 
            });
        });
        
        $rootScope.loadUserOrgsAndFacilitiesModal = function() {
            dataServices.getUserOrgsAndFacilities()
                .then(function (data) {
                    $rootScope.orgsAndFacilities = data;
                    if ($rootScope.orgsAndFacilities.length === 1 
                    	&& $rootScope.orgsAndFacilities[0].facilities.length === 1) {
                            $rootScope.view.hasLocation = false;
                            $rootScope.view.location = $rootScope.labels.NUMBER_OF_FACILITY_SELECTED;
                            $rootScope.selectedFacilities = $rootScope.orgsAndFacilities[0].facilities;
                            dataServices.saveSelectedFacilities(_.map($rootScope.selectedFacilities, 'id'))
                            .then(function(){
                                
                                $rootScope.$broadcast('facilitySelectionChanged');
                            }).catch(function(e) {
                                utility.redirectOnError(e); 
                            });
                            
                    } else {
                        $rootScope.view.hasLocation = true;
                        $rootScope.selectedFacilities = [];
                        if($rootScope.userData.facilitiesSelectedName){
                            _.forEach($rootScope.userData.facilitiesSelectedName.split(','),function(facilityName){
                                $rootScope.selectedFacilities.push({ 'name' : facilityName.trim()});
                            });
                        }
                        $rootScope.updateLocation();
                        $rootScope.$broadcast('facilitySelectionChanged');
                        if($rootScope.selectedFacilities.length === 0){
                            $rootScope.loadingData = false;
                            $rootScope.openLocation();
                        }
                    }
                                        
                }).catch(function(e) {
					utility.redirectOnError(e); 
				});
        }

        $rootScope.loadMaintRecConfig = function(){
        	dataServices.getMaintRecommendationConfig()
                .then(function (data) {
                		$rootScope.chartSortOrder = [];
                    for(var sIndex = 0; sIndex < data.length; sIndex++){
                    	$rootScope.chartSortOrder.push(data[sIndex].recommendationValue);
                    }
					$rootScope.$broadcast('MaintRecConfigLoaded');
                }).catch(function(e) {
					utility.redirectOnError(e); 
				});
        };
        
		$rootScope.loadMaintRecConfig();

        $rootScope.loadGroups = function () {
            if($rootScope.selectedFacilities.length > 0){
	            dataServices.getProductGroupForSelectedFacilities().then(
	            	function (data) {
						$rootScope.loadingData = false;
						$rootScope.productGroups = data;
						_.forEach($rootScope.productGroups, function(pGroup){
						    //Added below code to limit Product Group Name to 16 chars
							var originalText = pGroup.productGroupName;
							var parsedText = utility.parseHTML(originalText);
							if(parsedText.length > globalConstants.PG_NAME_LIMIT){
								originalText = originalText.substr(0,originalText.length - (parsedText.length - globalConstants.PG_NAME_LIMIT));
							}
							pGroup.productGroupName = originalText;
							pGroup.donutChartData = utility.getDonutChartData(pGroup);
						});
						$rootScope.$broadcast('productGroupLoaded');
		            }).catch(function(e) {
			          	utility.redirectOnError(e); 
			        }
				);
            }
        };
		
		$rootScope.isActiveItem = function(index){
			var active = true;
			if(typeof index !== 'object'){
				for(var group in $rootScope.productGroups){
					if($rootScope.productGroups[group].isActive){
						active = false;
					}
				}
			}else{
				active =  index.isActive;
			}
			return active;
		}
		
		$rootScope.$on('facilitySelectionChanged',function(){
				$rootScope.loadGroups();
		});
		
        $rootScope.openLocation = function () {
            $rootScope.currentOrgsAndFacilities = angular.copy($rootScope.orgsAndFacilities);
            var currentSelectedFacilities = angular.copy($rootScope.selectedFacilities);
            $rootScope.allFacilities = 0;
            _.forEach($rootScope.currentOrgsAndFacilities, function (org) {
                $rootScope.allFacilities += org.facilities.length;
                org.selectedNumber = 0;
                _.forEach(org.facilities, function (facility) {
                    facility.selected = false;
                    facility.lastModifiedDate = 0;
                    _.forEach(currentSelectedFacilities, function (selectedFacility) {
                        if (selectedFacility.id === facility.id) {
                            org.selectedNumber++;
                            facility.selected = true;
                            facility.lastModifiedDate = selectedFacility.lastModifiedDate;
                            return false;
                        }
                    });
                });
            });
			$rootScope.selectedAll = ($rootScope.selectedFacilities.length === $rootScope.allFacilities); 
            utility.locationModal.$promise.then(utility.locationModal.show);
        };

        $rootScope.snapshot = function () {
            if($rootScope.notSnapshot){
                $rootScope.notSnapshot = false;
                    $rootScope.go($rootScope.tabControl.tabCurrentPage);
            }
        };

        $rootScope.checkIfEnterKeyWasPressed = function($event){
            var keyCode = $event.which || $event.keyCode;
            if($rootScope.search.currentTerm.length >= 3){
                if (typeof term != 'object' && $rootScope.search.currentTerm && keyCode === 13) {
                    $rootScope.$apply($rootScope.search.send($rootScope.search.currentTerm));
                }
            }
        };
        
        $rootScope.search = {
            term: '',
            displayTerm: '',
            currentTerm: '',
            processingData : false,
            numberOfDevicesPerPage : 0,
            onFocus : function(eleSearchInput){
                eleSearchInput.placeholder = '';
                $rootScope.isTypingForSearch = true;
            },
            onBlur : function(eleSearchInput){
                eleSearchInput.placeholder = $rootScope.labels.Global_Search_Placeholder;
                $rootScope.isTypingForSearch = false;
                
            },  
            resetSearch : function($event){
                
                $rootScope.search.term = '';
                $rootScope.search.currentTerm = '';
                $event.preventDefault();
                
            },
            typing: function (term) {
                if (term === null || term.length < 3 || $rootScope.search.currentTerm === $rootScope.search.term){
                    
                    return;
                }else if(typeof term !== 'object'){
                    var regx = /^[0-9 _-]+$/;
                    if(!regx.test(term) && term.includes("--")){
                        var temp=[];
                        temp.push({
						deviceId: '', productNumber : '', label: $rootScope.labels.Nothing_matched
					    });
                        return temp;
                    }
                    $rootScope.search.currentTerm = term;
                    $rootScope.search.processingData = true;
                    return dataServices.getDynamicSearchResult(                          
                            escape(term)
                        ).then(function(data){
                            var searchResult =utility.processSearchResult(data);
                            $rootScope.search.processingData = false;
                            return searchResult;
                        }).catch(function(e) {
                            $rootScope.search.processingData = false;
                            utility.redirectOnError(e); 
                        });
                }else{
                    $rootScope.search.results = [];
                    return [];
                }
            },
            select: function (value) {
                if(value.label === $rootScope.labels.Search_More){
                    angular.element('#Home_SearchBox').blur();
                    //value.label = $rootScope.search.currentTerm;
                    $rootScope.$apply($rootScope.search.send($rootScope.search.currentTerm));
                    $rootScope.search.term = $rootScope.search.currentTerm;
                }else if(value.label === $rootScope.labels.Nothing_matched){
                    
                    $rootScope.search.term = $rootScope.search.currentTerm;

                }else{
                    angular.element('#Home_SearchBox').blur();
                    $rootScope.search.term = $rootScope.search.currentTerm;
                    $rootScope.openDeviceModal(value.deviceId, $rootScope.chartUsageData.days);
                    
                }
                
            },
            send: function (term) {
                if($rootScope.search.currentTerm.length >= 3 && typeof term !== 'object'){
                    $rootScope.search.currentTerm = term;
                    $rootScope.search.displayTerm = term;
                    $rootScope.search.searchPageResults = [];
                    $rootScope.processingData = true;
                    angular.element('#Home_SearchBox').blur();
					if($rootScope.search.numberOfDevicesPerPage === 0){
						$rootScope.loadingSearchData = true;
					}else{
						dataServices.getDevicesForSearch(escape(term),$rootScope.search.numberOfDevicesPerPage,0)
							.then(function (data) {
								if(data.length === 0){
									$rootScope.go('/search/'+term);
									$rootScope.search.message = $rootScope.labels.Nothing_matched;
								}else if(data.length === 1){
									$rootScope.search.searchPageResults = data;
									$rootScope.openDeviceModal(data[0].deviceId, $rootScope.chartUsageData.days);
								}else{
									data = _.sortBy(data,['product.productType.productGroup.name', 'productNumber', 'serialNumber']);
									$rootScope.search.searchPageResults = data;
									$rootScope.go('/search/'+term);
								}
								$rootScope.$broadcast('fetchedResult');
								$rootScope.processingData = false;
							}).catch(function(e) {
								utility.redirectOnError(e);
							});
					}
                }
            },
            results: []
        };
		
		dataServices.getDevicesPerPage()
			.then(function(noOfDevices){
				if(typeof noOfDevices !== 'undefined' && noOfDevices !== null){
					$rootScope.search.numberOfDevicesPerPage = noOfDevices;
					$rootScope.$broadcast("NoOfDevicesPerPageLoaded");
				}
			}).catch(function(e) {
				utility.redirectOnError(e); 
			});
			
		$rootScope.$on("NoOfDevicesPerPageLoaded",function(){
			if($rootScope.loadingSearchData){
				$rootScope.search.send($rootScope.search.currentTerm);
				$rootScope.loadingSearchData = false;
			}
		});
});
        
