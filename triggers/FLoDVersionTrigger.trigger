/***********************************************************************************************************************
* Appirio, Inc
* Trigger Name: FLoDVersionTrigger
* Description: [T-602006 FLoDVersion Trigger for creating Child records of FLoD
* Created Date: [12/05/2017]
* Created By: [Gaurav Dudani] (Appirio)
* 
* Date Modified                Modified By               Description of the update
* [06/08/2017]                 [Neeraj Kumawat]          [Executing Trigger Based on Value of Custom Setting]
* [13-June-2017]               [Neeraj Kumawat]          [Updated trigger and called UtilityCls method for fetching Custom Setting Value]
***********************************************************************************************************************/
trigger FLoDVersionTrigger on FLoD_Review__c (before insert) {
    //Fetching custom setting value
    //Updated on 13-June-2017 by Neeraj Kumawat
    boolean isExecute=utilityCls.triggerCustomSetting();
    if(isExecute){
        // Calling beforeInsert method on FLoD Before Insert.
        // By Gaurav Dudani on 12 May 2017 as per Task # T-602006.
        if(Trigger.isInsert && Trigger.isBefore){
            FLoDVersionTriggerHandler.beforeInsert(Trigger.new);
        }
    }
}