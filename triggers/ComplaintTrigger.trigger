/**********************************************************************
* Appirio a Wipro Company
* Trigger Name: ComplaintTrigger
* Description: Complaint Trigger to populate queue as ownerID based on user DB Region T-601115 / S-470559
* Created Date: 05-11-2017
* Created By: Gaurav Dudani
* 
*Date Modified                 Modified By               Description of the update
*[16-May 2017]                 [Aashish Sajwan]          [T-601582 / S-470712]
*[06/08/2017]                  [Neeraj Kumawat]          [Executing Trigger Based on Value of Custom Setting]
*[13-June-2017]                [Neeraj Kumawat]          [Updated trigger and called UtilityCls method for fetching Custom Setting Value]
*********************************************************************/
trigger ComplaintTrigger on Complaint__c (before insert,before update) {
    //Fetching custom setting value
    //Updated on 13-June-2017 by Neeraj Kumawat
    boolean isExecute=utilityCls.triggerCustomSetting();
    if(isExecute){
        // Calling beforeInsert method on Complaint before insert. 
        // By Gaurav dudani on 11 May 2017 as per T-601115 / S-470559.
        if (Trigger.isInsert && Trigger.isBefore) {
            ComplaintTriggerHandler.beforeInsert(Trigger.new);
            ComplaintTriggerHandler.updateAccoutFields(Trigger.new);
        }
        else if(Trigger.isUpdate && Trigger.isBefore){
            ComplaintTriggerHandler.updateAccoutFields(Trigger.new);
        }
    }
}