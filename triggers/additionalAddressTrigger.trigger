/***********************************************************************************************************************
* Appirio, Inc
* Trigger Name: additionalAddress
* Description: Trigger updates account address fields
* Created Date: [02/19/2017]
* Created By: Shannon Howe (Appirio)
* 
* Date Modified                Modified By                Description of the update
* [02/19/2017]                [Shannon Howe]               [Created to resolve process builder SOQL limits]
* [06/08/2017]                [Neeraj Kumawat]             [Executing Trigger Based on Value of Custom Setting]
* [13-June-2017]              [Neeraj Kumawat]          [Updated trigger and called UtilityCls method for fetching Custom Setting Value]
***********************************************************************************************************************/
trigger additionalAddressTrigger on Additional_Address__c (before update,after update, before insert, after insert) {
    //Updated on 13-June-2017 by Neeraj Kumawat
    //Fetching custom setting value
    boolean isExecute=utilityCls.triggerCustomSetting();
    if(isExecute){
        if(Trigger.isAfter){
            if(Trigger.isUpdate || Trigger.isInsert){
                System.debug('***********In After Block of additionalAddressTrigger*************');
                AdditionalAddressTriggerHandler.onAfterInsertUpdate(Trigger.New);
            }
        }
    }
}