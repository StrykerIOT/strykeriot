/***********************************************************************************************************************
* Appirio, Inc
* Trigger Name: EventTrigger
* Description: [T-565229 Event Trigger for Update the Case Fields]
* Created Date: [01/03/2017]
* Created By: [Gaurav Dudani] (Appirio)
* 
* Date Modified                Modified By                Description of the update
*  [02/03/2017]              [Aashish Sajwan]             [Code opitimization task # T-575846]
*  [02/08/2017]              [Neeraj Kumawat]             [Calling Trigger on After Event Update # T-576694]
*  [06/08/2017]              [Neeraj Kumawat]             [Executing Trigger Based on Value of Custom Setting]
*  [13-June-2017]            [Neeraj Kumawat]             [Updated trigger and called UtilityCls method for fetching Custom Setting Value]
***********************************************************************************************************************/
trigger EventTrigger on Event (before update,after update, before insert, after insert, after delete) {
    //Fetching custom setting value
    //Updated on 13-June-2017 by Neeraj Kumawat
    boolean isExecute=utilityCls.triggerCustomSetting();
    if(isExecute){
        if(Trigger.isBefore){
            if(Trigger.isUpdate || Trigger.isInsert){
                System.debug('***********In Before Block of EventTrigger*************');
                EventTriggerHandler.onBeforeInsertUpdate(Trigger.New);
            }
        }
        if(Trigger.isAfter){
            if (Trigger.isInsert) {
                System.debug('***********In isAfter Insert Block of EventTrigger*************');
                EventTriggerHandler.onAfterInsert(Trigger.new);
            } else if (Trigger.isDelete) {
                System.debug('***********In isAfter Delete Block of EventTrigger*************');
                EventTriggerHandler.onAfterDelete(Trigger.old);
            }else if (Trigger.isUpdate) {
                System.debug('***********In isAfter Update Block of EventTrigger*************');
                EventTriggerHandler.onAfterUpdate(Trigger.new);
            }
        }
    }
}