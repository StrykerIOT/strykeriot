/***************************************************************************************************************
* Appirio, Inc
* Trigger Name: AssetTrigger
* Description: [T-585639 Asset Trigger to update asset]
* Created Date: [17/03/2017]
* Created By: [Neeraj Kumawat] (Appirio)
* 
* Date Modified                 Modified By               Description of the update
*[24/03/2017]                [Neeraj Kumawat]             [T-585639 Handling scenario to uncheck Multi-Mid]
*[06/08/2017]                [Neeraj Kumawat]             [Executing Trigger Based on Value of Custom Setting]
*[13-June-2017]                [Neeraj Kumawat]          [Updated trigger and called UtilityCls method for fetching Custom Setting Value]
***************************************************************************************************************/
trigger AssetTrigger on Asset (after insert, after update) {
    //Fetching custom setting value
    //Updated on 13-June-2017 by Neeraj Kumawat
    boolean isExecute=utilityCls.triggerCustomSetting();
    if(isExecute){
        if(Trigger.isAfter){
            //Calling onAfterInsertUpdate method on Asset IsInsert
            if (Trigger.isInsert) {
                //newAssetList to contain asset which don't have current generated random number
                List<Asset> newAssetList=new List<Asset>();
                for(Asset asst:Trigger.new ){
                    //Initialize asset Insert list basesd on random number. if asset has random number 
                    //than not added this asset to asset Insert list
                    if(asst.IsAssetUpdate__c!=AssetTriggerHandler.randomNumber){
                        newAssetList.add(asst);
                    }
                }
                AssetTriggerHandler.onAfterInsertUpdate(newAssetList,null);
            }
            //Calling onAfterInsertUpdate method on Asset Update
            if (Trigger.isUpdate) {
                //newAssetUpdateList to contain asset which don't have current generated random number
                List<Asset> newAssetUpdateList=new List<Asset>();
                for(Asset asst:Trigger.new ){
                    //Initialize asset update list basesd on random number. if asset has random number 
                    //than not added this asset to asset update list
                    if(asst.IsAssetUpdate__c!=AssetTriggerHandler.randomNumber){
                        newAssetUpdateList.add(asst);
                    }
                }
                //oldAssetUpdateMap to contain asset which don't have current generated random number
                Map<Id,Asset> oldAssetUpdateMap=new Map<Id,Asset>();
                Map<Id,Asset> assetOldMap=Trigger.oldMap;
                for(Id assetId: assetOldMap.keyset()){
                    Asset asst=assetOldMap.get(assetId);
                    //Initialize asset update Map basesd on random number. if asset has random number 
                    //than not added this asset to asset update Map
                    if(asst.IsAssetUpdate__c!=AssetTriggerHandler.randomNumber){
                        oldAssetUpdateMap.put(assetId,asst);
                    }
                }
                AssetTriggerHandler.onAfterInsertUpdate(newAssetUpdateList,oldAssetUpdateMap);
            }
        }
    }
}