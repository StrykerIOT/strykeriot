/***********************************************************************************************************************
* Appirio, Inc
* Trigger Name: WorkOrderLineItemTrigger
* Description: [T-603559 WorkOrderLineItemTrigger Trigger ]
* Created Date: [24/05/2017]
* Created By: [Neeraj Kumawat] (Appirio)
* 
* Date Modified                Modified By               Description of the update
*[26-May-2017]                 [Neeraj Kumawat]          [Added After Insert and update method. #T-605293]
*[8-June-2017]                 [Neeraj Kumawat]          [Executing Trigger Based on Value of Custom Setting]
*[13-June-2017]				   [Neeraj Kumawat]			 [Updated trigger and called UtilityCls method for fetching Custom Setting Value]	
*[21-June-2017]                [Neeraj Kumawat]          [Added check for running the trigger for System administrator and Custom Integration profile
 														 based  on Custom Setting value]
***************************************************************************************************************/
trigger WorkOrderLineItemTrigger on WorkOrderLineItem (after insert,  after update, before insert, before update) {
    //Updated on 13-June-2017 by Neeraj Kumawat
    boolean isExecute=utilityCls.triggerCustomSetting();
    if(isExecute){
        //21 June 2017 Neeraj Kumawat Added check for running the trigger based on custom setting value
        Boolean isDisableValidation=utilityCls.getDisableValidationRule();
        if(!isDisableValidation){
            if(Trigger.isBefore){
                //Calling OnBeforeInsert method on WorkOrder LineItem before insert
                if (Trigger.isInsert) {
                    WorkOrderLineItemTriggerHandler.OnBeforeInsert(Trigger.new);
                }
                //Calling OnBeforeUpdate method on WorkOrder LineItem before update
                if (Trigger.isUpdate) {
                    WorkOrderLineItemTriggerHandler.OnBeforeUpdate(Trigger.new);
                }
            }
            if(Trigger.isAfter){
                //Calling OnAfterInsert method on WorkOrder LineItem after insert
                if (Trigger.isInsert) {
                    WorkOrderLineItemTriggerHandler.OnAfterInsert(Trigger.new);
                }
                //Calling OnAfterUpdate method on WorkOrder LineItem After update
                if (Trigger.isUpdate) {
                    WorkOrderLineItemTriggerHandler.OnAfterUpdate(Trigger.new);
                }
            }
        }
    }
}