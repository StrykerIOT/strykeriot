/***************************************************************************************************************
* Appirio, Inc
* Trigger Name: CaseTrigger
* Description: [Create Handler to replace Process Builder (Case Proess)]
* Created Date: [3/6/2017]
* Created By: [Kate Jovanovic] (Appirio)
* Date Modified                Modified By               Description of the update
* [12-April-2017]           [Neeraj Kumawat]             [T-590411 Updated to add OnAfterUpdate method to create case comments]
* [06/08/2017]              [Neeraj Kumawat]             [Executing Trigger Based on Value of Custom Setting]
* [13-June-2017]                [Neeraj Kumawat]          [Updated trigger and called UtilityCls method for fetching Custom Setting Value]
***************************************************************************************************************/
trigger caseTrigger on Case (after insert,  after update, before insert, before update) {
    //Fetching custom setting value
     //Updated on 13-June-2017 by Neeraj Kumawat
    boolean isExecute=utilityCls.triggerCustomSetting();
    if(isExecute){
        //Calling OnAfterInsert method on Case after insert
        if(Trigger.isInsert && Trigger.isAfter){
            caseTriggerHandler.OnAfterInsert(Trigger.new);
            
        }
        //Calling OnAfterUpdate method on Case after update
        if(Trigger.isUpdate && Trigger.isAfter){
            caseTriggerHandler.OnAfterUpdate(Trigger.OldMap,Trigger.new);
            
        }
        //Calling onBeforeInsert method on Case before insert
        if(Trigger.isInsert && Trigger.isBefore){
            caseTriggerHandler.onBeforeInsert(Trigger.new);
            
        }
        //Calling onBeforeUpdate method on Case before update
        if(Trigger.isBefore && Trigger.isUpdate){
            caseTriggerHandler.onBeforeUpdate(Trigger.OldMap,Trigger.new);
            
        }
    }
}