/***********************************************************************************************************************
* Appirio, Inc
* Trigger Name: TaskTrigger
* Description: [T-561974 Task Trigger for Update the Case Status]
* Created Date: [12/15/2016]
* Created By: [Neeraj Kumawat] (Appirio)
* 
* Date Modified                Modified By               Description of the update
* [01/03/2017]               [Gaurav Dudani]             [Added functionality is insert and is delete as per task #T-565229].
* [02/03/2017]              [Aashish Sajwan]             [Code opitimization task # T-575846]
* [02/06/2017]              [Aashish Sajwan]             [Code opitimization task # T-575952]
* [02/08/2017]              [Neeraj Kumawat]             [Calling Trigger on After Task Update # T-576694]
* [02/27/2017]              [Kate Jovanovic]             [Removing OnBeforeInsert T-580692]
* [06/08/2017]              [Neeraj Kumawat]             [Executing Trigger Based on Value of Custom Setting]
* [13-June-2017]            [Neeraj Kumawat]             [Updated trigger and called UtilityCls method for fetching Custom Setting Value]
***********************************************************************************************************************/
trigger TaskTrigger on Task (before update,after update, before insert, after insert, after delete) {
    //Fetching custom setting value
    //Updated on 13-June-2017 by Neeraj Kumawat
    boolean isExecute=utilityCls.triggerCustomSetting();
    if(isExecute){
        if(Trigger.isAfter){
            if (Trigger.isInsert) {
                TaskTriggerHandler.onAfterInsert(Trigger.new);
            } else if (Trigger.isDelete) {
                TaskTriggerHandler.onAfterDelete(Trigger.old);
            }else if (Trigger.isUpdate) {
                TaskTriggerHandler.onAfterUpdate(Trigger.new,Trigger.oldMap);
            }
        }
    }
}