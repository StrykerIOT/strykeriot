/***********************************************************************************************************************
* Appirio, Inc
* Trigger Name: WorkOrderTrigger
* Description: [T-601144 WorkOrder Trigger ]
* Created Date: [12/05/2017]
* Created By: [Neeraj Kumawat] (Appirio)
* 
* Date Modified                Modified By               Description of the update
* 30-May-2017                 [Aashish Sajwan]           [Added isBeforeUpdate]
* 6-Jun-2017                  [Aashish Sajwan]           [#T-607211 Update  in Logic Call Elavon API and update Added Before Insert]
* [06/08/2017]                [Neeraj Kumawat]           [Executing Trigger Based on Value of Custom Setting]
* [09-June-2017]              [Neeraj Kumawat]           [#T-608730/I-278559 Added isBeforeInsert check.]
* [13-June-2017]              [Neeraj Kumawat]           [Updated trigger and called UtilityCls method for fetching Custom Setting Value]
* [21-June-2017]              [Neeraj Kumawat]           [Added check for running the trigger for System administrator and Custom Integration profile
 														 based  on Custom Setting value]
***************************************************************************************************************/
trigger WorkOrderTrigger on WorkOrder (after insert,  after update, before insert, before update) {
    //Updated on 13-June-2017 by Neeraj Kumawat
    boolean isExecute=utilityCls.triggerCustomSetting();
    if(isExecute){
        //21 June 2017 Neeraj Kumawat Added check for running the trigger based on custom setting value
        Boolean isDisableValidation=utilityCls.getDisableValidationRule();
        if(!isDisableValidation){
            if(Trigger.isBefore){    
                if (Trigger.isUpdate) {
                    WorkOrderTriggerHandler.OnBeforeUpdate(Trigger.OldMap,Trigger.new);
                }
                if (Trigger.isInsert) {
                    WorkOrderTriggerHandler.OnBeforeInsert(Trigger.new);
                }
            }else if(Trigger.isAfter){
                //Calling OnAfterUpdate method on WorkOrder after update
                //Added 6-Jun-2017 Aashish Sajwan Added After Insert to Update Stamped field As per #T-607211 
                if (Trigger.isInsert) {
                    WorkOrderTriggerHandler.OnAfterInsert(Trigger.new);
                }else if (Trigger.isUpdate) {
                    WorkOrderTriggerHandler.OnAfterUpdate(Trigger.OldMap,Trigger.new);
                }
            }    
        }
    }
}