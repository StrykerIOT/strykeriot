/*********************************************************************
* Appirio a WiPro Company 
* Name: ServiceContractTrigger
* Description: Service Contract Trigger for all CRUD events. 
* Created Date: 04-26-2017
* Created By: Bobby Cheek
* 
*Date Modified                Modified By                  Description of the update
*[06/08/2017]                 [Neeraj Kumawat]             [Executing Trigger Based on Value of Custom Setting]
*[13-June-2017]               [Neeraj Kumawat]             [Updated trigger and called UtilityCls method for fetching Custom Setting Value]
**********************************************************************/
trigger ServiceContractTrigger on ServiceContract (before insert, after insert, before update, after update, before delete, after delete, after undelete) {
    //Fetching custom setting value
    //Updated on 13-June-2017 by Neeraj Kumawat
    boolean isExecute=utilityCls.triggerCustomSetting();
    if(isExecute){
        if(Trigger.isInsert && Trigger.isBefore){
            // before insert event 
            ServiceContractTriggerHandler.onBeforeInsert(trigger.new);   
        }
        else if(Trigger.isInsert && Trigger.isAfter){
            // after insert event        
        }
        else if(Trigger.isUpdate && Trigger.isBefore){
            // before update event  
        }
        else if(Trigger.isUpdate && Trigger.isAfter){
            // after update event   
        }
        else if(Trigger.isDelete && Trigger.isBefore){
            // before delete event        
        }
        else if(Trigger.isDelete && Trigger.isAfter){ 
            // after delete event       
        }
        else if(Trigger.isUnDelete){ 
            // undelete event        
        }   
    }
}