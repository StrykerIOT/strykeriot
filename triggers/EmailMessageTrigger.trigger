/**********************************************************************
* Appirio, Inc
* Trigger Name: EmailMessageTrigger
* Handler Class: EmailMessageTriggerHandler
* Description: [T-558200 Added After Delete Action to prevent deletion of Email Message.]
* Created Date: [01/12/2016]
* Created By: [Neeraj Kumawat] (Appirio)
* 
* Date Modified                Modified By              Description of the update
* [19/05/2017]                [Poornima Bhardwaj]       [Added Before Insert Functinoality T-603553]
* [06/08/2017]                [Neeraj Kumawat]          [Executing Trigger Based on Value of Custom Setting]
* [13-June-2017]              [Neeraj Kumawat]          [Updated trigger and called UtilityCls method for fetching Custom Setting Value]
**********************************************************************/

trigger EmailMessageTrigger on EmailMessage (Before Insert,After delete) {
    //Fetching custom setting value
    //Updated on 13-June-2017 by Neeraj Kumawat
    boolean isExecute=utilityCls.triggerCustomSetting();
    if(isExecute){
        if(Trigger.isAfter){
            if(Trigger.isDelete){
                //Check if deletion is allowed
                EmailMessageTriggerHandler.onAfterDelete(Trigger.New, Trigger.Old);
            }
        }    
        if(Trigger.isBefore && Trigger.isInsert){
            EmailMessageTriggerHandler.onBeforeInsert(trigger.new);      
        }
    }
}